<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: process_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Process Plans
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/
$_SESSION['module'] = 'Legal Reports';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];	

	// Temp data
	$alert = "";

	if($role == 1)
	{
		$assigned_to = "";
	}
	else if($role == 2)
	{
		$assigned_to = "";		
	}
	else
	{
		$assigned_to = $user;		
	}
	
	if(isset($_GET['process']))
	{
		$process_type = $_GET['process'];
	}
	else
	{
		$process_type = '';
	}
	
	$file_number   = "";
	$survey_number = "";	
	if(($role == '1') || ($role == '2'))
	{
		$search_user = "";
	}
	else
	{
		$search_user = $user;
	}

	// Search parameters
	if(isset($_POST["file_search_submit"]))
	{
		$process_type  = $_POST['ddl_process'];
		$file_number   = $_POST["search_file_no"];
		$survey_number = $_POST["search_survey_no"];
		if(($role == '1') || ($role == '2'))
		{
			$search_user = $_POST["search_user"];
		}
		else
		{
			$search_user = $user;
		}		
	}

	// Get list of process plans for this module
	$legal_bulk_search_data = array("legal_process_assigned_to"=>$search_user,"completed"=>'0');
	if($file_number != "")
	{
		$legal_bulk_search_data["file_no"] = $file_number;
	}
	if($survey_number != "")
	{
		$legal_bulk_search_data["survey_no"] = $survey_number;
	}
	if($process_type != "")
	{
		$legal_bulk_search_data["legal_bulk_process_type"] = $process_type;
	}
	$legal_process_plan_list = i_get_bulk_legal_process_plan_details($legal_bulk_search_data);

	if($legal_process_plan_list["status"] == SUCCESS)
	{
		$legal_process_plan_list_data = $legal_process_plan_list["data"];
		$bulk_process_count = count($legal_process_plan_list_data);
	}
	else
	{
		$alert = $alert."Alert: ".$legal_process_plan_list["data"];
		$bulk_process_count = 0;
	}
	
	// Get list of users
	$user_list = i_get_user_list('','','','','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
	}
	
	// Get list of process types
	$process_type_list = i_get_process_type_list('','','1','1');

	if($process_type_list["status"] == SUCCESS)
	{
		$process_type_list_data = $process_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$process_type_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Consolidated Report - Bulk Processing</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Bulk Consolidated Report&nbsp;&nbsp;&nbsp;Count: <span id="total_records"><?php echo $bulk_process_count; ?></span>&nbsp;&nbsp;&nbsp;Total Extent: <span id="total_extent_span"><i>Calculating</i></span> guntas&nbsp;&nbsp;&nbsp;Total Files: <span id="total_num_files"><i>Calculating</i></span></h3>
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="file_search_form" action="bulk_consolidated_report.php">			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="text" name="search_file_no" value="<?php echo $file_number; ?>" placeholder="Search by file number" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="text" name="search_survey_no" value="<?php echo $survey_number; ?>" placeholder="Search by survey number" />
			  </span>			  
			  <?php
			  if(($role == '1') || ($role == '2'))
			  {
			  ?>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_user">
			  <option value="">- - Select User - -</option>
			  <?php
			  for($user_count = 0; $user_count < count($user_list_data); $user_count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($search_user == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <?php
			  }
			  ?>
			  <span style="padding-left:20px; padding-right:20px;">			  
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($process_type_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $process_type_list_data[$process_count]["process_master_id"]; ?>" <?php if($process_type == $process_type_list_data[$process_count]["process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $process_type_list_data[$process_count]["process_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <input type="submit" name="file_search_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
			
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>					
				    <th>FILE NO(s)</th>
					<th>EXTENT</th>					    
					<th>VILLAGE</th>
					<th>PROCESS</th>
					<th>PROCESS START DATE</th>
					<th>PROCESS LEAD</th>																					
					<th>TASK</th>
					<th>START DATE</th>
					<th>DAYS</th>
					<th>REASON</th>
					<th>REMARKS</th>
					<th>USER</th>
					<th>&nbsp;</th>	
					<th>&nbsp;</th>	
				</tr>
				</thead>
				<tbody>
				 <?php
				$final_total_extent = 0;
				$no_of_files        = 0;
				if($legal_process_plan_list["status"] == SUCCESS)
				{		
					$sl_no = 0;
					$task_start_date = '';
					$task_type_name  = '';
					for($count = 0; $count < count($legal_process_plan_list_data); $count++)
					{
						$sl_no++;
						$file_count = 0;
						
						// Calculate task start and lead time
						$task_actual_result = i_get_task_plan_list('','','','','0000-00-00','','','slno',$legal_process_plan_list_data[$count]["legal_bulk_process_id"]);
						if($task_actual_result['status'] == SUCCESS)
						{
							if($task_actual_result['data'][0]['task_plan_actual_start_date'] != '0000-00-00')
							{
								$task_start_date = date('d-M-Y',strtotime($task_actual_result['data'][0]['task_plan_actual_start_date']));
							}
							else
							{
								$task_start_date = '';
							}
							$task_type_name  = $task_actual_result['data'][0]['task_type_name'];
						}
						
						// Calculate process start and lead time
						$task_plan_result = i_get_task_plan_list('','','','','','','','slno',$legal_process_plan_list_data[$count]["legal_bulk_process_id"]);
						if($task_plan_result["status"] == SUCCESS)
						{
							if(get_formatted_date($task_plan_result["data"][(count($task_plan_result['data']) - 1)]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00")
							{																
								$end_date         = date("Y-m-d");	
								$end_date_display = '';
							}
							else
							{
								$end_date 		  = $task_plan_result["data"][(count($task_plan_result['data']) - 1)]["task_plan_actual_end_date"];	
								$end_date_display = $end_date;
							}							

							if(get_formatted_date($task_plan_result["data"][0]["task_plan_actual_start_date"],"Y-m-d") == "0000-00-00")
							{																
								$start_date = '';								
							}
							else
							{
								$start_date = $task_plan_result["data"][0]["task_plan_actual_start_date"];
							}
						}
						else
						{
							$start_date = "0000-00-00";
							$end_date   = "0000-00-00";
							
							$actual_date = $end_date;
						}
						$start_date_display = $start_date;
						
						if(($start_date == '0000-00-00') || ($start_date == ''))
						{
							$start_date         = $end_date;
							$start_date_display = '';
						}
						
						if($task_actual_result["status"] == SUCCESS)
						{
							$variance = get_date_diff($task_start_date,date('Y-m-d'));
							if($variance["status"] == 1)
							{
								if((get_formatted_date($task_actual_result["data"][0]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($task_actual_result["data"][0]["task_plan_actual_end_date"],"Y-m-d") == "1969-12-31"))
								{
									$css_class = "#FF0000";
								}
								else						
								{
									$css_class = "#FFA500";
								}
							}
							else
							{
								if((get_formatted_date($task_actual_result["data"][0]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($task_actual_result["data"][0]["task_plan_actual_end_date"],"Y-m-d") == "1969-12-31"))
								{
									$css_class = "#000000";
								}
								else						
								{
									$css_class = "#00FF00";
								}
							}
						}
						else
						{
							$css_class = "#0000FF";
						}
						
						// Get file details																		
						$legal_bulk_files_search_data = array("legal_bulk_process_files_process_id"=>$legal_process_plan_list_data[$count]["legal_bulk_process_id"]);
						$process_files_result = i_get_bulk_legal_process_plan_files($legal_bulk_files_search_data);			
						$total_extent = 0;
						for($fcount = 0; $fcount < count($process_files_result['data']); $fcount++)
						{														
							$no_of_files++;
							$file_count++;
							$total_extent          = $total_extent + $process_files_result['data'][$fcount]["file_extent"];
							$village               = $process_files_result['data'][$fcount]["village_name"];
							if($process_files_result['data'][$fcount]["village_name"] != "")
							{
								$village = $process_files_result['data'][$fcount]["village_name"]; 
							}
							else
							{
								$village = $process_files_result['data'][$fcount]["file_village"]; 
							}							
						}						

						$final_total_extent = $final_total_extent + $total_extent;	

						$reason_details = i_get_delay_reason_list($task_actual_result["data"][0]["task_plan_legal_id"],'');
						if($reason_details['status'] == SUCCESS)
						{
							$disp_delay_reason  = $reason_details['data'][count($reason_details['data']) - 1]['reason'];
							$disp_delay_remarks = $reason_details['data'][count($reason_details['data']) - 1]['reason_remarks'];
						}
						else
						{
							$disp_delay_reason  = 'No Reason';
							$disp_delay_remarks = '';
						}
													
					?>
					<tr style="color:<?php echo $css_class; ?>">						
						<td style="word-wrap:break-word;"><a href="bulk_file_list.php?bprocess=<?php echo $legal_process_plan_list_data[$count]["legal_bulk_process_id"]; ?>" target="_blank">Click</a></td>	
						<td style="word-wrap:break-word;"><?php echo $total_extent; ?> (<?php echo ($total_extent/GUNTAS_PER_ACRE); ?> acres)</td>	
						<td style="word-wrap:break-word;"><?php echo $village; ?></td>
						<td style="word-wrap:break-word;"><?php echo $legal_process_plan_list_data[$count]["process_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($start_date_display,"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;"><?php $process_variance = get_date_diff($start_date,date("Y-m-d"));
						if($process_variance["status"] != 2)
						{
							echo $process_variance["data"];							
						}
						else
						{
							echo "";
						}?></td>
						<td style="word-wrap:break-word;"><?php echo $task_type_name; ?></td>						
						<td style="word-wrap:break-word;"><?php echo $task_start_date; ?></td>						
						<td style="word-wrap:break-word;"><?php echo $variance["data"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $disp_delay_reason; ?></td>
						<td style="word-wrap:break-word;"><a href="#" onClick="alert('<?php echo $disp_delay_remarks; ?>')"><?php echo substr($disp_delay_remarks,0,35); ?></a></td>
						<td style="word-wrap:break-word;"><?php echo $legal_process_plan_list_data[$count]["user_name"]; ?></td>
						<td style="word-wrap:break-word;"><a href="reason_list.php?task=<?php echo $task_plan_result["data"][0]["task_plan_legal_id"];
						?>" target="_blank">Delay Reason List</a></td>
						<td style="word-wrap:break-word;"><a href="process_task_list.php?bprocess=<?php echo $legal_process_plan_list_data[$count]["legal_bulk_process_id"]; ?>" target="_blank">View</a></td>
					</tr>
					<?php						
					}
				}
				else
				{
				?>
				<td colspan="11">No Bulk Process added yet</td>
				<?php
				}
				 ?>	
				<script>
				document.getElementById('total_extent_span').innerHTML = <?php echo $final_total_extent; ?>;
				document.getElementById('total_num_files').innerHTML = <?php echo $no_of_files; ?>;
				</script>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function confirm_deletion(process)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "pending_project_list.php";
				}
			}

			xmlhttp.open("GET", "legal_process_delete.php?process=" + process);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}

</script>
</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>
  </body>

</html>
