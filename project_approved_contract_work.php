<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_task_actual_manpower_list.php
CREATED ON	: 05-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/

/* DEFINES - START */
define('PROJECT_CONTRACT_PROJECT_APPROVED_WORK_FUNC_ID','266');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	$alert_type = -1;
	$alert = "";

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_CONTRACT_PROJECT_APPROVED_WORK_FUNC_ID,'2','1');
	$add_perms_list    = i_get_user_perms($user,'',PROJECT_CONTRACT_PROJECT_APPROVED_WORK_FUNC_ID,'1','1');


	// Query String Data
	// Nothing

	$search_project 	 = "-1";
	$search_vendor  	 = "";
	$search_task   		 = "";
	$search_work_type    = "";
	$search_process    	 = "";
	$boq_start_date		 = "";
	$boq_end_date	 	 = "";

	if(isset($_POST['boq_approved_search_submit']))
	{
		$search_project 	= $_POST["search_project"];
		$search_vendor 		= $_POST["search_vendor"];
		$search_task    	= $_POST["search_task"];
		$search_work_type   = $_POST["search_work_type"];
		$search_process 	= $_POST["search_process"];
		$boq_start_date 	= $_POST["dt_start_date"];
		$boq_end_date		= $_POST["dt_end_date"];
	}

	if(isset($_POST["items_submit"]))
	{
		$item_nos = $_POST['hd_boq_count'];
		$start_date = "0000-00-00";
		$end_date = "0000-00-00";
		$length = "";
		$breadth = "";
		$depth = "";
		$measurement = "";
		$total_amount = "";
		$item_count = 0;
		$number = 0;
		$bill_no = p_generate_contract_bill_no();
			$payment_contract_iresult = i_add_project_actual_contract_payment('','','','','','','','','','','','','','',$user,$bill_no);
			if($payment_contract_iresult["status"] == SUCCESS)
			{
				$contract_payment_id = $payment_contract_iresult["data"];
			}
			$total_amount = 0;
			for($item_count = 0 ; $item_count < $item_nos; $item_count++)
			{
				if(isset($_POST['cb_item_'.$item_count]))
				{
					$mn = $_POST['hd_measurement_count_'.$item_count];
					$length  			 = $length + $_POST['hd_length_count_'.$item_count];
					$breadth   			 = $breadth + $_POST['hd_breadth_count_'.$item_count];
					$depth    			 = $depth + $_POST['hd_depth_count_'.$item_count];
					$measurement         = $measurement + $_POST['hd_measurement_count_'.$item_count];
					$contract_rate		 = $_POST['hd_rate_count_'.$item_count];
					$uom	 			 = $_POST['hd_unit_count_'.$item_count];
					$number	 			 = $number	+ $_POST['hd_number_count_'.$item_count];
					$contrct_date	     = $_POST['hd_contract_date_'.$item_count];
					$actual_amount	     = $_POST['hd_contract_amount_'.$item_count];
					$contract_vendor_id  = $_POST['hd_vendor_count_'.$item_count];
					$contract_id 	     = $_POST['cb_item_'.$item_count];

					if($start_date == "0000-00-00")
					{
						$start_date = $contrct_date;
					}
					else if(strtotime($contrct_date) < strtotime($start_date))
					{
						$start_date = $contrct_date;
					}

					if($start_date == "0000-00-00")
					{
						$end_date = $contrct_date;
					}
					elseif(strtotime($contrct_date) > strtotime($end_date))
					{
						$end_date = $contrct_date;
					}
					$payment_contract_mapping_iresult = i_add_project_payment_contract_mapping($contract_id,$contract_payment_id,'',$user);

					$total_amount = $total_amount + $actual_amount;
				}
			}

		$contract_start_date = date("Y-m-d",strtotime($start_date)).' '."00:00:00";
		$contract_end_date = date("Y-m-d",strtotime($end_date)) .' '."00:00:00";

		//Update Payment Manpower
		$project_actual_contract_payment_update_data = array("vendor_id"=>$contract_vendor_id,"rate"=>$contract_rate,"amount"=>$total_amount,"uom"=>$uom,"number"=>$number,"total_measurement"=>$measurement,"length"=>$length,"breadth"=>$breadth,"depth"=>$depth,"uom"=>$uom,"from_date"=>$contract_start_date,"to_date"=>$contract_end_date);
		$payment_contract_uresults =  i_update_project_actual_contract_payment($contract_payment_id,$project_actual_contract_payment_update_data);
		if($payment_contract_uresults["status"] == SUCCESS)
		{
			header("location:project_add_contract_bill_no.php?payment_contract_id=$contract_payment_id");
		}
	}

	// Get Project Task BOQ modes already added
	$project_task_boq_actual_search_data = array("active"=>'1',"project"=>$search_project,"status"=>'Approved',"vendor_id"=>$search_vendor,"boq_start_date"=>$boq_start_date,"boq_end_date"=>$boq_end_date,"contract_task"=>$search_task,"process"=>$search_process,"work_type"=>$search_work_type);
	$project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
	if($project_task_boq_actual_list['status'] == SUCCESS)
	{
		$project_task_boq_actual_list_data = $project_task_boq_actual_list['data'];
	}
	else
	{
		$alert = $alert."Alert: ".$project_task_boq_actual_list["data"];
	}
	// Get Project Contract Process modes already added
	$project_contract_process_search_data = array("active"=>'1');
	$project_contract_process_list = i_get_project_contract_process($project_contract_process_search_data);
	if($project_contract_process_list['status'] == SUCCESS)
	{
		$project_contract_process_list_data = $project_contract_process_list["data"];
	}
	else
	{
		$alert = $project_contract_process_list["data"];
		$alert_type = 0;
	}

	// Project data
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_list["data"];
	}

	// Project Contract Rate data
	$project_contract_rate_master_search_data = array("active"=>'1',"vendor_id"=>$search_vendor,"process"=>$search_process);
	$project_contract_rate_master_list = i_get_project_contract_rate_master($project_contract_rate_master_search_data);
	if($project_contract_rate_master_list["status"] == SUCCESS)
	{
		$project_contract_rate_master_list_data = $project_contract_rate_master_list["data"];
	}
	else
	{
		$alert = $project_contract_rate_master_list["data"];
		$alert_type = 0;
	}

	// Get Project manpower_agency Master modes already added
	$project_manpower_agency_search_data = array("active"=>'1');
	$project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
	if($project_manpower_agency_list['status'] == SUCCESS)
	{
		$project_manpower_agency_list_data = $project_manpower_agency_list['data'];
	}
	 else
	{

	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Daily Approved Contract Work List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Daily Approved Contract Work List &nbsp;&nbsp;&nbsp;&nbsp; Total Amount: <span id="total_amount"><i>Calculating</i></span></h3>
            </div>

			<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_approved_contract_work.php">

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			   <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  <option value="">- - Select Contract Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($project_contract_process_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $project_contract_process_list_data[$process_count]["project_contract_process_id"]; ?>" <?php if($search_process == $project_contract_process_list_data[$process_count]["project_contract_process_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_contract_process_list_data[$process_count]["project_contract_process_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			   <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_vendor">
			  <option value="">- - Select Vendor - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_manpower_agency_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_manpower_agency_list_data[$project_count]["project_manpower_agency_id"]; ?>" <?php if($search_vendor == $project_manpower_agency_list_data[$project_count]["project_manpower_agency_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_manpower_agency_list_data[$project_count]["project_manpower_agency_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			   <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_task">
			  <option value="">- - Select Contract Task - -</option>
			  <?php
			  for($work_count = 0; $work_count < count($project_contract_rate_master_list_data); $work_count++)
			  {
			  ?>
			  <option value="<?php echo $project_contract_rate_master_list_data[$work_count]["project_contract_rate_master_id"]; ?>" <?php if($search_task == $project_contract_rate_master_list_data[$work_count]["project_contract_rate_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_contract_rate_master_list_data[$work_count]["project_contract_rate_master_work_task"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			   <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_work_type">
			  <option value="">- - Select Work Type - -</option>
			  <option value="Regular" <?php if($search_work_type == "Regular") { ?> selected="selected" <?php } ?>>- - Regular Work - -</option>
			  <option value="Rework" <?php if($search_work_type == "Rework") { ?> selected="selected" <?php } ?>>- - Rework - -</option>

			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_start_date" value="<?php echo $boq_start_date; ?>" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_end_date" value="<?php echo $boq_end_date; ?>" />
			  </span>
			  <input type="submit" name="boq_approved_search_submit" />
			  </form>

            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
				    <th>Project</th>
				    <th>Process</th>
					<th>Task</th>
					<th>Work Type</th>
					<th>Contract Process</th>
					<th>Contract Task</th>
					<th>Vendor</th>
					<th>Date</th>
					<th>UOM</th>
					<th>Number</th>
					<th>Length</th>
					<th>Breadth</th>
					<th>Depth</th>
					<th>Total Measurement</th>
					<th>Rate</th>
					<th>Total</th>
					<th>Remarks</th>
					<th>Approved By</th>
					<th>Approved On</th>
					<th colspan="5" style="text-align:center;">Actions</th>

				</tr>
				</thead>
				<tbody>
				<form method="post" action="project_approved_contract_work.php">
				<input type="hidden" name="hd_vendor_id" value="<?php echo $search_vendor; ?>" />
				<?php
				if($project_task_boq_actual_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$total_amount = 0;
					for($count = 0; $count < count($project_task_boq_actual_list_data); $count++)
					{
						?>
						<input type="hidden" name="hd_boq_count" value="<?php echo count($project_task_boq_actual_list_data); ?>" />
						<?php

						$measurement = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_total_measurement"];
						$rate = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_amount"];
						$total = $measurement * $rate;

						$project_payment_contract_mapping_search_data = array("contract_id"=>$project_task_boq_actual_list_data[$count]["project_task_boq_actual_id"]);
						$payment_manpower_mapping_list = i_get_project_payment_contract_mapping($project_payment_contract_mapping_search_data);
						if($payment_manpower_mapping_list["status"] == SUCCESS)
						{
							$payment_manpower = true;
						}
						else
						{
							$payment_manpower = false;
						}
						if($payment_manpower == false)
						{
							$total_amount = $total_amount + $project_task_boq_actual_list_data[$count]["project_task_actual_boq_amount"];
							$sl_no++;
						?>
							<tr <?php if($total != $project_task_boq_actual_list_data[$count]["project_task_actual_boq_amount"]){ ?> style="color:red;" <?php } ?>>
							<td><?php echo $sl_no; ?></td>
							<td><?php echo $project_task_boq_actual_list_data[$count]["project_master_name"]; ?></td>
							<td><?php echo $project_task_boq_actual_list_data[$count]["project_process_master_name"]; ?></td>
							<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_master_name"]; ?></td>
							<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_work_type"]; ?></td>
							<td><?php echo $project_task_boq_actual_list_data[$count]["project_contract_process_name"]; ?></td>
							<td><?php echo $project_task_boq_actual_list_data[$count]["project_contract_rate_master_work_task"]; ?></td>

							<input type="hidden" name="hd_vendor_count_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_vendor_id"]; ?>" />
							<td><?php echo $project_task_boq_actual_list_data[$count]["project_manpower_agency_name"]; ?></td>

							<input type="hidden" name="hd_contract_date_<?php echo $count; ?>" value="<?php echo date("d-M-Y",strtotime($project_task_boq_actual_list_data[$count]["project_task_actual_boq_date"])); ?>" />
							<td><?php echo get_formatted_date($project_task_boq_actual_list_data[$count]["project_task_actual_boq_date"],"d-M-Y"); ?></td>

							<input type="hidden" name="hd_unit_count_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_uom"]; ?>" />
							<td><?php echo $project_task_boq_actual_list_data[$count]["stock_unit_name"]; ?></td>

							<input type="hidden" name="hd_number_count_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_boq_actual_number"]; ?>" />
							<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_boq_actual_number"]; ?></td>

							<input type="hidden" name="hd_length_count_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_length"]; ?>" />
							<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_length"]; ?></td>

							<input type="hidden" name="hd_breadth_count_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_breadth"]; ?>" />
							<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_breadth"]; ?></td>

							<input type="hidden" name="hd_depth_count_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_depth"]; ?>" />
							<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_depth"]; ?></td>

							<input type="hidden" name="hd_measurement_count_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_total_measurement"]; ?>" />
							<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_total_measurement"]; ?></td>

							<input type="hidden" name="hd_rate_count_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_amount"]; ?>" />
							<input type="hidden" name="hd_contract_amount_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_lumpsum"]; ?>" />
							<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_amount"]; ?></td>

							<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_lumpsum"]; ?></td>

							<td style="text-wrap:break-word;"><a href="#" onclick="alert('<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_remarks"]; ?>');"><?php echo substr($project_task_boq_actual_list_data[$count]["project_task_actual_boq_remarks"],0,15); ?></a></td>

							<td><?php echo $project_task_boq_actual_list_data[$count]["approved_by"]; ?></td>

							<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" target="-blank" onclick="return go_to_project_contract_print(<?php echo $project_task_boq_actual_list_data[$count]["project_task_boq_actual_id"]; ?>);">Print</a></div></td>

							<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_task_boq_actual_list_data[$count][
							"project_task_actual_boq_added_on"])); ?></td>
							<td><input type="checkbox" name="cb_item_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_boq_actual_id"] ;?>"><br></td>
							</tr>
							<?php
						}
					}
					}
					else
					{
					?>
					<td colspan="6">No Project Master condition added yet!</td>

					<?php
					}
					 ?>

					</tbody>
				  </table>
      <?php } ?>
				  <?php if(($search_vendor != "") && ($search_project) && ($add_perms_list['status'] == SUCCESS))
				  {?>
				  <div class="modal-footer">
					 <button type="submit" name="items_submit" class="btn btn-primary">Submit</button>
					 </div>
					 <?php
				  }
				  ?>
				   <script>
				  document.getElementById('total_amount').innerHTML = '<?php echo round($total_amount); ?>';
				   </script>
				</div>
				<!-- /widget-content -->
			  </div>
			  <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_task_boq(boq_actual_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_task_boq_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/project_delete_task_boq.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("boq_actual_id=" + boq_actual_id + "&action=0");
		}
	}
}
function go_to_project_edit_task_boq(boq_actual_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_task_boq_actuals.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","boq_actual_id");
	hiddenField1.setAttribute("value",boq_actual_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}

function project_check_task_actual_contract(boq_id,task_id,search_project,search_vendor)
{
	var ok = confirm("Are you sure you want to Ok?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_task_boq_actuals_list.php?search_project=" +search_project+ "&search_vendor=" +search_vendor;
					}
				}
			}

			xmlhttp.open("POST", "project_check_contract_work.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("boq_id=" + boq_id + "&task_id=" +task_id+ "&action=approved");
		}
	}
}
function project_approve_task_actual_contract(boq_id,task_id,search_project,search_vendor)
{
	var ok = confirm("Are you sure you want to Approve?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					  window.location = "project_task_boq_actuals_list.php?search_project=" +search_project+ "&search_vendor=" +search_vendor;
					}
				}
			}

			xmlhttp.open("POST", "project_approve_contract_work.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("boq_id=" + boq_id + "&task_id=" +task_id+ "&action=approved");
		}
	}
}
function go_to_project_contract_print(boq_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_contract_work_print.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","boq_id");
	hiddenField1.setAttribute("value",boq_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>
