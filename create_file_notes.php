<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 15th June 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 1. Module ID should be from config
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET['file']))
	{
		$file_id = $_GET['file'];
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["add_create_file_notes_submit"]))
	{
		if(isset($_POST["file_task"]))
		{
			$file_task = '1';
		}
		else
		{
			$file_task = '0';
		}
		$file_notes = $_POST["file_notes"];
		$file_id    = $_POST["hd_file_id"];
				
		// Check for mandatory fields
		if($file_notes != "")
		{
			$file_notes_iresults = i_add_file_notes($file_id,$file_notes,$file_task,$user);
			
			if($file_notes_iresults["status"] == SUCCESS)
			{
				$alert_type = 1;
			}
			else
			{
				$alert_type = 0;
			}
			
			$alert = $file_notes_iresults["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	// Get list of Users Login
	$file_notes_list = i_get_file_notes_list($file_id,'','');
	if($file_notes_list["status"] == SUCCESS)
	{
		$file_notes_list_data = $file_notes_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$file_notes_list["data"];
	}
}	
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Notes</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Account</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Notes</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_file_note_form" class="form-horizontal" method="post" action="create_file_notes.php">
								<input type="hidden" name="hd_file_id" value="<?php echo $file_id; ?>" />
									<fieldset>										
																				
										<div class="control-group">											
											<label class="control-label" for="file_notes">Notes</label>
											<div class="controls">
												<input type="textarea" class="span6" name="file_notes" placeholder="Notes" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->	
										
										<div class="control-group">											
											<label class="control-label" for="file_task">Task</label>
											<div class="controls">
											    <input type="checkbox" name="file_task" value="<?php echo $file_task ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->	
										
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_create_file_notes_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    <div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>File Notes</th>
				    <th>Task</th>
					<th>Added By</th>					
					<th>Added On</th>
					<th>Update</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				<span id="span_msg"></span>
				<?php
				if($file_notes_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($file_notes_list_data); $count++)
					{						
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $file_notes_list_data[$count]["file_notes_remarks"]; ?></td>
						<td style="word-wrap:break-word;"><?php if($file_notes_list_data[$count]["file_notes_task"] == '1')
						{
							echo 'Pending';
						}
						else if($file_notes_list_data[$count]["file_notes_task"] == '2')
						{
							echo 'Completed';
						}
						else
						{
							echo 'NA';
						}?></td>
						<td style="word-wrap:break-word;"><?php echo $file_notes_list_data[$count]["user_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($file_notes_list_data[$count]["file_notes_added_on"])); ?></td>		
						<td style="word-wrap:break-word;">
						<?php if(($file_notes_list_data[$count]["file_notes_task"] == '1')){?>
						<a href="#" onclick="return update_file_notes_task(<?php echo $file_notes_list_data[$count]["file_notes_id"]; ?>,<?php echo $file_id; ?>);"><span style="color:black; text-decoration: underline;">Update</span></a>
						<?php
						}
						?></td>
						<td style="word-wrap:break-word;"><a href="add_general_task.php?details=<?php echo $file_notes_list_data[$count]["file_notes_remarks"]; ?>">Create Task</a></td>
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<tr>
				<td colspan="7">No File Notes Mapping added!</td>
				</tr>
				<?php
				}
				?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function update_file_notes_task(file_notes_id,file)
{
	var ok = confirm("Are you sure you want to Update?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "create_file_notes.php?file=" + file;
					}
				}
			}

			xmlhttp.open("POST","file_notes_update.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("file_notes_id=" + file_notes_id + "&task=2");
		}
	}	
}
</script>

  </body>

</html>
