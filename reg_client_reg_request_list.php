<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: reg_client_request_list.php
CREATED ON	: 08-Feb-2017
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
//define('REG_CLIENT_REQUEST_LIST_FUNC_ID','197');
define('REG_CLIENT_REQUEST_LIST_FUNC_ID','219');
/* DEFINES - END */

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'registration'.DIRECTORY_SEPARATOR.'reg_client_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
	
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	$alert_type = -1;
	$alert = "";
	
	// Get permission settings for this user for this page
	$add_perms_list    = i_get_user_perms($user,'',REG_CLIENT_REQUEST_LIST_FUNC_ID,'1','1');
	$view_perms_list   = i_get_user_perms($user,'',REG_CLIENT_REQUEST_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',REG_CLIENT_REQUEST_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',REG_CLIENT_REQUEST_LIST_FUNC_ID,'4','1');


	// Query String Data
	// Nothing
	
	$search_req_for   	 = "";
	
	if(isset($_POST["file_search_submit"]))
	{
		$search_req_for   = $_POST["search_req_for"];
	}
	
	$search_added_by   	 = "";
	
	if(isset($_POST["file_search_submit"]))
	{
		$search_added_by   = $_POST["search_added_by"];
	}
	
	// Get BD Own Account Master already added
	$bd_account_list = i_get_account_list('','');
	if($bd_account_list['status'] == SUCCESS)
	{
		$bd_account_list_data = $bd_account_list['data'];
	}
	else
	{
		$alert = $bd_account_list["data"];
		$alert_type = 0;
	}
	
	 // Get Users modes already added
	$user_list = i_get_user_list('','','','1');
	if($user_list['status'] == SUCCESS)
	{
		$user_list_data = $user_list['data'];
	}
	else
	{
		$alert = $user_list["data"];
		$alert_type = 0;
	}

	// Get Reg Request already added
	$reg_client_request_search_data = array("active"=>'1',"request_for"=>$search_req_for,"added_by"=>$search_added_by);
	$reg_client_request_list = i_get_reg_client_request($reg_client_request_search_data);
	if($reg_client_request_list['status'] == SUCCESS)
	{
		$reg_client_request_list_data = $reg_client_request_list['data'];
	}
	else
	{
		
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Reg Client Request List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Reg Client Request List</h3>
			  
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="file_search_form" action="reg_client_reg_request_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_req_for">
			  <option value="">- - Select Registrant - -</option>
			  <?php
			  for($user_count = 0; $user_count < count($bd_account_list_data); $user_count++)
			  {
			  ?>
			  <option value="<?php echo $bd_account_list_data[$user_count]["bd_own_accunt_master_id"]; ?>" <?php if($search_req_for == $bd_account_list_data[$user_count]["bd_own_accunt_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $bd_account_list_data[$user_count]["bd_own_account_master_account_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_added_by">
			  <option value="">- - Select Assigned To - -</option>
			  <?php
			  for($user_count = 0; $user_count < count($user_list_data); $user_count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($search_added_by == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			 
			  <input type="submit" name="file_search_submit" />
			  </form>			  
            </div>
            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Client</th>
					<th>Project</th>
					<th>Site No</th>
					<th>Registrant</th>
					<th>Plan Date</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>									
					<th colspan="5" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($reg_client_request_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($reg_client_request_list_data); $count++)
					{
						$sl_no++;
						
						// Get booking details
						$booking_details = i_get_site_booking($reg_client_request_list_data[$count]["reg_client_request_booking_id"],'','','','','','','','','','','');
						
						if($booking_details['status'] == SUCCESS)
						{
							$cust_name    = $booking_details['data'][0]['name'];
							$project_name = $booking_details['data'][0]['project_name'];
							$site_no      = $booking_details['data'][0]['crm_site_no'];
						}
						else
						{
							$cust_name    = '<i>Some error</i>';
							$project_name = '<i>Some error</i>';
							$site_no      = '<i>Some error</i>';
						}
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $cust_name; ?></td>
					<td><?php echo $project_name; ?></td>
					<td><?php echo $site_no; ?></td>
					<td><?php echo $reg_client_request_list_data[$count]["bd_own_account_master_account_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($reg_client_request_list_data[$count][
					"reg_client_requested_on"])); ?></td>
					<td><?php echo $reg_client_request_list_data[$count]["reg_client_request_remarks"]; ?></td>
					<td><?php echo $reg_client_request_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($reg_client_request_list_data[$count][
					"reg_client_request_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_reg_client_edit_request('<?php echo $reg_client_request_list_data[$count]["reg_client_request_id"]; ?>');">Edit </a><?php } ?></td>
					<td><?php if(($reg_client_request_list_data[$count]["reg_client_request_active"] == "1") && ($delete_perms_list['status'] == SUCCESS)){?><a href="#" onclick="return reg_client_delete_request(<?php echo $reg_client_request_list_data[$count]["reg_client_request_id"]; ?>);">Delete</a><?php } ?></td>
					<td><?php if(($reg_client_request_list_data[$count]["reg_client_request_status"] == "Pending")){?><a href="#" onclick="return reg_client_status_request(<?php echo $reg_client_request_list_data[$count]["reg_client_request_id"]; ?>);">Mark Complete</a><?php } else { ?>Completed <?php }?></td>
					<td style="word-wrap:break-word;"><?php if($add_perms_list['status'] == SUCCESS){ ?><a style="padding-right:10px" href="#" onclick="return go_to_reg_client_process('<?php echo $reg_client_request_list_data[$count]["reg_client_request_id"]; ?>');">Process </a><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if($view_perms_list['status'] == SUCCESS){ ?><a style="padding-right:10px" href="#" onclick="return go_to_reg_client_upload_document('<?php echo $reg_client_request_list_data[$count]["reg_client_request_id"]; ?>');">Documents </a><?php } ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="14">No registration request added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			   <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function reg_client_delete_request(request_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "reg_client_reg_request_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/reg_client_delete_reg_request.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("request_id=" + request_id + "&action=0");
		}
	}	
}

function reg_client_status_request(request_id)
{
	var ok = confirm("Are you sure you want to complete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "reg_client_reg_request_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/reg_client_status_reg_request.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("request_id=" + request_id + "&action=Completed");
		}
	}	
}

function go_to_reg_client_edit_request(request_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "reg_client_edit_reg_request.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","request_id");
	hiddenField1.setAttribute("value",request_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_reg_client_process(request_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "reg_client_reg_process_list.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","request_id");
	hiddenField1.setAttribute("value",request_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
function go_to_reg_client_upload_document(request_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "reg_client_document_list.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","request_id");
	hiddenField1.setAttribute("value",request_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>