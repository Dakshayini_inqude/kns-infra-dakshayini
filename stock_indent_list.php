<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: crm_indent_list.php

CREATED ON	: 28-Sep-2016

CREATED BY	: Lakshmi

PURPOSE     : List of indent for customer withdrawals

*/



/*

TBD: 

*/



/* DEFINES - START */

define('INDENT_FUNC_ID','164');

/* DEFINES - END */



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	

	// Get permission settings for this user for this page

	$view_perms_list   = i_get_user_perms($user,'',INDENT_FUNC_ID,'2','1');

	$edit_perms_list   = i_get_user_perms($user,'',INDENT_FUNC_ID,'3','1');

	$delete_perms_list = i_get_user_perms($user,'',INDENT_FUNC_ID,'4','1');

	$add_perms_list    = i_get_user_perms($user,'',INDENT_FUNC_ID,'1','1');

	

	/* DATA INITIALIZATION - START */

	$alert_type = -1;

	$alert = "";



	// Query String Data

	// Nothing

	//Temp Data

	

	$search_department	 = "";

	$search_project   	 = "";	

	$search_requested_by = $user;

	$start_date			 = "";

	$end_date			 = "";

	$search_status		 = "Pending";

	

	if(isset($_POST["indent_search_submit"]))

	{

		$search_department    = $_POST["search_department"];

		$search_project  	  = $_POST["search_project"];		

		$search_status 	 	  = $_POST["search_status"];		

		if($edit_perms_list['status'] == SUCCESS)

		{

			$search_requested_by = $_POST["requested_by"];

		}

		else

		{

			$search_requested_by = $user;

		}

		$start_date 		  = $_POST["start_date"];

		$end_date 			  = $_POST["end_date"];

	}

	

	// Indent data

	$stock_indent_search_data = array("active"=>'1');

	if($search_department != '')

	{

		$stock_indent_search_data['department'] = $search_department;

	}

	if($search_project != '')

	{

		$stock_indent_search_data['project'] = $search_project;

	}

	if($search_requested_by != '')

	{

		$stock_indent_search_data['requested_by'] = $search_requested_by;

	}

	if($start_date != '')

	{

		$stock_indent_search_data['start_date'] = $start_date;

	}

	if($end_date != '')

	{

		$stock_indent_search_data['end_date'] = $end_date;

	}

	$indent_list = i_get_stock_indent_list($stock_indent_search_data);

	if($indent_list["status"] == SUCCESS)

	{

		$indent_list_data = $indent_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$indent_list["data"];

	}

	

	//Get Department List

	$department_list = i_get_department_list('','');

	if($department_list["status"] == SUCCESS)

	{

		$department_list_data = $department_list["data"];

	}

	else

	{

		$alert = $department_list["data"];

		$alert_type = 0;

	}

	

	//Get Project List

	$stock_project_search_data = array();

	$project_list = i_get_project_list($stock_project_search_data);

	if($project_list["status"] == SUCCESS)

	{

		$project_list_data = $project_list["data"];

	}

	else

	{

		$alert = $project_list["data"];

		$alert_type = 0;

	}	

	

	// Get User List	

	$user_list = i_get_user_list('','','','','1');

	if($user_list["status"] == SUCCESS)

	{

		$user_list_data = $user_list["data"];

	}	

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Indent List</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>

    



<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">

       

          <div class="span6" style="width:100%;">

          

          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>Indent List</h3><?php if($add_perms_list['status'] == SUCCESS){ ?><span style="float:right; padding-right:20px;"><a href="stock_add_indent.php">Add Indent</a></span><?php } ?>

            </div>

            <!-- /widget-header -->

			

			<div class="widget-header" style="height:80px; padding-top:10px;">               

			  <form method="post" id="file_search_form" action="stock_indent_list.php">

			  <span style="padding-left:20px; padding-right:20px;">

			  <select name="search_project">

			  <option value="">- - Select Project - -</option>

			  <?php

			  for($process_count = 0; $process_count < count($project_list_data); $process_count++)

			  {

			  ?>

			  <option value="<?php echo $project_list_data[$process_count]["stock_project_id"]; ?>" <?php if($search_project == $project_list_data[$process_count]["stock_project_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_list_data[$process_count]["stock_project_name"]; ?></option>

			  <?php

			  }

			  ?>

			  </select>

			  </span>

			  

			  <?php if($edit_perms_list['status'] == SUCCESS)

			  {

				  ?>

				<span style="padding-left:20px; padding-right:20px;">

				<select name="requested_by">

				  <option value="">- - Select Requester - -</option>

				  <?php

				  for($user_count = 0; $user_count < count($user_list_data); $user_count++)

				  {

				  ?>

				  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($search_requested_by == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>

				  <?php

				  }

				  ?>

				  </select>

				</span>

				<?php

			  }

			  ?>

			  

			  <span style="padding-left:20px; padding-right:20px;">

			  <input type="date" name="start_date" value="<?php echo $start_date; ?>" placeholder="Search by Start date" />

			  </span>

			  

			  <span style="padding-left:20px; padding-right:20px;">

			  <input type="date" name="end_date" value="<?php echo $end_date; ?>" placeholder="Search by End Date" />

			  </span>

			  

			  <span style="padding-left:20px; padding-right:20px;">

			  <select name="search_department">

			  <option value="">- - Select Department - -</option>

			  <?php

			  for($process_count = 0; $process_count < count($department_list_data); $process_count++)

			  {

			  ?>

			  <option value="<?php echo $department_list_data[$process_count]["general_task_department_id"]; ?>" <?php if($search_department == $department_list_data[$process_count]["general_task_department_id"]) { ?> selected="selected" <?php } ?>><?php echo $department_list_data[$process_count]["general_task_department_name"]; ?></option>

			  <?php

			  }

			  ?>

			  </select>

			  </span>  

			  <span style="padding-left:20px; padding-right:20px;">

			  <select name="search_status">

				<option value="Pending" <?php if($search_status == "Pending"){?> selected <?php } ?>>Pending</option>										

				<option value="Completed" <?php if($search_status == "Completed"){?> selected <?php } ?>>Completed</option>

			  </select>

			  </span>

			  

			  <input type="submit" name="indent_search_submit" />

			  </form>			  

            </div>

			

            <div class="widget-content">

			

              <table class="table table-bordered" style="table-layout: fixed;">

                <thead>

                  <tr>

				    <th>SL No</th>

					<th>Indent No</th>

					<th>Project</th>

					<th>Department</th>					

					<th>Requested By</th>

					<th>Requested On</th>

					<th>Remarks</th>										

					<th style="text-align:center;">Actions</th>									

				</tr>

				</thead>

				<tbody>							

				<?php

				if($indent_list["status"] == SUCCESS)

				{

					$sl_no = 0;

					for($count = 0; $count < count($indent_list_data); $count++)

					{

						$sl_no++;	

						$stock_indent_items_search_data = array("indent_id"=>$indent_list["data"][$count]["stock_indent_id"],"active"=>'1');

						$indent_item_list = i_get_indent_items_list($stock_indent_items_search_data);

						if($indent_item_list['status'] == SUCCESS)

						{

							$stock_indent_items_search_data = array("status"=>'Approved',"indent_id"=>$indent_list["data"][$count]["stock_indent_id"],"active"=>'1');

							$indent_item_list = i_get_indent_items_list($stock_indent_items_search_data);

							if($indent_item_list["status"] == SUCCESS)

							{

								$status = 'Pending';								

							}

							else

							{

								$stock_indent_items_search_data = array("status"=>'Waiting',"indent_id"=>$indent_list["data"][$count]["stock_indent_id"],"active"=>'1');

								$indent_item_list = i_get_indent_items_list($stock_indent_items_search_data);

								if($indent_item_list["status"] == SUCCESS)

								{

									$status = 'Pending';								

								}

								else

								{

									$stock_indent_items_search_data = array("status"=>'Pending',"indent_id"=>$indent_list["data"][$count]["stock_indent_id"],"active"=>'1');

									$indent_item_list = i_get_indent_items_list($stock_indent_items_search_data);

									if($indent_item_list["status"] == SUCCESS)

									{

										$status = 'Pending';										

									}

									else

									{

										$status = 'Completed';								

									}

								}

							}

						}	

						else

						{

							$status = 'Pending';							

						}

						if($status == $search_status)

						{							

						?>

							<tr>

							<td><?php echo $sl_no; ?></td>

							<td><?php echo $indent_list_data[$count]["stock_indent_no"]; ?></td>

							<td><?php echo $indent_list_data[$count]["stock_project_name"]; ?></td>

							<td><?php echo $indent_list_data[$count]["general_task_department_name"]; ?></td>					

							<td><?php echo $indent_list_data[$count]["user_name"]; ?></td>

							<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($indent_list_data[$count]["stock_indent_added_on"])); ?></td>

							<td><?php echo $indent_list_data[$count]["stock_indent_remarks"]; ?></td>								

							<td><a href="#" onclick="return go_to_item_details('<?php echo $indent_list_data[$count]["stock_indent_id"]; ?>','<?php echo $indent_list_data[$count]["stock_indent_status"];?>');">Details</a></td>

							</tr>

						<?php

						}

					}

				}

				else

				{

				?>

				<td colspan="8">No indent added yet!</td>

				

				<?php

				}

				 ?>	



                </tbody>

              </table>

            </div>

            <!-- /widget-content --> 

          </div>

          <!-- /widget --> 

         

          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function approve_indent(indent_id)

{

	var ok = confirm("Are you sure you want to Approve?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "http://localhost/kns/Legal/stock_indent_list.php";

					}

				}

			}



			xmlhttp.open("POST", "approve_indent.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("indent_id=" + indent_id + "&action=Approved");

		}

	}	

}



function reject_indent(indent_id)

{

	var ok = confirm("Are you sure you want to Reject?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "http://localhost/kns/Legal/stock_indent_list.php";

					}

				}

			}



			xmlhttp.open("POST", "reject_indent.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("indent_id=" + indent_id + "&action=Rejected");

		}

	}	

}

function delete_indent(indent_id)

{

	var ok = confirm("Are you sure you want to Delete?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "http://localhost/kns/Legal/stock_indent_list.php";

					}

				}

			}



			xmlhttp.open("POST", "delete_indent.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("indent_id=" + indent_id + "&action=0");

		}

	}	

}

function go_to_item_details(indent_id,status)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_add_indent_material.php");

	

	var hiddenField2 = document.createElement("input");

	hiddenField2.setAttribute("type","hidden");

	hiddenField2.setAttribute("name","indent_id");

	hiddenField2.setAttribute("value",indent_id);

	

	var hiddenField3 = document.createElement("input");

	hiddenField3.setAttribute("type","hidden");

	hiddenField3.setAttribute("name","status");

	hiddenField3.setAttribute("value",status);

    	

	form.appendChild(hiddenField2);	

	form.appendChild(hiddenField3);	

	

	document.body.appendChild(form);

    form.submit();

}



</script>



  </body>



</html>