<?php
  session_start();
  $_SESSION['module'] = 'PM Masters';
  define('PROJECT_MASTER_FUNC_ID', '180');

  $base = $_SERVER["DOCUMENT_ROOT"];
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

  if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")){
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_FUNC_ID, '2', '1');
    $edit_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_FUNC_ID, '3', '1');
    $delete_perms_list = i_get_user_perms($user, '', PROJECT_MASTER_FUNC_ID, '4', '1');
    $add_perms_list    = i_get_user_perms($user, '', PROJECT_MASTER_FUNC_ID, '1', '1');

    $alert_type = -1;
    $alert = "";

    $search_project = "";

    if (isset($_POST["search_project"])) {
        $search_project   = $_POST["search_project"];
    // set here
    } else {
        $search_project = "";
    }

    $search_vendor = "";
    if (isset($_REQUEST["search_vendor"])) {
        $search_vendor = $_REQUEST["search_vendor"];
    }

    $start_date = "";
    if (isset($_REQUEST["dt_start_date"])) {
        $start_date = $_REQUEST["dt_start_date"];
    }

    $end_date = "";
    if (isset($_REQUEST["dt_end_date"])) {
        $end_date = $_REQUEST["dt_end_date"];
    }

    $show_checkbox = isset($search_vendor)
                     && !empty($search_vendor)
                     && isset($search_project)
                     && !empty($search_project)
                     && isset($start_date)
                     && !empty($start_date)
                     && isset($end_date)
                     && !empty($end_date);

    // Process Master
    $project_process_master_search_data = array("active"=>'1');
    $project_process_master_list = i_get_project_process_master($project_process_master_search_data);
    if ($project_process_master_list["status"] == SUCCESS) {
        $project_process_master_list_data = $project_process_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_process_master_list["data"];
    }

    // Project Manpower Vendor Master List
    $project_manpower_agency_search_data = array("active"=>'1');
    $project_manpower_vendor_master_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
    if ($project_manpower_vendor_master_list["status"] == SUCCESS) {
        $project_manpower_vendor_master_list_data = $project_manpower_vendor_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_manpower_vendor_master_list["data"];
    }

    //Get Location List
    $stock_location_master_search_data = array();
    $location_list = i_get_stock_location_master_list($stock_location_master_search_data);
    if ($location_list["status"] == SUCCESS) {
        $location_list_data = $location_list["data"];
    } else {
        $alert = $location_list["data"];
        $alert_type = 0;
    }

    // Project data
    $project_management_master_search_data = array("active"=>'1',"project_id"=>$search_project,"user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list["status"] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_management_master_list["data"];
    }

  } else {
  	header("location:login.php");
  }
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Daily Approved Manpower List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
    <script type="text/javascript">

    var table;
    function toggleSelection(ele) {
      $("input.input-checkbox").prop("checked", ele.checked);
    }

    function drawTable(){
      table.draw();
    }

    function submit_values() {

      var manpower = [];
      var tableData = table.data();
      // get all the 'checked' input:checkbox(s)
      $("table.DTFC_Cloned input.input-checkbox:checked").each(function(i, ele){
        manpower.push( tableData[parseInt(ele.value)] );
      });

      if($('input#toggle_checkbox').is(':checked')){
        manpower = tableData;
      } else if(!manpower.length){
        manpower = [];
      }

      if(!manpower.length){
        alert('Error: Select atleast one row to Bill');
        return false;
      }

      var tmpdata = [];
      $.each(manpower, function(i, value){
        tmpdata.push(value);
      });

        $.ajax({
        url: 'ajax/project_select_payment.php',
        type: 'POST',
        data: {
          val: tmpdata,
          vendor_id: $('#search_vendor').val(),
        },
        dataType: 'json',
        success: function(response) {
          if (response["status"] == 0) {
            var form = document.createElement("form");
            form.setAttribute("method", "get");
            form.setAttribute("action", "project_add_bill_no.php");
            form.setAttribute("target", "_blank");
            var hiddenField1 = document.createElement("input");
            hiddenField1.setAttribute("type", "hidden");
            hiddenField1.setAttribute("name", "manpower_id");
            hiddenField1.setAttribute("value", response.id);
            form.appendChild(hiddenField1);
            document.body.appendChild(form);
            form.submit();
          }
        }
      });
    }

      $(document).ready(function() {

        // get hidden values saved from filters
        function getVars(key) {
          var element = document.getElementById(key);
          if (element) {
            return element.value;
          } else {
            return '';
          }
        }

        // set search filters' as hidden value
        function setVars(elements) {

          for (var element in elements) {
            if (element && elements[element].length) {
              var hiddenField1 = document.createElement("input");
              hiddenField1.setAttribute("type", "hidden");
              hiddenField1.setAttribute("id", element);
              hiddenField1.setAttribute("value", elements[element]);
              document.body.appendChild(hiddenField1);
            }
          }
        }

        table = $('#example').DataTable({
          stateSave: true,
          serverSide: true,
          dataSrc: 'aaData',
          ajax: 'datatable/project_task_approved_actual_mp_list.php',
          pageLength: 10,
          scrollY: 600,
          processing: true,
          scrollCollapse: true,
          fixedHeader: true,
          searchDelay: 1000,
          drawCallback: function(){
            manpower = [];
            $("input#toggle_checkbox").prop("checked", false);
            $("input.input-checkbox").prop("checked", false);
          },
          fnServerParams: function(aoData) {
            aoData.aaSorting = aoData.order;
            aoData.iDisplayLength = aoData.length;
            aoData.iDisplayStart = aoData.start;
            aoData.table = "project_task_actual_manpower_list";
            aoData.search_project = $('#search_project').val();
            aoData.search_vendor = $('#search_vendor').val();
            aoData.start_date = $('#start_date').val();
            aoData.end_date = $('#end_date').val();

            if(aoData.search && aoData.search.value){
              aoData.sSearch = aoData.search.value;
            }
          },
          buttons: [{
              extend: 'colvis',
              columns: ':not(.noVis)'
            }
          ],
          fixedColumns: {
              leftColumns: 3,
              rightColumns: <?php echo $show_checkbox? 3 : 2; ?>
          },
          scrollX: true,
          columns: [
            {
              className: 'noVis',
              "orderable": false,
              "data": function(){
                return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
              }
            },
            { "data": "project_manpower_agency_name", "orderable": false },
            { "data": "project_master_name", "orderable": false },
            { "data": "project_process_master_name", "orderable": false },
            { "data": "project_task_master_name", "orderable": false },
            { "data": function(data, type, full) {
                if (type !== 'display') {
                  return '';
                }
                if (data.project_task_actual_manpower_road_id == 'No Roads') {
                  return 'No Roads';
                }
                return data.project_site_location_mapping_master_name;
              },
              "orderable": false
            },
            { "data": "project_task_actual_manpower_work_type", "orderable": false },
            { "data": function(data, type, full) {
                if (type !== 'display') {
                  return '';
                }

                if (data.project_task_actual_manpower_work_type === 'Regular') {
                  return data.project_task_actual_manpower_completion_percentage;
                }
                return data.project_task_actual_manpower_rework_completion;
              },
              "orderable": false
            },
            { "data": "project_task_actual_manpower_completed_msmrt", "orderable": false },
// Men
            { "data": function(data, type, full) {
                if (type !== 'display') {
                  return '';
                }
                return data.project_task_actual_manpower_no_of_men;
              },
              "orderable": false
            },

            { "data": function(data, type, full) {
                if (type !== 'display') {
                  return '';
                }
                return data.project_task_actual_manpower_men_rate;
              },
              "orderable": false
            },

            { "data": function(data, type, full) {
                if (type !== 'display') {
                  return '';
                }
                return data.project_task_actual_manpower_no_of_men / 8;
              },
              "orderable": false
            },
// Women
            { "data": function(data, type, full) {
                if (type !== 'display') {
                  return '';
                }
                return data.project_task_actual_manpower_no_of_women;
              },
              "orderable": false
            },
            { "data": function(data, type, full) {
                if (type !== 'display') {
                  return '';
                }
                return data.project_task_actual_manpower_women_rate;
              },
              "orderable": false
            },
            { "data": function(data, type, full) {
                if (type !== 'display') {
                  return '';
                }
                return data.project_task_actual_manpower_no_of_women / 8;
              },
              "orderable": false
            },
// Mason
            { "data": function(data, type, full) {
                if (type !== 'display') {
                  return '';
                }
                return data.project_task_actual_manpower_no_of_mason;
              },
              "orderable": false
            },
            { "data": function(data, type, full) {
                if (type !== 'display') {
                  return '';
                }
                return data.project_task_actual_manpower_mason_rate;
              },
              "orderable": false
            },
            { "data": function(data, type, full) {
                if (type !== 'display') {
                  return '';
                }
                return data.project_task_actual_manpower_no_of_mason / 8;
              },
              "orderable": false
            },

            { "data": function(data, type, full) {
                if (type !== 'display') {
                  return '';
                }
                return parseFloat(data.project_task_actual_manpower_no_of_men / 8) + parseFloat(data.project_task_actual_manpower_no_of_women / 8) + parseFloat(data.project_task_actual_manpower_no_of_mason / 8);
              },
              "orderable": false
            },
            { "data": function(data, type, full) {
                if (type !== 'display') {
                  return '';
                }
                return (parseFloat(data.project_task_actual_manpower_no_of_men) * parseFloat(data.project_task_actual_manpower_men_rate)) + (parseFloat(data.project_task_actual_manpower_no_of_women) * parseFloat(data.project_task_actual_manpower_women_rate)) + (parseFloat(data.project_task_actual_manpower_no_of_mason) * parseFloat(data.project_task_actual_manpower_mason_rate));
              },
              "orderable": false
            },
            { "data": "project_task_actual_manpower_remarks", "orderable": false },
            { "data": "user_name", "orderable": false },
            { "data": function(data, type, full) {
              if (type !== 'display') {
                  return '';
              }
                return moment(data.project_task_actual_manpower_added_on).format('DD-MM-YYYY');
              },
              "orderable": false
            },
            { "data": function(data, type, full) {
                if (type !== 'display') {
                  return '';
                }
                return moment(data.project_task_actual_manpower_date).format('DD-MM-YYYY');
              },
              "orderable": false
            },
            {
              "orderable": false,
              "className": 'noVis',
              "data": function(data, type, full) {
                if (type !== 'display') {
                  return '';
                }
                return '<a target="_blank" href="project_labour_report_print.php?manpower_id='+data.project_task_actual_manpower_id+'"><span class="glyphicon glyphicon-print"></span></a>';
              }
            },
            <?php if($show_checkbox){ ?>
            {
              "orderable": false,
              "className": 'noVis',
              "data": function() {
                if (arguments[1] !== 'display') {
                  return '';
                }
                return '<input type="checkbox" class="input-checkbox" value="'+arguments[3].row+'" >';
              }
            }
            <?php } ?>
          ]
        });
      } );
    </script>
    <link href="./css/style.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
    <style media="screen">
      table.dataTable {
        margin-top: 0px !important;
      }
    </style>
  </head>
  <body>
    <?php
      include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
    ?>
    <div class="main margin-top">
        <div class="main-inner">
          <div class="container">
            <div class="row">

                <div class="span6" style="width:100%;">

                <div class="widget widget-table action-table">
                  <div class="widget-header">
                    <h3>Project Task Actual Man Power List</h3>
                    <?php if ($show_checkbox) { ?>
                    <button type="button" id="submit"
                            onclick="submit_values()" name="items_submit"
                            class="btn btn-success pull-right btn-sm"
                            style="margin: 5px 5px 0px 0px;">Generate Bill</button>
           				 <?php } ?>
                  </div>

                  <div class="widget-header widget-toolbar">
                    <?php
                          if ($view_perms_list['status'] == SUCCESS) {
                              ?>
                      <form class="form-inline" method="post" id="file_search_form">

                      <select id="search_vendor" name="search_vendor" class="form-control">
                      <option value="">- - Select Vendor - -</option>
                      <?php
                                      for ($project_count = 0; $project_count < count($project_manpower_vendor_master_list_data); $project_count++) {
                                          ?>
    								  <option value="<?php echo $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_id"]; ?>" <?php if ($search_vendor == $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_id"]) {
                                              ?> selected="selected" <?php
                                          } ?>><?php echo $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_name"]; ?></option>
    								  <?php
                                      } ?>
                      </select>
                      <select id="search_project" name="search_project" class="form-control">
                        <option value="">- - Select Project - -</option>
      								  <?php
                                        for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) {
                                            ?>
      								  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if ($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) {
                                                ?> selected="selected" <?php
                                            } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
      								  <?php
                                        } ?>
      								  </select>
                      <input type="date" id="start_date" name="dt_start_date" value="<?php echo $start_date; ?>"  class="form-control"/>
                      <input type="date" id="end_date" name="dt_end_date" value="<?php echo $end_date; ?>"  class="form-control"/>
                      <button type="submit" class="btn btn-primary">Submit</button>
                      </form>
                      <?php } ?>
                    </div>
                  </div>

                  <!-- /widget-header -->
                  <div class="widget-content">
                    <table id="example" class="table table-striped table-bordered display nowrap">
                      <thead>
                          <tr>
                            <th colspan="9"></th>
                            <th colspan="3" class="center">Men</th>
                            <th colspan="3" class="center">Women</th>
                            <th colspan="3" class="center">Mason</th>
                            <?php if($show_checkbox){ ?>
                              <th colspan="8"></th>
                            <?php } else { ?>
                            <th colspan="7"></th>
                            <?php }  ?>
                          </tr>
                          <tr>
                              <th>#</th>
                              <th>Agency</th>
                              <th>Project</th>
                              <th>Process</th>
                              <th>Task Name</th>
                              <th>Road Name</th>
                              <th>Work Type</th>
                              <th>%</th>
                              <th>Msrmt</th>
                              <th>Hour</th>
                              <th>Rate</th>
                              <th>Men</th>

                              <th>Hour</th>
                              <th>Rate</th>
                              <th>Women</th>

                              <th>Hour</th>
                              <th>Rate</th>
                              <th>Mason</th>

                              <th>No of People</th>
                              <th>Amount</th>
                              <th>Remarks</th>
                              <th>Aproved By</th>
                              <th>Aproved On</th>
                              <th>Billed Date</th>
                              <th>Print</th>
                              <?php if($show_checkbox){ ?>
                                <th><input type="checkbox" id="toggle_checkbox"/ onchange="toggleSelection(this)"></th>
                              <?php } ?>
                          </tr>
                      </thead>
                  </table>
                </div>
                <!-- /widget-content -->

                </div>
                <!-- /widget -->

                </div>
                <!-- /widget -->
              </div>
              <!-- /span6 -->
            </div>
            <!-- /row -->
          </div>
          <!-- /container -->
        </div>

  </body>
</html>
