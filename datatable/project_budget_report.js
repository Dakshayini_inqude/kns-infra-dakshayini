function checkFileAttached(){
  $('#formImport').submit();
}

$(document).ready(function() {
  var columnsMapping = {
    '6': 'planned_mp',
    '7': 'planned_mc',
    '8': 'planned_cw',
    '9': 'planned_material',

    '10': 'total_mp_cost',
    '11': 'total_mc_cost',
    '12': 'total_actual_cw_cost',

    '13': 'variance_mp_cost',
    '14': 'variance_mc_cost',
    '15': 'variance_actual_cw_cost'
  };

  var diffMapping = {
    "total_mp_cost": "planned_mp",
    "total_mc_cost": "planned_mc",
    "total_actual_cw_cost": "planned_cw",
    "total_actual_material_cost": "planned_material",
  };

  function createToolTip(str, length){
    var substr = (str.length <= 20)? str : str.substr(0, 20)+'...';
    return '<abbr data-toggle="tooltip" data-placement="right" title="'+str+'">'+substr+'</abbr>'
  }

  var table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    scrollX: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_budget_report_datatable.php',
    fnRowCallback: function(row, full, index){
      $(row).attr('id', 'row_'+index);

      // get the planned
      $.ajax({
       url: 'ajax/project_get_budget_planned_data.php',
       data: "task_id=" + full['task_id'] + "&road_id=" + full['road_id'],
       dataType: 'json',
       success: function(planned_response) {
         $.each(planned_response, function(key, value){
           $('span.'+key+'_'+index).html(value);
         });

         // get the actuals
         $.ajax({
          url: 'ajax/project_get_actual_data.php',
          data: "task_id=" + full['task_id'] + "&road_id=" + full['road_id'],
          dataType: 'json',
          success: function(actual_response) {
            $.each(actual_response, function(key, value){
              // first print the actual
              $('span.'+key+'_'+index).html(value);

              // next print the diff
              // diff = planned - actual
              var diff_value = planned_response[diffMapping[key]] - value;
              var className = key.replace('total', 'variance')+'_'+index;
              $('span.'+className).html(diff_value);
            })
          }});
       }});
    },
    fnServerParams: function(aoData) {
      aoData.project_id = $('#project_id').val();
      aoData.table = "project_budget_details";
      aoData.search_project = getVars('search_project');
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
    },
    scrollY: 600,
    scrollCollapse: true,
    fixedHeader: true,
    drawCallback: function(){
      $('[data-toggle="tooltip"]').tooltip();
    },
    columns: [{
        orderable: false,
        data: function(){
          return arguments[3].row + 1;
        }
      },
      {
        orderable: true,
        data: 'project_master_name'
      },
      {
        orderable: false,
        data: function(data){
          return createToolTip(data.project_process_master_name, 20);
        },
      },
      {
        orderable: false,
        data: function(data){
          return createToolTip(data.project_task_master_name, 20);
        },
      },
      {
        orderable: false,
        data: function(data, type, full) {
          if(data['road_id'] == 'No Roads') {
            return 'No Roads';
          }
          return data['project_site_location_mapping_master_name'];
        }
      },
      {
        orderable: false,
        data: `project_uom_name`
      }
    ],
    columnDefs: [
      {
        orderable: false,
        targets: [6, 7, 8, 9, 10, 11, 12,13,14,15],
        data: function(data, type, full, meta) {
          var classname = columnsMapping[meta.col]+'_'+meta.row;
          return '<span class="'+classname+'">loading..</span>';
        }
      }
    ]
  });
});

// get hidden values saved from filters
function getVars(key) {
  var element = document.getElementById(key);
  if (element) {
    return element.value;
  } else {
    return '';
  }
}
