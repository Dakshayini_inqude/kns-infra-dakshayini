var manpower = [];
var tabledata = [];
$(document).ready(function() {
  var table = $('#example').dataTable({
    "sScrollX": "100%",
    "sScrollXInner": "110%",
    "sPaginationType": "full_numbers",
    "bScrollCollapse": true,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": 'datatable/project_task_approved_actual_mp_list.php',
    "iDisplayLength": 10,
    // "sScrollY": "150%",
    "bInfo": true,
    "fixedColumns": true,
    "iTabIndex": 1,
    "lengthChange": false,
    language: {
      search: "_INPUT_",
      searchPlaceholder: "Search...",
      "infoFiltered": ""
    },
    "fnServerParams": function(aoData) {
      aoData.push({
        "name": "table",
        "value": "project_task_actual_manpower_list"
      });
      aoData.push({
        "name": "search_project",
        "value": getVars('search_project')
      });
      aoData.push({
        "name": "search_vendor",
        "value": getVars('search_vendor')
      });
      aoData.push({
        "name": "start_date",
        "value": getVars('start_date')
      });
      aoData.push({
        "name": "end_date",
        "value": getVars('end_date')
      });
    },
    "aaSorting": [
      [17, 'desc']
    ],
    "fnCreatedRow": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      $(nRow).attr('data-row-index', iDisplayIndex);
    },
    "fnDrawCallback": function(oSettings) {
      /* Need to redo the counters if filtered or sorted */
      if (oSettings.bSorted || oSettings.bFiltered || oSettings.iDraw > 1) {
        for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
          this.fnUpdate(i + oSettings._iDisplayStart + 1, oSettings.aiDisplay[i], 0, false, false);
        }
      }
    },
    "aoColumnDefs": [{
        "bSortable": false,
        "aTargets": [0]
      },
      {
        "bSortable": false,
        "aTargets": [1],
        "mData": `project_manpower_agency_name`
      },
      {
        "bSortable": false,
        "aTargets": [2],
        "mData": `project_master_name`,
      },
      {
        "bSortable": false,
        "aTargets": [3],
        "mData": 'project_process_master_name'
      },
      {
        "bSortable": false,
        "aTargets": [4],
        "mData": `project_task_master_name`
      },
      {
        "bSortable": false,
        "aTargets": [5],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          if (data.project_task_actual_manpower_road_id == 'No Roads') {
            return 'No Roads';
          }
          return data.project_site_location_mapping_master_name;
        }
      },
      {
        "bSortable": false,
        "aTargets": [6],
        "mData": `project_task_actual_manpower_work_type`
      },
      {
        "bSortable": false,
        "aTargets": [7],
        "mData": function(data, type, full) {

          if (type !== 'display') {
            return '';
          }

          if (data.project_task_actual_manpower_work_type === 'Regular') {
            return data.project_task_actual_manpower_completion_percentage;
          } else {
            return data.project_task_actual_manpower_rework_completion;
          }
        }
      },
      {
        "bSortable": false,
        "aTargets": [8],
        "mData": `project_task_actual_manpower_completed_msmrt`
      },
      {
        "bSortable": false,
        "aTargets": [9],
        "mData": function(data, type, full) {

          if (type !== 'display') {
            return '';
          }
          return moment(data.project_task_actual_manpower_date).format('DD-MM-YYYY');
        }
      },
      {
        "bSortable": false,
        "aTargets": [10],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return ' Hours: ' + data.project_task_actual_manpower_no_of_men + '<br/>' + ' Rate : ' + data.project_task_actual_manpower_men_rate + ' <br/> ' + ' Men: ' + data.project_task_actual_manpower_no_of_men / 8;
        }
      },
      {
        "bSortable": false,
        "aTargets": [11],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return ' Hours: ' + data.project_task_actual_manpower_no_of_women + '<br/>' + ' Rate : ' + data.project_task_actual_manpower_women_rate + '<br/>' + ' Women: ' + data.project_task_actual_manpower_no_of_women / 8;
        }
      },
      {
        "bSortable": false,
        "aTargets": [12],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return ' Hours: ' + data.project_task_actual_manpower_no_of_mason + '<br/>' + ' Rate : ' + data.project_task_actual_manpower_mason_rate + ' <br/> ' + ' Mason: ' + data.project_task_actual_manpower_no_of_mason / 8;
        }
      },
      {
        // No of People
        "bSortable": false,
        "aTargets": [13],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return parseFloat(data.project_task_actual_manpower_no_of_men / 8) + parseFloat(data.project_task_actual_manpower_no_of_women / 8) + parseFloat(data.project_task_actual_manpower_no_of_mason / 8);
        }
      },
      {
        // Amount
        "bSortable": false,
        "aTargets": [14],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return (parseFloat(data.project_task_actual_manpower_no_of_men) * parseFloat(data.project_task_actual_manpower_men_rate)) + (parseFloat(data.project_task_actual_manpower_no_of_women) * parseFloat(data.project_task_actual_manpower_women_rate)) + (parseFloat(data.project_task_actual_manpower_no_of_mason) * parseFloat(data.project_task_actual_manpower_mason_rate));
        }
      },
      {
        // Remarks
        "bSortable": false,
        "aTargets": [15],
        "mData": `project_task_actual_manpower_remarks`
      },
      {
        // Remarks
        "bSortable": false,
        "aTargets": [16],
        "mData": `user_name`
      },
      {
        "bSortable": true,
        "aTargets": [17],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return moment(data.project_task_actual_manpower_added_on).format('DD-MM-YYYY');
        }
      },
      {
        "bSortable": false,
        "aTargets": [18],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return '<a href="#" id="print">Print</a>';
        }
      },
      {
        "bSortable": false,
        "aTargets": [19],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return '<input type="checkbox" id="cb_item_" >';
        }
      },
    ],
  });
  new FixedColumns(table, {
    "iLeftColumns": 3,
    "iRightColumns": 2,
    "sRightWidth": 'relative',
    "iRightWidth": 15,
    "sLeftWidth": 'relative',
    "iLeftWidth": 26,
    "fnDrawCallback": function() {
      attachClickEvent();
    }
  });

  function attachClickEvent() {
    tableData = table.fnGetData();
    $('table.dataTable.DTFC_Cloned a, table.dataTable.DTFC_Cloned input,table.dataTable.DTFC_Cloned button').on('click', function(event) {
      tableData = table.fnGetData();
      var rowIndex = $(event.target).closest('tr').attr('data-row-index');
      var rowData = tableData[rowIndex];
      // rowData['project_task_actual_manpower_agency']=project_task_actual_manpower_agency;
      rowIndex = rowIndex ? parseInt(rowIndex) : -1;
      if (event.target.tagName == 'A' && event.target.id == 'print') {
        go_to_manpower_print(rowData['project_task_actual_manpower_id']);
      } else if (event.target.tagName == 'INPUT' && event.target.id == 'cb_item_') {
        var index = _.findIndex(manpower, rowData);
        index == -1 ? manpower.push(rowData) : manpower.splice(index, 1);
      }
    });
  }
});

function selectAll() {
  $(":checkbox").prop("checked", true);
  manpower = tableData;
}

function deSelectAll() {
  $(":checkbox").prop("checked", false);
  manpower = [];
}

function submit_values() {
  if (manpower.length > 0) {
    $.ajax({
      url: 'ajax/project_select_payment.php',
      type: 'POST',
      data: {
        val: manpower,
        vendor_id: $('#search_vendor').val()
      },
      dataType: 'json',
      success: function(response) {
        if (response["status"] == 0) {
          var form = document.createElement("form");
          form.setAttribute("method", "get");
          form.setAttribute("action", "project_add_bill_no.php");
          form.setAttribute("target", "_blank");
          var hiddenField1 = document.createElement("input");
          hiddenField1.setAttribute("type", "hidden");
          hiddenField1.setAttribute("name", "manpower_id");
          hiddenField1.setAttribute("value", response.id);
          form.appendChild(hiddenField1);
          document.body.appendChild(form);
          form.submit();
        }
      }
    });
  }
}

// get hidden values saved from filters
function getVars(key) {
  var element = document.getElementById(key);
  if (element) {
    return element.value;
  } else {
    return '';
  }
}

// set search filters' as hidden value
function setVars(elements) {

  for (var element in elements) {
    if (element && elements[element].length) {
      var hiddenField1 = document.createElement("input");
      hiddenField1.setAttribute("type", "hidden");
      hiddenField1.setAttribute("id", element);
      hiddenField1.setAttribute("value", elements[element]);
      document.body.appendChild(hiddenField1);
    }
  }
}

function go_to_manpower_print(manpower_id) {
  var form = document.createElement("form");
  form.setAttribute("method", "get");
  form.setAttribute("action", "project_labour_report_print.php");
  form.setAttribute("target", "_blank");
  form.setAttribute("data-title", "Labour Report");
  var hiddenField1 = document.createElement("input");
  hiddenField1.setAttribute("type", "hidden");
  hiddenField1.setAttribute("name", "manpower_id");
  hiddenField1.setAttribute("value", manpower_id);

  form.appendChild(hiddenField1);

  document.body.appendChild(form);
  form.submit();
}