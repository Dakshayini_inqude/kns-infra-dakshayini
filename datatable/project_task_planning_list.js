//globalization variable for data rendaring
var table;
//data render function
function redrawTable() {
  table.fnDraw();
}

$(document).ready(function() {
  var columnsMapping = {
    '8': 'planned_mp',
    '9': 'planned_mc',
    '10': 'planned_cw',

    '11': 'total_mp_cost',
    '12': 'total_mc_cost',
    '13': 'total_actual_cw_cost',
  };

  function createToolTip(str, length) {
    var substr = (str.length <= length) ? str : str.substr(0, length) + '...';
    return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
  }

  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    fixedColumns: {
      leftColumns: 4,
      rightColumns: 1
    },
    scrollX: true,
    ajax: 'datatable/project_task_planning_datatable.php',
    fnRowCallback: function(row, full, index) {
      if ($(row).attr('id')) {
        return false;
      } else {
        $(row).attr('id', 'row_' + index);
        // get the planned
        $.ajax({
          url: 'ajax/project_get_budget_planned_data.php',
          data: "task_id=" + full['project_task_planning_task_id'] + "&road_id=" + full['project_task_planning_no_of_roads'],
          dataType: 'json',
          success: function(planned_response) {
            $.each(planned_response, function(key, value) {
              $('span.' + key + '_' + index).html(value);
            });

            // get the actuals
            $.ajax({
              url: 'ajax/project_get_actual_data.php',
              data: "task_id=" + full['project_task_planning_task_id'] + "&road_id=" + full['project_task_planning_no_of_roads'],
              dataType: 'json',
              success: function(actual_response) {
                $.each(actual_response, function(key, value) {
                  // first print the actual
                  $('span.' + key + '_' + index).html(value);
                })
              }
            });

            //get total measurment
            $.ajax({
              url: 'ajax/project_get_actual_total_measurment.php',
              data: "task_id=" + full['project_task_planning_task_id'] + "&road_id=" + full['project_task_planning_no_of_roads'],
              dataType: 'json',
              success: function(actual_response) {
                // first print the actual
                $('span#total' + index).html(actual_response);
              }
            });
          }
        });
      }
    },
    fnServerParams: function(aoData) {
      aoData.project_id = $('#project_id').val();
      aoData.process_id = $('#process_id').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    scrollY: 600,
    scrollCollapse: true,
    fixedHeader: true,
    processing: true,
    searchDelay: 1500,
    drawCallback: function() {
      $('[data-toggle="tooltip"]').tooltip();
    },
    columns: [{
        orderable: false,
        data: function() {
          return arguments[3].row + 1;
        }
      },
      {
        orderable: true,
        data: 'project_master_name'
      },
      {
        orderable: false,
        data: function(data) {
          return createToolTip(data.project_process_master_name, 20);
        },
      },
      {
        orderable: false,
        data: function(data) {
          return createToolTip(data.project_task_master_name, 20);
        },
      },
      {
        orderable: false,
        data: function(data, type, full) {
          if (data['project_task_planning_no_of_roads'] == 'No Roads') {
            return 'No Roads';
          }
          return data['project_site_location_mapping_master_name'];
        }
      },
      {
        orderable: false,
        data: `uom_name`
      },
      {
        orderable: false,
        data: `project_task_planning_measurment`
      },
      {
        orderable: false,
        data: function() {
          return '<span id="total' + arguments[3].row + '">loading..</span>';
        }
      },
    ],
    columnDefs: [{
        orderable: false,
        targets: [7, 8, 9, 10, 11, 12, 13],
        data: function(data, type, full, meta) {
          var classname = columnsMapping[meta.col] + '_' + meta.row;
          return '<span class="' + classname + '">loading..</span>';
        }
      },
      {
        orderable: false,
        targets: [14],
        data: function(data) {
          return createToolTip(data.user_name, 7);
        }
      },
      {
        orderable: false,
        targets: [15],
        data: function(data) {
          return createToolTip(data.project_task_planning_added_on, 7);
        }
      },
      {
        orderable: false,
        targets: [16],
        data: 'project_task_planning_total_days'

      },
      {
        orderable: false,
        targets: [17],
        data: `project_task_planning_start_date`
      },
      {
        orderable: false,
        targets: [18],
        data: `project_task_planning_end_date`

      },
      {
        orderable: false,
        targets: [19],
        data: function(data) {
          return '<a href="project_upload_documents.php?task_id=' + data.project_task_planning_task_id + '" target="_blank"><button type="button" class="btn btn-primary btn-xs">Plan</button></a>';
        }
      },
      {
        orderable: false,
        targets: [20],
        data: function(data) {
          return '<button type="button" class="btn btn-primary btn-xs" onclick="update_actual(' + arguments[3].row + ')">Actual</button>';
        }
      },
      {
        orderable: false,
        targets: [21],
        data: function(data) {
          return '<button type="button" class="btn btn-primary btn-xs" onclick="update_pause(' + arguments[3].row + ')">Pause</button>';
        }
      }
    ]
  });
});

function update_pause(row_id) {
  var rowData = table.fnGetData()[row_id];
  var road_id = rowData['project_task_planning_no_of_roads'];
  project_process_task_id = rowData['project_task_planning_task_id'];
  project_id = rowData['project_task_planning_project_id'];
  location_id = rowData['project_task_planning_no_of_roads']

  if ((road_id != -1) && (moment(rowData['project_task_planning_end_date']).format()) > moment().format()) {
    var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_upload_documents.php");
    form.setAttribute("target", "blank");

    var hiddenField1 = document.createElement("input");
    hiddenField1.setAttribute("type", "hidden");
    hiddenField1.setAttribute("name", "task_id");
    hiddenField1.setAttribute("value", project_process_task_id);

    var hiddenField2 = document.createElement("input");
    hiddenField2.setAttribute("type", "hidden");
    hiddenField2.setAttribute("name", "road_id");
    hiddenField2.setAttribute("value", location_id);

    var hiddenField3 = document.createElement("input");
    hiddenField3.setAttribute("type", "hidden");
    hiddenField3.setAttribute("name", "project_id");
    hiddenField3.setAttribute("value", project_id);

    form.appendChild(hiddenField1);
    form.appendChild(hiddenField2);
    form.appendChild(hiddenField3);

    document.body.appendChild(form);
    form.submit();
    $('#myModal1').modal('hide');
  } else {
    showMessage('Update Plan End Date');
  }
}

function update_actual(row_id) {
  var rowData = table.fnGetData()[row_id];
  var total_plan_buget = parseInt($('.planned_mp_' + row_id).html()) + parseInt($('.planned_mc_' + row_id).html()) + parseInt($('.planned_cw_' + row_id).html());
  var total_actual_buget = parseInt($('.total_mp_cost_' + row_id).html()) + parseInt($('.total_mc_cost_' + row_id).html()) + parseInt($('.total_actual_cw_cost_' + row_id).html());

  // date condition
  if ((moment(rowData['project_task_planning_end_date']).format()) < moment().format() && total_actual_buget < total_plan_buget) {
    showMessage('Update Plan End Date And Actual Buget Plan');
  } else if ((moment(rowData['project_task_planning_end_date']).format()) < moment().format()) {
    showMessage('Update Plan End Date');
  } else if (total_actual_buget < total_plan_buget) {
    showMessage('Update Actual Buget Plan');
  } else if (total_plan_buget == 0) {
    showMessage('Update Plan Buget');
  } else {
    $('#row_id').val(row_id);
    $('#myModal1').modal('show');
  }
}

function showMessage(alert) {
  $('#myModal2').modal('show');
  $('#modalContent').show().html(alert);
}

function go_to_project_task_actual() {
  var row_id = parseInt($('#row_id').val());
  var data = table.fnGetData()[row_id];
  project_id = data.project_task_planning_project_id;
  project_process_task_id = data.project_task_planning_task_id;
  location_id = data.project_task_planning_no_of_roads;

  if (parseInt($('.planned_mp_' + row_id).html()) > (parseInt($('.total_mp_cost_' + row_id).html()))) {
    var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_task_actual_manpower.php");
    form.setAttribute("target", "blank");

    var hiddenField1 = document.createElement("input");
    hiddenField1.setAttribute("type", "hidden");
    hiddenField1.setAttribute("name", "project_process_task_id");
    hiddenField1.setAttribute("value", project_process_task_id);

    var hiddenField2 = document.createElement("input");
    hiddenField2.setAttribute("type", "hidden");
    hiddenField2.setAttribute("name", "project_id");
    hiddenField2.setAttribute("value", project_id);

    var hiddenField3 = document.createElement("input");
    hiddenField3.setAttribute("type", "hidden");
    hiddenField3.setAttribute("name", "location_id");
    hiddenField3.setAttribute("value", location_id);

    form.appendChild(hiddenField1);
    form.appendChild(hiddenField2);
    form.appendChild(hiddenField3);

    document.body.appendChild(form);
    form.submit();
    $('#myModal1').modal('hide');
  } else {
    showMessage('Update Plan Man Power Budget');
  }
}

function go_to_project_task_actual_machine_planning() {
  var row_id = parseInt($('#row_id').val());
  var data = table.fnGetData()[row_id];

  project_id = data.project_task_planning_project_id;
  project_process_task_id = data.project_task_planning_task_id;
  location_id = data.project_task_planning_no_of_roads;

  if (parseInt($('.planned_mc_' + row_id).html()) > (parseInt($('.total_mc_cost_' + row_id).html()))) {
    var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_task_actual_machine_plan.php");
    form.setAttribute("target", "blank");

    var hiddenField1 = document.createElement("input");
    hiddenField1.setAttribute("type", "hidden");
    hiddenField1.setAttribute("name", "project_process_task_id");
    hiddenField1.setAttribute("value", project_process_task_id);

    var hiddenField2 = document.createElement("input");
    hiddenField2.setAttribute("type", "hidden");
    hiddenField2.setAttribute("name", "project_id");
    hiddenField2.setAttribute("value", project_id);

    var hiddenField3 = document.createElement("input");
    hiddenField3.setAttribute("type", "hidden");
    hiddenField3.setAttribute("name", "location_id");
    hiddenField3.setAttribute("value", location_id);

    form.appendChild(hiddenField1);
    form.appendChild(hiddenField2);
    form.appendChild(hiddenField3);

    document.body.appendChild(form);
    form.submit();
    $('#myModal1').modal('hide');
  } else {
    showMessage('Update Plan Machine Budget');
  }
}

function go_to_project_task_boq_actuals() {
  var row_id = parseInt($('#row_id').val());
  var data = table.fnGetData()[row_id];

  project_id = data.project_task_planning_project_id;
  project_process_task_id = data.project_task_planning_task_id;
  location_id = data.project_task_planning_no_of_roads;

  if (parseInt($('.planned_cw_' + row_id).html()) > (parseInt($('.total_actual_cw_cost_' + row_id).html()))) {
    var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_add_task_boq_actuals.php");
    form.setAttribute("target", "blank");

    var hiddenField1 = document.createElement("input");
    hiddenField1.setAttribute("type", "hidden");
    hiddenField1.setAttribute("name", "project_process_task_id");
    hiddenField1.setAttribute("value", project_process_task_id);

    var hiddenField2 = document.createElement("input");
    hiddenField2.setAttribute("type", "hidden");
    hiddenField2.setAttribute("name", "project_id");
    hiddenField2.setAttribute("value", project_id);

    var hiddenField3 = document.createElement("input");
    hiddenField3.setAttribute("type", "hidden");
    hiddenField3.setAttribute("name", "location_id");
    hiddenField3.setAttribute("value", location_id);

    form.appendChild(hiddenField1);
    form.appendChild(hiddenField2);
    form.appendChild(hiddenField3);

    document.body.appendChild(form);
    form.submit();
    $('#myModal1').modal('hide');
  } else {
    showMessage('Update Plan Contract Budget');
  }
}
