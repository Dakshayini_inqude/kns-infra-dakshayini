<?php

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */

	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
  $aColumns = array(
    'remarks_list_id',
    'remarks_list_process_name',
    'project_task_master_name',
    'remarks_list_manpower',
    'remarks_list_machine',
    'remarks_list_contract',
    'remarks_list_material',
    'remarks_list_remarks',
    'user_name',
    'added_on',
    'remarks_list_status'
  );

   /* Indexed column (used for fast and accurate table cardinality) */
   $sIndexColumn = "remarks_list_id";

   /* DB table to use */
   $sTable = $_GET['table'];


	/* Database connection information */
	$gaSql['user']       = "root";
	$gaSql['password']   = "";
	$gaSql['db']         = "kns_erp_live";
	$gaSql['server']     = "localhost";


	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
	 * no need to edit below this line
	 */

	/*
	 * Local functions
	 */
	function fatal_error ( $sErrorMessage = '' )
	{
		header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
		die( $sErrorMessage );
	}

	/*
	 * MySQL connection
	 */
  $con  =  mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'], $gaSql['db'] );

   if (mysqli_connect_errno())
   {
     echo "Failed to connect to MySQL: " . mysqli_connect_error();
   }

   mysqli_select_db($con, $gaSql['db']);

	// if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'], $gaSql['db']  ) )
	// {
	// 	fatal_error( 'Could not open connection to server' );
	// }
  //
	// if ( ! mysqli_select_db( $gaSql['db'], $gaSql['link'] ) )
	// {
	// 	fatal_error( 'Could not select database ' );
	// }


	/*
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
			intval( $_GET['iDisplayLength'] );
	}


	/*
	 * Ordering
	 */
	$sOrder = "";
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY";
		for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
		{
			if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
			{
				$sOrder .= "`".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."` ".
					($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
			}
		}

		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";
		}
	}


	/*
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	$sWhere = "";
	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
	{
		$sWhere = "WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "`".$aColumns[$i]."` LIKE '%".mysqli_real_escape_string( $con, $_GET['sSearch'] )."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		 $sWhere .= ") AND `remarks_list_status` = 'Pending'";
	}

	/* Individual column filtering */
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
		{
			if ( $sWhere == "" )
			{
				$sWhere = "WHERE ";
			}
			else
			{
				$sWhere .= " AND ";
			}
			$sWhere .= "`".$aColumns[$i]."` LIKE '%".mysqli_real_escape_string($con, $_GET['sSearch_'.$i])."%' ";
		}
	}

    if($sWhere == "") {
      $sWhere = "WHERE `remarks_list_status` = 'Completed'";
    }

    if(isset($_GET['search_project']) && $_GET['search_project'] != '') {
      $sWhere .= " AND `remarks_list_project_id` = ". $_GET['search_project'];
    }

    if(isset($_GET['search_process']) && $_GET['search_process'] != '') {
      $sWhere .= " AND `process_master_id` = ". $_GET['search_process'];
    }

    if(isset($_GET['search_task']) && $_GET['search_task'] != '') {
      $sWhere .= " AND `remarks_list_task_id` = ". $_GET['search_task'];
    }
	/*
	 * SQL queries
	 * Get data to display
	 */
	$sQuery = "
		SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
		FROM   $sTable
		$sWhere
    $sOrder
		$sLimit
		";
	$rResult = mysqli_query( $con, $sQuery ) or fatal_error( 'MySQL Error: '  );

	/* Data set length after filtering */
	$sQuery = "
		SELECT FOUND_ROWS()
	";
	$rResultFilterTotal = mysqli_query( $con, $sQuery) or fatal_error( 'MySQL Error: ' . 'mysqli_errno' );
	$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal[0];

	/* Total data set length */
	$sQuery = "
		SELECT COUNT(`".$sIndexColumn."`)
		FROM   $sTable
	";
	$rResultTotal = mysqli_query( $con, $sQuery ) or fatal_error( 'MySQL Error: ' . 'mysqli_errno' );
	$aResultTotal = mysqli_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];


	/*
	 * Output
	 */
	$output = array(
		"sEcho" => intval($_GET['sEcho']),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
	);

	while ( $aRow = mysqli_fetch_array( $rResult ) )
	{
		$row = array();
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( $aColumns[$i] == "version" )
			{
				/* Special output formatting for 'version' column */
				$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
			}
			else if ( $aColumns[$i] != ' ' )
			{
				/* General output */
				$row[$aColumns[$i]] = $aRow[ $aColumns[$i] ];
			}
		}
    $row[0] = 'index';
		$output['aaData'][] = $row;
    $output['where'] = $_GET['search_process'];
	}
	echo json_encode( $output );
?>
