$(document).ready(function() {

  var table = $('#example').dataTable({
    "sScrollX": "100%",
    "sScrollXInner": "110%",
    "bScrollCollapse": true,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": 'datatable/persona_monthly_salary_list.php',
    "iDisplayLength": 20,
    "sScrollY": "50%",
    "bInfo": true,
    "iTabIndex": 1,
    "lengthChange": false,
    dom: 'Bfrtip',
    buttons: [
      {
        extend: 'excelHtml5',
        text: '<i class="fa fa-file-excel-o"></i> Excel',
        titleAttr: 'Export to Excel',
        title: 'Monthly Salary List',
        exportOptions: { orthogonal: 'export' }
      },
    ],
    language: {
      search: "_INPUT_",
      searchPlaceholder: "Search...",
      "infoFiltered": ""
    },
    "fnServerParams": function(aoData) {
      aoData.push({
        "name": "table",
        "value": "persona_salary_list"
      });
      aoData.push({
        "name": "search_department",
        "value": getVars('search_department')
      });
      aoData.push({
        "name": "search_company",
        "value": getVars('search_company')
      });
    },
    "aaSorting": [
      // [1, 'desc']
    ],
    "fnDrawCallback": function(oSettings) {
      /* Need to redo the counters if filtered or sorted */
      if (oSettings.bSorted || oSettings.bFiltered || oSettings.iDraw > 1) {
        for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
          $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + oSettings._iDisplayStart + 1);
        }
      }
    },
    "aoColumnDefs": [{
        "bSortable": false,
        "aTargets": [0]
      },
      {
        "bSortable": false,
        "aTargets": [1],
        "mData": `employee_code`
      },
      {
        "bSortable": false,
        "aTargets": [2],
        "mData": `company_name`
      },
      {
        "bSortable": false,
        "aTargets": [3],
        "mData": 'user_name'
      },
      {
        "bSortable": false,
        "aTargets": [4],
        "mData": `department_name`
      },

      {
        "bSortable": false,
        "aTargets": [5],
        "mData": `designation_name`
      },
      {
        "bSortable": false,
        "aTargets": [6],
        "mData": `manager_name`
      },
      {
        "bSortable": false,
        "aTargets": [7],
        "mData": `user_location`
      },
      // No of days
      {
        "bSortable": false,
        "aTargets": [8],
        "mRender": function(data, type, full) {
          return parseInt(moment(getVars('end_date')).format('DD'))
        }
      },
      {
        "bSortable": false,
        "aTargets": [9, 10, 18, 19, 20, 21, 24, 26, 27, 31],
        "mRender": function(data, type, full, meta) {
          var currentCell = $("#example").DataTable().cells({
            "row": meta.row,
            "column": meta.col
          }).nodes(0);

          var loan_deduction, mobile_deduction, total_days, pli_amount, gross, pt, deduction, ctc, lop_days, lop_amount;

          $.ajax({
            url: 'ajax/get_persona_pending_loan_amount.php',
            data: "user_id=" + full['persona_salary_user_id'],
            dataType: 'json',
            success: function(response) {
              loan_deduction = response ? response : 0;
              // $(currentCell).text(response.toLocaleString('en-IN'));

              $.ajax({
                url: 'ajax/get_mobile_deduction_amount.php',
                data: "user_id=" + full['persona_salary_user_id'] + "&start_date=" + getVars('start_date') + "&end_date=" + getVars('end_date'),
                dataType: 'json',
                success: function(response) {
                  // console.log('loan deduction in diff col ', response);
                  if (response && response.length) {

                    var actual = parseInt(full['data_limit']) + parseInt(full['voice_limit']);
                    var spent = parseInt(response[0]['mobile_deduction_voice_bill']) + parseInt(response[0]['mobile_deduction_data_bill']);

                    if (spent > actual) {
                      mobile_deduction = spent - actual;
                    } else {
                      mobile_deduction = 0;
                    }
                  } else {
                    mobile_deduction = 0;
                  }

                  $.ajax({
                    url: 'ajax/get_employee_attendence.php',
                    data: "user_id=" + full['persona_salary_user_id'] + "&start_date=" + getVars('start_date') + "&end_date=" + getVars('end_date'),
                    dataType: 'json',
                    success: function(response) {
                      total_days = response ? response : 0;

                      $.ajax({
                        url: 'ajax/get_persona_pli_amount.php',
                        data: "user_id=" + full['persona_salary_user_id'] + "&start_date=" + getVars('start_date') + "&end_date=" + getVars('end_date'),
                        dataType: 'json',
                        success: function(response) {
                          pli_amount = response ? response : 0;

                          // Begin ajax response calculation

                gross = parseInt(full['basic']) + parseInt(full['hra']) + parseInt(full['conveyance_allowance']) +
                      parseInt(full['special_allowance']) + parseInt(full['educational_allowance']) +
                      parseInt(full['medical_reimbursement'] || 0) + parseInt(pli_amount);

                pt = gross >= 15000 ? 200 : 0;

                lop_days = parseInt(moment(getVars('end_date')).format('DD')) - total_days;
                lop_amount = lop_days * (gross / parseInt(moment(getVars('end_date')).format('DD')));

                deduction = lop_amount + loan_deduction + mobile_deduction + parseInt(full['pf_amount']) + parseInt(full['esi_employee_amount'] || 0) + pt;
                          // deduction = deduction + lop;
                ctc = gross + parseInt(full['statutory_bonus']) + parseInt(full['pf_amount']) + parseInt(full[['esi_company_amount']] || 0);

                          // Employee present days
                          if (meta.col == 9) {
                            $(currentCell).text(total_days);
                          }
                          // Absent days
                          else if (meta.col == 10) {
                            $(currentCell).text(parseInt(lop_days));
                          }
                          // PLI amount
                          else if (meta.col == 18) {
                            $(currentCell).text((parseInt(pli_amount)).toLocaleString('en-IN'));
                          }
                          // Gross
                          else if (meta.col == 19) {
                            $(currentCell).text(gross);
                          }
                          // Loan deduction
                          else if (meta.col == 20) {
                            $(currentCell).text(loan_deduction);
                          }
                          // Mobile deduction
                          else if (meta.col == 21) {
                            $(currentCell).text(mobile_deduction);
                          }
                          // PT
                          else if (meta.col == 24) {
                            $(currentCell).text((parseInt(pt)).toLocaleString('en-IN'));
                          }
                          // Total deduction
                          else if (meta.col == 26) {
                            $(currentCell).text((parseInt(deduction)).toLocaleString('en-IN'));
                          }
                          // NET
                          else if (meta.col == 27) {
                            $(currentCell).text((parseInt(gross - deduction)).toLocaleString('en-IN'));
                          }
                          // CTC
                          else if (meta.col == 31) {
                            $(currentCell).text(ctc).toLocaleString('en-IN');
                          }

                          // End ajax response calculation
                        }
                      });
                    }
                  });
                }
              });

            }
          });
          return 'Loading...';
        }
      },
      {
        "bSortable": false,
        "aTargets": [11],
        "mData": `basic`
      },
      {
        "bSortable": false,
        "aTargets": [12],
        "mData": `hra`
      },
      {
        "bSortable": false,
        "aTargets": [13],
        "mData": `conveyance_allowance`
      },
      {
        "bSortable": false,
        "aTargets": [14],
        "mData": `medical_reimbursement`
      },

      {
        "bSortable": false,
        "aTargets": [15],
        "mData": `educational_allowance`
      },
      {
        "bSortable": false,
        "aTargets": [16],
        "mData": `special_allowance`
      },
      // Arrears
      {
        "bSortable": false,
        "aTargets": [17],
        "mRender": function(data, type, ful) {
          return '0';
        }
      },
      {
        "bSortable": false,
        "aTargets": [22],
        "mData": `pf_amount`
      },
      {
        "bSortable": false,
        "aTargets": [23],
        "mData": `esi_employee_amount`
      },
      // TDS
      {
        "bSortable": false,
        "aTargets": [25],
        "mRender": function(data, type, full) {
          return '0';
        }
      },
      {
        "bSortable": false,
        "aTargets": [28],
        "mData": `statutory_bonus`
      },
      // Employer PF Amount
      {
        "bSortable": false,
        "aTargets": [29],
        "mData": `pf_amount`
      },
      // Company ESI amount
      {
        "bSortable": false,
        "aTargets": [30],
        "mData": `esi_employee_amount`
      },
      {
        "bSortable": false,
        "aTargets": [32, 33, 34, 35, 36],
        "mRender": function(data, type, full, meta) {

          if (meta.col == 32) {
            return full['bank_name'];
          } else if (meta.col == 33) {
            return full['bank_account_number'];
          } else if (meta.col == 34) {
            return full['ifsc_code'];
          }
          else if (meta.col == 35) {
            return full['bank_branch'];
          } else if (meta.col == 36) {
            return full['bank_city'];
          }
        }
      },
      // Mobile deduction
      // {
      //   "bSortable": false,
      //   "aTargets": [27],
      //   "mRender": function(data, type, full, meta) {
      //     var currentCell = $("#example").DataTable().cells({
      //       "row": meta.row,
      //       "column": meta.col
      //     }).nodes(0);
      //
      //     $.ajax({
      //       url: 'ajax/get_mobile_deduction_amount.php',
      //       data: "user_id=" + full['persona_salary_user_id'] + "&start_date=" + getVars('start_date') + "&end_date=" + getVars('end_date'),
      //       dataType: 'json',
      //       success: function(response) {
      //         if (response && response.length) {
      //
      //           var actual = parseInt(full['data_limit']) + parseInt(full['voice_limit']);
      //           var spent = parseInt(response[0]['mobile_deduction_voice_bill']) + parseInt(response[0]['mobile_deduction_data_bill']);
      //
      //           if(spent > actual) {
      //             $(currentCell).text(spent- actual);
      //           } else {
      //             $(currentCell).text(0);
      //           }
      //         } else {
      //           $(currentCell).text(0);
      //         }
      //       }
      //     });
      //     return 'Loading...';
      //   }
      // },
      // Loan deduction
      // Total deduction
      // {
      //   "bSortable": false,
      //   "aTargets": [30],
      //   "mRender": function(data, type, full) {
      //     return ((parseInt(full['pf_amount']) / 12) + parseInt(full['esi_employee_amount'] || 0) + 200).toLocaleString('en-IN');
      //   }
      // },
      // // NET Payable
      // {
      //   "bSortable": false,
      //   "aTargets": [31],
      //   "mRender": function(data, type, full) {
      //     var gross = parseInt(full['basic']) + parseInt(full['hra']) + parseInt(full['conveyance_allowance']) + parseInt(full['special_allowance']) +
      //       parseInt(full['educational_allowance']) + parseInt(full['medical_reimbursement'] || 0);
      //     var deduction = (parseInt(full['pf_amount']) + parseInt(full['esi_employee_amount'] || 0) + 200);
      //     return parseInt(((gross * 12) - deduction) / 12).toLocaleString('en-IN');
      //   }
      // },
    ]
  });

  $('#example tbody').on('click', 'tr', function(event) {

    if (event.target.tagName == 'A' && event.target.id == 'edit') {

      var rowData = table.api().row(this).data();
      go_to_persona_edit_list(rowData['persona_salary_user_id']);
    }
  });
});

// get hidden values saved from filters
// function getVars(key) {
//   var element = document.getElementById(key);
//   console.log('getVars ',  key, element);
//   if (element) {
//     return element.value;
//   } else {
//     return '';
//   }
// }

function getVars(key) {
  var element = document.getElementById(key);
  if (element) {
    return element.value;
  } else {
    return '';
  }
}

// set search filters' as hidden value
function setVars(elements) {

  for (var element in elements) {
    if (element && elements[element].length) {
      var hiddenField1 = document.createElement("input");
      hiddenField1.setAttribute("type", "hidden");
      hiddenField1.setAttribute("id", element);
      hiddenField1.setAttribute("value", elements[element]);
      document.body.appendChild(hiddenField1);
    }
  }
}

function go_to_persona_edit_list(user_id) {
  var form = document.createElement("form");
  form.setAttribute("method", "get");
  form.setAttribute("action", "edit_persona_salary.php");
  form.setAttribute("target", "blank")

  var hiddenField1 = document.createElement("input");
  hiddenField1.setAttribute("type", "hidden");
  hiddenField1.setAttribute("name", "persona_salary_user_id");
  hiddenField1.setAttribute("value", user_id);

  form.appendChild(hiddenField1);

  document.body.appendChild(form);
  form.submit();
}
