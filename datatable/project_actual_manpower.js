$(document).ready(function() {

  var table = $('#example').dataTable({
    "sScrollX": "100%",
    "sScrollXInner": "110%",
    "sPaginationType": "full_numbers",
    "bScrollCollapse": true,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": 'datatable/project_actual_manpower_datatable.php',
    "iDisplayLength": 10,
    "bStateSave": true,
    // "sScrollY": "150%",
    "bInfo": true,
    "iTabIndex": 1,
    "lengthChange": false,
    language: {
      search: "_INPUT_",
      searchPlaceholder: "Search...",
      "infoFiltered": ""
    },
    "fnServerParams": function(aoData) {
      aoData.push({
        "name": "table",
        "value": "project_task_actual_manpower_list"
      });
      aoData.push({
        "name": "search_project",
        "value": getVars('search_project')
      });
      aoData.push({
        "name": "search_vendor",
        "value": getVars('search_vendor')
      });
      aoData.push({
        "name": "start_date",
        "value": getVars('start_date')
      });
      aoData.push({
        "name": "end_date",
        "value": getVars('end_date')
      });
    },
    "aaSorting": [
      [17, 'desc']
    ],
    "fnCreatedRow": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      $(nRow).attr('data-row-index', iDisplayIndex);
    },
    /* Need to redo the counters if filtered or sorted */
    "fnDrawCallback": function(oSettings) {
      /* Need to redo the counters if filtered or sorted */
      if (oSettings.bSorted || oSettings.bFiltered || oSettings.iDraw > 1) {
        for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
          this.fnUpdate(i + oSettings._iDisplayStart + 1, oSettings.aiDisplay[i], 0, false, false);
        }
      }
      // if (oSettings.bSorted || oSettings.bFiltered || oSettings.iDraw > 1) {
      //   for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
      //     $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + oSettings._iDisplayStart + 1);
      //   }
      // }
    },
    "aoColumnDefs": [{
        "bSortable": false,
        "aTargets": [0]
      },
      {
        "bSortable": false,
        "aTargets": [1],
        "mData": `project_manpower_agency_name`
      },
      {
        "bSortable": false,
        "aTargets": [2],
        "mData": `project_master_name`
      },
      {
        "bSortable": false,
        "aTargets": [3],
        "mData": 'project_process_master_name'
      },
      {
        "bSortable": false,
        "aTargets": [4],
        "mData": `project_task_master_name`
      },
      {
        "bSortable": false,
        "aTargets": [5],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          if (data.project_task_actual_manpower_road_id == 'No Roads') {
            return 'No Roads';
          }
          return data.project_site_location_mapping_master_name;
        }
      },
      {
        "bSortable": false,
        "aTargets": [6],
        "mData": `project_task_actual_manpower_work_type`
      },
      {
        "bSortable": false,
        "aTargets": [7],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }

          if (data.project_task_actual_manpower_work_type === 'Regular') {
            return data.project_task_actual_manpower_completion_percentage;
          } else {
            return data.project_task_actual_manpower_rework_completion;
          }
        }
      },
      {
        "bSortable": false,
        "aTargets": [8],
        "mData": `project_task_actual_manpower_completed_msmrt`
      },
      {
        "bSortable": false,
        "aTargets": [9],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return moment(data.project_task_actual_manpower_date).format('DD-MM-YYYY');
        }
      },
      {
        "bSortable": false,
        "aTargets": [10],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return ' Hours ' + data.project_task_actual_manpower_no_of_men + '<br/>' + ' Rate : ' + data.project_task_actual_manpower_men_rate + ' <br/> ' + ' Men: ' + data.project_task_actual_manpower_no_of_men / 8;
        }
      },
      {
        "bSortable": false,
        "aTargets": [11],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return ' Hours: ' + data.project_task_actual_manpower_no_of_women + '<br/>' + ' Rate : ' + data.project_task_actual_manpower_women_rate + '<br/>' + ' Women: ' + data.project_task_actual_manpower_no_of_women / 8;
        }
      },
      {
        "bSortable": false,
        "aTargets": [12],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return ' Hours: ' + data.project_task_actual_manpower_no_of_mason + '<br/>' + ' Rate : ' + data.project_task_actual_manpower_mason_rate + ' <br/> ' + ' Mason: ' + data.project_task_actual_manpower_no_of_mason / 8;
        }
      },
      {
        // No of People
        "bSortable": false,
        "aTargets": [13],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return parseFloat(data.project_task_actual_manpower_no_of_men / 8) + parseFloat(data.project_task_actual_manpower_no_of_women / 8) + parseFloat(data.project_task_actual_manpower_no_of_mason / 8);
        }
      },
      {
        // Amount
        "bSortable": false,
        "aTargets": [14],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return (parseFloat(data.project_task_actual_manpower_no_of_men) * parseFloat(data.project_task_actual_manpower_men_rate)) + (parseFloat(data.project_task_actual_manpower_no_of_women) * parseFloat(data.project_task_actual_manpower_women_rate)) + (parseFloat(data.project_task_actual_manpower_no_of_mason) * parseFloat(data.project_task_actual_manpower_mason_rate));
        }
      },
      {
        // Remarks
        "bSortable": false,
        "aTargets": [15],
        "mData": `project_task_actual_manpower_remarks`
      },
      {
        // Remarks
        "bSortable": false,
        "aTargets": [16],
        "mData": `user_name`
      },
      {
        "bSortable": true,
        "aTargets": [17],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return moment(data.project_task_actual_manpower_added_on).format('DD-MM-YYYY');
        }
      },
      {
        "bSortable": false,
        "aTargets": [18],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return '<a href="#" id="edit">Edit</a>';
        }
      },
      {
        "bSortable": false,
        "aTargets": [19],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return '<a href="#" id="delete">Delete</a>';
        }
      },
      {
        "bSortable": true,
        "aTargets": [20],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          var percentage;
          if (data.project_task_actual_manpower_work_type == 'Regular') {
            percentage = parseFloat(data.project_task_actual_manpower_completion_percentage);
          } else {
            percentage = parseFloat(data.project_task_actual_manpower_rework_completion);
          }
          if (data.project_task_actual_manpower_check_status == 0 && percentage > 0) {
            return '<a href="#" id="ok">OK</a>';
          } else if (data.project_task_actual_manpower_check_status == 1 && data.project_task_actual_manpower_display_status == 'not approved') {
            return '<a href="#" id="approve">Approve</a>';
          } else if (percentage == 0) {
            return 'Enter	completed msrmnt'
          }
        }
      },
    ],
  });
  new FixedColumns(table, {
    "iRightColumns": 3,
    "sRightWidth": 'relative',
    "iRightWidth": 26,
    "iLeftColumns": 3,
    "sLeftWidth": 'relative',
    "iLeftWidth": 26,
    "fnDrawCallback": function() {
      attachClickEvent();
    }
  });


  function attachClickEvent() {
    $('table.dataTable.DTFC_Cloned a, table.dataTable.DTFC_Cloned input').on('click', function(event) {
      var rowIndex = $(event.target).closest('tr').attr('data-row-index');
      rowIndex = rowIndex ? parseInt(rowIndex) : -1;
      var tableData = table.fnGetData();
      var rowData = tableData[rowIndex];
      if (event.target.tagName == 'A' && event.target.id == 'edit') {
        go_to_project_edit_task_actual_manpower(rowData['project_task_actual_manpower_id'], rowData['project_task_actual_manpower_task_id'], rowData['project_task_actual_manpower_road_id']);
      } else if (event.target.tagName == 'A' && event.target.id == 'delete') {
        project_delete_task_actual_manpower(table, rowData['project_task_actual_manpower_id']);
      } else if (event.target.tagName == 'A' && event.target.id == 'ok') {
        project_check_task_actual_manpower(table, rowData['project_task_actual_manpower_id'], rowData['project_task_actual_manpower_task_id']);
      } else if (event.target.tagName == 'A' && event.target.id == 'approve') {
        project_approve_task_actual_manpower(table, rowData['project_task_actual_manpower_id'], rowData['project_task_actual_manpower_task_id']);
      }
    });
  }
});

// get hidden values saved from filters
function getVars(key) {
  var element = document.getElementById(key);
  if (element) {
    return element.value;
  } else {
    return '';
  }
}

// set search filters' as hidden value
function setVars(elements) {

  for (var element in elements) {
    if (element && elements[element].length) {
      var hiddenField1 = document.createElement("input");
      hiddenField1.setAttribute("type", "hidden");
      hiddenField1.setAttribute("id", element);
      hiddenField1.setAttribute("value", elements[element]);
      document.body.appendChild(hiddenField1);
    }
  }
}

function project_approve_task_actual_manpower(table, man_power_id, task_id) {
  var ok = confirm("Are you sure you want to Approve?");

  if (ok) {

    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        alert(xmlhttp.responseText);
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          table.fnDraw(false);
        }
      }
    }

    xmlhttp.open("POST", "project_approve_man_power.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("man_power_id=" + man_power_id + "&task_id=" + task_id + "&action=approved");
  }
}

function project_delete_task_actual_manpower(table, man_power_id) {
  var ok = confirm("Are you sure you want to Delete?");

  if (ok) {

    if (window.XMLHttpRequest) {
      xmlhttp = new XMLHttpRequest();
    } else {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          table.fnDraw(false);
        }
      }
    }

    xmlhttp.open("POST", "project_delete_task_actual_manpower.php");
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("man_power_id=" + man_power_id + "&action=0");
  }
}

function project_check_task_actual_manpower(table, man_power_id, task_id) {
  var ok = confirm("Are you sure you want to Ok?")
  if (ok) {

    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          table.fnDraw(false);
        }
      }
    }
    xmlhttp.open("POST", "project_check_man_power.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("man_power_id=" + man_power_id + "&task_id=" + task_id + "&action=approved");
  }
}

function go_to_project_edit_task_actual_manpower(man_power_id, task_id, road_id) {
  var form = document.createElement("form");
  form.setAttribute("method", "get");
  form.setAttribute("action", "project_edit_task_actual_manpower.php");
  form.setAttribute("target", "_blank");

  var hiddenField1 = document.createElement("input");
  hiddenField1.setAttribute("type", "hidden");
  hiddenField1.setAttribute("name", "man_power_id");
  hiddenField1.setAttribute("value", man_power_id);

  var hiddenField2 = document.createElement("input");
  hiddenField2.setAttribute("type", "hidden");
  hiddenField2.setAttribute("name", "task_id");
  hiddenField2.setAttribute("value", task_id);

  var hiddenField3 = document.createElement("input");
  hiddenField3.setAttribute("type", "hidden");
  hiddenField3.setAttribute("name", "road_id");
  hiddenField3.setAttribute("value", road_id);

  form.appendChild(hiddenField1);
  form.appendChild(hiddenField2);
  form.appendChild(hiddenField3);

  document.body.appendChild(form);
  form.submit();
}