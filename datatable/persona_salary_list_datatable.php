<?php

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */

	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
  $aColumns = array(
    'persona_salary_department_id',
    'persona_salary_user_id',
    'designation_name',
    'department_name',
    'user_name',
    'manager_name',
    'user_role',
    'user_email',
    'user_location',
    'basic',
    'company_name',
    'hra',
    'conveyance_allowance',
    'special_allowance',
    'educational_allowance',
    'medical_reimbursement',
    'pf_amount',
    'statutory_bonus',
    'gratuity',
    'lta',
    'pli',
    'bank_name',
    'bank_account_number',
    'ifsc_code',
    'bank_branch',
    'bank_city',
    'esi_employee_amount',
    'esi_company_amount',
    'employee_code'
  );

   /* Indexed column (used for fast and accurate table cardinality) */
   $sIndexColumn = "persona_salary_user_id";

   /* DB table to use */
   $sTable = $_GET['table'];


	/* Database connection information */
	$gaSql['user']       = "root";
	$gaSql['password']   = "";
	$gaSql['db']         = "kns_erp_live";
	$gaSql['server']     = "localhost";


	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
	 * no need to edit below this line
	 */

	/*
	 * Local functions
	 */
	function fatal_error ( $sErrorMessage = '' )
	{
		header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
		die( $sErrorMessage );
	}

	/*
	 * MySQL connection
	 */
  $con  =  mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'], $gaSql['db'] );

   if (mysqli_connect_errno())
   {
     echo "Failed to connect to MySQL: " . mysqli_connect_error();
   }

   mysqli_select_db($con, $gaSql['db']);

	// if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'], $gaSql['db']  ) )
	// {
	// 	fatal_error( 'Could not open connection to server' );
	// }
  //
	// if ( ! mysqli_select_db( $gaSql['db'], $gaSql['link'] ) )
	// {
	// 	fatal_error( 'Could not select database ' );
	// }


	/*
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
			intval( $_GET['iDisplayLength'] );
	}

	/*
	 * Ordering
	 */
	$sOrder = "";
	// if ( isset( $_GET['iSortCol_0'] ) )
	// {
	// 	$sOrder = "ORDER BY";
	// 	for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
	// 	{
	// 		if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
	// 		{
	// 			$sOrder .= "`".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."` ".
	// 				($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
	// 		}
	// 	}
  //
	// 	$sOrder = substr_replace( $sOrder, "", -2 );
	// 	if ( $sOrder == "ORDER BY" )
	// 	{
	// 		$sOrder = "";
	// 	}
	// }


	/*
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	$sWhere = "";
	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
	{
		$sWhere = "WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "`".$aColumns[$i]."` LIKE '%".mysqli_real_escape_string( $con, $_GET['sSearch'] )."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= " )";
	}

	/* Individual column filtering */
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
		{
			if ( $sWhere == "" )
			{
				$sWhere = "WHERE ";
			}
			else
			{
				$sWhere .= " AND ";
			}
			$sWhere .= "`".$aColumns[$i]."` LIKE '%".mysqli_real_escape_string($con, $_GET['sSearch_'.$i])."%' ";
		}
    // $sWhere .= " )";
	}

    // if($sWhere == "") {
    //   $sWhere = "WHERE `project_task_actual_manpower_display_status` = 'not approved' AND `project_task_actual_manpower_active` = 1";
    //   $sWhere = ""
    // }

    if(isset($_GET['search_department']) && $_GET['search_department'] != '' && $_GET['search_department'] != '0') {
      $sWhere .= "WHERE `persona_salary_department_id` = ". $_GET['search_department'];
    }

    // if(isset($_GET['search_vendor']) && $_GET['search_vendor'] != '') {
    //   $sWhere .= " AND `project_task_actual_manpower_agency` = ". $_GET['search_vendor'];
    // }
    //
    // if(isset($_GET['start_date']) && $_GET['start_date'] != '') {
    //   $sWhere .= " AND `project_task_actual_manpower_added_on` >= '". $_GET['start_date']." 00:00:00'";
    // }
    //
    // if(isset($_GET['end_date']) && $_GET['end_date'] != '') {
    //   $sWhere .= " AND `project_task_actual_manpower_added_on` <= '". $_GET['end_date']." 11:59:00'";
    // }

	/*
	 * SQL queries
	 * Get data to display
	 */
	$sQuery = "
		SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
		FROM   $sTable
		$sWhere
    $sOrder
		$sLimit
		";
	$rResult = mysqli_query( $con, $sQuery ) or fatal_error( 'MySQL Error: '.$sQuery);

	/* Data set length after filtering */
	$sQuery = "
		SELECT FOUND_ROWS()
	";
	$rResultFilterTotal = mysqli_query( $con, $sQuery) or fatal_error( 'MySQL Error: ' . 'mysqli_errno' );
	$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal[0];

	/* Total data set length */
	$sQuery = "
		SELECT COUNT(`".$sIndexColumn."`)
		FROM   $sTable
	";
	$rResultTotal = mysqli_query( $con, $sQuery ) or fatal_error( 'MySQL Error: ' . 'mysqli_errno' );
	$aResultTotal = mysqli_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];


	/*
	 * Output
	 */
	$output = array(
		"sEcho" => intval($_GET['sEcho']),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
	);

	while ( $aRow = mysqli_fetch_array( $rResult ) )
	{
		$row = array();
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( $aColumns[$i] == "version" )
			{
				/* Special output formatting for 'version' column */
				$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
			}
			else if ( $aColumns[$i] != ' ' )
			{
				/* General output */
				$row[$aColumns[$i]] = $aRow[ $aColumns[$i] ];
			}
		}
    $row[0] = 'index';
		$output['aaData'][] = $row;
    $output['where'] = $sWhere;
	}
	echo json_encode( $output );
?>
