<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 02-Jan-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	
	// Capture the form data
	if(isset($_POST["add_machine_rate_master_submit"]))
	{		
		$machine_id          = $_POST["ddl_machine_id"];
		$kns_fuel            = $_POST["kns_fuel"];
		$vendor_fuel         = $_POST["vendor_fuel"];
		$kns_bata            = $_POST["kns_bata"];
		$remarks 	         = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($machine_id != "") && ($kns_fuel != "") && ($vendor_fuel != "") && ($kns_bata != ""))
		{
			$project_machine_rate_master_iresult = i_add_project_machine_rate_master($machine_id,'',$kns_fuel,$vendor_fuel,$kns_bata,'',$remarks,$user);
			
			if($project_machine_rate_master_iresult["status"] == SUCCESS)
				
			{	
				$alert_type = 1;
			}
			
			$alert = $project_machine_rate_master_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
		
		$machine_type = $_POST['ddl_machine_type_id'];
	}
	else if(isset($_POST['ddl_machine_type_id']))
	{
		$machine_type = $_POST['ddl_machine_type_id'];
	}
	else
	{
		$machine_type = '-1';
	}
	
	// Get Project machine Master modes already added
	$project_machine_master_search_data = array("active"=>'1',"machine_type"=>$machine_type);
	$project_machine_master_list = i_get_project_machine_master($project_machine_master_search_data);
	if($project_machine_master_list['status'] == SUCCESS)
	{
		$project_machine_master_list_data = $project_machine_master_list['data'];
		
    }
     else
    {
		$alert = $project_machine_master_list["data"];
		$alert_type = 0;
	}	
	
	// Get Project machine types already added
	$project_machine_type_master_search_data = array("active"=>'1');
	$project_machine_type_master_list = i_get_project_machine_type_master($project_machine_type_master_search_data);
	if($project_machine_type_master_list['status'] == SUCCESS)
	{
		$project_machine_type_master_list_data = $project_machine_type_master_list['data'];
		
    }
     else
    {
		$alert = $project_machine_type_master_list["data"];
		$alert_type = 0;
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Master - Add Machine Rate</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Project Master - Add Machine Rate</h3><span style="float:right; padding-right:20px;"><a href="project_master_machine_rate_list.php">Machine Rate List</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Machine Rate</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_master_add_machine_rate_form" class="form-horizontal" method="post" action="project_master_add_machine_rate.php">
									<fieldset>										
										
										<div class="control-group">											
											<label class="control-label" for="ddl_machine_type_id">Machine Type*</label>
											<div class="controls">
												<select name="ddl_machine_type_id" id="ddl_machine_type_id" required onchange="this.form.submit();">
												<option value="">- - Select Machine Type - -</option>
												<?php
												for($count = 0; $count < count($project_machine_type_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_machine_type_master_list_data[$count]["project_machine_type_master_id"]; ?>" <?php if($machine_type == $project_machine_type_master_list_data[$count]["project_machine_type_master_id"]){ ?> selected <?php } ?>><?php echo $project_machine_type_master_list_data[$count]["project_machine_type_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_machine_id">Machine*</label>
											<div class="controls">
												<select name="ddl_machine_id" id="ddl_machine_id" required onchange="return get_vendor()" value="1">
												<option value="">- - Select Machine - -</option>
												<?php
												for($count = 0; $count < count($project_machine_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_machine_master_list_data[$count]["project_machine_master_id"]; ?>"><?php echo $project_machine_master_list_data[$count]["project_machine_master_name"]; ?>-<?php echo $project_machine_master_list_data[$count]["project_machine_master_id_number"]; ?></option>
												<?php
												}
												?>
												</select></br><br>Vendor : <span id="machine_vendor" style="width:10%" ></span>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="kns_fuel">Rate/hr (Fuel by KNS)*</label>
											<div class="controls">
												<input type="number" class="span6" min="0" step="0.01" name="kns_fuel" placeholder="Kns Fuel" required="required">
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="vendor_fuel">Rate/hr (Fuel by Vendor)*</label>
											<div class="controls">
												<input type="number" class="span6" min="0" step="0.01" name="vendor_fuel" placeholder="Vendor Fuel" required="required">
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="kns_bata">Bata(Rate/day)*</label>
											<div class="controls">
												<input type="number" class="span6" min="0" step="0.01" name="kns_bata" placeholder="Bata" required="required">
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_machine_rate_master_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function get_vendor()
{        
		
	var machine_id = document.getElementById("ddl_machine_id").value;
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange = function()
	{
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{			
			if(xmlhttp.responseText == "-1")
			{
			 document.getElementById("machine_vendor").innerHTML = xmlhttp.responseText;					 
			}
			else					
			{
				document.getElementById('machine_vendor').innerHTML = xmlhttp.responseText;
			}
		}
	}

	xmlhttp.open("POST", "ajax/get_machine_vendor.php");   //
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("machine_id=" + machine_id);
}
</script>

  </body>

</html>
