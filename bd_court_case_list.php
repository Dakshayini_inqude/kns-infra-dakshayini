<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: bd_file_doc_list.php
CREATED ON	: 13-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of BD documents of a file
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/$_SESSION['module'] = 'BD';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_court_case_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["file"]))
	{
		$file_id = $_GET["file"];
	}
	else
	{
		$file_id = "";
	}
	
	if(isset($_GET["type"]))
	{
		$case_type = $_GET["type"];
	}
	else
	{
		$case_type = "";
	}
	if(isset($_GET["ddl_establishment"]))
	{
		$establishment_type = $_GET["ddl_establishment"];
	}
	else
	{
		$establishment_type = "";
	}
	
	if(isset($_GET["ddl_status"]))
	{
		$status = $_GET["ddl_status"];
	}
	else
	{
		$status = "";
	}
	
	if(isset($_GET["cb_no_fup"]))
	{
		$is_no_fup = $_GET["cb_no_fup"];
	}
	else
	{
		$is_no_fup = "";
	}
	// Nothing here
	
	// Temp data
	$alert = "";
	
	// Get list of court cases
	$court_case_data = array();
	if($case_type != '')
	{
		$court_case_data['legal_court_case_type'] = $case_type;
	}
	if($establishment_type != '')
	{
		$court_case_data['case_establishment'] = $establishment_type;
	}
	if($status != '')
	{
		$court_case_data['case_status'] = $status;
	}
	if($file_id != '')
	{
		$court_case_data['legal_court_case_file_id'] = $file_id;
	}
	$court_case_list = i_get_legal_court_case($court_case_data);
	if($court_case_list["status"] == SUCCESS)
	{
		$court_case_list_data = $court_case_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$court_case_list["data"];
	}	
	
	// Get list of court case types
	$court_case_type_list = i_get_court_case_type('','1');
	if($court_case_type_list["status"] == SUCCESS)
	{
		$court_case_type_list_data = $court_case_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$court_case_type_list["data"];
	}	
	
	// Get list of Establishment
	$establishment_list = i_get_court_establishment('','','','','','');
	if($establishment_list["status"] == SUCCESS)
	{
		$establishment_list_data = $establishment_list["data"];
	}
	else
	{
		$alert      = $alert."Alert: ".$establishment_list["data"];
		$alert_type = 0;
	}	
	
	// Get list of Status
	$status_list = i_get_court_status_list('','');
	if($status_list["status"] == SUCCESS)
	{
		$status_list_data = $status_list["data"];
	}
	else
	{
		$alert      = $alert."Alert: ".$status_list["data"];
		$alert_type = 0;
	}
	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>BD Court Case List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Court Case List</h3><?php if($file_id != "") { ?><span style="float:right; padding-right:10px;"><a href="bd_add_legal_court_case.php?file=<?php echo $file_id; ?>">Add Court Case</a></span><?php } ?>	  
            </div>
            <!-- /widget-header -->
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="get" id="case_search_form" action="bd_court_case_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="type">
			  <option value="">- - Select Case Type - -</option>
			  <?php
			  for($count = 0; $count < count($court_case_type_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $court_case_type_list_data[$count]["bd_court_case_type_master_id"]; ?>" <?php if($case_type == $court_case_type_list_data[$count]["bd_court_case_type_master_id"]){ ?> selected <?php } ?>><?php echo $court_case_type_list_data[$count]["bd_court_case_type_master_type"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>	
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_establishment">
			  <option value="">- - Select Establishment Type - -</option>
			  <?php
			  for($count = 0; $count < count($establishment_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $establishment_list_data[$count]["bd_court_establishment_id"]; ?>" <?php if($establishment_type == $establishment_list_data[$count]["bd_court_establishment_id"]){ ?> selected <?php } ?>><?php echo $establishment_list_data[$count]["bd_court_establishment_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>	

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_status">
			  <option value="">- - Select Status - -</option>
			  <?php
			  for($count = 0; $count < count($status_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $status_list_data[$count]["bd_court_status_master_id"]; ?>" <?php if($status == $status_list_data[$count]["bd_court_status_master_id"]){ ?> selected <?php } ?>><?php echo $status_list_data[$count]["bd_court_status_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>	
			  <span style="padding-left:20px; padding-right:20px;">
			  No Follow Up&nbsp;&nbsp;&nbsp;<input type="checkbox" name="cb_no_fup" value="1" <?php if($is_no_fup == '1') {?> checked <?php } ?> />
			  </span>
			  <input type="submit" name="case_type_search_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
			
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>				    
				    <th style="word-wrap:break-word;">SL No</th>
					<th style="word-wrap:break-word;">Case Type</th>
					<th style="word-wrap:break-word;">Case Number</th>
					<th style="word-wrap:break-word;">Survey Number</th>
					<th style="word-wrap:break-word;">Village</th>
					<th style="word-wrap:break-word;">Filing Date</th>
					<th style="word-wrap:break-word;">Plaintiff</th>
					<th style="word-wrap:break-word;">Defendant</th>
					<th style="word-wrap:break-word;">Jurisdiction</th>
					<th style="word-wrap:break-word;">Case Status</th>															
					<th style="word-wrap:break-word;">Case Added By</th>						
					<th style="word-wrap:break-word;">Hearing Date</th>	
					<th style="word-wrap:break-word;">Remarks</th>
					<th style="word-wrap:break-word;">Follow Up</th>					
					<th style="word-wrap:break-word;">Documents</th>											
					<th style="word-wrap:break-word;">Payment</th>	
					<th style="word-wrap:break-word;">&nbsp;</th>						
				</tr>
				</thead>
				<tbody>
				 <?php
				if($court_case_list["status"] == SUCCESS)
				{				
					$sl_no = 0;
					for($count = 0; $count < count($court_case_list_data); $count++)
					{			
						// Get Follow up Date
						
						$fup_date_start = date("Y-m-d");
						$court_case_fup_data = array("case_id"=>$court_case_list_data[$count]["legal_court_case_id"],"fup_date_start"=>$fup_date_start);
						$fup_list = i_get_court_case_fup_details($court_case_fup_data);
						if($fup_list["status"] == SUCCESS)
						{
							$fup_list_data = $fup_list["data"];
							$fup_list_date = date('d-M-Y',strtotime($fup_list_data[0]["legal_court_case_follow_up_date"]));
							$case_status   = $fup_list_data[0]["bd_court_status_name"];
						}
						else
						{
							$fup_list_date = "No Follow up";
							$case_status = $court_case_list_data[$count]["bd_court_status_name"];
						}
						$sl_no++;
						
						if(($is_no_fup != '1') || ($fup_list['status'] != SUCCESS))
						{
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]["bd_court_case_type_master_type"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]["legal_court_case_number"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]["legal_court_case_survey_no"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]["village_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($court_case_list_data[$count]["legal_court_case_date"])); ?></td>						
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]["legal_court_case_plaintiff"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]["legal_court_case_diffident"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]["bd_court_establishment_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $case_status; ?></td>																	
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]["user_name"]; ?></td>						
						<td style="word-wrap:break-word;"><?php echo $fup_list_date;?></td>
						<td style="word-wrap:break-word;"><a href="#" onclick="alert('Year: <?php echo $court_case_list_data[$count]["legal_court_case_year"].'\n\n'.$court_case_list_data[$count]["legal_court_case_details"]; ?>');">View</a></td>
						<td style="word-wrap:break-word;"><a href="bd_court_case_fup.php?case=<?php echo $court_case_list_data[$count]["legal_court_case_id"]; ?>" target="_blank">Follow Up</a></td>
						<td style="word-wrap:break-word;"><a href="court_case_document_list.php?case=<?php echo $court_case_list_data[$count]["legal_court_case_id"]; ?>" target="_blank">Documents</a></td>
						<td style="word-wrap:break-word;"><a href="court_payment_release_list.php?case=<?php echo $court_case_list_data[$count]["legal_court_case_id"]; ?>" target="_blank">Payment</a></td>
						<?php
						if($role == '1')
						{
						?>
						<td style="word-wrap:break-word;"><a href="bd_edit_legal_court_case.php?case=<?php echo $court_case_list_data[$count]["legal_court_case_id"]; ?>">Edit</a></td>
						<?php
						}
						?>
						</tr>
					<?php 	
						}
					}
				}
				else
				{
				?>
				<td colspan="17">No Court Case added yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>
