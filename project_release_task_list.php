<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Query String Data
	if(isset($_REQUEST['project_process_task_id']))
	{
		$task_id = $_REQUEST['project_process_task_id'];
	}
	else
	{
		$task_id = $_REQUEST['hd_task_id'];
	}

	if(isset($_REQUEST['reason_id']))
	{
		$reason_id = $_REQUEST['reason_id'];
	}
	else
	{
		$reason_id = $_REQUEST['hd_reason_id'];
	}

	if(isset($_REQUEST['process_id']))
	{
		$process_id = $_REQUEST['process_id'];
	}
	else
	{
		$process_id = '';
	}
	if(isset($_REQUEST['project_id']))
	{
		$project_id = $_REQUEST['project_id'];
	}
	else
	{
		$project_id =  $_REQUEST['hd_project_id'];
	}

	if(isset($_REQUEST['road_id']))
	{
		$road_id = $_REQUEST['road_id'];
	}
	else
	{
		$road_id = $_REQUEST['hd_road_id'];
	}

	// Get Project Delay Reason
	$delay_reason_search_data = array("reason_id"=>$reason_id);
	$project_delay_reason_list = i_get_project_delay_reason($delay_reason_search_data);
	if($project_delay_reason_list["status"] == SUCCESS)
	{
		$project_delay_reason_list_data = $project_delay_reason_list["data"];
		$reason_name  = $project_delay_reason_list_data[0]["project_reason_master_name"];
	}
	else
	{
		$reason_name = "" ;
	}

	//Get Regular Measurement
	$get_details_data = i_get_details('Regular', $task_id, $road_id);
	$road_name = $get_details_data["road"];
	$project = $get_details_data["project"];
	$process_name = $get_details_data["process"];
	$task = $get_details_data["task"];

	//Get Email list
	$project_email_search_data = array("project_id"=>$project_id);
	$email_list = db_get_project_email_list($project_email_search_data);
	if ($email_list["status"] == DB_RECORD_ALREADY_EXISTS) {
			$email_list_data = $email_list["data"];
			$to = $email_list_data[0]["project_email_details_to"];
			$cc = $email_list_data[0]["project_email_details_cc"];
			$bcc = $email_list_data[0]["project_email_details_bcc"];
	}

	// Capture the form data
	if(isset($_POST["add_delay_reason_submit"]))
	{
		$task_id     = $_POST["hd_task_id"];
		$road_id     = $_POST["hd_road_id"];
		$project_id     = $_POST["hd_project_id"];
		$reason_id   = $_POST["hd_reason_id"];
		$process_id  = $_POST["hd_process_id"];
		$end_date	 = date("Y-m-d H:i:s");
		$remarks     = $_POST["txt_remarks"];

		// Check for mandatory fields
		if(($end_date != ""))
		{
			$delay_reason_update_data = array("end_date"=>$end_date);
			$delay_reason_iresult = i_update_project_delay_reason($reason_id,$delay_reason_update_data);

			if($delay_reason_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$project_process_task_update_data = array("status"=>'Release');
				$process_task = i_update_project_process_task($task_id,$project_process_task_update_data);
				$subject = '[Notice] Project : '.$project . ' has been released';

// 				$message = 'Project : ' . $project. ',<br /><br />Process : '. $process_name.
// 				',<br /><br />Task : ' . $task.     ',<br /><br />Road : '.  $road_name.',<br /><br />Paused By : '. $loggedin_name.',<br /><br />Paused By : '.date("d-M-Y h:i:s").
// ',<br /><br />Reason : '. $reason[1];
$heading = "<h3>
						Paused By : <span style='color:#00ba8b;'>".$loggedin_name ."</span>
						</h3>
						<h3>
						Paused On: <span style='color:#00ba8b;'>". date('d-m-y h:m A') ."</span>
						</h3>
						<br/>";
				$table = "<table  style=border-collapse:collapse;width:50%>
				<tr>
				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099; text-align:center;'>Project</td>
				<td style='border: 1px solid #ddd;padding: 8px; text-align:center;'>".$project."</td>
				</tr>
		    <tr>
				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Process</td>
			 <td style='border: 1px solid #ddd;padding: 8px; text-align:center;'>".$process_name."</td>
				</tr>
		    <tr>
				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Task</td>
 			 <td style='border: 1px solid #ddd;padding: 8px;text-align:center; '>".$task."</td>
				</tr>
				<tr>
				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Road</td>
 			 <td style='border: 1px solid #ddd;padding: 8px;text-align:center; '>".$road_name."</td>
				</tr>
				<tr>
				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Reason</td>
 			 <td style='border: 1px solid #ddd;padding: 8px;text-align:center; '>".$reason_name."</td>
				</tr>
				<tr>
				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Remarks</td>
			 <td style='border: 1px solid #ddd;padding: 8px;text-align:center; '>".$remarks."</td>
				</tr>";
				$message = $heading;
					$message .= $table;
				$cc = explode(',',$cc);

	      $cc_string = '[';
	    	for($count = 0; $count < count($cc); $count++)
	    	{
	    		$cc_string = $cc_string.'{"email":"'.$cc[$count].'"},';
	    	}
	    	$cc_string = trim($cc_string,',');
	    	$cc_string = $cc_string.']';

				$bcc = explode(',',$bcc);
				$bcc_string = '[';
	    	for($count = 0; $count < count($bcc); $count++)
	    	{
	    		$bcc_string = $bcc_string.'{"email":"'.$bcc[$count].'"},';
	    	}
	    	$bcc_string = trim($bcc_string,',');
	    	$bcc_string = $bcc_string.']';

	    	$request_body = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/','','{
	    		"personalizations": [
	    			{
	    				"to": [
	    					{
	    					  "email": "'.$to.'"
	    					}
	    				],
	    				"cc": '.$cc_string.',
	    				"bcc": '.$bcc_string.',
	    				"subject": "'.$subject.'"
	    			}
	    		],
	    		"from": {
	    			"email": "venkataramanaiah@knsgroup.in"
	    		},
	    		"content": [
	    			{
	    				"type": "text/html",
	    				"value": "'.$message.'"
	    			}
	    		]
	    	}'));

	    	$apiKey = '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';
	    	$sg = new \SendGrid($apiKey);


	    	$response = $sg->client->mail()->send()->post($request_body);
				//header("location:project_plan_process_task_list.php?project_plan_process_id=$process_id");
			}

			$alert = $delay_reason_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get Delay Reason Master
	$delay_reason_master_search_data = array();
	$project_reason_master_list = i_get_delay_reason_master($delay_reason_master_search_data);
	if($project_reason_master_list["status"] == SUCCESS)
	{
		$project_reason_master_list_data = $project_reason_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_reason_master_list["data"];
	}

	// Get Project Delay Reason
	$delay_reason_search_data = array("task_id"=>$task_id);
	$project_delay_reason_list = i_get_project_delay_reason($delay_reason_search_data);
	if($project_delay_reason_list["status"] == SUCCESS)
	{
		$project_delay_reason_list_data = $project_delay_reason_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_delay_reason_list["data"];
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Master - Add Delay Reason</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">

	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Project Master</h3>
	  				</div> <!-- /widget-header -->

					<div class="widget-content">



						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Project Master Add Delay Reason Master</a>
						  </li>
						</ul>
						<br>
							<div class="control-group">
								<div class="controls">
								<?php
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>

								<?php
								if($alert_type == 1) // Success
								{
								?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_master_add_process_form" class="form-horizontal" method="post" action="project_release_task_list.php" enctype="multipart/form-data">
								<input type="hidden" name="hd_task_id" value="<?php echo $task_id; ?>" />
								<input type="hidden" name="hd_reason_id" value="<?php echo $reason_id; ?>" />
								<input type="hidden" name="hd_process_id" value="<?php echo $process_id; ?>" />
								<input type="hidden" name="hd_road_id" value="<?php echo $road_id; ?>" />
								<input type="hidden" name="hd_project_id" value="<?php echo $project_id; ?>" />
									<fieldset>


										<div class="control-group">
											<label class="control-label" for="date">End Date</label>
											<div class="controls">
												<input type="datetime" class="span6" value="<?php echo  date("Y-m-d H:i:s") ;?>" disabled name="date">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />


										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_delay_reason_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>

							</div>

					</div> <!-- /widget-content -->

				</div> <!-- /widget -->

		    </div> <!-- /span8 -->




	      </div> <!-- /row -->

	    </div> <!-- /container -->

	</div> <!-- /main-inner -->

</div> <!-- /main -->




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
