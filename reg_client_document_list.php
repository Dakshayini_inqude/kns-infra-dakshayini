<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 22-March-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* DEFINES - START */
define('REG_CLIENT_DOCUMENT_LIST_FUNC_ID','284');
/* DEFINES - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'registration'.DIRECTORY_SEPARATOR.'reg_client_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',REG_CLIENT_DOCUMENT_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',REG_CLIENT_DOCUMENT_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',REG_CLIENT_DOCUMENT_LIST_FUNC_ID,'4','1');


	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	// Query String Data
	
	if(isset($_REQUEST['request_id']))
	{
		$request_id = $_REQUEST['request_id'];
	}
	else
	{
		$request_id = '';
	}
	
	// Get reg client Process Master modes already added
	$reg_client_process_master_search_data = array("active"=>'1');
	$reg_client_process_master_list = i_get_reg_client_process_master($reg_client_process_master_search_data);
	if($reg_client_process_master_list['status'] == SUCCESS)
	{
		$reg_client_process_master_list_data = $reg_client_process_master_list['data'];
		
    }
     else
    {
		$alert = $reg_client_process_master_list["data"];
		$alert_type = 0;
	}
		
	//Get Document List
	$reg_client_document_search_data = array("active"=>'1',"request_id"=>$request_id);
	$reg_client_document_list = i_get_reg_client_document($reg_client_document_search_data);
	if($reg_client_document_list["status"] == SUCCESS)
	{
		$reg_client_document_list_data = $reg_client_document_list["data"];
	}
	else
	{
		
	}
		
}
else
{
	header("location:login.php");
}	

?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title> Reg client Document List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Reg client Document List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
				    <th>Process</th>
					<th>Document</th>
					<th>Added By</th>				
					<th>Added On</th>
                    <th colspan="1" style="text-align:center;">Actions</th>					
				</tr>
				</thead>
				<tbody>	
				<?php
					if($reg_client_document_list["status"] == SUCCESS)
					{
						$sl_no = 0;
						for($count = 0; $count < count($reg_client_document_list_data); $count++)
						{
							$sl_no++;
							$doc = $reg_client_document_list_data[$count]["reg_client_document_file_path"];
						if($doc != 0)
						{
						 ?>
						    <?php
							if(($edit_perms_list['status'] == SUCCESS) || ($reg_client_document_list_data[$count]["reg_client_document_added_by"] == $user))  
							{
							 ?>
								<tr>
								<td><?php echo $sl_no; ?></td>
								<td><?php echo $reg_client_document_list_data[$count]["reg_client_process_master_name"]; ?></td>
								<td><a href="documents/<?php echo $reg_client_document_list_data[$count]["reg_client_document_file_path"]; ?>" target="_blank"><?php echo $reg_client_document_list_data[$count]["reg_client_document_file_path"]; ?></a></td>
								<td><?php echo $reg_client_document_list_data[$count]["user_name"]; ?></td>
								<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($reg_client_document_list_data[$count][
								"reg_client_document_added_on"])); ?></td>
								<td><?php if(($reg_client_document_list_data[$count]["reg_client_document_active"] == "1") && ($delete_perms_list['status'] == SUCCESS)){?>
								<a href="#" 
								onclick="return reg_client_delete_document('<?php echo $reg_client_document_list_data[$count]["reg_client_document_id"]; ?>','<?php echo $request_id;?>');">Delete</a><?php } ?></td>
								
								</tr>
						     <?php
						   }
					    }
					}					
				}
				else
				{
				?>
				<td colspan="6">No Documents added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			   <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>

function reg_client_delete_document(document_id,request_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "reg_client_document_list.php?request_id=" +request_id;
					}
				}
			}

			xmlhttp.open("POST", "ajax/reg_client_delete_document.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("document_id=" + document_id + "&action=0");
		}
	}	
}
</script>

  </body>

</html>
