<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 25th June 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_payment_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
/* INCLUDES - END */
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$alert_type = -1; // No alert
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_POST["borrow_id"]))
	{
		$borrow_id = $_POST["borrow_id"];
	}
	else
	{
		$borrow_id = "";
	}
	
	if(isset($_POST["file_id"]))
	{
		$file_id = $_POST["file_id"];
	}
	else
	{
		$file_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["add_borrow_pay_submit"]))
	{
		$file_id     = $_POST["hd_bd_file_id"];
		$borrow_id   = $_POST["hd_bd_borrow_id"];
		$amount      = $_POST["num_amount"];
		$reason      = '';
		$payable_to  = $_POST["txt_payable_to"];
		$remarks     = $_POST["remarks"];
		
		// Check for mandatory fields
		if(($borrow_id !="")  && ($amount !="") && ($payable_to !=""))
		{
			$add_borrow_payment_result = i_add_bd_borrow_payment_request($borrow_id,$amount,$reason,$remarks,$payable_to,$user);
			if($add_borrow_payment_result["status"] == SUCCESS)
			{
				$alert = $add_borrow_payment_result["data"];
				$alert_type = 1;
				
				header('location:bd_file_add_borrow_details.php?file_id='.$file_id);
			}
			else
			{
				$alert = $add_borrow_payment_result["data"];
				$alert_type = 0;
			}	
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	$bank_list = i_get_bank_list('','1');
	if($bank_list["status"] == SUCCESS)
	{
		$bank_list_data = $bank_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bank_list["data"];
	}
	
	// Get Borrow file List
	$bd_file_borrow_list = i_get_bd_borrow_list('','','','','','','','','','',$borrow_id);
	if($bd_file_borrow_list["status"] == SUCCESS)
	{
		$bd_file_borrow_list_data   = $bd_file_borrow_list["data"];
		$borrow_id 				 	= $bd_file_borrow_list["data"][0]["bd_file_borrow_id"];		
		$bank_name					= $bd_file_borrow_list["data"][0]["crm_bank_name"];
		$mortgage 					= $bd_file_borrow_list["data"][0]["bd_file_borrow_mortgage"];
		$loan_value					= $bd_file_borrow_list["data"][0]["bd_file_borrow_recieved_loan_value"];		
	}
	else
	{
		header('location:bd_file_borrow_list.php');
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add borrow Payment Details</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Bank: <?php echo $bank_name; ?>&nbsp;&nbsp;&nbsp;Loan Value: <?php echo $loan_value; ?>&nbsp;&nbsp;&nbsp;Mortgage Date: <?php echo date('d-M-Y',strtotime($mortgage)); ?></h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Borrow Payment Details</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_borrow_payment_form" class="form-horizontal" method="post" action="bd_file_add_borrow_payments_details.php" enctype="multipart/form-data">
								<input type="hidden" name="hd_bd_borrow_id" value="<?php echo $borrow_id; ?>" />
								<input type="hidden" name="hd_bd_file_id" value="<?php echo $file_id; ?>" />
									<fieldset>										
															
										<div class="control-group">											
											<label class="control-label" for="num_amount">Amount*</label>
											<div class="controls">
												<input type="number" class="span6" name="num_amount"  required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
																					
										<div class="control-group">											
											<label class="control-label" for="txt_payable_to">Paid To*</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_payable_to" required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->


										<div class="control-group">											
											<label class="control-label" for="remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="remarks">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
																																																	<br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_borrow_pay_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
							<div>
							<table>
							<tr>
							<td></td>
							<tr>
							</table>
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>