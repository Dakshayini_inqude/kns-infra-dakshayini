<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 25th March 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */$_SESSION['module'] = 'HR';
/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_masters_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$alert_type = -1;
	/* DATA INITIALIZATION - END */

	// Capture the form data
	if(isset($_POST['add_holiday_submit']))
	{
		$date = $_POST['dt_holiday_date'];
		$name = $_POST['stxt_holiday_name'];
		$type = $_POST['rd_holiday_type'];
		
		if(($date != "") && ($name != ""))
		{
			$add_holiday_result = i_add_holiday($date,$name,$type,$user);
			
			if($add_holiday_result["status"] == SUCCESS)
			{
				$alert = "Holiday successfully added";
				$alert_type = 1;
			}
			else
			{
				$alert = $add_holiday_result["data"];
				$alert_type = 0;
			}
		}
		else
		{
			$alert = "Please fill all mandatory fields";
			$alert_type = 1;
		}
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Holiday</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>HR</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Holiday</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_holiday_form" class="form-horizontal" method="post" action="hr_add_holiday.php">
									<fieldset>										
															
										<div class="control-group">											
											<label class="control-label">Holiday Date <span class="validate">*</span></label>
											<div class="controls">
												<input type="date" class="span6" name = "dt_holiday_date" id = "dt_holiday_date" onchange="set_day_of_week();" required>
												<p class="help-block" id="day_of_week"></p>
											</div> <!-- /controls -->											
										</div> <!-- /control-group -->
									
										<div class="control-group">											
											<label class="control-label">Holiday Name <span class="validate">*</span></label>
											<div class="controls">
												<input type="text" class="span6" name ="stxt_holiday_name" placeholder="Ex: Independence Day" required>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->																
										
										<div class="control-group">											
											<label class="control-label">Holiday Type <span class="validate">*</span></label>
											<div class="controls">
												<input type="radio" name="rd_holiday_type" value="1" checked>&nbsp;&nbsp;&nbsp;Mandatory&nbsp;&nbsp;&nbsp;
												<input type="radio" name="rd_holiday_type" value="2">&nbsp;&nbsp;&nbsp;Optional
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->																				
                                                                                                                                                               										 <br />
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_holiday_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function set_day_of_week()
{
	given_date = document.getElementById("dt_holiday_date").value;
	var day = new Date(given_date).getDay();
	
	var day_array = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
	
	// Set day of the week
	document.getElementById("day_of_week").innerHTML = "Day of the week: " + day_array[day];
}
</script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>
  </body>

</html>
