<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/* FILE HEADER - START */

// LAST UPDATED ON: 5th Sep 2015

// LAST UPDATED BY: Nitin Kashyap

/* FILE HEADER - END */



/* TBD - START */

/* TBD - END */



/* DEFINES - START */
define('INDENT_MATERIAL_FUNC_ID','164');
/* DEFINES - END */



/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

/* INCLUDES - END */



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	// Get permission settings for this user for this page

	$view_perms_list   = i_get_user_perms($user,'',INDENT_MATERIAL_FUNC_ID,'2','1');

	$edit_perms_list   = i_get_user_perms($user,'',INDENT_MATERIAL_FUNC_ID,'3','1');

	$delete_perms_list = i_get_user_perms($user,'',INDENT_MATERIAL_FUNC_ID,'4','1');

	$add_perms_list    = i_get_user_perms($user,'',INDENT_MATERIAL_FUNC_ID,'1','1');

	

	/* DATA INITIALIZATION - START */

	$alert_type = -1;

	$alert = "";

	/* DATA INITIALIZATION - END */

	

	/* QUERY STRING - START */

	if(isset($_REQUEST["indent_id"]))

	{

		$indent_id = $_REQUEST["indent_id"];

	}

	else

	{

		$indent_id = "";

	}

	

	// Capture the form data

	if(isset($_POST["add_material_submit"]))

	{

		$indent_id	   = $_POST["hd_indent_id"];

		$material_id   = $_POST["hd_material_id"];

		$uom 		   = $_POST["hd_uom_id"];

		$required_date = $_POST["required_date"];

		$quantity	   = $_POST["num_qty"];

		$remarks 	   = $_POST["txt_remarks"];

		

		

		// Check for mandatory fields

		if(($material_id != "") && ($quantity != ""))

		{

			if((strtotime($required_date)) >= (strtotime(date('Y-m-d'))))

			{

				$indent_iresult = i_add_stock_indent_items($material_id,$indent_id,$quantity,$uom,$required_date,'',$remarks,'','',$user);

				

				if($indent_iresult["status"] == SUCCESS)

				{

					$alert_type = 1;					

				}

				else

				{

					$alert_type = 0;

				}

				$alert = $indent_iresult["data"];

			}

			else

			{

				$alert = "Required date should not be lesser than today";

				$alert_type = 0;

			}

		}

		else

		{

			$alert = "Please fill all the mandatory fields";

			$alert_type = 0;

		}

	}

	//Get Uom List

	$stock_unit_search_data = array();

	$uom_list = i_get_stock_unit_measure_list($stock_unit_search_data);

	if($uom_list["status"] == SUCCESS)

	{

		$uom_list_data = $uom_list["data"];

	}

	else

	{

		$alert = $uom_list["data"];

		$alert_type = 0;

	}

	

	// Get Material Details

	$stock_material_search_data = array();

	$material_list = i_get_stock_material_master_list($stock_material_search_data);

	if($material_list["status"] == SUCCESS)

	{

		$material_list_data = $material_list["data"];

	}

	else

	{

		$alert = $material_list["data"];

		$alert_type = 0;

	}

	// Get Indent Item Details

	$stock_indent_search_data = array("indent_id"=>$indent_id,"active"=>'1');

	$indent_item_list = i_get_indent_items_list($stock_indent_search_data);

	if($indent_item_list["status"] == SUCCESS)

	{

		$indent_item_list_data = $indent_item_list["data"];

	}

	

	//Get Indent List

	$stock_indent_search_data = array("indent_id"=>$indent_id);

	$indent_list = i_get_stock_indent_list($stock_indent_search_data);

	if($indent_list["status"] == SUCCESS)

	{

		$indent_list_data = $indent_list["data"];

		$indent_status = $indent_list_data[0]['stock_indent_status'];

	}

	else

	{

		$alert = $indent_list["data"];

		$alert_type = 0;

		$indent_status = 'Waiting';

	}

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Add Indent Material</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

    <link href="css/style1.css" rel="stylesheet">





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>    



<div class="main">

	

	<div class="main-inner">



	    <div class="container">

	

	      <div class="row">

	      	

	      	<div class="span12">      		

	      		

	      		<div class="widget ">

	      			

	      			<div class="widget-header">

	      				<i class="icon-user"></i>

	      				<h3>Indent Details</h3>

						<?php

						if($indent_item_list["status"] == SUCCESS)

						{		

						if($indent_status == 'Waiting')

						{

						?>

						<div class="pull-right" style="padding-right:10px;"><a href="#" onclick="return item_for_approval('<?php echo $indent_id; ?>');"><button>Send for approvals</button></a></div>

						<?php

						}

						}

						?>

	  				</div> <!-- /widget-header -->

					

					<div class="widget-content">

						

						<br>

							<div class="control-group">												

								<div class="controls">

								<?php 

								if($alert_type == 0) // Failure

								{

								?>

									<div class="alert">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>  

								<?php

								}

								?>

                                

								<?php 

								if($alert_type == 1) // Success

								{

								?>								

                                    <div class="alert alert-success">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>

								<?php

								}

								?>					

									<fieldset>	

										<table class="table table-bordered" style="table-layout: fixed;">

										  <thead>

											<?php if($indent_list["status"] == SUCCESS)

											{?>

											  <tr>

											  <td width="25%">Project</td>

											  <td width="70%"><?php echo $indent_list_data[0]["stock_project_name"]; ?> </td>

											  </tr>											 

											  <tr>

											  <td>Department</td>										  

											  <td><?php echo $indent_list_data[0]["general_task_department_name"]; ?></td>

											  </tr>

											  <tr>

											  <td>Status</td>										  

											  <td><?php echo $indent_list_data[0]["stock_indent_status"]; ?></td>

											  </tr>

											  <?php 

											}

											?>

											  </tr>

											  </tbody>

											</table>

											</fieldset>

										</div>

									</div>

								

								

								

								 <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>Material List</h3>

            </div>

            <!-- /widget-header -->

            <div class="widget-content">

			<form action="task_list.php" method="post" id="task_update_form">	

			<input type="hidden" name="hd_indent_id" value="<?php echo $indent_id; ?>" />					

			

              <table class="table table-bordered">

                <thead>

                  <tr>

					<th>Indent No</th>	

					<th>Material</th>

					<th>Material Code</th>

					<th>Lead Time</th>

					<th>Indent Qty</th>

					<th>Issued Qty</th>

					<th>Available Qty in Stock</th>

					<th>Unit Of Measure</th>

					<th>Required Date</th>
					
					<th>Remarks</th>

					<th>Added By</th>

					<th>Added On</th>

					<th colspan="1" style="text-align:center;">Actions</th>	

				</tr>

				</thead>

				<tbody>

				 <?php

				 if($indent_item_list["status"] == SUCCESS)

				 {					 

					for($count = 0; $count < count($indent_item_list_data); $count++)

					{					

						 //Get Stock list
						$material_stock_search_data = array("material_id"=>$indent_item_list_data[$count]["stock_indent_item_material_id"],"project"=>$indent_list_data[0]["stock_indent_project"]);
						$stock_material = i_get_material_stock($material_stock_search_data);

						if($stock_material["status"] == SUCCESS)

						{

							$stock_material_data = $stock_material["data"];

							$qunatity = $stock_material_data[0]["material_stock_quantity"];

							$material = $stock_material_data[0]["material_id"];

						}

						else

						{

							$alert = $alert."Alert: ".$stock_material["data"];

							$qunatity = "0";

							

						}

						//Get Stock Issue List

						$issued_qty = 0;

						$stock_issue_item_search_data = array("material_id"=>$indent_item_list_data[$count]["stock_indent_item_material_id"],"indent_id"=>$indent_id);

						$stock_issue_item_list = i_get_stock_issue_item($stock_issue_item_search_data);

						

						if($stock_issue_item_list["status"] == SUCCESS)

						{

							for($qty_count = 0 ; $qty_count < count($stock_issue_item_list["data"]) ; $qty_count++)

							{

								$stock_issue_list_data = $stock_issue_item_list["data"];

								$issued_qty = $issued_qty + $stock_issue_list_data[$qty_count]["stock_issue_item_qty"];

							}

						}

						else

						{

							$issued_qty = 0;

						}						

						

						//Get Returnable Type from material master

						$stock_material_search_data = array("material_id"=>$indent_item_list_data[$count]["stock_indent_item_material_id"]);

						$material_master_list = i_get_stock_material_master_list($stock_material_search_data);

						if($material_master_list["status"] == SUCCESS)

						{

							$returnable_type = $material_master_list["data"][0]["stock_material_type"];

						}

						$lead_time = $indent_item_list_data[$count]["stock_material_lead_time_procurement"];

						$required_date = strtotime(date("d-M-Y",strtotime($indent_item_list_data[$count]["stock_indent_item_required_date"])));

						$current_date  = strtotime(date("d-M-Y",strtotime($indent_item_list_data[$count]["stock_indent_item_added_on"].' +'.($lead_time + 1).' days')));



					?>				

					<tr>

						<td><?php echo $indent_item_list_data[$count]["stock_indent_no"]; ?></td>

						<td><?php echo $indent_item_list_data[$count]["stock_material_name"]; ?></td>

						<td><?php echo $indent_item_list_data[$count]["stock_material_code"]; ?></td>

						<td><?php echo $lead_time; ?></td>

						<td><?php echo $required_qunatity = $indent_item_list_data[$count]["stock_indent_item_quantity"]; ?></td>

						<td><?php echo $issued_qty; ?></td>

						<td <?php if($qunatity > $required_qunatity ) { ?> style="color:#00FF00;" <?php } else { ?> style="color:#FF0000;" <?php } ?>><strong><?php echo $qunatity ; ?></strong></td>

						<td><?php echo $indent_item_list_data[$count]["stock_unit_name"]; ?></td>

						<td <?php if($current_date > $required_date ) { ?> style="color:#FF0000;" <?php } else { ?> style="color:#00FF00;" <?php } ?>><strong><?php echo date("d-M-Y",strtotime($indent_item_list_data[$count]["stock_indent_item_required_date"])) ; ?></strong></td>
						
						<td><?php echo $indent_item_list_data[$count]["stock_indent_item_remarks"]; ?></td>

						<td><?php echo $indent_item_list_data[$count]["added_by"]; ?></td>

						<td><?php echo date("d-M-Y",strtotime($indent_item_list_data[$count]["stock_indent_item_added_on"])); ?></td>

						<td><?php if(($indent_status == 'Waiting') || ($delete_perms_list['status'] == SUCCESS)){?><a href="#" onclick="return delete_indent_item('<?php echo $indent_item_list_data[$count]["stock_indent_item_id"]; ?>','<?php echo $indent_id; ?>');">Delete</a><?php } ?></td>

					</tr>

					<?php 		

					}

				}

				else

				{

				?>

				<td colspan="2">No Items added</td>

				<?php

				}

				 ?>	



                </tbody>

				</table>

				<br/>		

				<div class="modal-body">

			    <div class="row">

                <center><?php if($indent_status == 'Waiting') { ?><a href="#" data-toggle="modal" data-target="#myModal"><strong>+ Add Material</strong></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php } ?></center>

				  </div>

				  </div>			  

			</form>

            </div>

            <!-- /widget-content --> 

          </div>

								</div> <!-- /controls -->	                                                

							</div> <!-- /control-group -->

               <br><br>

               <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

                  <div class="modal-dialog" role="document"  style="padding:20px;">

                     <div class="modal-content">

                       

                           <div class="modal-header">

                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                              <h4 class="modal-title" id="myModalLabel">Add Material</h4>

                           </div>

                            <form method="post" action="stock_add_indent_material.php">

							<input type="hidden" name="hd_indent_id" value="<?php echo $indent_id; ?>" />



							<input type="hidden" name="hd_material_id" id="hd_material_id" value="" />

							<input type="hidden" name="hd_uom_id" id="hd_uom_id" value="" />

							

										<div class="control-group">											

											<label class="control-label" for="skills">Item</label> <span id="span_material_code" style="display:block"></span>

											<div class="controls dropdown">

												<input type="text" name="stxt_material" class="span5" autocomplete="off" id="stxt_material" onkeyup="return get_material_list();" />

												<div id="search_results" class="dropdown-content"></div>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

                                                                              

										

										<div class="control-group" style="width:50%; float:left">											

											<label class="control-label" for="required_date">Required Date</label>										

											<div class="controls">

												<input type="date" name="required_date" placeholder="Date" required="required">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group" style="width:50%; float:left">											

											<label class="control-label" for="num_qty">Quantity</label>

											<div class="controls">

												<input type="number" name="num_qty" placeholder="Quantity" min="0.01" step="0.01" required="required"><span id="span_material_uom" style="display:inline"></span>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="num_qty">Remarks</label>										

											<div class="controls">

												<input type="text" name="txt_remarks" placeholder="Remarks" required="required">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

                           <div class="modal-footer">                              

                            <button type="submit" name="add_material_submit" class="btn btn-primary">Save changes</button>					  

                           </div>

                           </form>

                     </div>

                  </div>

               </div>

						  

					</div> <!-- /widget-content -->

						

				</div> <!-- /widget -->

	      		

		    </div> <!-- /span8 -->

	      	

	      </div> <!-- /row -->

	

	    </div> <!-- /container -->

	    

	</div> <!-- /main-inner -->

    

</div> <!-- /main -->

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->  

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function delete_indent_item(indent_item_id,indent_id)

{

	var ok = confirm("Are you sure you want to Delete?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{
					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

						var url = "stock_add_indent_material.php?indent_id=" +indent_id

						window.location = url;

					}

				}

			}



			xmlhttp.open("POST", "ajax/delete_indent_item.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("indent_item_id=" + indent_item_id + "&indent_id=" + indent_id + "&action=0");

		}

	}	

}

function go_to_issue_item(indent_id,indent_item_id,returnable_type)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_add_issue.php");

	

	var hiddenField2 = document.createElement("input");

	hiddenField2.setAttribute("type","hidden");

	hiddenField2.setAttribute("name","indent_id");

	hiddenField2.setAttribute("value",indent_id);

	

	var hiddenField3 = document.createElement("input");

	hiddenField3.setAttribute("type","hidden");

	hiddenField3.setAttribute("name","indent_item_id");

	hiddenField3.setAttribute("value",indent_item_id);

	

	var hiddenField4 = document.createElement("input");

	hiddenField4.setAttribute("type","hidden");

	hiddenField4.setAttribute("name","returnable_type");

	hiddenField4.setAttribute("value",returnable_type);

    	

	form.appendChild(hiddenField2);	

	form.appendChild(hiddenField3);	

	form.appendChild(hiddenField4);	

	

	document.body.appendChild(form);

    form.submit();

}

function go_to_quotation(indent_id,indent_item_id)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_add_quotation_compare.php");

	

	var hiddenField2 = document.createElement("input");

	hiddenField2.setAttribute("type","hidden");

	hiddenField2.setAttribute("name","indent_id");

	hiddenField2.setAttribute("value",indent_id);

	

	var hiddenField3 = document.createElement("input");

	hiddenField3.setAttribute("type","hidden");

	hiddenField3.setAttribute("name","indent_item_id");

	hiddenField3.setAttribute("value",indent_item_id);

    	

	form.appendChild(hiddenField2);	

	form.appendChild(hiddenField3);	

	

	document.body.appendChild(form);

    form.submit();

}

function item_for_approval(indent_id)

{

	var ok = confirm("Are you sure you want to send for approvals?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

						var url = "stock_add_indent.php"

						window.location = url;

					}

				}

			}



			xmlhttp.open("POST", "stock_items_for_approval.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("indent_id=" + indent_id + "&action=0");

		}

	}	

}



function get_material_list()

{ 

	var searchstring = document.getElementById('stxt_material').value;

	

	if(searchstring.length >= 3)

	{

		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}



		xmlhttp.onreadystatechange = function()

		{				

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{		
				if(xmlhttp.responseText != 'FAILURE')

				{

					document.getElementById('search_results').style.display = 'block';

					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;

				}

			}

		}



		xmlhttp.open("POST", "ajax/get_material.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring);				

	}

	else	

	{

		document.getElementById('search_results').style.display = 'none';

	}

}



function select_material(material_id,material_name,material_code,unit_measure)
{
	document.getElementById('hd_material_id').value = material_id;
	
	document.getElementById('stxt_material').value = material_name;
	
	var name_code = material_name.concat('-',material_code);
	
	document.getElementById('span_material_code').innerHTML = name_code;
	
	document.getElementById('span_material_uom').innerHTML = unit_measure;
	
	document.getElementById('search_results').style.display = 'none';
}


</script>

</body>

</html>

