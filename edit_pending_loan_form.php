<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */

// LAST UPDATED ON: 7th June 2015

// LAST UPDATED BY: Nitin Kashyap

/* FILE HEADER - END */



/* TBD - START */

// 1. Role dropdown should be from database

/* TBD - END */



/* DEFINES - START */
define('ADD_USER_FUNC_ID', '1');
/* DEFINES - END */



/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_user.php');

/* INCLUDES - END */



if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {

    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // // Get permission settings for this user for this page

    $view_perms_list = i_get_user_perms($user, '', ADD_USER_FUNC_ID, '2', '1');

    $add_perms_list  = i_get_user_perms($user, '', ADD_USER_FUNC_ID, '1', '1');

    /* DATA INITIALIZATION - START */
    $alert = "";
    $alert_type = -1;
    /* DATA INITIALIZATION - END */

    if(isset($_GET["user_id"]))
    {
      $persona_user_id = $_GET["user_id"];
    }
    else
    {
      $persona_user_id = "";
    }

    // Capture the form data
    if (isset($_POST["edit_pending_loan"])) {

        $persona_department_id   = $_POST["ddl_department_id"];
        $persona_user_id         = $_POST["sel_user"];
        $total_amount            = $_POST["pending_loan_full_amount"];
        $total_tenure            = $_POST["pending_loan_total_tenure"];
        $pending_amount          = $_POST["pending_loan_remaining_amount"];
        $pending_loan_remarks    = $_POST["pending_loan_remarks"];


        if(($persona_department_id != "0") && ($persona_user_id != "0")  &&( $total_amount != "0") && ($total_tenure != "0") && ($pending_amount != "0")) {

          $nukePersona = db_pending_loan_drop($persona_user_id);

          $response = db_add_pending_loan_amount($persona_department_id, $persona_user_id, $total_amount, $total_tenure, $pending_amount, $pending_loan_remarks);

           var_dump($response);
          if($response["status"] == 'SUCCESS') {
            $_POST = array();
            ?>
            <script>
            alert("Loan Record updated successfully ");
             console.log('values ', <?= json_encode($_POST); ?>);
            </script>
            <?php
          }

        }
        else {
          $alert = "Please fill all the mandatory fields";
          $alert_type = 0;

        ?>
          <script>
          alert("Fill all feilds");
            console.log('values ', <?= json_encode($_POST); ?>);
          </script>
          <?php
        }

    }

    // Get list of Department*

    $department_list = i_get_department_list('', '');

    if ($department_list["status"] == SUCCESS) {
        $department_list_data = $department_list["data"];
    } else {
        $alert = $alert."Alert: ".$department_list["data"];
      }

      $persona_details = db_get_pending_loan($persona_user_id);

      if($persona_details["status"] = "DB_RECORD_ALREADY_EXISTS") {
        $persona_details_data = $persona_details["data"][0];

        $user_list = i_get_user_list('', '', '', '', '1', $persona_details_data['pending_loan_department_id']);
        if ($user_list['status'] == SUCCESS) {
            $user_list_data = $user_list['data'];
        }

        $designation_list = db_get_designation_list();
        if($designation_list['status'] == 'DB_RECORD_ALREADY_EXISTS') {
          $designation_list_data = $designation_list['data'];
        }
      }



} else {
    header("location:login.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>Pending Loan</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->
    <!-- Custom styles -->
    <link href="assets/multistepform/css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
  <?php

  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

  ?>
<!-- MultiStep Form -->
<div class="row">
    <div class="col-md-6 col-md-offset-3" style="margin-top:-50px; margin-bottom:50px">
        <form id="msform" method="post">
            <!-- progressbar -->
            <!-- <ul id="progressbar">
                <li class="active">Performance Linked Incentives</li>
                <li>Address Details</li>
                <li>Amenities</li>
            </ul> -->
            <!-- fieldsets -->
            <fieldset>
                <h2 class="fs-title">Existing Loan Form</h2>
                <h3 class="fs-subtitle">Enter User Pending Loan Details</h3>
                <div class="" style="text-align:left">
                  <div class="form-group">
                  <label>Department *</label>
                    <select class="form-control" id="ddl_department_id" name="ddl_department_id" disabled>
                      <option value='0'>- - Select Department - -</option>
                      <?php
                        for ($count = 0; $count < count($department_list_data); $count++) {
                      ?>
                      <option value="<?php echo $department_list_data[$count]["general_task_department_id"]; ?>"
                        <?php if($persona_details_data['pending_loan_department_id'] == $department_list_data[$count]["general_task_department_id"]) {
                        ?> selected="selected"
                      <?php } ?>>
                        <?php echo $department_list_data[$count]["general_task_department_name"]; ?></option>

                      <?php
                      }
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                      <label>Name *</label>
                      <select class="form-control" disabled>
                      <?php
                      for($count = 0; $count < count($user_list_data); $count++)
                      {
                      ?>
                      <option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($persona_details_data["pending_loan_user_id"] == $user_list_data[$count]["user_id"]){ ?> selected <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?></option>
                      <?php
                      }
                      ?>

                      </select>
                  </div>

                  <label style="font-weight:300">Total Loan Amount</label>
                  <input type="number" min="1" value="<?= $persona_details_data['pending_loan_total_amount']; ?>" id="pending_loan_full_amount" name="pending_loan_full_amount" placeholder="Loan Amount" onchange="return calculate()"/>
                  <label style="font-weight:300">Total Tenure</label>
                  <input type="number" name="pending_loan_total_tenure" id="pending_loan_total_tenure" onchange="return calculate()" placeholder="Total Tenure in Months"/>
                  <label style="font-weight:300">Balance Loan Amount</label>
                  <input type="number" name="pending_loan_remaining_amount" id="pending_loan_remaining_amount" onchange="return calculate()" placeholder="Pending Loan Amount"/>
                  <label style="font-weight:300">Pending Tenure</label>
                  <input type="text" name="pending_loan_tenure" id="pending_loan_tenure" placeholder="Pending Tenure" disabled/>
                  <label style="font-weight:300">Monthly Loan Deduction Amount</label>
                  <input type="text" name="pending_loan_monthly_deduction" id="pending_loan_monthly_deduction" placeholder="Monthly Loan Deduction Amount" disabled/>
                  <input type="text" name="pending_loan_remarks" id="pending_loan_remarks" placeholder="Remarks"/>
              </div>
              <input type="submit" id="edit_pending_loan" name="edit_pending_loan" class="submit action-button" value="Submit"/>
                <br/>
            </fieldset>


        </form>
    </div>
</div>
<!-- /.MultiStep Form -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/multistepform/js/msform.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->

<script>

function calculate() {
  var total_amount = document.getElementById('pending_loan_full_amount').value ? document.getElementById('pending_loan_full_amount').value : 0,
      total_tenure = document.getElementById('pending_loan_total_tenure').value ? document.getElementById('pending_loan_total_tenure').value : 0,
      pending_amount = document.getElementById('pending_loan_remaining_amount').value ? document.getElementById('pending_loan_remaining_amount').value : 0;

      console.log('am in calculate ', total_amount, total_tenure, pending_amount);

      var monthly_amount, pending_tenure;

      total_amount = parseInt(total_amount);
      total_tenure = parseInt(total_tenure);
      pending_amount = parseInt(pending_amount);

      if(total_amount >=1 && total_tenure >=1 && pending_amount >=1) {
        if(pending_amount > total_amount || pending_amount == total_amount) {
          alert("Enter Valid Loan Pending Amount");
      } else {
        monthly_amount = parseInt(parseInt(total_amount) / parseInt(total_tenure));
        pending_tenure = (parseInt(pending_amount) / monthly_amount);

        document.getElementById('pending_loan_tenure').value = pending_tenure;
        document.getElementById('pending_loan_monthly_deduction').value = monthly_amount;
      }
    }



}

$(document).ready(function() {
  $("#ddl_department_id").change(function() {
		var department_id = $(this).val();
		console.log('department_id ', department_id);
		if(department_id == 0) {
			$("#sel_user").empty();
      $("#sel_user").append("  <option value='0'>- - Select User - -</option>");
			return false;
		}

		$.ajax({
			url: 'ajax/getUsers.php',
			data: {department_id: department_id},
			dataType: 'json',
			success: function(response) {
				console.log('hope i got all users ', response);

				$("#sel_user").empty();
        $("#sel_user").append("  <option value='0'>- - Select User - -</option>");
				for(var i=0; i< response.length; i++) {
					var id = response[i]['user_id'];
					var name = response[i]['user_name'];
          // var class = 'list-group-item';

					$("#sel_user").append("<option value='"+id+"'>"+name+"</option>");
					// $("#sel_user").append("<div id='"+id+"'>"+name+"</div>");
				}
			}
		})
	})
})

</script>
</body>
</html>
