<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_process_master_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Query String Data
    if (isset($_REQUEST['process_id'])) {
        $process_id = $_REQUEST['process_id'];
    } else {
        $process_id = "";
    }
    // Get Already added process
    $project_task_master_search_data = array("process"=>$process_id);
    $project_task_master_list = i_get_project_task_master($project_task_master_search_data);
    if ($project_task_master_list["status"] == SUCCESS) {
        $project_task_master_list_data = $project_task_master_list["data"];
    }

   echo json_encode($project_task_master_list_data);
} else {
    header("location:login.php");
}
