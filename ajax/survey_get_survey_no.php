<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$search               = $_POST["search"];
	$village              = $_POST['ddl_village'];

	$survey_master_search_data = array("survey_no"=>$search,"village"=>$village,"active"=>'1');	
	$survey_master_list_result = i_get_survey_master($survey_master_search_data);
	if($survey_master_list_result["status"] == SUCCESS)
	{		
		$survey_list = '';

		for($count = 0; $count < count($survey_master_list_result['data']); $count++)
		{
			$survey_list = $survey_list.'<a style="display:block; width:100%; border-bottom:1px solid #efefef; text-decoration:none; cursor:pointer; padding:5px;" onclick="return select_survey_no(\''.$survey_master_list_result['data'][$count]['survey_master_id'].'\',\''.$survey_master_list_result['data'][$count]['survey_no'].'\',\''.$survey_master_list_result['data'][$count]['land_owner'].'\');">'.$survey_master_list_result['data'][$count]['survey_no'].' '.$survey_master_list_result['data'][$count]['land_owner'].'</a>';
		}
	}
	else
	{
		$survey_list = 'FAILURE';
	}
	
	echo $survey_list;
}
else
{
	header("location:login.php");
}
?>