<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$task_id      = $_GET["task_id"];
	$road_id      = $_GET["road_id"];


	$total_planned_mp_cost = 0 ;
	$total_planned_mc_cost = 0 ;
	$total_planned_cw_cost = 0 ;
	$total_planned_material_cost = 0 ;
	$project_planned_data = array("task_id"=>$task_id,"road_id"=>$road_id);
	$get_planned_data = db_get_planned_data($project_planned_data);
	if($get_planned_data["status"] == DB_RECORD_ALREADY_EXISTS)
	{
			for($plan_count = 0 ; $plan_count < count($get_planned_data["data"]) ; $plan_count++)
			{
				$total_planned_mp_cost = $total_planned_mp_cost + $get_planned_data["data"][$plan_count]["planned_manpower"];
				$total_planned_mc_cost = $total_planned_mc_cost + $get_planned_data["data"][$plan_count]["planned_machine"];
				$total_planned_cw_cost = $total_planned_cw_cost + $get_planned_data["data"][$plan_count]["planned_contract"];
				$total_planned_material_cost = $total_planned_material_cost + $get_planned_data["data"][$plan_count]["planned_material"];
			}
	}
	else {
		$total_actual_mp_cost = "";
	}
	 $planned_data = array("planned_mp"=>$total_planned_mp_cost,
	 											 "planned_mc"=>$total_planned_mc_cost,
											 "planned_cw"=>$total_planned_cw_cost,
										 		"planned_material"=>$total_planned_material_cost) ;
	echo json_encode($planned_data);
}
else
{
	header("location:login.php");
}
?>
