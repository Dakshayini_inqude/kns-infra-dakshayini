<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */
/*
TBD:
*/
// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];
    // Update attendance details
    $project_id      = $_GET["project_id"];
    //Manpower
    $total_actual_mp_cost = 0 ;
    $budget_manpower_list =  db_get_project_overall_budget_manpower('project_id', $project_id);
    if ($budget_manpower_list["status"] == DB_RECORD_ALREADY_EXISTS) {
        for ($mp_count = 0 ; $mp_count < count($budget_manpower_list["data"]) ; $mp_count++) {
            $total_actual_mp_cost = $total_actual_mp_cost + $budget_manpower_list["data"][$mp_count]["total_cost"];
        }
    } else {
        $total_actual_mp_cost = "";
    }

    //Machine
    $total_actual_mc_cost = 0 ;
    $budget_machine_list =  db_get_project_overall_budget_machine('project_id', $project_id);
    if ($budget_machine_list["status"] == DB_RECORD_ALREADY_EXISTS) {
        $budget_machine_list_data = $budget_machine_list["data"];
        for ($mp_count = 0 ; $mp_count < count($budget_machine_list["data"]) ; $mp_count++) {
            $machine_rate = $budget_machine_list_data[$mp_count]['fuel_charge'];
            if ($budget_machine_list_data[$mp_count]["machine_end_date"] != "0000-00-00 00:00:00") {
                $start_date_time = strtotime($budget_machine_list_data[$mp_count]["machine_start_date"]);
                $end_date_time = strtotime($budget_machine_list_data[$mp_count]["machine_end_date"]);
                $date_diff = $end_date_time - $start_date_time;
                $no_hrs_worked = $date_diff/3600;
            } else {
                $start_date_time = strtotime($budget_machine_list_data[$mp_count]["machine_start_date"]);
                $end_date_time = strtotime(date("Y-m-d H:i:s"));
                $date_diff = $end_date_time - $start_date_time;
                $no_hrs_worked = $date_diff/3600;
            }
            // Off time related calculation
            $off_time = $budget_machine_list_data[$mp_count]["off_time"]/60;
            $eff_hrs = $no_hrs_worked - $off_time;
            $actual_mc_cost = ($machine_rate * $eff_hrs) + $budget_machine_list_data[$mp_count]["machine_cost"];
            $total_actual_mc_cost = $total_actual_mc_cost + $actual_mc_cost ;
        }
    } else {
        $total_actual_mc_cost = 0;
    }
    //Contract
    $total_actual_cw_cost = 0 ;
    $budget_cw_list =  db_get_project__overall_budget_contract('project_id', $project_id);
    if ($budget_cw_list["status"] == DB_RECORD_ALREADY_EXISTS) {
        for ($cw_count = 0 ; $cw_count < count($budget_cw_list["data"]) ; $cw_count++) {
            $total_actual_cw_cost = $total_actual_cw_cost + $budget_cw_list["data"][$cw_count]["total_amount"];
        }
    } else {
        $total_actual_cw_cost = "";
    }

    $total_cost = $total_actual_mp_cost + $total_actual_mc_cost + $total_actual_cw_cost;
    echo $total_cost ;
} else {
    header("location:login.php");
}
