<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$project_id      = $_GET["project_id"];

	$total_actual_mp_cost = 0 ;
	$budget_manpower_list =  db_get_project_overall_budget_manpower('project_id',$project_id);
	if($budget_manpower_list["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		for($mp_count = 0 ; $mp_count < count($budget_manpower_list["data"]) ; $mp_count++)
		{
			$total_actual_mp_cost = $total_actual_mp_cost + $budget_manpower_list["data"][$mp_count]["total_cost"];
		}
	}
	else {
		$total_actual_mp_cost = "";
	}
	echo $total_actual_mp_cost;
}
else
{
	header("location:login.php");
}
?>
