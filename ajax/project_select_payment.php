<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
  $rowdata = $_POST["val"];
	$vendor_id = $_POST["vendor_id"];
	$length = count($rowdata);

  if($length > 0){
    $start_date = "0000-00-00";
    $end_date = "0000-00-00";
    $no_of_men_hrs = "";
    $no_of_women_hrs = "";
    $no_of_mason_hrs = "";
    $total_amount = "";
    $bill_no= p_generate_manpower_bill_no();
    $payment_manpower_iresult = i_add_project_actual_payment_manpower('', '', '', '', '', '', '', $user, $bill_no);
    if ($payment_manpower_iresult["status"] == SUCCESS) {
        $payment_manpower_id = $payment_manpower_iresult["data"];
    }
    for ($item_count = 0 ; $item_count < $length ; $item_count++) {
      $no_of_men_hrs  		 = $no_of_men_hrs + $rowdata[$item_count]['project_task_actual_manpower_no_of_men'];
      $no_of_women_hrs   		 = $no_of_women_hrs+$rowdata[$item_count]['project_task_actual_manpower_no_of_women'];
      $no_of_mason_hrs    	 = $no_of_mason_hrs+$rowdata[$item_count]['project_task_actual_manpower_no_of_mason'];
      $men_rate		         = $rowdata[$item_count]['project_task_actual_manpower_men_rate'];
      $women_rate		         = $rowdata[$item_count]['project_task_actual_manpower_women_rate'];
      $mason_rate		         = $rowdata[$item_count]['project_task_actual_manpower_mason_rate'];
      $manpower_date		     = $rowdata[$item_count]['project_task_actual_manpower_date'];
      $man_power_vendor_id 	 = $vendor_id;
      $man_power_id 			 = $rowdata[$item_count]['project_task_actual_manpower_id'];

      if ($start_date == "0000-00-00") {
          $start_date = $manpower_date;
      } elseif (strtotime($manpower_date) < strtotime($start_date)) {
          $start_date = $manpower_date;
      }

      if ($start_date == "0000-00-00") {
          $end_date = $manpower_date;
      } elseif (strtotime($manpower_date) > strtotime($end_date)) {
          $end_date = $manpower_date;
      }
      $payment_manpower_mapping_iresult = i_add_project_payment_manpower_mapping($man_power_id, $payment_manpower_id, '', $user);
			if ($payment_manpower_iresult["status"] == SUCCESS) {
			$man_power_update_data = array("status1"=>"Bill Generated");
			$manpower_results = i_update_man_power($man_power_id, '', $man_power_update_data);
		  }
    }
   $total_men_cost_count = $no_of_men_hrs * $men_rate;
   $total_women_cost_count = $no_of_women_hrs  * $women_rate;
   $total_mason_cost_count = $no_of_mason_hrs * $mason_rate;
   $total_amount = $total_men_cost_count + $total_women_cost_count + $total_mason_cost_count;
   $man_power_start_date = date("Y-m-d", strtotime($start_date)).' '."00:00:00";
   $man_power_end_date = date("Y-m-d", strtotime($end_date)) .' '."00:00:00";
   $total_no_of_hrs_worked = $no_of_men_hrs + $no_of_women_hrs + $no_of_mason_hrs ;

   //Update Payment Manpower
   $project_actual_payment_manpower_update_data = array("vendor_id"=>$man_power_vendor_id,"amount"=>$total_amount,"no_of_hours"=>$total_no_of_hrs_worked,"men_hrs"=>$no_of_men_hrs,"women_hrs"=>$no_of_women_hrs,"mason_hrs"=>$no_of_mason_hrs,"from_date"=>$man_power_start_date,"to_date"=>$man_power_end_date);
   $payment_manpower_uresults = i_update_project_actual_payment_manpower($payment_manpower_id, $project_actual_payment_manpower_update_data);
   // if ($payment_manpower_uresults["status"] == SUCCESS) {
   //      header("location:../project_add_bill_no.php?manpower_id=$payment_manpower_id");
   //  }

	 $output = array('data'=> $payment_manpower_uresults['data'],'status'=> $payment_manpower_uresults['status'],'id'=>$payment_manpower_id);

	  // echo json_encode($payment_manpower_uresults);
		echo json_encode($output);
  }
 }
 else{
   echo "FAILURE";
 }
?>
