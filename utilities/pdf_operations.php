<?php
include_once('mpdf60/mpdf.php');

function save_to_pdf($content, $file_name, $file_type, $temporary)
{
	// Get file path
	$file_path = get_file_path($file_type, $temporary);
	
	$mpdf = new mPDF();
	$mpdf->WriteHTML($content);
	if($temporary == 1)
	{
		// Create file name
		$file_name = $file_name.'_'.date("dmyHis").'.pdf';
		$mpdf->Output($file_name,'I');
		$return = $file_name;
	}
	else
	{
		// Create file name
		$file_name = $file_name.'_'.date("dmyHis").'.pdf';
		$mpdf->Output($file_path.'/'.$file_name,'F');
		$return = $file_path.'/'.$file_name;
	}
	
	return $return;
}

function get_file_path($file_type, $temporary)
{
	$file_path_root = $_SERVER['DOCUMENT_ROOT'].'/twigs/';
	
	if($temporary == 0)
	{
		switch($file_type)
		{
			case 'employee_card':
			$file_path = $file_path_root.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'employee_cards';
			break;
			
			case 'accident_register':
			$file_path = $file_path_root.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'accident_registers';
			break;
			
			default:
			$file_path = $file_path_root.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'other_documents';
			break;
		}
	}
	else
	{
		$file_path = $file_path_root.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'temporary_files';
	}
	
	return $file_path;
}
?>