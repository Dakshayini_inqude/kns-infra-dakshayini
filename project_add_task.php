<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Initialization
	$alert = "";
	$alert_type = -1; // No alert

	// Query String
	if(isset($_REQUEST["project_plan_process_id"]))
	{
		$process_id = $_REQUEST["project_plan_process_id"];
	}
	else
	{
		$process_id = "";
	}

	if(isset($_REQUEST["process_master_id"]))
	{
		$process_master_id = $_REQUEST["process_master_id"];
	}
	else
	{
		$process_master_id = "";
	}

	if(isset($_REQUEST["plan_id"]))
	{
		$plan_id = $_REQUEST["plan_id"];
	}
	else
	{
		$plan_id = "";
	}

	if(isset($_REQUEST["project_id"]))
	{
		$project_id = $_REQUEST["project_id"];
	}
	else
	{
		$project_id = "";
	}

	// Get process details
	$project_plan_process_search_data = array("active"=>'1',"process_id"=>$process_id);
	$project_plan_process_list = i_get_project_plan_process($project_plan_process_search_data);
	if($project_plan_process_list['status'] == SUCCESS)
	{
			$project_plan_process_list_data    = $project_plan_process_list["data"];
			$process_start_date                = $project_plan_process_list_data[0]["project_plan_process_start_date"];
			$process_end_date                  = $project_plan_process_list_data[0]["project_plan_process_end_date"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_plan_process_list["data"];
		$alert_type = 0; // Failure
		$project_process_name  ="";
	}

	// Get process details
	$project_process_master_search_data = array("process_id"=>$process_master_id,"active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list['status'] == SUCCESS)
	{
			$project_plan_process_list_data = $project_process_master_list["data"];
			$project_process_name = $project_plan_process_list_data[0]["project_process_master_name"];
			$process_road = $project_plan_process_list_data[0]["project_process_master_road"];
	}
	else
	{
		$project_process_name  = "";
		$process_road  = "";
	}

	// Get Project Man Power Type Master modes already added
	$project_man_power_master_search_data = array();
	$project_man_power_type_list = i_get_project_man_power_master($project_man_power_master_search_data);
	if($project_man_power_type_list['status'] == SUCCESS)
	{
		$project_man_power_type_list_data = $project_man_power_type_list['data'];

    }

	if(isset($_POST["add_tasks_submit"]))
	{
		$project_id             = $_POST["hd_project_id"];
		// Get Project Location already added
		$project_site_location_mapping_master_search_data = array("active"=>'1',"project_id"=>$project_id,"is_road"=>'1');
		$project_site_location_list = i_get_project_site_location_mapping_master($project_site_location_mapping_master_search_data);
		if($project_site_location_list['status'] == SUCCESS)
		{
			$project_site_location_list_data = $project_site_location_list['data'];
		}
		else
		{
			//
		}
		$task_type_array = $_POST["cb_task_type"];
		for($task_type_count = 0; $task_type_count < count($task_type_array); $task_type_count++)
		{
			// Capture all form data
			$process_id             = $_POST["hd_process_id"];
			$process_road           = $_POST["hd_process_road"];
			$plan_id		        = $_POST["hd_plan_id"];
			$process_start_date     = $_POST["hd_start_date"];
			$process_end_date       = $_POST["hd_end_date"];
			$task_type 				= $task_type_array[$task_type_count];

			//Check For Road Process
			if($process_road == "1")
			{
				// Add task
				for($location_count = 0 ; $location_count < count($project_site_location_list_data) ; $location_count++)
				{
					$task_iresult = i_add_project_process_task($process_id,$task_type,$project_site_location_list_data[$location_count]["project_site_location_mapping_master_id"],'','','','',$user);
					if($task_iresult["status"] == SUCCESS)
					{
						$task_id = $task_iresult["data"];
						$project_task_planning_iresult = i_add_project_task_planning($project_id,$task_id,'',$project_site_location_list_data[$location_count]["project_site_location_mapping_master_id"],'','','','','','','','','','',$user);

					header("location:project_plan_process_list.php?plan_id=$plan_id");
						$alert =  "Task Successfully Added";
						$alert_type = 1;
					}
				}
			}
			else if($process_road == "0")
			{
				$task_iresult = i_add_project_process_task($process_id,$task_type,'No Roads','','','','',$user);
				if($task_iresult["status"] == SUCCESS)
				{
					$task_id = $task_iresult["data"];
					$project_task_planning_iresult = i_add_project_task_planning($project_id,$task_id,'',"No Roads",'','','','','',"",'','','','',$user);
					header("location:project_plan_process_list.php?plan_id=$plan_id");
				}
			}
		}
	}

	// Get list of tasks for this process type
	$project_task_master_search_data = array("active"=>'1',"process"=>$process_master_id);
	$project_task_master_list = i_get_project_task_master($project_task_master_search_data);

	if($project_task_master_list["status"] == SUCCESS)
	{
		$project_task_master_list_data = $project_task_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_task_master_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Add Task</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">

	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">

	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Process:  &nbsp;  &nbsp;<?php echo $project_process_name; ?>&nbsp;&nbsp;&nbsp;&nbsp</h3>
	  				</div> <!-- /widget-header -->

					<div class="widget-content">



						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add tasks to this process</a>
						  </li>
						</ul>

						<br>
						    <div class="control-group">
								<div class="controls">
								<?php
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>

								<?php
								if($alert_type == 1) // Success
								{
								?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
							<div class="tab-content">

								<div class="tab-pane active" id="formcontrols">
								<form id="add_tasks" class="form-horizontal" method="post" action="project_add_task.php">
								<input type="hidden" name="hd_process_id" value="<?php echo $process_id; ?>" />
								<input type="hidden" name="hd_bprocess_id" value="<?php echo $bprocess_id; ?>" />
								<input type="hidden" name="hd_start_date" value="<?php echo $process_start_date; ?>" />
								<input type="hidden" name="hd_end_date" value="<?php echo $process_end_date; ?>" />
								<input type="hidden" name="hd_plan_id" value="<?php echo $plan_id; ?>" />
								<input type="hidden" name="hd_project_id" value="<?php echo $project_id; ?>" />
								<input type="hidden" name="hd_process_road" value="<?php echo $process_road; ?>" />
									<fieldset>

										<div class="control-group">
										<label class="control-label" for="process_type">Choose Task</label>
											<div class="controls">
											<?php
											if($project_task_master_list["status"] == SUCCESS)
											{
											?>

											<input type="hidden" name="hd_task_master_count" value="<?php echo count($project_task_master_list_data); ?>" />
											<?php
											for($count = 0; $count < count($project_task_master_list_data); $count++)
											{
												// Get already added tasks for this process
												$project_process_task_search_data = array("process_id"=>$process_id,"task_type"=>$project_task_master_list_data[$count]["project_task_master_id"],"active"=>'1');
												$project_task_process = i_get_project_process_task($project_process_task_search_data);

												if($project_task_process['status'] != SUCCESS)
												{
												?>
													<input type="checkbox" name="cb_task_type[]" value="<?php echo $project_task_master_list_data[$count]["project_task_master_id"]; ?>" />&nbsp;&nbsp;&nbsp;
												<?php echo $project_task_master_list_data[$count]["project_task_master_name"]; ?>&nbsp;&nbsp;&nbsp;<br /><br />
											<?php
												}
											}
									?>

									<?php
											}
											else
											{
												echo "No Task Added for This process!";
											}?>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

								      <br />
										<div class="form-actions">
											<button type="submit" class="btn btn-primary" name="add_tasks_submit">Submit</button>
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>


							</div>


						</div>





					</div> <!-- /widget-content -->

				</div> <!-- /widget -->

		    </div> <!-- /span8 -->




	      </div> <!-- /row -->

	    </div> <!-- /container -->

	</div> <!-- /main-inner -->

</div> <!-- /main -->




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function get_no_hrs(mode,count)
{
	if(mode == 'days')
	{
		var days = document.getElementById("no_of_days_" + count).value;
		var calc_hrs = days*8;
		document.getElementById("no_of_hrs_" + count).value = calc_hrs;
	}
	else if(mode == 'hrs')
	{
		var calc_hrs = document.getElementById('no_of_hrs_' + count).value;
		var calc_days = (calc_hrs/8);
		document.getElementById("no_of_days_" + count).value = calc_days;
	}
}
function get_no_calendar_days(mode,count)
{
	var days = document.getElementById("no_of_days_" + count).value;
	var no_of_people     = document.getElementById("no_of_people_" + count).value;
	var no_of_calender = (days/no_of_people);
	document.getElementById("no_of_calendar_" + count).value = no_of_calender;


}

</script>

  </body>

</html>
