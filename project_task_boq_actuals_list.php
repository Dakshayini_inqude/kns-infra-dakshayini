<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_task_actual_manpower_list.php
CREATED ON	: 05-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/

/* DEFINES - START */
define('PROJECT_TASK_PROJECT_BOQ_ACTUALS_FUNC_ID', '264');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list    = i_get_user_perms($user, '', PROJECT_TASK_PROJECT_BOQ_ACTUALS_FUNC_ID, '2', '1');
    $edit_perms_list    = i_get_user_perms($user, '', PROJECT_TASK_PROJECT_BOQ_ACTUALS_FUNC_ID, '3', '1');
    $delete_perms_list  = i_get_user_perms($user, '', PROJECT_TASK_PROJECT_BOQ_ACTUALS_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_PROJECT_BOQ_ACTUALS_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', PROJECT_TASK_PROJECT_BOQ_ACTUALS_FUNC_ID, '6', '1');

    $alert_type = -1;
    $alert = "";

    // Query String Data
    // Nothing

    $search_project = '';
    $search_vendor  = '';
    $search_contract_process  = '';
    $search_task    = '';
    $start_date     = '';
    $end_date	    = '';

    if (isset($_REQUEST["search_project"])) {
        $search_project = $_REQUEST["search_project"];
    }

    if (isset($_GET['process_id'])) {
        $process_id = $_GET['process_id'];
    } else {
        $process_id = "";
    }

    $search_work_type	= "";
    if (isset($_REQUEST["search_work_type"])) {
        $search_work_type   = $_REQUEST["search_work_type"];
    }

    if (isset($_GET['task_id'])) {
        $task_id = $_GET['task_id'];
        $display_status = "";
    } else {
        $task_id = "";
        $display_status ="pending";
    }

    if (isset($_POST['boq_search_submit'])) {
        $search_project = $_POST['search_project'];
        $search_vendor  = $_POST['search_vendor'];
        $search_contract_process    = $_POST['search_contract_process'];
        $search_task    = $_POST['search_task'];
        $start_date     = $_POST['dt_start_date'];
        $end_date	    = $_POST['dt_end_date'];
    }

    // Get Project Task BOQ modes already added
    $project_task_boq_actual_search_data = array("active"=>'1',"project"=>$search_project,"task_id"=>$task_id,"status"=>$display_status,"secondary_contract_status"=>"Checked","vendor_id"=>$search_vendor,"boq_start_date"=>$start_date,"boq_end_date"=>$end_date,"process_id"=>$process_id,"contract_task"=>$search_task,"process"=>$search_contract_process,"work_type"=>$search_work_type);
    $project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
    if ($project_task_boq_actual_list['status'] == SUCCESS) {
        $project_task_boq_actual_list_data = $project_task_boq_actual_list['data'];
    } else {
        $alert = $alert."Alert: ".$project_task_boq_actual_list["data"];
    }
    // Project data
    $project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list["status"] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_management_master_list["data"];
    }

    // Project Contract Rate data
    $project_contract_rate_master_search_data = array("active"=>'1',"vendor_id"=>$search_vendor,"process"=>$search_contract_process);
    $project_contract_rate_master_list = i_get_project_contract_rate_master($project_contract_rate_master_search_data);
    if ($project_contract_rate_master_list["status"] == SUCCESS) {
        $project_contract_rate_master_list_data = $project_contract_rate_master_list["data"];
    } else {
        $alert = $project_contract_rate_master_list["data"];
        $alert_type = 0;
    }

    // Get Project Contract Process modes already added
    $project_contract_process_search_data = array("active"=>'1');
    $project_contract_process_list = i_get_project_contract_process($project_contract_process_search_data);
    if ($project_contract_process_list['status'] == SUCCESS) {
        $project_contract_process_list_data = $project_contract_process_list["data"];
    } else {
        $alert = $project_contract_process_list["data"];
        $alert_type = 0;
    }

    // Contract Vendor data
    $project_manpower_agency_search_data = array("active"=>'1');
    $project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
    if ($project_manpower_agency_list['status'] == SUCCESS) {
        $project_manpower_agency_list_data = $project_manpower_agency_list['data'];
    } else {
    }
} else {
    header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Daily contract Work List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Daily contract Work List &nbsp;&nbsp;&nbsp;&nbsp; Total Amount: <span id="total_amount"><i>Calculating</i></span></h3>
            </div>
			<?php if ($view_perms_list['status'] == SUCCESS) {
    ?>
			<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_task_boq_actuals_list.php">

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
              for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) {
                  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if ($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) {
                      ?> selected="selected" <?php
                  } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
              } ?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_vendor">
			  <option value="">- - Select Vendor - -</option>
			  <?php
              for ($ven_count = 0; $ven_count < count($project_manpower_agency_list_data); $ven_count++) {
                  ?>
			  <option value="<?php echo $project_manpower_agency_list_data[$ven_count]["project_manpower_agency_id"]; ?>" <?php if ($search_vendor == $project_manpower_agency_list_data[$ven_count]["project_manpower_agency_id"]) {
                      ?> selected="selected" <?php
                  } ?>><?php echo $project_manpower_agency_list_data[$ven_count]["project_manpower_agency_name"]; ?></option>
			  <?php
              } ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_contract_process">
			  <option value="">- - Select Contract Process - -</option>
			  <?php
              for ($process_count = 0; $process_count < count($project_contract_process_list_data); $process_count++) {
                  ?>
			  <option value="<?php echo $project_contract_process_list_data[$process_count]["project_contract_process_id"]; ?>" <?php if ($search_contract_process == $project_contract_process_list_data[$process_count]["project_contract_process_id"]) {
                      ?> selected="selected" <?php
                  } ?>><?php echo $project_contract_process_list_data[$process_count]["project_contract_process_name"]; ?></option>
			  <?php
              } ?>
			  </select>
			  </span>

			   <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_task">
			  <option value="">- - Select Contract Task - -</option>
			  <?php
              for ($work_count = 0; $work_count < count($project_contract_rate_master_list_data); $work_count++) {
                  ?>
			  <option value="<?php echo $project_contract_rate_master_list_data[$work_count]["project_contract_rate_master_id"]; ?>" <?php if ($search_task == $project_contract_rate_master_list_data[$work_count]["project_contract_rate_master_id"]) {
                      ?> selected="selected" <?php
                  } ?>><?php echo $project_contract_rate_master_list_data[$work_count]["project_contract_rate_master_work_task"]; ?></option>
			  <?php
              } ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_work_type">
			  <option value="">- - Select Work Type - -</option>
			  <option value="Regular" <?php if ($search_work_type == "Regular") {
                  ?> selected="selected" <?php
              } ?>>- - Regular Work - -</option>
			  <option value="Rework" <?php if ($search_work_type == "Rework") {
                  ?> selected="selected" <?php
              } ?>>- - Rework - -</option>

			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_start_date" value="<?php echo $start_date; ?>" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_end_date" value="<?php echo $end_date; ?>" />
			  </span>
			  <input type="submit" name="boq_search_submit" />
			  </form>
			<?php
} else {
                echo 'You are not authorized to view this page';
            }
            ?>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php if ($view_perms_list['status'] == SUCCESS) {
                ?>

      <table class="table table-bordered">
        <thead>
          <tr>
				    <th class="sl-no">#</th>
				    <th class="project-heading">Project</th>
				    <th class="process-heading">Process</th>
						<th class="task-heading">Task</th>
						<th class="task-heading">Road <br/> Name</th>
						<th class="worktype-heading">Work Type</th>
						<th class="percent-heading">%</th>
						<th class="process-heading">Contract Process</th>
						<th class="task-heading">Contract Task</th>
						<th class="road-heading">Location</th>
						<th class="vendor-heading">Vendor</th>
						<th class="date-heading">Date</th>
						<th class="uom-heading">UOM</th>
						<th class="numeric-heading">Number</th>
						<th class="numeric-heading">Length</th>
						<th class="numeric-heading">Breadth</th>
						<th class="numeric-heading">Depth</th>
						<th class="numeric-heading">Total <br/>Planned Msmt</th>
						<th class="numeric-heading">Total <br/>Actual Msmt</th>
						<th class="numeric-heading">Today <br/>Measurement</th>
						<th class="numeric-heading">Total <br/>Pending Msmt</th>
						<th class="numeric-heading">Rate</th>
						<th class="numeric-heading">Total</th>
						<th class="remarks-heading">Remarks</th>
						<th class="name-heading">Added By</th>
						<th class="date-heading">Added On</th>
						<th colspan="4" style="text-align:center;">Actions</th>
				</tr>
				</thead>
				<tbody>
				<?php
                if ($project_task_boq_actual_list["status"] == SUCCESS) {
                    $sl_no = 0;
                    $total_amount = 0;
                    $actual_total_msmt = 0;
                    $pending_msmt = 0;
                    for ($count = 0; $count < count($project_task_boq_actual_list_data); $count++) {
                        $sl_no++;
                        $measurement = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_total_measurement"];
                        $numbers = $project_task_boq_actual_list_data[$count]["project_task_boq_actual_number"];
                        $rate = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_amount"];
                        /*if($numbers != 0)
                        {
                            $total = $measurement * $numbers * $rate;
                        }
                        else
                        {
                            $total = $measurement * $rate;
                        }*/

                        // Get Project Task BOQ modes already added
                        $project_task_actual_boq_search_data = array("active"=>'1',"task_id"=>$project_task_boq_actual_list_data[$count]["project_boq_actual_task_id"],"contract_task"=>$project_task_boq_actual_list_data[$count]["project_task_actual_contract_task"]);
                        $boq_sum_list = i_get_task_boq_sum($project_task_actual_boq_search_data);

                        if ($boq_sum_list['status'] == SUCCESS) {
                            $boq_sum_list_data = $boq_sum_list['data'];
                            $actual_total_msmt = $boq_sum_list_data[0]["total_msmt"];
                        } else {
                            $actual_total_msmt = 0;
                        }

                        $pending_msmt = $actual_total_msmt - $measurement;
                        $actual_total = $measurement * $rate;
                        $total = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_lumpsum"];

                        $total_amount = $total_amount + $total;
                        $project_task_boq_search_data = array("active"=>'1',"task_id"=>$project_task_boq_actual_list_data[$count]["project_boq_actual_task_id"],"location"=>$project_task_boq_actual_list_data[$count]["project_task_actual_boq_location"],"contract_task"=>$project_task_boq_actual_list_data[$count]["project_task_actual_contract_task"]);
                        $task_boq_list = i_get_project_task_boq($project_task_boq_search_data);
                        if ($task_boq_list["status"] == SUCCESS) {
                            $task_boq_list_data = $task_boq_list["data"];
                            $total_plan_measurement =  $task_boq_list_data[0]["project_task_boq_total_measurement"];
                        } else {
                            $total_plan_measurement = 0;
                        }

                        $work_type = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_work_type"];
                        if ($work_type == "Regular") {
                            $percentage =  $project_task_boq_actual_list_data[$count]["project_task_actual_boq_completion"];
                        } elseif ($work_type == "Rework") {
                            $percentage =  $project_task_boq_actual_list_data[$count]["project_task_actual_boq_rework_completion"];
                        } ?>
					<tr <?php if (round($total) != round($actual_total)) {
                            ?> style="color:red;" <?php
                        } ?>>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_master_name"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_process_master_name"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_master_name"]; ?></td>
					<td><?php  if($project_task_boq_actual_list_data[$count]["project_site_location_mapping_master_name"] == "")
           {
             echo "No Roads" ;
           } else {
             echo $project_task_boq_actual_list_data[$count]["project_site_location_mapping_master_name"] ;
          }?></td>
					<td><?php echo $work_type = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_work_type"]; ?></td>
					<td><?php
                    if ($work_type == "Regular") {
                        echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_completion"];
                    } elseif ($work_type == "Rework") {
                        echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_rework_completion"];
                    } ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_contract_process_name"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_contract_rate_master_work_task"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["location_name"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_manpower_agency_name"]; ?></td>
					<td><?php echo get_formatted_date($project_task_boq_actual_list_data[$count]["project_task_actual_boq_date"], "d-M-Y"); ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["stock_unit_name"]; ?></td>

					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_boq_actual_number"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_length"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_breadth"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_depth"]; ?></td>
					<td style="color:#219B4D;"><?php echo $total_plan_measurement; ?></td>
					<td style="color:blue;"><?php echo $actual_total_msmt; ?></td>
					<td style="color:#ff00ff;"><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_total_measurement"]; ?></td>
					<td style="color:red;"><?php echo $pending_msmt; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_amount"]; ?></td>
					<td><?php echo  round($total) ; ?></td>
					<td style="word-wrap:break-word;"><a href="#" onclick="alert('<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_remarks"]; ?>');"><?php echo substr($project_task_boq_actual_list_data[$count]["project_task_actual_boq_remarks"], 0, 30); ?></a></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["added_by"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y", strtotime($project_task_boq_actual_list_data[$count][
                    "project_task_actual_boq_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if ($edit_perms_list['status'] == SUCCESS) {
                        ?><a style="padding-right:10px" onclick="return go_to_project_edit_task_boq('<?php echo $project_task_boq_actual_list_data[$count]["project_task_boq_actual_id"]; ?>','<?php echo $project_task_boq_actual_list_data[$count]["project_boq_actual_task_id"]; ?>','<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_road_id"]; ?>');">Edit </a><?php
                    } ?></td>
					<td><?php if ($delete_perms_list['status'] == SUCCESS) {
                        if (($project_task_boq_actual_list_data[$count]["project_task_actual_boq_active"] == "1")) {
                            ?><a href="#" onclick="return project_delete_task_boq(<?php echo $project_task_boq_actual_list_data[$count]["project_task_boq_actual_id"]; ?>);">Delete</a><?php
                        }
                    } ?></td>
					<!--<td style="word-wrap:break-word;"><?php if (($project_task_boq_actual_list_data[$count]["project_task_actual_boq_status"] == "Pending") && ($ok_perms_list['status'] == SUCCESS)) {
                        ?><a href="#" onclick="return project_check_task_actual_contract('<?php echo $project_task_boq_actual_list_data[$count]["project_task_boq_actual_id"]; ?>','<?php echo $project_task_boq_actual_list_data[$count]["project_boq_actual_task_id"]; ?>','<?php echo $search_project ; ?>');">OK</a><?php
                    } elseif ($project_task_boq_actual_list_data[$count]["project_task_actual_boq_status"] == "Checked") {
                        ?> Checked <?php
                    } ?></td>-->

					<td style="word-wrap:break-word;"><?php if (($project_task_boq_actual_list_data[$count]["project_task_actual_boq_status"] == "Pending") && ($ok_perms_list['status'] == SUCCESS) && ($percentage > 0)) {
                        ?><a href="#" onclick="return project_check_task_actual_contract('<?php echo $project_task_boq_actual_list_data[$count]["project_task_boq_actual_id"]; ?>','<?php echo $project_task_boq_actual_list_data[$count]["project_boq_actual_task_id"]; ?>','<?php echo $search_project ; ?>');">OK</a><?php
                    } elseif (($percentage == 0)) {
                        ?>Enter completion % <?php
                    } elseif (($project_task_boq_actual_list_data[$count]["project_task_actual_boq_status"] == "Checked")) {
                        ?>Checked<?php
                    } ?></td>

					<td style="word-wrap:break-word;"><?php if (($project_task_boq_actual_list_data[$count]["project_task_actual_boq_status"] == "Checked") && ($approve_perms_list['status'] == SUCCESS) && ($percentage > 0)) {
                        ?><a href="#" onclick="return project_approve_task_actual_contract('<?php echo $project_task_boq_actual_list_data[$count]["project_task_boq_actual_id"]; ?>','<?php echo $project_task_boq_actual_list_data[$count]["project_boq_actual_task_id"]; ?>','<?php echo $search_project ; ?>');">Approve</a><?php
                    } ?></td>
					</tr>
					<?php
                    }
                } else {
                    ?>
				<td colspan="6">No Project Master condition added yet!</td>

				<?php
                } ?>

                </tbody>
              </table>   <?php
            }
            ?>
			   <script>
			  document.getElementById('total_amount').innerHTML = '<?php echo round($total_amount); ?>';
			   </script>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_task_boq(boq_actual_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_task_boq_actuals_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_task_boq_actuals.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("boq_actual_id=" + boq_actual_id + "&action=0");
		}
	}
}
function go_to_project_edit_task_boq(boq_actual_id,task_id, road_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_task_boq_actuals.php");
    form.setAttribute("target", "blank");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","boq_actual_id");
	hiddenField1.setAttribute("value",boq_actual_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","task_id");
	hiddenField2.setAttribute("value",task_id);

	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","road_id");
	hiddenField3.setAttribute("value",road_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);

	document.body.appendChild(form);
    form.submit();
}

function project_check_task_actual_contract(boq_id,task_id,search_project,search_vendor)
{
	var ok = confirm("Are you sure you want to Ok?")
	{
		if (ok)
		{
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_task_boq_actuals_list.php?search_project=" +search_project;
					}
				}
			}

			xmlhttp.open("POST", "project_check_contract_work.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("boq_id=" + boq_id + "&task_id=" +task_id+ "&action=approved");
		}
	}
}
function project_approve_task_actual_contract(boq_id,task_id,search_project,search_vendor)
{
	var ok = confirm("Are you sure you want to Approve?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					  window.location = "project_task_boq_actuals_list.php?search_project=" +search_project;
					}
				}
			}

			xmlhttp.open("POST", "project_approve_contract_work.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("boq_id=" + boq_id + "&task_id=" +task_id+ "&action=approved");
		}
	}
}
</script>

  </body>

</html>
