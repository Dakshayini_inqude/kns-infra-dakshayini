<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 16th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_POST["schedule_id"]))
	{
		$schedule_id = $_POST["schedule_id"];
	}
	else
	{
		$schedule_id = "-1";
	}
	if(isset($_POST["booking_id"]))
	{
		$booking_id = $_POST["booking_id"];
	}
	else
	{
		$booking_id = "-1";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["edit_payment_schedule_submit"]))
	{
		$schedule_id = $_POST["hd_schedule_id"];
		$booking_id  = $_POST["hd_booking_id"];
		$pay_date    = $_POST["dt_pay_date"];
		$amount      = $_POST["num_amount"];
		$milestone   = $_POST["ddl_milestone"];
		
		$old_date    = $_POST["hd_old_date"];
		$old_amount  = $_POST["hd_old_amount"];
		
		// Check for mandatory fields
		if(($schedule_id != "") && ($pay_date != "") && ($amount != "") && ($milestone != ""))
		{
			$schedule_data = array('date'=>$pay_date,'amount'=>$amount,'milestone'=>$milestone);
			$old_schedule_data = array('old_date'=>$old_date,'old_amount'=>$old_amount,'booking'=>$booking_id);
			$edit_pay_schedule_result = i_edit_payment_schedule($schedule_id,$schedule_data,$old_schedule_data,$user);
			
			if($edit_pay_schedule_result["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert      = $edit_pay_schedule_result["data"];
				
				header('location:crm_view_payment_schedule.php?booking='.$booking_id);
			}
			else
			{
				$alert_type = 0;
				$alert      = $edit_pay_schedule_result["data"];
			}						
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get the customer details as captured already
	$cust_details = i_get_cust_profile_list('',$booking_id,'','','','','','','','');
	if($cust_details["status"] == SUCCESS)
	{
		$cust_details_list = $cust_details["data"];
	}
	else
	{
		$alert = $alert.' '.$cust_details["data"];
		$alert_type = $alert_type || 0;
	}
	
	// Get booking details
	$booking_list = i_get_site_booking($booking_id,'','','','','','','','','','','');
	if($booking_list["status"] == SUCCESS)
	{
		$booking_list_data = $booking_list["data"];
	}
	else
	{
		$alert = $alert." ".$booking_list["data"];
		$alert_type = $alert_type || 0;
	}
	
	// Get already entered payment schedule details
	$already_entered_schedule_amount = 0;
	$pay_schedule_list = i_get_pay_schedule('','','','','','',$schedule_id);
	if($pay_schedule_list["status"] == SUCCESS)
	{
		$pay_schedule_list_data = $pay_schedule_list['data'];
	}
	else
	{
		header('location:crm_view_payment_schedule.php');
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Payment Schedule</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
						<?php 
						if($cust_details["status"] == SUCCESS)
						{
						?>
						<h3>Project: <?php echo $cust_details_list[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;Site No: <?php echo $cust_details_list[0]["crm_site_no"]; ?>&nbsp;&nbsp;&nbsp;Customer: <?php echo $cust_details_list[0]["crm_customer_name_one"]." (".$cust_details_list[0]["crm_customer_contact_no_one"].")"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Site Cost: <?php echo ($cust_details_list[0]["crm_booking_consideration_area"]*$cust_details_list[0]["crm_booking_rate_per_sq_ft"]); ?>&nbsp;&nbsp;&nbsp;Already Planned: <?php echo $already_entered_schedule_amount; ?></h3>
						<?php
						}
						else
						{
							?>
	      				<h3>Project: <?php echo $booking_list_data[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;Site No: <?php echo $booking_list_data[0]["crm_site_no"]; ?>&nbsp;&nbsp;&nbsp;Customer: <?php echo $booking_list_data[0]["name"]." (".$booking_list_data[0]["cell"].")"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Site Cost: <?php echo ($booking_list_data[0]["crm_booking_consideration_area"]*$booking_list_data[0]["crm_booking_rate_per_sq_ft"]); ?>&nbsp;&nbsp;&nbsp;Already Planned: <?php echo $already_entered_schedule_amount; ?></h3>
						<?php
						}?>			

						<span style="float:right; padding-right:10px;"><a href="crm_customer_transactions.php?booking=<?php echo $booking_list_data[0]["crm_booking_id"]; ?>">Back to customer profile</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Payment Schedule</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>								
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="edit_pay_schedule" class="form-horizontal" method="post" action="crm_edit_payment_schedule.php">
								<input type="hidden" name="hd_schedule_id" value="<?php echo $schedule_id; ?>" />	
								<input type="hidden" name="hd_booking_id" value="<?php echo $booking_id; ?>" />	
								<input type="hidden" name="hd_old_date" value="<?php echo $pay_schedule_list_data[0]['crm_payment_schedule_date']; ?>" />	
								<input type="hidden" name="hd_old_amount" value="<?php echo $pay_schedule_list_data[0]['crm_payment_schedule_amount']; ?>" />	
									<fieldset>										
																														
										<div class="control-group">											
											<label class="control-label" for="dt_pay_date">Date*</label>
											<div class="controls">
												<input type="date" name="dt_pay_date" class="span6" value="<?php echo $pay_schedule_list_data[0]['crm_payment_schedule_date']; ?>" required />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="num_amount">Amount*</label>
											<div class="controls">
												<input type="number" name="num_amount" min="1" step="0.01" class="span6" value="<?php echo $pay_schedule_list_data[0]['crm_payment_schedule_amount']; ?>" required />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->	
										<div class="control-group">											
											<label class="control-label" for="ddl_milestone">Milestone*</label>
											<div class="controls">
												<select name="ddl_milestone" required>
												<option value="BOOKING" <?php if($pay_schedule_list_data[0]['crm_payment_schedule_milestone'] == 'BOOKING'){ ?> selected <?php } ?>>BOOKING</option>
												<option value="AGREEMENT" <?php if($pay_schedule_list_data[0]['crm_payment_schedule_milestone'] == 'AGREEMENT'){ ?> selected <?php } ?>>AGREEMENT</option>
												<option value="REGISTRATION" <?php if($pay_schedule_list_data[0]['crm_payment_schedule_milestone'] == 'REGISTRATION'){ ?> selected <?php } ?>>REGISTRATION</option>
												<option value="KHATHA TRANSFER" <?php if($pay_schedule_list_data[0]['crm_payment_schedule_milestone'] == 'KHATHA TRANSFER'){ ?> selected <?php } ?>>KHATHA TRANSFER</option>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																														
                                      <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_payment_schedule_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
