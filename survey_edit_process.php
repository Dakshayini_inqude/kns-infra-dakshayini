<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 05-Jan-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* DEFINES - START */
define('SURVEY_EDIT_PROCESS_FUNC_ID','');
/* DEFINES - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$delete_perms_list = i_get_user_perms($user,'',SURVEY_EDIT_PROCESS_FUNC_ID,'4','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET['survey_id']))
	{
		$survey_id = $_GET['survey_id'];
	}
	else
	{
		$survey_id = "";
	}
	
	if(isset($_GET["survey_process_id"]))
	{
		$survey_process_id = $_GET["survey_process_id"];
	}
	else
	{
		$survey_process_id = "";
	}

	// Capture the form data
	if(isset($_POST["edit_survey_process_submit"]))
	{
		$survey_process_id        = $_POST["hd_survey_process_id"];
		$survey_id                = $_POST["hd_survey_id"];
		$process_id               = $_POST["ddl_process_id"];
		$process_start_date       = $_POST["process_start_date"];
		$process_end_date         = $_POST["process_end_date"];
		
		// Check for mandatory fields
		if(($process_start_date != "") && ($process_end_date != ""))
		{
			$survey_process_update_data = array("process_id"=>$process_id,"process_start_date"=>$process_start_date,"process_end_date"=>$process_end_date);
			$survey_process_iresult = i_update_survey_process($survey_id,$survey_process_update_data);
			
			if($survey_process_iresult["status"] == SUCCESS)				
			{	
				$alert_type = 1;
		    
				header("location:survey_process_list.php?survey_id=$survey_process_id");
			}
			else
			{
				$alert 		= $survey_process_iresult["data"];
				$alert_type = 0;	
			}						
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
    // Get Survey Details modes already added
	$survey_details_search_data = array("active"=>'1');
	$survey_details_list = i_get_survey_details($survey_details_search_data);
	if($survey_details_list["status"] == SUCCESS)
	{
		$survey_details_list_data = $survey_details_list["data"];
	}
	else
	{
		$alert = $survey_details_list["data"];
		$alert_type = 0;
	}
	
	// Get Survey Process Master modes already added
	$survey_process_master_search_data = array("active"=>'1');
	$survey_process_master_list = i_get_survey_process_master($survey_process_master_search_data);
	if($survey_process_master_list['status'] == SUCCESS)
	{
		$survey_process_master_list_data = $survey_process_master_list['data'];
	}	
	else
	{
		$alert = $survey_process_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Survey Process modes already added
	$survey_process_search_data = array("active"=>'1',"survey_process_id"=>$survey_id);
	$survey_process_list = i_get_survey_process($survey_process_search_data);
	if($survey_process_list['status'] == SUCCESS)
	{
		$survey_process_list_data = $survey_process_list['data'];
	}	
}
else
{
	header("location:login.php");
}	

?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Survey - Edit Process</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Survey - Edit Process</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Survey Edit Process</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="survey_edit_process_form" class="form-horizontal" method="post" action="survey_edit_process.php">
								<input type="hidden" name="hd_survey_id" value="<?php echo $survey_id; ?>" />
								<input type="hidden" name="hd_survey_process_id" value="<?php echo $survey_process_id; ?>" />
									<fieldset>										
										
										<div class="control-group">											
											<label class="control-label" for="ddl_process_id">Process*</label>
											<div class="controls">
												<select class="span6" name="ddl_process_id" disabled required>
												<option value="">- - Select Process - -</option>
												<?php
												for($count = 0; $count < count($survey_process_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $survey_process_master_list_data[$count]["survey_process_master_id"]; ?>" <?php if($survey_process_master_list_data[$count]["survey_process_master_id"] == $survey_process_list_data[0]["process_id"]){ ?> selected="selected" <?php } ?>><?php echo $survey_process_master_list_data[$count]["survey_process_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="process_start_date">Start Date*</label>
											<div class="controls">
											<?php if($survey_process_list_data[0]["survey_process_start_date"] != '0000-00-00')
											{ 
											    if($delete_perms_list['status'] == SUCCESS)
												{
													$disabled = '';
												}
												else
												{
													$disabled = ' disabled';
												}
											}
											else
											{
												$disabled = '';
											}
											?>
											<input type="date" class="span6" name="process_start_date" placeholder="Date" value="<?php echo $survey_process_list_data[0]["survey_process_start_date"]; ?>"<?php echo $disabled; ?>>											
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="process_end_date">End Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="process_end_date" placeholder="Date" value="<?php echo $survey_process_list_data[0]["survey_process_end_date"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_survey_process_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
