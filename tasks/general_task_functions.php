<?php
/**
 * @author Nitin kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_general_tasks.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');

/*
PURPOSE : To add general task plan
INPUT 	: Type, Assigned User, Details, Department, Planned End Date, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_task_plan_general($type,$user,$details,$department,$project,$planned_end_date,$added_by)
{
    $task_plan_general_iresult = db_add_gen_task_plan($type,$user,$details,$department,$project,$planned_end_date,$added_by);

	if($task_plan_general_iresult['status'] == SUCCESS)
	{
		$return["data"]   = $task_plan_general_iresult["data"];
		$return["status"] = SUCCESS;

		/* SEND EMAIL - START */
		// Get user details
		$user_sresult = i_get_user_list($user,'','','','','');
		if($user_sresult['status'] == SUCCESS)
		{
			$to = $user_sresult['data'][0]['user_email_id'];
			$name = $user_sresult['data'][0]['user_name'];

			$adder_sresult = i_get_user_list($added_by,'','','','','');
			if($adder_sresult['status'] == SUCCESS)
			{
				$assigned_by    = $adder_sresult['data'][0]['user_name'];
				$assigned_email = $adder_sresult['data'][0]['user_email_id'];
			}
			else
			{
				$assigned_by    = '';
				$assigned_email = '';
			}

			// Prepare content
			// Get Project Details
			$project_management_master_search_data = array("project_id"=>$project, "user_id"=>$user);
			$t_proj_sresult = i_get_project_management_master_list($project_management_master_search_data);

			if($t_proj_sresult['status'] == SUCCESS)
			{
				$project_name = $t_proj_sresult['data'][0]['project_master_name'];
			}
			else
			{
				$project_name = '';
			}

			$subject = 'Task Assigned By '.$assigned_by.'. Deadline: '.date('d-M-Y',strtotime($planned_end_date));
			$message = 'Dear '.$name.',<br /><br />You have been assigned a task by '.$assigned_by.'. The details are as follows:<br /><br />Task Details: '.$details.'<br /><br/>Planned End Date: '.date('d-M-Y',strtotime($planned_end_date)).'<br /><br/>Project Name: '.$project_name.'<br /><br/>From: '.$assigned_by.'<br /><br/>To: '.$name;
			$attachment = '';

			$cc = array();
			$cc_count = 0;
			$assigner_perms = i_get_user_email_perms('','','1','2','1');

			if($assigner_perms['status'] == SUCCESS)
			{
				if((!(in_array($assigned_email,$cc))) && ($assigned_email != $to))
				{
					$cc[$cc_count] = $assigned_email;
					$cc_count++;
				}
			}
			$manager_perms = i_get_user_email_perms('','','1','3','1');
			if($manager_perms['status'] == SUCCESS)
			{
				$manager_sresult = i_get_user_list($user_sresult['data'][0]['user_manager'],'','','','1','');
				if((!(in_array($manager_sresult['data'][0]['user_email_id'],$cc))) && ($manager_sresult['data'][0]['user_email_id'] != $to))
				{
					$cc[$cc_count] = $manager_sresult['data'][0]['user_email_id'];
					$cc_count++;
				}
			}
			$admin_perms = i_get_user_email_perms('','','1','4','1');
			if($admin_perms['status'] == SUCCESS)
			{
				for($ucount = 0; $ucount < count($admin_perms['data']); $ucount++)
				{
					$admin_sresult = i_get_user_list($admin_perms['data'][$ucount]['permission_user'],'','','','1','');
					if((!(in_array($admin_sresult['data'][0]['user_email_id'],$cc))) && ($admin_sresult['data'][0]['user_email_id'] != $to))
					{
						$cc[$cc_count] = $admin_sresult['data'][0]['user_email_id'];
						$cc_count++;
					}
				}
			}

			send_sendgrid_email($to,$name,$cc,$subject,$message,$attachment);
		}
		/* SEND EMAIL - END */
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}

	return $return;
}

/*
PURPOSE : To add general task type
INPUT 	: Task Type, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_gen_task_type($task_type,$added_by)
{
	$active = '1';
	$existing_gen_task_list = db_get_gen_task_type_list($task_type,$active,'','','');

	if($existing_gen_task_list["status"] == DB_NO_RECORD)
	{
		$task_type_general_iresult = db_add_task_type_general($task_type,$added_by);

		if($task_type_general_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "General task type successfully added";
			$return["status"] = SUCCESS;
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "The task type $task_type already exists";
		$return["status"] = FAILURE;
	}

	return $return;
}

/*
PURPOSE : To add department
INPUT 	: Department name, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_department($department_name,$added_by)
{
	$existing_department_list = db_get_gen_task_department_list($department_name,'','','','');

	if($existing_department_list["status"] == DB_NO_RECORD)
	{
		$department_iresult = db_add_department($department_name,$added_by);

		if($department_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Department successfully added";
			$return["status"] = SUCCESS;
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "The department $department_name already exists";
		$return["status"] = FAILURE;
	}

	return $return;
}

/*
PURPOSE : To get general task type list
INPUT 	: Task Type Name, Active
OUTPUT 	: Task Type List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_gen_task_type_list($task_type_name,$active)
{
	$gen_task_type_sresult = db_get_gen_task_type_list($task_type_name,$active,'','','');

	if($gen_task_type_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $gen_task_type_sresult["data"];
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No process type added. Please contact the system admin";
    }

	return $return;
}

/*
PURPOSE : To get task list
INPUT 	: Task ID, Task Type, Process Plan ID, Planned End Date, Actual End Date, Applicable, Added By, Order
OUTPUT 	: Task List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_gen_task_plan_list($task_id,$task_type,$task_user,$task_department,$planned_end_date,$actual_start_date,$actual_end_date,$added_by,$status,$assigner_or_assignee='',$status_exclude='',$project='')
{
	$gen_task_plan_sresult = db_get_gen_task_plan_list($task_id,$task_type,$task_user,$task_department,$planned_end_date,$actual_start_date,$actual_end_date,$added_by,'','',$status,$assigner_or_assignee,$status_exclude,$project);

	if($gen_task_plan_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $gen_task_plan_sresult["data"];
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No task plan added!";
    }

	return $return;
}

/*
PURPOSE : To get department list
INPUT 	: Department Name, Active, Added By, Start Date, End Date
OUTPUT 	: Department List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_department_list($department_name,$active)
{
	$department_sresult = db_get_gen_task_department_list($department_name,$active,'','','');

	if($department_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $department_sresult["data"];
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No department added!";
    }

	return $return;
}

/*
PURPOSE : To update task status
INPUT 	: Task Plan ID, Planned End Date, Start Date, End Date, Status
OUTPUT 	: success or failure message
BY 		: Nitin Kashyap
*/
function i_update_gen_task_plan($task_plan_id,$planned_end_date,$start_date,$end_date,$status)
{
	$gen_task_plan_uresult = db_update_gen_task_plan($task_plan_id,$planned_end_date,$start_date,$end_date,$status);

	if($gen_task_plan_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = "Task successfully updated!";
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No task plan added!";
    }

	return $return;
}

/*
PURPOSE : To add remarks
INPUT 	: Task, Remarks, File, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_remarks($task_id,$remarks,$remarks_file,$added_by,$subtask)
{
    $remarks_iresult = db_add_gen_task_remarks($task_id,$remarks,$remarks_file,$added_by,$subtask);

	if($remarks_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Remarks added";
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}

	return $return;
}

/*
PURPOSE : To update assignee
INPUT 	: Task, Assignee
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_update_assignee($task_id,$assigned_to)
{
    $assignee_uresult = db_gen_update_assignee($task_id,$assigned_to);

	if($assignee_uresult['status'] == SUCCESS)
	{
		$return["data"]   = "Assignee updated";
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}

	return $return;
}

/*
PURPOSE : To get remarks
INPUT 	: Task
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_remarks($task_id)
{
    $remarks_sresult = db_get_gen_task_remarks($task_id);

	if($remarks_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
	{
		$return["data"]   = $remarks_sresult["data"];
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "No remarks added to this task";
		$return["status"] = FAILURE;
	}

	return $return;
}

/*
PURPOSE : To update task as deleted
INPUT 	: Task Plan ID
OUTPUT 	: success or failure message
BY 		: Nitin Kashyap
*/
function i_delete_gen_task_plan($task_plan_id)
{
	$gen_task_plan_delete_uresult = db_delete_gen_task_plan($task_plan_id);

	if($gen_task_plan_delete_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = "Task Deleted!";
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No task plan added!";
    }

	return $return;
}

/*
PURPOSE : To update task assignee
INPUT 	: Task Plan ID, Assignee
OUTPUT 	: success or failure message
BY 		: Nitin Kashyap
*/
function i_update_assigne_gen_task_plan($task_plan_id,$assigned_to)
{
	$gen_task_plan_assignee_uresult = db_gen_update_assignee($task_plan_id,$assigned_to);

	if($gen_task_plan_assignee_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = "Task Assigned!";
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No task plan added!";
    }

	return $return;
}

/*
PURPOSE : To update task details
INPUT 	: Task Plan ID, Task Type, Task Details, Start Date, End Date, Assignee, Department, Start Date, End Date, Assignee
OUTPUT 	: success or failure message
BY 		: Nitin Kashyap
*/
function i_edit_task_plan_general($task_id,$gen_task_type,$gen_task_details,$gen_task_start_date,$gen_task_end_date,$gen_task_assignee,$department,$status,$project="")
{
	$gen_task_plan_uresult = db_update_gen_task_details($task_id,$gen_task_type,$gen_task_details,$gen_task_start_date,$gen_task_end_date,$gen_task_assignee,$department,$status,$project);

	if($gen_task_plan_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = "Task Details Updated!";
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No task plan added!";
    }

	return $return;
}

/*
PURPOSE : To get task status
INPUT 	: Task Status Name
OUTPUT 	: success or failure message
BY 		: Nitin Kashyap
*/
function i_get_task_status_name($status)
{
	switch($status)
	{
	case "0":
	$return = "Not Started";
	break;

	case "1":
	$return = "Just Started";
	break;

	case "2":
	$return = "In Progress";
	break;

	case "3":
	$return = "Waiting";
	break;

	case "4":
	$return = "Complete";
	break;

	case "5":
	$return = "Deferred";
	break;

	default:
	$return = "";
	break;
	}

	return $return;
}
/*
PURPOSE : To update task remarks status
INPUT 	: Task ID,Task Status
OUTPUT 	: success or failure message
BY 		: Nitin Kashyap
*/
function i_update_gen_task_remarks($general_task_remarks_id,$gen_task_remrks_status)
{
	$gen_task_remarks_uresult = db_update_general_remarks($gen_task_remrks_status,$general_task_remarks_id);

	if($gen_task_remarks_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = "Task Status successfully updated!";
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No task Status added!";
    }

	return $return;
}
?>
