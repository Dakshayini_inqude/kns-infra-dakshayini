<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: bd_file_list.php
CREATED ON	: 11-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of BD files
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["msg"]))
	{
		$msg = $_GET["msg"];
	}
	else
	{
		$msg = "";
	}
	// Nothing here

	// Temp data
	$alert = "";
	
	$file_id        = "";	
	$survey_no      = "";	
	$owner_status   = "";		

	// Search parameters
	if(isset($_POST["bd_file_search_submit"]))
	{		
		$file_id      = $_POST["stxt_file_id"];
		$survey_no    = $_POST["stxt_survey_no"];		
		$owner_status = $_POST["ddl_owner_status"];
	}
	
	// Get list of files
	$bd_file_list = i_get_bd_files_list($file_id,'',$survey_no,'','',$owner_status,'','0');

	if($bd_file_list["status"] == SUCCESS)
	{
		$bd_file_list_data = $bd_file_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_file_list["data"];
	}	
	
	// Get list of villages
	$village_list = i_get_village_list('');

	if($village_list["status"] == SUCCESS)
	{
		$village_list_data = $village_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$village_list["data"];
	}	
	
	// Get list of owner status
	$owner_status_list = i_get_owner_status_list('');
	if($owner_status_list["status"] == SUCCESS)
	{
		$owner_status_list_data = $owner_status_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$owner_status_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>BD File List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>BD Open File List&nbsp;&nbsp;&nbsp;Total Extent: <span id="total_extent_span"><i>Calculating</i></span> guntas</h3>			  
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="file_search_form" action="bd_orphan_file_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="text" name="stxt_file_id" value="<?php echo $file_id; ?>" placeholder="Search by file number" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="text" name="stxt_survey_no" value="<?php echo $survey_no; ?>" placeholder="Search by survey number" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_owner_status">
			  <option value="">- - Select Land Status - -</option>
			  <?php for($count = 0; $count < count($owner_status_list_data); $count++)
			  {?>
			  <option value="<?php echo $owner_status_list_data[$count]["bd_file_owner_status_id"]; ?>" <?php if($owner_status_list_data[$count]["bd_file_owner_status_id"] == $owner_status){ ?> selected="selected" <?php } ?>><?php echo $owner_status_list_data[$count]["bd_file_owner_status_name"]; ?></option>
			  <?php
			  }?>
			  </select>
			  </span>
			  <input type="submit" name="bd_file_search_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php echo $msg; ?>
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>				   
					<th>File id</th>
				    <th>Survey No</th>
				    <th>Survey Alias</th>
				    <th>Survey Alias Date</th>
					<th>Previous Project</th>
					<th>Land Owner</th>
					<th>Land Owner No</th>					
					<th>Village</th>						
					<th>Extent</th>
					<th>Land Status</th>					
					<th>Land Cost</th>					
					<th>Process Status</th>	
					<th>Account</th>	
					<th>JDA %</th>					
					<th>Added By</th>
					<th>&nbsp;</th>	
					<th>&nbsp;</th>						
				</tr>
				</thead>
				<tbody>
				<?php
				$total_extent = 0;
				if($bd_file_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($bd_file_list_data); $count++)
					{														
						// Corresponding Legal File Details
						$legal_file_sresult = i_get_file_list('',$bd_file_list_data[$count]["bd_project_file_id"],'','','','','','','','','','');
						if($legal_file_sresult['status'] == SUCCESS)
						{
							$is_legal_file = true;
							$legal_file_no = $legal_file_sresult['data'][0]['file_number']; 
							$legal_file_id = $legal_file_sresult['data'][0]['file_id']; 
						}
						else
						{
							$is_legal_file = false;
							$legal_file_no = 'NO LEGAL FILE';
							$legal_file_id = '';
						}
						
						$file_handover_data = i_get_file_handover_details($legal_file_id);
						if($file_handover_data['status'] != SUCCESS)
						{
							$total_extent = $total_extent + $bd_file_list_data[$count]["bd_file_extent"];
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_project_file_id"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_survey_no"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_survey_sale_deed"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_survey_sale_deed_date"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_project_name"]; ?></td>						
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_owner"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_owner_phone_no"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["village_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_extent"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_owner_status_name"]; ?><?php if($role == '1'){ ?><br /><a href="bd_update_land_status.php?file=<?php echo $bd_file_list_data[$count]["bd_project_file_id"]; ?>">Update</a><?php } ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_land_cost"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["process_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_own_account_master_account_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_project_file_jda_share_percent"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["user_name"]; ?></td>
						<td style="word-wrap:break-word;"><a href="bd_file_details.php?file=<?php echo $bd_file_list_data[$count]["bd_project_file_id"]; ?>">Details</a></td>
						<td style="word-wrap:break-word;"><?php if($is_legal_file != false) { ?> <br /><br /><a href="print_lawyer_report.php?file=<?php echo $bd_file_list_data[$count]["bd_project_file_id"]; ?>" target="_blank">Lawyer Report</a> <?php } ?></td>						
					</tr>
					<?php
						}
					}
				}
				else
				{
				   ?>
				   <td colspan="15">No inactive file</td>
				   <?php
				}
				 ?>	
				 <script>
				 document.getElementById('total_extent_span').innerHTML = <?php echo $total_extent; ?>;
				 </script>

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function confirm_deletion(bd_file)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "bd_orphan_file_list.php";
				}
			}

			xmlhttp.open("GET", "bd_file_delete.php?file=" + bd_file);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}
</script>

  </body>

</html>
