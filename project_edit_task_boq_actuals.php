<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	if(isset($_GET['boq_actual_id']))
	{
		$boq_id = $_GET['boq_actual_id'];
	}
	else
	{
		$boq_id = $_POST["hd_boq_actual_id"];
	}

	if(isset($_REQUEST["task_id"]))
	{
		$task_id = $_REQUEST["task_id"];
	}
	else
	{
		$task_id = $_POST["hd_task_id"];
	}
	if(isset($_REQUEST["road_id"]))
	{
		$road_id = $_REQUEST["road_id"];
	}
	else
	{
		$road_id = $_POST["hd_road_id"];
	}

	// Temp data for header
	$project_process_task_search_data = array("task_id"=>$task_id);
	$project_plan_process_task_list = i_get_project_process_task($project_process_task_search_data);
	if ($project_plan_process_task_list["status"] == SUCCESS) {
			$project_plan_process_task_list_data = $project_plan_process_task_list["data"];
			$task_name = $project_plan_process_task_list_data[0]["project_task_master_name"];
			$project = $project_plan_process_task_list_data[0]["project_master_name"];
			$project_id = $project_plan_process_task_list_data[0]["project_management_master_id"];
			$process_name= $project_plan_process_task_list_data[0]["project_process_master_name"];
	} else {
			$alert = $project_plan_process_task_list["data"];
			$alert_type = 0;
			$task_name = "";
			$project = "";
			$process = "";
			$project_id = "";
	}

	$get_details_data = i_get_details("Regular", $task_id, $road_id);

	$road_name =  $get_details_data["road"];
	$uom = $get_details_data['uom'];
	$planned_msmrt = $get_details_data["planned_msmrt"];
	var_dump($planned_msmrt);

	$reg_mp_msmrt = $get_details_data["mp_msmrt"];
	$reg_mc_msmrt = $get_details_data["mc_msmrt"];
	$reg_cw_msmrt = $get_details_data["cw_msmrt"];
	$reg_overall_msmrt = $get_details_data["overall_msmrt"];

	$get_rework_data = i_get_details("Rework", $task_id, $road_id);
	$rework_mp_msmrt = $get_rework_data["mp_msmrt"];
	$rework_mc_msmrt = $get_rework_data["mc_msmrt"];
	$rework_cw_msmrt = $get_rework_data["cw_msmrt"];
	$rework_overall_msmrt = $get_rework_data["overall_msmrt"];

	// $new_completion_percent = $planned_msmrt * $reg_overall_msmrt / 100;
	$new_completion_percent = ($reg_overall_msmrt / $planned_msmrt) * 100;

	// Get Project Task BOQ modes already added
	$project_task_boq_actual_search_data = array("active"=>'1',"boq_id"=>$boq_id);
	$project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
	if($project_task_boq_actual_list['status'] == SUCCESS)
	{
		$project_task_boq_actual_list_data = $project_task_boq_actual_list['data'];
		$work_type = $project_task_boq_actual_list_data[0]["project_task_actual_boq_work_type"];
		$process_master_id = $project_task_boq_actual_list_data[0]["project_process_master_id"];
	}

	// Get Project manpower_agency Master modes already added
	$project_manpower_agency_search_data = array("active"=>'1');
	$project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
	if($project_manpower_agency_list['status'] == SUCCESS)
	{
		$project_manpower_agency_list_data = $project_manpower_agency_list['data'];
	}

	 else
	{
		$alert = $project_manpower_agency_list["data"];
		$alert_type = 0;
	}
	// Capture the form data
	if(isset($_POST["edit_project_task_boq_actual_submit"]))
	{
		$boq_id	     	   		= $_POST["hd_boq_actual_id"];
		$work_type	     		= $_POST["hd_work_type"];
		$task_id	       		= $_POST["hd_task_id"];
		$road_id	       		= $_POST["hd_road_id"];
		$number          		= $_POST["number"];
		$length    		   		= $_POST["length"];
		$breadth   		   		= $_POST["breadth"];
		$depth    		   		= $_POST["depth"];
		$total_sqft		   		= $_POST["total_sqft"];
		// $completion_percent	= $_POST["completion_percent"];
		$completion_percent	= $_POST["hd_completion_percent"];
		$amount			   			= $_POST["amount"];
		$remarks 	       		= $_POST["txt_remarks"];

		// var_dump($_POST);

		if($completion_percent == 100)
		{
			$actual_end_date = date("Y-m-d H:i:s");
		}
		else
		{
			$actual_end_date = "0000-00-00";
		}

		//Get boq
		$project_task_actual_boq_search_data = array("active"=>'1',"boq_id"=>$boq_id,"work_type"=>"Regular");
		$contract_list = i_get_project_task_boq_actual($project_task_actual_boq_search_data);
		if($contract_list["status"] == SUCCESS)
		{
			$percentage = $contract_list["data"][0]["project_task_actual_boq_completion"];
		}
		else
		{
			$percentage = "";
		}

		$project_process_task_search_data = array("active"=>'1',"task_id"=>$task_id);
		$project_task_list = i_get_project_process_task($project_process_task_search_data);
		if($project_task_list["status"] == SUCCESS)
		{
			$project_task_list_data = $project_task_list["data"];
			$task_manpower_completion = $project_task_list_data[0]["project_process_task_completion"];
			$task_machine_completion = $project_task_list_data[0]["project_process_machine_task_completion"];
			$task_contract_completion = $project_task_list_data[0]["project_process_contract_task_completion"];

			$task_name = $project_task_list_data[0]["project_task_master_name"];
			$project = $project_task_list_data[0]["project_master_name"];
			$project_id = $project_task_list_data[0]["project_management_master_id"];
			$process_name= $project_task_list_data[0]["project_process_master_name"];
		}
		else
		{
			$task_manpower_completion = 0;
			$task_machine_completion = 0;
			$task_contract_completion = 0;
			$task_name = "";
			$project = "";
			$process = "";
			$project_id = "";
		}

		$allow_edit = false;
		if(($process_master_id == 32) || ($process_master_id == 33))
		{
			if(($completion_percent > 0) && ($completion_percent <=99 ))
			{
				$allow_edit = true;
			}
		}
		else
		{
			if($work_type == "Regular")
			{
				if(($boq_id != "") && ($completion_percent >0)  && ($completion_percent <= 100 ))
					{
						$allow_edit = true;
					}
				}
			elseif($work_type == "Rework")
			{
				if(($boq_id != "") && ($completion_percent >0) && ($completion_percent <= 100 ))
				{
					$allow_edit = true;
				}

			}
			else
			{
				$allow_edit = false;
			}
		}

		// Check for mandatory fields
		if($allow_edit == true)
		{

			if($work_type == "Regular")
			{
				$project_task_boq_actual_update_data = array("number"=>$number,"completion"=>$completion_percent,"length"=>$length,"breadth"=>$breadth,"depth"=>$depth,"total_sqft"=>$total_sqft,"remarks"=>$remarks,"total_amount"=>$amount);
				$project_task_boq_actual_iresult = i_update_project_task_boq_actual($boq_id,$project_task_boq_actual_update_data);

				if($project_task_boq_actual_iresult["status"] == SUCCESS)
				{

					$alert_type = 1;
					$project_process_task_update_data = array("contract_completion"=>$completion_percent,"actual_end_date"=>$actual_end_date);
					$task_update_list = i_update_project_process_task($task_id,$project_process_task_update_data);
					header("location:project_task_boq_actuals_list.php");
				}
				else
				{
				   $alert = "Project Already Exists";
				  $alert_type = 0;
				}
			}
			elseif($work_type == "Rework")
			{
				$project_task_boq_actual_update_data = array("number"=>$number,"rework_completion"=>$completion_percent,"length"=>$length,"breadth"=>$breadth,"depth"=>$depth,"total_sqft"=>$total_sqft,"remarks"=>$remarks,"total_amount"=>$amount);
				$project_task_boq_actual_iresult = i_update_project_task_boq_actual($boq_id,$project_task_boq_actual_update_data);

				if($project_task_boq_actual_iresult["status"] == SUCCESS)

				{

					$alert_type = 1;
					header("location:project_task_boq_actuals_list.php");
				}
				else
				{
				   $alert = "Project Already Exists";
				  $alert_type = 0;
				}

			}

			$alert = $project_task_boq_actual_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";

		}
	}
	else if(isset($_POST["ddl_process"]))
	{
		$process           = $_POST["ddl_process"];
		$boq_id			   = $_POST["hd_boq_actual_id"];
		// Get Project Contract Rate modes already added
		$project_contract_rate_master_search_data = array("active"=>'1',"process"=>$process);
		$project_contract_rate_master_list = i_get_project_contract_rate_master($project_contract_rate_master_search_data);
		if($project_contract_rate_master_list['status'] == SUCCESS)
		{
			$project_contract_rate_master_list_data = $project_contract_rate_master_list['data'];
		}
		else
		{
			$alert = $alert."Alert: ".$project_contract_rate_master_list["data"];
		}
	}

	// Get project Contract Process modes already added
	$project_contract_process_search_data = array("active"=>'1');
	$project_contract_process_list = i_get_project_contract_process($project_contract_process_search_data);
	if($project_contract_process_list['status'] == SUCCESS)
	{
		$project_contract_process_list_data = $project_contract_process_list['data'];
	}
	else
	{
		$alert = $project_contract_process_list["data"];
		$alert_type = 0;
	}

   // Get Stock master uom modes already added
	$stock_unit_measure_search_data = array("active"=>'1');
	$stock_unit_measure_list = i_get_stock_unit_measure_list($stock_unit_measure_search_data);
	if($stock_unit_measure_list['status'] == SUCCESS)
	{
		$stock_unit_measure_list_data = $stock_unit_measure_list['data'];
	}
	else
	{
		$alert = $stock_unit_measure_list["data"];
		$alert_type = 0;
	}



	// Get Project Location already added
	$project_site_location_mapping_master_search_data = array("active"=>'1');
	$project_site_location_list = i_get_project_site_location_mapping_master($project_site_location_mapping_master_search_data);
	if($project_site_location_list['status'] == SUCCESS)
	{
		$project_site_location_list_data = $project_site_location_list['data'];
	}
	else
	{
		$alert = $alert."Alert: ".$project_site_location_list["data"];
	}

	// Get list of task plans for this process plan
	$task_manpower_completion = 0;
	$task_machine_completion = 0;
	$task_contract_completion = 0;
	$avg_count = 0;
	$project_process_task_search_data = array("active"=>'1',"task_id"=>$task_id);
	$project_process_task_list = i_get_project_process_task($project_process_task_search_data);
	if($project_process_task_list["status"] == SUCCESS)
	{
		$project_process_task_list_data = $project_process_task_list["data"];
		$task_manpower_completion = $project_process_task_list_data[0]["project_process_task_completion"];
		$task_machine_completion = $project_process_task_list_data[0]["project_process_machine_task_completion"];
		$task_contract_completion = $project_process_task_list_data[0]["project_process_contract_task_completion"];

		$project_process_task_list_data = $project_process_task_list["data"];
		//$no_of_task = count($project_process_task_list_data);

		// Actual Manpower data
		$man_power_search_data = array("active"=>'1',"task_id"=>$task_id);
		$man_power_list1 = i_get_man_power_list($man_power_search_data);
		if($man_power_list1["status"] == SUCCESS)
		{
			$avg_count = $avg_count + 1;
		}
		else
		{
			// Get Project Man Power Estimate modes already added
			$project_man_power_estimate_search_data = array("active"=>'1',"task_id"=>$task_id);
			$project_man_power_estimate_list = i_get_project_man_power_estimate($project_man_power_estimate_search_data);
			if($project_man_power_estimate_list['status'] == SUCCESS)
			{
				$avg_count = $avg_count + 1;
			}
			else
			{
				//Do not Inncrement
			}
		}

		// Machine Planning data
		$actual_machine_plan_search_data = array("active"=>'1',"task_id"=>$task_id,"work_type"=>"Regular");
		$actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
		if($actual_machine_plan_list["status"] == SUCCESS)
		{
			$avg_count = $avg_count + 1;
		}
		else
		{
			//Machine Planning
			$project_machine_planning_search_data = array("active"=>'1',"task_id"=>$task_id);
			$project_machine_planning_list = i_get_project_machine_planning($project_machine_planning_search_data);
			if($project_machine_planning_list["status"] == SUCCESS)
			{
				$avg_count = $avg_count + 1;
			}
			else
			{
				//Do not Increment
			}
		}
		// Actual Contract  Data
		$project_task_boq_actual_search_data = array("active"=>'1',"task_id"=>$task_id);
		$project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
		if($project_task_boq_actual_list['status'] == SUCCESS)
		{
			$avg_count = $avg_count + 1;
		}
		else
		{
			$project_task_boq_search_data = array("active"=>'1',"task_id"=>$task_id);
			$project_task_boq_list = i_get_project_task_boq($project_task_boq_search_data);
			if($project_task_boq_list['status'] == SUCCESS)
			{
				$avg_count = $avg_count + 1;
			}
			else
			{
				///Do not Increment
			}
		}
	}
	else
	{
		//
	}
	$overall_percentage = $task_manpower_completion + $task_machine_completion +$task_contract_completion ;
	if($avg_count != 0)
	{
		$avg_overall_percentage = $overall_percentage/$avg_count;
	}
	else
	{
		$avg_overall_percentage = 0;
	}
	$overall_pending_percentage = 100 - $avg_overall_percentage;
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Edit Project Task BOQ Actual</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">

	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">

	      			<div class="widget-header custom-header">
	      				<i class="icon-user"></i>
	      				<!-- <h3>Edit Project Task Actual Man Power&nbsp;&nbsp;&nbsp;Completion % as of now: <?php echo round($avg_overall_percentage).'%'; ?> (This includes &nbsp;&nbsp;&nbsp; <?php echo $task_manpower_completion.' '.'%'.' '.'Manpower' ; ?> &nbsp;&nbsp;&nbsp;  <?php echo $task_machine_completion.'%'.'Machine' ; ?> &nbsp;&nbsp;&nbsp; <?php echo $task_contract_completion.'%'.'Contract' ; ?> )</h3> -->

								<h3>
									<span class="header-label">Project :</span> <?= $project; ?>
									<span class="header-label">Process :</span> <?= $process_name; ?>
									<span class="header-label">Task:</span>  <?= $task_name; ?>
								</h3>
								<h3 style="margin-left:25px">
									<span class="header-label">Road:</span>  <?= $road_name; ?>
									<span class="header-label">Planned Msrmnt:</span> <?= $planned_msmrt; ?>
									<!-- <span class="header-label">UOM</span> <?= $uom; ?> -->
									<span class="header-label">UOM</span> <?= $project_task_boq_actual_list_data[0]["stock_unit_name"]; ?>
								</h3>
								<h3 >
									<span class="header-label">Regular</span> <?= $reg_overall_msmrt; ?> (MP : <?= $reg_mp_msmrt; ?>  MC : <?= $reg_mc_msmrt; ?> CW : <?= $reg_cw_msmrt; ?>)
									<span class="header-label">Rework</span> <?= $rework_overall_msmrt; ?> (MP : <?= $rework_mp_msmrt; ?>   MC : <?= $rework_mc_msmrt; ?> CW : <?= $rework_cw_msmrt; ?>)
								</h3>


						</div> <!-- /widget-header -->

					<div class="widget-content">



						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li class="right">
						    <a href="#formcontrols" data-toggle="tab">Edit Project Task BOQ Actual</a>
						  </li>
						</ul>
						<br>
							<div class="control-group">
								<div class="controls">
								<?php
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>

								<?php
								if($alert_type == 1) // Success
								{
								?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_edit_task_boq_form" class="form-horizontal" method="post">
								<input type="hidden" name="hd_boq_actual_id" value="<?php echo $boq_id; ?>" />
								<input type="hidden" name="hd_task_id" value="<?php echo $task_id; ?>" />
								<input type="hidden" name="hd_road_id" value="<?php echo $road_id; ?>" />
								<input type="hidden" name="hd_work_type" value="<?php echo $work_type; ?>" />
								<input type="hidden" id="hd_percent" name="hd_completion_percent" value="<?php echo $new_completion_percent; ?>" />
									<fieldset>
										<div class="control-group">
											<label class="control-label" for="number">Work Type</label>
											<div class="controls">
												<input type="text" style="font-size:20px;" disabled min="1" name="number" id="num_number" value="<?php echo $project_task_boq_actual_list_data[0]["project_task_actual_boq_work_type"]?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="ddl_process">Contract Process*</label>
											<div class="controls">
												<select name="ddl_process"disabled  onchange="this.form.submit();" required>
												<option value="">- - Select Process - -</option>
												<?php
												for($count = 0; $count < count($project_contract_process_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_contract_process_list_data[$count]["project_contract_process_id"]; ?>"<?php if($project_contract_process_list_data[$count]["project_contract_process_id"] == $project_task_boq_actual_list_data
												[0]["project_task_boq_actual_contract_process"]){ ?> selected="selected" <?php } ?>><?php echo $project_contract_process_list_data[$count]["project_contract_process_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->
											</div>

											<div class="control-group">
											<label class="control-label" for="work_task">Work Task</label>
											<div class="controls">
												<input type="work_task" name="work_task"  disabled value="<?php echo $project_task_boq_actual_list_data[0]["project_contract_rate_master_work_task"]?>" placeholder="Nos">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
												<label class="control-label" for="ddl_unit">UOM*</label>
												<div class="controls">
													<select name="ddl_unit" disabled required>
													<option value="">- - Select UOM - -</option>
													<?php
													for($count = 0; $count < count($stock_unit_measure_list_data); $count++)
													{
													?>
													<option value="<?php echo $stock_unit_measure_list_data[$count]["stock_unit_id"]; ?>" <?php if($stock_unit_measure_list_data[$count]["stock_unit_id"] == $project_task_boq_actual_list_data[0]["project_task_actual_boq_uom"]){ ?> selected="selected" <?php } ?>><?php echo $stock_unit_measure_list_data[$count]["stock_unit_name"]; ?></option>
													<?php
													}
													?>
													</select>
												</div> <!-- /controls -->
											</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="ddl_venodr">Vendor</label>
											<div class="controls">
												<select name="ddl_venodr" disabled required>
												<option value="">- - Select Vendor- -</option>
												<?php
												for($count = 0; $count < count($project_manpower_agency_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_manpower_agency_list_data[$count]["project_manpower_agency_id"]; ?>" <?php if($project_manpower_agency_list_data[$count]["project_manpower_agency_id"] == $project_task_boq_actual_list_data[0]["project_task_actual_boq_vendor_id"]){ ?> selected="selected" <?php } ?>><?php echo $project_manpower_agency_list_data[$count]["project_manpower_agency_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="ddl_location">Road Number</label>
											<div class="controls">
												<select name="ddl_location" id="ddl_location" disabled>
												<option value="">- - Select Site Location - -</option>
												<?php
												for($count = 0; $count < count($project_site_location_list_data); $count++)
												{
												?>
													<option value="<?php echo $project_site_location_list_data[$count]["project_site_location_mapping_master_id"]; ?>" <?php if( $project_site_location_list_data[$count]["project_site_location_mapping_master_id"] == $project_task_boq_actual_list_data[0]["project_task_actual_boq_location"]){ ?> selected <?php } ?>><?php echo $project_site_location_list_data[$count]["project_site_location_mapping_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->


										<div class="control-group">
											<label class="control-label" for="number">Number</label>
											<div class="controls">
												<input type="number" min="1" name="number" id="num_number" value="<?php echo $project_task_boq_actual_list_data[0]["project_task_boq_actual_number"]?>" placeholder="Nos" onkeyup="return total_sqft_no();">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="length">Length</label>
											<div class="controls">
												<input type="number" step="0.01" min="0.01" name="length" id="length" value="<?php echo $project_task_boq_actual_list_data[0]["project_task_actual_boq_length"]?>" onkeyup="return total_sqft_no();" placeholder="Length">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="breadth">Breadth</label>
											<div class="controls">
												<input type="number" step="0.01" min="0.01" name="breadth"  id="breadth" value="<?php echo $project_task_boq_actual_list_data[0]["project_task_actual_boq_breadth"]?>" onkeyup="return total_sqft_no();" placeholder="Breadth">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="depth">Depth</label>
											<div class="controls">
												<input type="number" step="0.01" min="0.01" name="depth"  id="depth"  value="<?php echo $project_task_boq_actual_list_data[0]["project_task_actual_boq_depth"]?>" value="1"  onkeyup="return total_sqft_no();"  placeholder="Depth">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="total_sqft">Total Measurement</label>
											<div class="controls">
												<input type="number" step="0.01" min="0.01" name="total_sqft" onchange="calculate_msrmnt()" value="<?php echo $project_task_boq_actual_list_data[0]["project_task_actual_boq_total_measurement"]?>" id="total_sqft"  placeholder="Total Sqft">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->


										<div class="control-group">
											<label class="control-label" for="amount">Total Value (in Rs.)</label>
											<div class="controls">
												<input type="number" step="0.01" min="0.01" name="amount"  id="amount" value="<?php echo $project_task_boq_actual_list_data[0]["project_task_actual_boq_lumpsum"]; ?>" placeholder="Contract Rate">
												<p>Rate: <?php echo $project_task_boq_actual_list_data[0]["project_task_actual_boq_amount"]; ?></p>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="completion_percent">Completion Percent</label>
											<div class="controls">
												<!-- <input type="hidden" name="hd_completion_percent" id="hd_percent" placeholder="Percent" value="<?php echo $new_completion_percent ;?>"> -->
												<input type="number" disabled name="completion_percent" id="percent" placeholder="Percent" value="<?php echo $new_completion_percent ;?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks" rows="4" cols="50" placeholder="Remarks"><?php echo $project_task_boq_actual_list_data[0]["project_task_actual_boq_remarks"] ;?></textarea>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_project_task_boq_actual_submit" id="edit_project_task_boq_actual_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>

							</div>

					</div> <!-- /widget-content -->

				</div> <!-- /widget -->

		    </div> <!-- /span8 -->




	      </div> <!-- /row -->

	    </div> <!-- /container -->

	</div> <!-- /main-inner -->

</div> <!-- /main -->




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<?php
if(!isset($_POST["edit_project_task_boq_actual_submit"]))
{
?>
 <script>
	$(document).ready(function(){
		document.getElementById('edit_project_task_boq_actual_submit').disabled = false;
	})
 </script>
 <?php
 }
 ?>
<script>
function calculate_msrmnt() {
	// document.getElementById("edit_project_task_boq_actual_submit").disabled = false;

	var planned_msrmnt = <?= json_encode($planned_msmrt); ?>;
	console.log(<?= json_encode($planned_msmrt); ?>);
	if(!planned_msrmnt || planned_msrmnt == 0) {
		alert('Kindly plan the measurement');
		document.getElementById("edit_project_task_boq_actual_submit").disabled = true;
		return false;
	}
	var work_type = <?= json_encode($work_type); ?>;
	var cw_msrmnt = <?= json_encode($reg_cw_msmrt); ?>;

	var new_msrmnt = document.getElementById("total_sqft").value;
	new_msrmnt -= cw_msrmnt;

	var total_msrmnt, completed_msrmnt;

	var old_total_value = document.getElementById("amount").value;
	var old_percent = document.getElementById("percent").value;
	var new_total_value = parseFloat(document.getElementById("total_sqft").value) * parseFloat(<?= json_encode($project_task_boq_actual_list_data[0]["project_task_actual_boq_amount"]); ?>);

	if(work_type == 'Regular') {
		total_msrmnt = planned_msrmnt;
		completed_msrmnt = <?= json_encode($reg_overall_msmrt); ?>;
	} else {
		total_msrmnt = <?= json_encode($reg_overall_msmrt); ?>;
		completed_msrmnt = <?= json_encode($rework_overall_msmrt); ?>;
	}
	document.getElementById("amount").value = new_total_value;
	document.getElementById("percent").value = parseFloat(((completed_msrmnt + parseFloat(document.getElementById("total_sqft").value)) / planned_msrmnt) * 100).toFixed(2);
	document.getElementById("hd_percent").value = document.getElementById("percent").value;

 console.log(document.getElementById("percent").value, completed_msrmnt, parseFloat(document.getElementById("total_sqft").value), planned_msrmnt);


	if(parseFloat(document.getElementById("percent").value) > 100) {
		alert('Entered Measurement Exceeds than total measurement');
		document.getElementById("amount").value = old_total_value;
		document.getElementById("percent").value = old_percent;
		document.getElementById("hd_percent").value = old_percent;
		document.getElementById("total_sqft").value = cw_msrmnt;
		document.getElementById("edit_project_task_boq_actual_submit").disabled = true;
	}


	if((completed_msrmnt + parseFloat(new_msrmnt)) > total_msrmnt ) {
		alert('Entered Msrmnt ('+ (new_msrmnt + cw_msrmnt) + ') exceeds Total msrmnt ('+ total_msrmnt + ')');
		document.getElementById("amount").value = old_total_value;
		document.getElementById("percent").value = old_percent;
		document.getElementById("hd_percent").value = old_percent;
		document.getElementById("total_sqft").value = cw_msrmnt;
    document.getElementById("edit_project_task_boq_actual_submit").disabled = true;
  }
}

function total_sqft_no()
{
	var number = parseFloat(document.getElementById('num_number').value);
	var length = parseFloat(document.getElementById('length').value);
	var breadth = parseFloat(document.getElementById('breadth').value);
	var depth = parseFloat(document.getElementById('depth').value);

	var total_sqft = (length * breadth * depth * number);

	document.getElementById("total_sqft").value = parseFloat(total_sqft).toFixed(2);

}

function get_contract_rate()
{
	var contract_rate_id = document.getElementById("ddl_task").value;
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function()
	{
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{
			document.getElementById("amount").value = xmlhttp.responseText;
			console.log('xmlhttp.responseText ', xmlhttp.responseText);
		}
	}

	xmlhttp.open("POST", "ajax/project_get_contract_rate.php");   //
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("contract_rate_id=" + contract_rate_id);
}
</script>
  </body>

</html>
