<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: process_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Process Plans
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/
$_SESSION['module'] = 'BD';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_payment_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing here

	// Temp data
	$alert = "";

	$file_number    = "";
	$reason         = "";
	$status         = "";
	$start_date     = "";
	$end_date       = "";
	
	$start_dt_form = "";
	$end_dt_form   = "";
	
	if(($role == "1") || ($role == "2") || ($role == "12"))
	{
		$added_by = "";
	}
	else
	{
		$added_by = $user;
	}
	
	if(isset($_GET['file']))
	{
		$file_number = $_GET['file'];
	}
	else
	{
		$file_number = '';
	}

	// Search parameters
	if(isset($_POST["pay_request_search_submit"]))
	{
		$file_number    = $_POST["search_file_no"];				
		$status         = $_POST["ddl_status"];
		$reason         = $_POST["ddl_reason"];		
		if(($role == "1") || ($role == "2") || ($role == "12"))
		{
			$added_by = $_POST["ddl_requested_by"];
		}
		else
		{
			$added_by = $user;
		}
		if((isset($_POST["dt_start_date_form"])) && ($_POST["dt_start_date_form"] != ""))
		{
			$start_dt_form  = $_POST["dt_start_date_form"];		
			$start_date = $start_dt_form." 00:00:00";
		}
		if((isset($_POST["dt_end_date_form"])) && ($_POST["dt_end_date_form"] != ""))
		{
			$end_dt_form = $_POST["dt_end_date_form"];
			$end_date    = $end_dt_form." 23:59:59";
		}	
	}
	
	// Get list of payment requests
	$payment_request_data = array();
	if($file_number != '')
	{
		$payment_request_data['file_id'] = $file_number;
	}
	if($status != '')
	{
		$payment_request_data['status'] = $status;
	}
	if($reason != '')
	{
		$payment_request_data['reason'] = $reason;
	}
	if($added_by != '')
	{
		$payment_request_data['added_by'] = $added_by;
	}
	if($start_date != '')
	{
		$payment_request_data['start_date'] = $start_date;
	}
	if($end_date != '')
	{
		$payment_request_data['end_date'] = $end_date;
	}
	
	$payment_request_data["status"] == 1;
	$pay_request_list = i_get_bd_payment_list($payment_request_data);

	if($pay_request_list["status"] == SUCCESS)
	{
		$pay_request_list_data = $pay_request_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$pay_request_list["data"];
	}	
	
	// Get list of users
	$user_list = i_get_user_list('','','','','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>BD - Payment Request List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>BD - Payment Request List</h3>			  
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:100px; padding-top:10px;">               
			  <form method="post" id="pay_request_search_form" action="bd_payment_request_list.php">			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="text" name="search_file_no" value="<?php echo $file_number; ?>" placeholder="Search by file number" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_reason">
			  <option value="">- - Select Reason - -</option>
			  <option value="1" <?php if($reason == "1") { ?> selected <?php } ?>>Payment</option>
			  <option value="2" <?php if($reason == "2") { ?> selected <?php } ?>>Change of Land use Fee</option>
			  <option value="3" <?php if($reason == "3") { ?> selected <?php } ?>>Conversion Fee</option>
			  <option value="4" <?php if($reason == "4") { ?> selected <?php } ?>>Brokerage Amount</option>
			  <option value="5" <?php if($reason == "5") { ?> selected <?php } ?>>Stamp Duty</option>
			  <option value="6" <?php if($reason == "6") { ?> selected <?php } ?>>Registration Fee</option>
			  <option value="7" <?php if($reason == "7") { ?> selected <?php } ?>>Other Expenses</option>
			  </select>
			  </span>
			  <?php
			  if(($role == "1") || ($role == "2") || ($role == "12"))
			  {
			  ?>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_requested_by">
			  <option value="">- - Select User - -</option>
			  <?php
			  for($count = 0; $count < count($user_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($added_by == $user_list_data[$count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <?php
			  }
			  ?>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_status">
			  <option value="">- - Select Status - -</option>
			  <option value="1" <?php if($status == "1") { ?> selected <?php } ?>>HOD Approval Pending</option>
			  <option value="2" <?php if($status == "2") { ?> selected <?php } ?>>HOD Approved</option>
			  <option value="3" <?php if($status == "3") { ?> selected <?php } ?>>HOD Rejected</option>
			  <option value="4" <?php if($status == "4") { ?> selected <?php } ?>>Payment Released</option>
			  <option value="5" <?php if($status == "5") { ?> selected <?php } ?>>Finance Rejected</option>
			  <option value="6" <?php if($status == "6") { ?> selected <?php } ?>>Finance Held</option>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_start_date_form" value="<?php echo $start_dt_form; ?>" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_end_date_form" value="<?php echo $end_dt_form; ?>" />
			  </span>
			  <input type="submit" name="pay_request_search_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>Survey Number</th>										
					<th>Project</th>
					<th>Amount</th>
					<th>Reason</th>
					<th>Remarks</th>
					<th>Payable To</th>
					<th>Requested By</th>
					<th>Requested On</th>
					<th>Status</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				 <?php
				if($pay_request_list["status"] == SUCCESS)
				{		
					for($count = 0; $count < count($pay_request_list_data); $count++)
					{						
						// Get Legal File ID
						$legal_file_details = i_get_file_list('',$pay_request_list_data[$count]['bd_payment_requests_file_id'],'','','','','','','','','','','','');
						
						if($legal_file_details['status'] == SUCCESS)
						{
							$legal_file_id = $legal_file_details['data'][0]['file_id'];
						}
						else
						{
							$legal_file_id = '-1';
						}
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $pay_request_list_data[$count]['bd_file_survey_no']; ?></td>
						<td style="word-wrap:break-word;"><?php echo $pay_request_list_data[$count]["bd_project_name"]; ?></td>								
						<td style="word-wrap:break-word;"><?php echo $pay_request_list_data[$count]["bd_payment_requests_amount"]; ?></td>
						<td style="word-wrap:break-word;"><?php switch($pay_request_list_data[$count]["bd_payment_requests_reason"])
						{
						case "1":
						$pay_reason = "Payment";
						break;
						
						case "2":
						$pay_reason = "Change of Land Use Fee";
						break;
						
						case "3":
						$pay_reason = "Conversion Fee";
						break;
						
						case "4":
						$pay_reason = "Brokerage Amount";
						break;
						
						case "5":
						$pay_reason = "Stamp Duty";
						break;
						
						case "6":
						$pay_reason = "Registration Fee";
						break;
						
						default:
						$pay_reason = "Other Expenses";
						break;
						}
						echo $pay_reason; ?></td>
						<td style="word-wrap:break-word;"><?php echo $pay_request_list_data[$count]["bd_payment_requests_remarks"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $pay_request_list_data[$count]["bd_payment_requests_payable_to"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $pay_request_list_data[$count]["user_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($pay_request_list_data[$count]["bd_payment_requests_added_on"])); ?></td>
						<td style="word-wrap:break-word;"><?php 
						switch($pay_request_list_data[$count]["bd_payment_requests_status"])
						{
						case "1":
						$pay_status = "HOD Approval Pending";
						break;
						
						case "2":
						$pay_status = "HOD Approved. Pending with Finance";
						break;
						
						case "3":
						$pay_status = "HOD Rejected";
						break;
						
						case "4":
						$pay_status = "Payment Released";
						break;
						
						case "5":
						$pay_status = "Finance Rejected";
						break;
						
						case "6":
						$pay_status = "Finance Held";
						break;
						
						default:
						$pay_status = "HOD Approval Pending";
						break;
						}
						?>
						<a href="pay_request_status_list.php?request=<?php echo $pay_request_list_data[$count]["bd_payment_requests_id"]; ?>"><?php echo $pay_status; ?></a>
						<?php
						if($pay_request_list_data[$count]["bd_payment_requests_status"] == "4")
						{							
							$pay_release_details = i_get_file_payment_list('','',$pay_request_list_data[$count]["bd_payment_requests_id"]);
							
							if($pay_release_details["status"] == SUCCESS)
							{
								?>
								<br />
								<b>Mode</b>: <?php echo $pay_release_details["data"][0]["payment_mode_name"]; ?>&nbsp;&nbsp;&nbsp;
								<b>Bill Date</b>: <?php echo date("d-M-Y",strtotime($pay_release_details["data"][0]["file_payment_date"])); ?>&nbsp;&nbsp;&nbsp;
								<?php
							}
						}?></td>
						<td style="word-wrap:break-word;"><?php if(($pay_request_list_data[$count]["bd_payment_requests_status"] == "1") && (($role == "1") || ($role == "2")))
						{
						?>
						<a href="update_bd_payment_request.php?request=<?php echo $pay_request_list_data[$count]["bd_payment_requests_id"]; ?>&status=2">Approve</a> / <a href="update_bd_payment_request.php?request=<?php echo $pay_request_list_data[$count]["bd_payment_requests_id"]; ?>&status=3">Reject</a>
						<?php
						}
						else if(($pay_request_list_data[$count]["bd_payment_requests_status"] == "2") && (($role == "1") || ($role == "12")))						
						{
						?>
						<a href="add_file_payment.php?request=<?php echo $pay_request_list_data[$count]["bd_payment_requests_id"]; ?>&file=<?php echo $legal_file_id; ?>">Release</a> / <a href="update_bd_payment_request.php?request=<?php echo $pay_request_list_data[$count]["bd_payment_requests_id"]; ?>&status=5">Reject</a> / <a href="update_bd_payment_request.php?request=<?php echo $pay_request_list_data[$count]["bd_payment_requests_id"]; ?>&status=6">Hold</a>
						<?php
						}
						else
						{
							echo "Nothing to be done by you";
						}
						?></td>
					</tr>
					<?php 	
					}
				}
				else
				{
				?>
				<td colspan="10">No Payment Request added yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function confirm_deletion(file_id)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "pending_file_list.php";
				}
			}

			xmlhttp.open("GET", "legal_file_delete.php?file=" + file_id);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}

</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>
