<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 25-July-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');


if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Capture the form data
	if(isset($_POST["add_module_mapping_submit"]))
	{
		$mgmnt_project_id    = $_POST["ddl_mgmnt_project_id"];
		$stock_project_id    = $_POST["ddl_stock_project_id"];

		// Check for mandatory fields
		if(($mgmnt_project_id != "") && ($stock_project_id != ""))
		{
			$project_stock_module_mapping_iresult =  i_add_project_stock_module_mapping($mgmnt_project_id,$stock_project_id,$user);

			if($project_stock_module_mapping_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
			}

			$alert = $project_stock_module_mapping_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get Project Management Master modes already added
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list['status'] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list['data'];
	}

	else
	{
		$alert = $project_management_master_list["data"];
		$alert_type = 0;
	}

    // Get Stock project modes already added
	$stock_project_search_data = array("active"=>'1');
	$stock_project_list = i_get_project_list($stock_project_search_data);
	if($stock_project_list['status'] == SUCCESS)
	{
		$stock_project_list_data = $stock_project_list['data'];
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Stock - Add Module Mapping</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">

	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">

	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Module Mapping</h3><span style="float:right; padding-right:20px;"><a href="project_stock_module_mapping_list.php">Project Module Mapping List</a></span>
	  				</div> <!-- /widget-header -->

					<div class="widget-content">

						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Project Stock - Module Mapping</a>
						  </li>
						</ul>
						<br>
							<div class="control-group">
								<div class="controls">
								<?php
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>

								<?php
								if($alert_type == 1) // Success
								{
								?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_stock_add_module_mapping_form" class="form-horizontal" method="post" action="project_stock_add_module_mapping.php">
									<fieldset>

										<div class="control-group">
											<label class="control-label" for="ddl_mgmnt_project_id">Mgmnt Project*</label>
											<div class="controls">
												<select name="ddl_mgmnt_project_id" required>
												<option>- - Select Mgmnt Project - -</option>
												<?php
												for($count = 0; $count < count($project_management_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_management_master_list_data[$count]["project_management_master_id"]; ?>"><?php echo $project_management_master_list_data[$count]["project_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->


										<div class="control-group">
											<label class="control-label" for="ddl_stock_project_id">Stock project*</label>
											<div class="controls">
												<select name="ddl_stock_project_id" required>
												<option>- - Select Stock Project - -</option>
												<?php
												for($count = 0; $count < count($stock_project_list_data); $count++)
												{
												?>
												<option value="<?php echo $stock_project_list_data[$count]["stock_project_id"]; ?>"><?php echo $stock_project_list_data[$count]["stock_project_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />


										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_module_mapping_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>

							</div>

					</div> <!-- /widget-content -->

				</div> <!-- /widget -->

		    </div> <!-- /span8 -->




	      </div> <!-- /row -->

	    </div> <!-- /container -->

	</div> <!-- /main-inner -->

</div> <!-- /main -->




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
