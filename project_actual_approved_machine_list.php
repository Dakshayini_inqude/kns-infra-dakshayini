<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_user_mapping_list.php
CREATED ON	: 28-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
define('PROJECT_ACTUAL_MACHINE_APPROVED_LIST_FUNC_ID','274');
/* DEFINES - END */

/*
TBD:
*/
$_SESSION['module'] = 'Projectmgmnt';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	$alert_type = -1;
	$alert = "";

	// Get permission settings for this user for this page
	$add_perms_list     = i_get_user_perms($user,'',PROJECT_ACTUAL_MACHINE_APPROVED_LIST_FUNC_ID,'1','1');
	$view_perms_list    = i_get_user_perms($user,'',PROJECT_ACTUAL_MACHINE_APPROVED_LIST_FUNC_ID,'2','1');
	$edit_perms_list    = i_get_user_perms($user,'',PROJECT_ACTUAL_MACHINE_APPROVED_LIST_FUNC_ID,'3','1');
	$delete_perms_list  = i_get_user_perms($user,'',PROJECT_ACTUAL_MACHINE_APPROVED_LIST_FUNC_ID,'4','1');

	// Query String Data
	// Nothing

	$search_project   	 = "-1";

	if(isset($_REQUEST["hd_machine_id"]))
	{
		$search_machine = $_REQUEST["hd_machine_id"];
	}
	else
	{
		$search_machine = "";
	}

	if(isset($_POST["search_project"]))
	{
		$search_project   = $_POST["search_project"];
	}
	else if(isset($_GET["search_project"]))
	{
		$search_project   = $_GET["search_project"];
	}

	$search_machine_vendor   	 = "-1";

	if(isset($_POST["search_machine_vendor"]))
	{
		$search_machine_vendor   = $_POST["search_machine_vendor"];
	}

	if(isset($_POST["hd_machine_id"]))
	{
		$search_machine = $_POST["hd_machine_id"];
	}
	else
	{
		$search_machine = "";
	}

	$search_process  	 = "";

	if(isset($_POST["search_process"]))
	{
		$search_process   = $_POST["search_process"];
	}

	$search_task  	 = "";

	if(isset($_POST["search_task"]))
	{
		$search_task   = $_POST["search_task"];
	}

	$search_work_type   	 = "";
	if(isset($_POST["search_work_type"]))
	{
		$search_work_type   = $_POST["search_work_type"];
	}



	$start_date 	   = '1970-01-01';
	$start_date_filter = '';
	if(isset($_POST["dt_start_date"]))
	{
		if($_POST["dt_start_date"] != '')
		{
			$start_date = $_POST["dt_start_date"];
		}
		else
		{
			$start_date = '1970-01-01';
		}
		$start_date_filter = $_POST["dt_start_date"];
	}

	$end_date 		 = date('Y-m-d');
	$end_date_filter = '';
	if(isset($_POST["dt_end_date"]))
	{
		if($_POST["dt_start_date"] != '')
		{
			$end_date = $_POST["dt_end_date"];
		}
		else
		{
			$end_date = date('Y-m-d');
		}
		$end_date_filter = $_POST["dt_end_date"];
	}

	if(isset($_POST["items_submit"]))
	{
		$item_nos     		= $_POST['hd_machine_count'];
		$search_machine     = $_POST['hd_machine_id'];
		$start_date   		= "0000-00-00";
		$end_date 		    = "0000-00-00";
		$total_amount 		= "";
		$item_count   		= 0;
		$no_of_hrs    		= 0;
		$total_amount 		= 0;
		$bill_no = p_generate_machine_bill_no();
		$payment_machine_iresult = i_add_project_payment_machine($search_machine_vendor,'',$bill_no,'','','','','','','','',$user);
		if($payment_machine_iresult["status"] == SUCCESS)
		{
			$machine_payment_id = $payment_machine_iresult["data"];
		}
		for($item_count = 0 ; $item_count < $item_nos; $item_count++)
		{
			if(isset($_POST['cb_item_'.$item_count]))
			{
				$machine_rate	   = $_POST['hd_rate_count_'.$item_count];
				$machine_date	   = $_POST['hd_machine_date_'.$item_count];
				$machine_vendor_id = $_POST['hd_vendor_count_'.$item_count];
				$machine_actual_id = $_POST['cb_item_'.$item_count];

				if($start_date == "0000-00-00")
				{
					$start_date = $machine_date;
				}
				else if(strtotime($machine_date) < strtotime($start_date))
				{
					$start_date = $machine_date;
				}

				if($start_date == "0000-00-00")
				{
					$end_date = $machine_date;
				}
				else if(strtotime($machine_date) > strtotime($end_date))
				{
					$end_date = $machine_date;
				}

				$payment_machine_mapping_iresult = i_add_project_payment_machine_mapping($machine_actual_id,$machine_payment_id,'',$user);
				$machine_duration = strtotime($end_time) - strtotime($start_time);
				$total_amount = $total_amount + $machine_rate;
			}
		}

		$machine_start_date = date("Y-m-d",strtotime($start_date)).' '."00:00:00";
		$machine_end_date   = date("Y-m-d",strtotime($end_date)) .' '."00:00:00";

		//Update Payment Manpower
		$project_payment_machine_update_data = array("vendor_id"=>$machine_vendor_id,"amount"=>$total_amount,"from_date"=>$machine_start_date,"to_date"=>$machine_end_date);

		$payment_contract_uresults =  i_update_project_payment_machine($machine_payment_id,$project_payment_machine_update_data);
		if($payment_contract_uresults["status"] == SUCCESS)
		{
			header("location:project_add_machine_bill_no.php?payment_machine_id=$machine_payment_id");
		}
	}

	// Machine Planning data
	$actual_machine_plan_search_data = array("active"=>'1',"display_status"=>"approved","project"=>$search_project,"machine_vendor"=>$search_machine_vendor,"machine_id"=>$search_machine,"process"=>$search_process,"task"=>$search_task,"start_date_time"=>$start_date.' 00:00:00',"end_date_time"=>$end_date.' 23:59:59',"work_type"=>$search_work_type);
	$actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
	if($actual_machine_plan_list["status"] == SUCCESS)
	{
		$actual_machine_plan_list_data = $actual_machine_plan_list["data"];
	}
	else
	{
		$alert = $actual_machine_plan_list["data"];
		$alert_type = 0;
	}

	// Project data
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_list["data"];
	}

	// Machine Vendor data
	$project_machine_vendor_search_data = array("active"=>'1');
	$project_machine_vendor_list = i_get_project_machine_vendor_master_list($project_machine_vendor_search_data);
	if($project_machine_vendor_list["status"] == SUCCESS)
	{
		$project_machine_vendor_list_data = $project_machine_vendor_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_machine_vendor_list["data"];
	}

	// Machine data
	$project_machine_master_search_data = array("active"=>'1');
	$project_machine_master_list = i_get_project_machine_master($project_machine_master_search_data);
	if($project_machine_master_list["status"] == SUCCESS)
	{
		$project_machine_master_list_data = $project_machine_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_machine_master_list["data"];
	}

	// Process Master
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list["status"] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_process_master_list["data"];
	}

	// Task Master
	$project_task_master_search_data = array("active"=>'1',"process"=>$search_process);
	$project_task_master_list = i_get_project_task_master($project_task_master_search_data);
	if($project_task_master_list["status"] == SUCCESS)
	{
		$project_task_master_list_data = $project_task_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_task_master_list["data"];
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Approved Actual Machine List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Approved Actual Machine List</h3>
            </div>
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
			<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_actual_approved_machine_list.php?hd_machine_id=<?php echo $search_machine ;?>">

			  <input type="hidden" name="hd_machine_id" id="hd_machine_id" value="<?php echo $search_machine ;?>" />
			  <span style=" padding-left:20px; padding-right:20px;">
			  <input type="text" name="stxt_machine"  autocomplete="off" id="stxt_machine" onkeyup="return get_machine_list();" placeholder="Search Machine by name or code" />
			  <div id="search_results" class="dropdown-content"></div>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			 <!--  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($project_process_master_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $project_process_master_list_data[$process_count]["project_process_master_id"]; ?>" <?php if($search_process == $project_process_master_list_data[$process_count]["project_process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_process_master_list_data[$process_count]["project_process_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_task">
			  <option value="">- - Select Task - -</option>
			  <?php
			  for($task_count = 0; $task_count < count($project_task_master_list_data); $task_count++)
			  {
			  ?>
			  <option value="<?php echo $project_task_master_list_data[$task_count]["project_task_master_id"]; ?>" <?php if($search_task == $project_task_master_list_data[$task_count]["project_task_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_task_master_list_data[$task_count]["project_task_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>	-->

			   <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_machine_vendor">
			  <option value="">- - Select Vendor - -</option>
			  <?php
			  for($vendor_count = 0; $vendor_count < count($project_machine_vendor_list_data); $vendor_count++)
			  {
			  ?>
			  <option value="<?php echo $project_machine_vendor_list_data[$vendor_count]["project_machine_vendor_master_id"]; ?>" <?php if($search_machine_vendor == $project_machine_vendor_list_data[$vendor_count]["project_machine_vendor_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_machine_vendor_list_data[$vendor_count]["project_machine_vendor_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>


			   <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_machine_type">
			  <option value="">- - Select Machine Type - -</option>
			  <option value="OWN" <?php if($search_machine_type == 'OWN') { ?> selected="selected" <?php } ?>>- - Own - -</option>
			  <option value="Rent" <?php if($search_machine_type == 'RENT') { ?> selected="selected" <?php } ?>>- - Rent - -</option>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_start_date" value="<?php echo $start_date_filter; ?>" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_end_date" value="<?php echo $end_date_filter; ?>" />
			  </span>
			  <input type="submit" name="app_mac_search_submit" />
			  </form>
            </div>
			<?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th style="word-wrap:break-word;">SL No</th>
					<th style="word-wrap:break-word;">Project</th>
				    <th style="word-wrap:break-word;">Process</th>
					<th style="word-wrap:break-word;">Task Name</th>
					<th style="word-wrap:break-word;">Work Type</th>
					<th style="word-wrap:break-word;">%</th>
					<th style="word-wrap:break-word;">Machine Type</th>
					<th style="word-wrap:break-word;">Machine Vendor</th>
					<th style="word-wrap:break-word;">Machine</th>
					<th style="word-wrap:break-word;">Actual Start Date Time</th>
					<th style="word-wrap:break-word;">Actual End date Time</th>
					<th style="word-wrap:break-word;">Hours Worked</th>
					<th style="word-wrap:break-word;">Off Time</th>
					<th style="word-wrap:break-word;">Machine Rate</th>
					<th style="word-wrap:break-word;">Additional Cost</th>
					<th style="word-wrap:break-word;">Machine Bata</th>
					<th style="word-wrap:break-word;">Machine Issued Fuel</th>
					<th style="word-wrap:break-word;">Total Amount</th>
                    <th style="word-wrap:break-word;">Remarks</th>
                    <th style="word-wrap:break-word;">Approved By</th>
					<th style="word-wrap:break-word;">Approved On</th>
					<th colspan="2" style="text-align:center;">Actions</th>

				</tr>
				</thead>
				<tbody>
				<form method="post" action="project_actual_approved_machine_list.php">
				<?php
				if($actual_machine_plan_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($actual_machine_plan_list_data); $count++)
					{
						$machine_rate = $actual_machine_plan_list_data[$count]['project_task_actual_machine_fuel_charges'];

						//get stock quantity
						$project_machine_rate_master_search_data = array("machine_id"=>$actual_machine_plan_list_data[$count]["project_task_machine_id"]);
						$project_machine_rate_master_data =  i_get_project_machine_rate_master($project_machine_rate_master_search_data);
						if($project_machine_rate_master_data["status"] == SUCCESS)
						{
							$vendor_machine_number = $project_machine_rate_master_data["data"][0]["project_machine_master_id_number"];
							$machine_type = $project_machine_rate_master_data["data"][0]["project_machine_type"];
						}
						else
						{
							$vendor_machine_number = 'NA';
							$machine_type = 'NA';
						}

						//get stock quantity
						$machine_vendor = $actual_machine_plan_list_data[$count]['project_machine_vendor_master_name'];

						//No of hrs worked
						if($actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_end_date_time"] != "0000-00-00 00:00:00")
						{
							$start_date_time = strtotime($actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_start_date_time"]);
							$end_date_time = strtotime($actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_end_date_time"]);
							$date_diff = $end_date_time - $start_date_time;
							$no_hrs_worked = $date_diff/3600;

							$end_date_display = $actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_end_date_time"];
						}
						else
						{
							$start_date_time = strtotime($actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_start_date_time"]);
							$end_date_time = strtotime(date("Y-m-d H:i:s"));
							$date_diff = $end_date_time - $start_date_time;
							$no_hrs_worked = $date_diff/3600;

							$end_date_display = date('Y-m-d H:i:s');
						}

						// Off time related calculation
						$off_time = $actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_off_time"]/60;
						$eff_hrs = $no_hrs_worked - $off_time;

						$cost = ($machine_rate * $eff_hrs) + $actual_machine_plan_list_data[$count]["project_task_actual_machine_bata"] + $actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_additional_cost"] - $actual_machine_plan_list_data[$count]["project_task_actual_machine_issued_fuel"];

						// Check if this plan ID is already taken for approval and payment
						$project_payment_machine_mapping_search_data = array('machine_actuals_id'=>$actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_id"]);
						$mapped_sresult = i_get_project_payment_machine_mapping($project_payment_machine_mapping_search_data);

						if($mapped_sresult['status'] != SUCCESS)
						{
							$sl_no++;
					?>
					<input type="hidden" name="hd_machine_count" value="<?php echo count($actual_machine_plan_list_data); ?>" />
					<input type="hidden" name="hd_rate_count_<?php echo $count; ?>" value="<?php echo $cost; ?>" />
					<input type="hidden" name="hd_machine_date_<?php echo $count; ?>" value="<?php echo date('Y-m-d',strtotime($actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_start_date_time"])); ?>" />
					<input type="hidden" name="hd_vendor_count_<?php echo $count; ?>" value="<?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_vendor"]; ?>" />

					<tr>
					<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_process_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_task_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $work_type = $actual_machine_plan_list_data[$count]["project_task_actual_machine_work_type"]; ?></td>
					<td style="word-wrap:break-word;"><?php if($work_type == "Regular"){ echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_completion"]; } elseif($work_type == "Rework"){ echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_rework_completion"]; } ?></td>
					<td style="word-wrap:break-word;"><?php echo $machine_type; ?></td>
					<td style="word-wrap:break-word;"><?php echo $machine_vendor; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_machine_master_name"]; ?>-<?php echo $vendor_machine_number; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i:s",strtotime($actual_machine_plan_list_data[$count][
					"project_task_actual_machine_plan_start_date_time"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i:s",strtotime($end_date_display)); ?></td>
					<td style="word-wrap:break-word;"><?php echo round($no_hrs_worked,2) ; ?></td>
					<td style="word-wrap:break-word;"><?php echo round($off_time,2) ; ?></td>
					<td style="word-wrap:break-word;"><?php echo $machine_rate ; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_additional_cost"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_bata"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_issued_fuel"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo round($cost); ?></td>

					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_remarks"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["approver"]; ?></td>
					<td><?php echo date('d-M-Y H:i:s',strtotime($actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_approved_on"])); ?></td>
					<td></td>
					<td><input type="checkbox" name="cb_item_<?php echo $count; ?>" value="<?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_id"] ;?>"><br></td>
					</tr>
					<?php
						}
					}

				}
				else
				{
				?>
				<td colspan="21">No machine details added yet!</td>

				<?php
				}
				 ?>

                </tbody>
              </table>
			  <?php
			   if(($search_machine_vendor != "") && ($search_project != "") && ($search_machine != "") && ($add_perms_list['status'] == SUCCESS) && ($actual_machine_plan_list["status"] == SUCCESS))
					{
						?>
					<div cass="modal-footer">
						<button type="submit" name="items_submit" class="btn btn-primary">Submit</button>
					</div>
					<?php
					}
				?>
			  <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_check_task_actual_machine(machine_id,task_id,search_project)
{
	var ok = confirm("Are you sure you want to Ok?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
						document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
						document.getElementById("span_msg").style.color = "red";
					}
					else
					{
						window.location = "project_actual_machine_planning_list.php?search_project=" +search_project;
					}
				}
			}

			xmlhttp.open("POST", "project_check_machine.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("machine_actuals_id=" + machine_id + "&task_id=" +task_id+ "&action=approved");
		}
	}
}

function get_machine_list()
{
	var searchstring = document.getElementById('stxt_machine').value;

	if(searchstring.length >= 3)
	{
		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}

		xmlhttp.onreadystatechange = function()

		{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{
				if(xmlhttp.responseText != 'FAILURE')

				{
					document.getElementById('search_results').style.display = 'block';

					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;
				}

			}

		}

		xmlhttp.open("POST", "ajax/project_get_machine.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring);
	}

	else

	{
		document.getElementById('search_results').style.display = 'none';
	}
}

function select_machine(machine_id,search_machine,number)
{
	document.getElementById('hd_machine_id').value 	= machine_id;
	document.getElementById('stxt_machine').value = number;
	//var name_code = search_machine.concat('-',number);
	//document.getElementById('stxt_machine').value = name_code;
	document.getElementById('search_results').style.display = 'none';
}
</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>
