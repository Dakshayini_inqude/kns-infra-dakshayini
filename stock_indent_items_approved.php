<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/* FILE HEADER - START */

// LAST UPDATED ON: 5th Sep 2015

// LAST UPDATED BY: Nitin Kashyap

/* FILE HEADER - END */



/* TBD - START */

/* TBD - END */
$_SESSION['module'] = 'Stock Transactions';


/* DEFINES - START */

define('QUOTATION_FUNC_ID','167');

/* DEFINES - END */



/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

/* INCLUDES - END */



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	

	// Get permissions

	$add_perms_list    = i_get_user_perms($user,'',QUOTATION_FUNC_ID,'1','1');	



	/* DATA INITIALIZATION - START */

	$alert_type = -1;

	$alert = "";

	/* DATA INITIALIZATION - END */

	/* QUERY STRING - START */

	if(isset($_REQUEST["indent_id"]))

	{

		$indent_id = $_REQUEST["indent_id"];

	}

	else

	{

		$indent_id = "";

	}

	if(isset($_POST["status"]))

	{

		$indent_status = $_POST["status"];

	}

	else

	{

		$indent_status = "";

	}
	
	if(isset($_GET["page"]))
	{
		$page = $_GET["page"];
	}
	else
	{
		$page = 1;
	}
	
	$start = (string)($page - 1) * 10;
	$limit = 10;

	if(isset($_POST["ii_search_submit"]))

	{

		$search_status = $_POST["search_status"];
		$project       = $_POST["ddl_project"];
	}

	else

	{

		$search_status = 'Pending';
		if(isset($_GET['project']))
		{
			$project = $_GET['project'];
		}
		else
		{
			$project      = '-1';
		}		
	}
	
	if(isset($_POST["hd_material_id"]))
	{
		if($_POST["stxt_material"] != '')
		{
			$search_material 	  = $_POST["hd_material_id"];
			$search_material_name = $_POST["stxt_material"];
         }
		else
		{
			$search_material 	  = "";
			$search_material_name = "";	
		}		
	}
	else
	{				
		if(isset($_GET['hd_material_id']))
		{
			if($_GET["material_name"] != '')
			{
				$search_material 	  = $_GET["hd_material_id"];
				$search_material_name = $_GET["material_name"];	
			}
			else
			{
				$search_material 	  = "";
				$search_material_name = "";
			}
		}
		else
		{
			$search_material 	  = "";
			$search_material_name = "";
		}
	}
	
	// Get material modes already added
	$stock_material_search_data = array("material_active"=>'1');
	$material_list = i_get_stock_material_master_list($stock_material_search_data);
	if($material_list['status'] == SUCCESS)
	{
		$material_list_data = $material_list['data'];
	}

	else
	{
		$alert = $material_list["data"];

		$alert_type = 0;
	}

	//Get Uom List

	$stock_unit_search_data = array();

	$uom_list = i_get_stock_unit_measure_list($stock_unit_search_data);

	if($uom_list["status"] == SUCCESS)

	{

		$uom_list_data = $uom_list["data"];

	}

	else

	{

		$alert = $uom_list["data"];

		$alert_type = 0;

	}
	

	// Get Indent Item Details		
	$stock_indent_search_data = array("start"=>$start,"limit"=>$limit,"active"=>'1','approved_on'=>'1','project'=>$project,"material_id"=>$search_material,"status"=>'Approved');	
	$indent_item_list = i_get_indent_sum_of_items_list($stock_indent_search_data);	
	if($indent_item_list["status"] == SUCCESS)

	{

		$indent_item_list_data = $indent_item_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$indent_item_list["data"];

	}
	
	// Check if there are more indents to be processed for the filtered criterion
	$stock_indent_search_data_next = array("status"=>'Approved',"is_quote"=>'1',"start"=>($page * 10),"limit"=>$limit,"active"=>'1','approved_on'=>'1','project'=>$project,"material_id"=>$search_material);	
	$indent_item_list_next = i_get_indent_sum_of_items_list($stock_indent_search_data_next);	
	
	if($indent_item_list_next["status"] == SUCCESS)
	{
		$show_next = true;
	}	
	else
	{
		$show_next = false;
	}
	
	// Get Project List
	$stock_project_search_data = array();
	$project_result_list = i_get_project_list($stock_project_search_data);
	if($project_result_list['status'] == SUCCESS)
	{
		$project_result_list_data = $project_result_list['data'];		
	}	

	else

	{
		$alert = $project_result_list["data"];
		$alert_type = 0;		

	}

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Quotation Process</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>    



<div class="main">

	

	<div class="main-inner">



	    <div class="container">

	

	      <div class="row">

	      	

	      	<div class="span12">      		

	      		

	      		<div class="widget ">

	      		

					

					<div class="widget-content">

						

	

								

								

			 <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>Quotation process</h3><?php if($add_perms_list['status'] == SUCCESS){ ?><span style="float:right; padding-right:20px;"><a href="stock_add_indent.php">Add Indent</a></span><?php } ?>

            </div>

			<div class="widget-header" style="height:50px; padding-top:10px;">               

			  <form method="post" id="file_search_form" action="stock_indent_items_approved.php">

			  <span style="padding-left:20px; padding-right:20px; float:left;">

			  <select name="search_status">

				<option value="Pending" <?php if($search_status == "Pending"){?> selected <?php } ?>>Pending</option>			

				<option value="Completed" <?php if($search_status == "Completed"){?> selected <?php } ?>>Completed</option>

			  </select>

			  </span>
			  <span style="padding-right:20px; float:left;">
			  <select name="ddl_project">
				<option value="">- - Select Project - -</option>
				<?php
				for($count = 0; $count < count($project_result_list_data); $count++)
				{
				?>
				<option value="<?php echo $project_result_list_data[$count]["stock_project_id"]; ?>"<?php if($project_result_list_data[$count]["stock_project_id"] == $project){?> selected="selected" <?php } ?>><?php echo $project_result_list_data[$count]["stock_project_name"]; ?></option>
				<?php
				}
				?>
			  </select>
			  </span>
			  
			  <input type="hidden" name="hd_material_id" id="hd_material_id" value="<?php echo $search_material; ?>" />			  

			  <span style="padding-right:20px; float:left;">										

					<input type="text" name="stxt_material" autocomplete="off" id="stxt_material" onkeyup="return get_material_list();" placeholder="Search Material by name or code" value="<?php echo $search_material_name; ?>" />

					<div id="search_results" class="dropdown-content"></div>

			  </span>

			<input type="submit" name="ii_search_submit" />

			  </form>			  

            </div>

            <!-- /widget-header -->

            <div class="widget-content">

			<form action="task_list.php" method="post" id="task_update_form">	

			<input type="hidden" name="hd_indent_id" value="<?php echo $indent_id; ?>" />			

			<input type="hidden" name="indent_status" value="<?php echo $indent_status; ?>" />			

			

              <table class="table table-bordered"  style="table-layout: fixed;">

                <thead>

                  <tr>

					<th style="word-wrap:break-word;">Sl No</th>

					<th style="word-wrap:break-word;">Material</th>

					<th style="word-wrap:break-word;">Material Code</th>

					<th style="word-wrap:break-word;">Project</th>

					<th style="word-wrap:break-word;">Total Indent qty</th>

					<th style="word-wrap:break-word;">Stock Qty</th>

					<th style="word-wrap:break-word;">Status</th>
			
					<th style="word-wrap:break-word;">Requested By</th>

					<th colspan="2" style="text-align:center;">Actions</th>	

				</tr>

				</thead>

				<tbody>

				 <?php

				 if($indent_item_list["status"] == SUCCESS)

				 {

					 $sl_no = 0;

						for($count = 0; $count < count($indent_item_list_data); $count++)

						{		
							$sl_no++;
							$material_stock_search_data = array("material_id"=>$indent_item_list_data[$count]["stock_indent_item_material_id"],"project"=>$indent_item_list_data[$count]["stock_indent_project"]);
							$stock_material = i_get_material_stock($material_stock_search_data);

							if($stock_material["status"] == SUCCESS)

							{

								$stock_material_data = $stock_material["data"];

								$qunatity = $stock_material_data[0]["material_stock_quantity"];

								$material = $stock_material_data[0]["material_id"];

							}

							else

							{

								$alert = $alert."Alert: ".$stock_material["data"];

								$qunatity = "0";

								

							}

							// Get Stock Issue List

							$issued_qty ="0";

							$stock_issue_search_data = array("indent_item_id"=>$indent_item_list_data[$count]["stock_indent_item_id"],"indent_id"=>$indent_id);

							$stock_issue_list = i_get_stock_issue($stock_issue_search_data);

							if($stock_issue_list["status"] ==  SUCCESS)

							{

								for($qty_count = 0 ; $qty_count < count($stock_issue_list["data"]) ; $qty_count++)

								{

								$stock_issue_list_data = $stock_issue_list["data"];

								$issued_qty = $issued_qty + $stock_issue_list_data[$qty_count]["stock_issue_qty"];

								}

							}

							else

							{

								$issued_qty = "0";

							}

							// Get Returnable Type from material master

							$stock_material_search_data = array("material_id"=>$indent_item_list_data[$count]["stock_indent_item_material_id"]);

							$material_master_list = i_get_stock_material_master_list($stock_material_search_data);

							if($material_master_list["status"] == SUCCESS)

							{

								$returnable_type = $material_master_list["data"][0]["stock_material_type"];

							}

							

						?>				

						<tr>

							
								

									<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>

									<td style="word-wrap:break-word;"><?php echo $indent_item_list_data[$count]["stock_material_name"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $indent_item_list_data[$count]["stock_material_code"]; ?></td>

									<td><?php echo $indent_item_list_data[$count]["stock_project_name"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $required_qunatity = $indent_item_list_data[$count]["total_stock_indent_item_quantity"]; ?></td>

									<td <?php if( $qunatity > $required_qunatity ) { ?> style="color:#00FF00;" <?php } else { ?> style="color:#FF0000;" <?php } ?>><strong><?php echo $qunatity ; ?></strong></td>

									<td style="word-wrap:break-word;"> &nbsp;     </td>

									<td style="word-wrap:break-word;"> <?php echo $indent_item_list_data[$count]["user_name"]; ?></td>														

									<td style="word-wrap:break-word;"><a href="stock_master_vendor_item_mapping_list.php?indent_id=<?php echo $indent_item_list_data[$count]["stock_indent_id"]; ?>&indent_item_id=<?php echo $indent_item_list_data[$count]["stock_indent_item_material_id"]; ?>" target="_blank">View Vendors</a></td>

									<td style="word-wrap:break-word;"><a href="stock_add_quotation_compare.php?indent_id=<?php echo $indent_item_list_data[$count]["stock_indent_id"]; ?>&indent_item_id=<?php echo $indent_item_list_data[$count]["stock_indent_item_material_id"];?>&project=<?php echo $indent_item_list_data[$count]["stock_indent_project"]; ?>">Add Quotations</a></td>	
								

							

						</tr>

						<?php 		

					}
					
						$next_page = $page + 1;
						$prev_page = $page - 1;
				

				}

				else

				{

				?>

				<td colspan="9">No Items added</td>
				

				<?php

				}

				 ?>	



                </tbody>

				</table>
				<br/>	
				<br/>	
				<?php
				if($page > 1)
				{
				?>
				<a href="stock_indent_items_approved.php?page=<?php echo $prev_page; ?>&project=<?php echo $project ;?>&hd_material_id=<?php echo $search_material ;?>&material_name=<?php echo $search_material_name; ?>" class="pull-left">Prev Page</a>
				<?php
				}
				?>
				<?php
				if($show_next == true)
				{
				?>
				<a href="stock_indent_items_approved.php?page=<?php echo $next_page; ?>&project=<?php echo $project; ?>&hd_material_id=<?php echo $search_material; ?>&material_name=<?php echo $search_material_name; ?>" class="pull-right">Next Page</a>
				<?php
				}
				?>
				<br/>		

				<div class="modal-body">

			    <div class="row">

				  </div>

				  </div>			  

			</form>

            </div>

            <!-- /widget-content --> 

          </div>

								</div> <!-- /controls -->	                                                

							</div> <!-- /control-group -->

  

						  

					</div> <!-- /widget-content -->

						

				</div> <!-- /widget -->

	      		

		    </div> <!-- /span8 -->

	      	

	      </div> <!-- /row -->

	

	    </div> <!-- /container -->

	    

	</div> <!-- /main-inner -->

    

</div> <!-- /main -->

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->  

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function get_products(count,material)

{			

	product_type = document.getElementById("ddl_material_type").value;			

	if (window.XMLHttpRequest)

	{// code for IE7+, Firefox, Chrome, Opera, Safari

		xmlhttp = new XMLHttpRequest();

	}

	else

	{// code for IE6, IE5

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

	}



	xmlhttp.onreadystatechange = function()

	{				

		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

		{																	

			document.getElementById("product_area").innerHTML = xmlhttp.responseText;					

		}

	}			

	

	xmlhttp.open("POST", "select_material.php");   // file name where delete code is written

	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	xmlhttp.send("product_type=" + product_type);

}



function go_to_issue_item(indent_id,indent_item_id,returnable_type)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_add_issue.php");

	

	var hiddenField2 = document.createElement("input");

	hiddenField2.setAttribute("type","hidden");

	hiddenField2.setAttribute("name","indent_id");

	hiddenField2.setAttribute("value",indent_id);

	

	var hiddenField3 = document.createElement("input");

	hiddenField3.setAttribute("type","hidden");

	hiddenField3.setAttribute("name","indent_item_id");

	hiddenField3.setAttribute("value",indent_item_id);

	

	var hiddenField4 = document.createElement("input");

	hiddenField4.setAttribute("type","hidden");

	hiddenField4.setAttribute("name","returnable_type");

	hiddenField4.setAttribute("value",returnable_type);

    	

	form.appendChild(hiddenField2);	

	form.appendChild(hiddenField3);	

	form.appendChild(hiddenField4);	

	

	document.body.appendChild(form);

    form.submit();

}

function go_to_view_quotation(indent_id,indent_item_id)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_quotation_compare_list.php");

	

	var hiddenField2 = document.createElement("input");

	hiddenField2.setAttribute("type","hidden");

	hiddenField2.setAttribute("name","indent_id");

	hiddenField2.setAttribute("value",indent_id);

	

	var hiddenField3 = document.createElement("input");

	hiddenField3.setAttribute("type","hidden");

	hiddenField3.setAttribute("name","indent_item_id");

	hiddenField3.setAttribute("value",indent_item_id);

    	

	form.appendChild(hiddenField2);	

	form.appendChild(hiddenField3);	

	

	document.body.appendChild(form);

    form.submit();

}

function go_to_view_vendor(indent_id,indent_item_id)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_vendor_item_mapping_list.php");

	

	var hiddenField2 = document.createElement("input");

	hiddenField2.setAttribute("type","hidden");

	hiddenField2.setAttribute("name","indent_id");

	hiddenField2.setAttribute("value",indent_id);

	

	var hiddenField3 = document.createElement("input");

	hiddenField3.setAttribute("type","hidden");

	hiddenField3.setAttribute("name","indent_item_id");

	hiddenField3.setAttribute("value",indent_item_id);

    	

	form.appendChild(hiddenField2);	

	form.appendChild(hiddenField3);	

	

	document.body.appendChild(form);

    form.submit();

}function go_to_quotation(indent_id,indent_item_id)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_add_quotation_compare.php");

	

	var hiddenField2 = document.createElement("input");

	hiddenField2.setAttribute("type","hidden");

	hiddenField2.setAttribute("name","indent_id");

	hiddenField2.setAttribute("value",indent_id);

	

	var hiddenField3 = document.createElement("input");

	hiddenField3.setAttribute("type","hidden");

	hiddenField3.setAttribute("name","indent_item_id");

	hiddenField3.setAttribute("value",indent_item_id);

    	

	form.appendChild(hiddenField2);	

	form.appendChild(hiddenField3);	

	

	document.body.appendChild(form);

    form.submit();

}

function delete_indent_item(indent_item_id,indent_id)

{

	var ok = confirm("Are you sure you want to Delete?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

						var url = "http://kns/Legal/stock_indent_items_approved.php";

						window.location = url;

					}

				}

			}



			xmlhttp.open("POST", "delete_indent_item.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("indent_item_id=" + indent_item_id + "indent_id=" + indent_id + "&action=0");

		}

	}	

}
function get_material_list()

{ 

	var searchstring = document.getElementById('stxt_material').value;

	

	if(searchstring.length >= 3)

	{

		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}



		xmlhttp.onreadystatechange = function()

		{				

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{		

				if(xmlhttp.responseText != 'FAILURE')

				{

					document.getElementById('search_results').style.display = 'block';

					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;

				}

			}

		}



		xmlhttp.open("POST", "ajax/get_material.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring);				

	}

	else	

	{

		document.getElementById('search_results').style.display = 'none';

	}

}



function select_material(material_id,search_material)

{

	document.getElementById('hd_material_id').value 	= material_id;

	document.getElementById('stxt_material').value = search_material;

	

	document.getElementById('search_results').style.display = 'none';

}



</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>


</body>

</html>

