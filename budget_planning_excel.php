<?php
session_start();
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START */
define('PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID', '359');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '2', '1');
    $edit_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '3', '1');
    $delete_perms_list = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '4', '1');
    $add_perms_list    = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '1', '1');

    // Query String Data
    // Nothing

    // Temp data
    $alert_type = -1;
    $alert 	    = "";

    if (isset($_REQUEST['project_id'])) {
        $project_id = $_REQUEST['project_id'];
    } else {
        $project_id = "";
    }

    // Get Already added Object Output
    $project_task_planning_search_data = array("active"=>'1',"project_id"=>$project_id);

    $project_task_planning_list = i_get_project_task_planning($project_task_planning_search_data);
    if ($project_task_planning_list["status"] == SUCCESS) {
        $project_task_planning_list_data = $project_task_planning_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_task_planning_list["data"];
    }

    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    $filename = $project_task_planning_list["data"][0]['project_master_name']."-exported-data_".date("m-d-Y-h-m-s").".xlsx";
    header("Content-Disposition: attachment; filename=".$filename);
    header("Cache-Control: max-age=0");
} else {
    header("location:login.php");
}
?>

              <table>
                <thead>
                  <tr>
				    <th>Project</th>
				    <th>planned_project_id</th>
					<th>Process_name</th>
					<th>planned_process_id</th>
					<th>Task</th>
					<th>planned_task_id</th>
          <th>Road Number</th>
					<th>planned_road_id</th>
					<th>UOM</th>
					<th>Qty</th>
					<th>Manpower</th>
					<th>Machine</th>
					<th>Contract</th>
					<th>Material</th>

				</tr>
				</thead>
				<tbody>
				<?php
                      $sl_no = 0;
                      for ($count = 0; $count < count($project_task_planning_list_data); $count++) {
                          $sl_no ++ ;
                          $project_planned_data = array("task_id"=>$project_task_planning_list_data[$count]["project_task_planning_task_id"],"road_id"=>$project_task_planning_list_data[$count]["project_task_planning_no_of_roads"]);
                          $planned_data_list = db_get_planned_data($project_planned_data);
                          if($planned_data_list["status"] == DB_RECORD_ALREADY_EXISTS)
                          {
                            $planned_mp = $planned_data_list["data"][0]["planned_manpower"];
                            $planned_mc = $planned_data_list["data"][0]["planned_machine"];
                            $planned_cw = $planned_data_list["data"][0]["planned_contract"];
                            $planned_material = $planned_data_list["data"][0]["planned_material"];
                          }
                          else {
                            $planned_mp = 0;
                            $planned_mc = 0;
                            $planned_cw = 0;
                            $planned_material = 0;
                          }

                          if (($project_task_planning_list_data[$count]["project_task_planning_no_of_roads"] != "No Roads")) {
                              $road_name = $project_task_planning_list_data[$count]["project_site_location_mapping_master_name"];
                          } else {
                              $road_name = "No Roads";
                          } ?>

          <tr>
					<td><?php echo $project_task_planning_list_data[$count]["project_master_name"]; ?></td>
					<td><?php echo $project_task_planning_list_data[$count]["project_management_master_id"]; ?></td>
					<td><?php echo $project_task_planning_list_data[$count]["project_process_master_name"]; ?></td>
					<td><?php echo $project_task_planning_list_data[$count]["project_plan_process_id"]; ?></td>
					<td><?php echo $project_task_planning_list_data[$count]["project_task_master_name"]; ?></td>
					<td><?php echo $project_task_planning_list_data[$count]["project_process_task_id"]; ?></td>
          <td><?php echo $road_name; ?></td>
          <td><?php echo $project_task_planning_list_data[$count]["project_task_planning_no_of_roads"]; ?></td>

					<td><?php echo $project_task_planning_list_data[$count]["project_uom_name"]; ?></td>
					<td><?php echo $project_task_planning_list_data[$count]["project_task_planning_measurment"]; ?></td>
          <td><?php echo $planned_mp ;?></td>
          <td><?php echo $planned_mc ;?></td>
          <td><?php echo $planned_cw ;?></td>
          <td><?php echo $planned_material ;?></td>

					</tr>
					<?php
                    }
?>
                </tbody>
              </table>
