<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 14-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Capture the form data
	if(isset($_POST["add_project_plan_process_task_submit"]))
	{

		$process_id           = $_POST["ddl_process"];
		$task_type 	          = $_POST["ddl_task_type"];
		$actual_start_date 	  = $_POST["actual_start_date"];
		$actual_end_date 	  = $_POST["actual_end_date"];
		$remarks 	          = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($process_id != "") && ($task_type != "") && ($actual_start_date != "") && ($actual_end_date != ""))
		{
			if(((strtotime($actual_start_date)) <= (strtotime($actual_end_date))) && ((strtotime($actual_start_date)) >= strtotime(date('Y-m-d'))))
			{
				$project_plan_process_task_iresult = i_add_project_process_task($process_id,$task_type,$actual_start_date,$actual_end_date,$remarks,$user);
				
				if($project_plan_process_task_iresult["status"] == SUCCESS)
				
				{	
					$alert_type = 1;
				}
				
				$alert = $project_plan_process_task_iresult["data"];
			}
			else
			{
				$alert_type = 0;
				$alert      = 'Start Date and End Date cannot be earlier than today. End Date cannot be earlier than start date';
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

  // Get Project Process Master modes already added
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list['status'] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $project_process_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Project Task Master modes already added
	$project_task_master_search_data = array("active"=>'1');
	$project_task_master_list = i_get_project_task_master($project_task_master_search_data);
	if($project_task_master_list['status'] == SUCCESS)
	{
		$project_task_master_list_data = $project_task_master_list['data'];
	}

	else
	{
		$alert = $project_task_master_list["data"];
		$alert_type = 0;
	}
	
// Get Stock Project Plan Process Task modes already added
	$project_process_task_search_data = array();
	$project_plan_process_task_list = i_get_project_process_task($project_process_task_search_data);
	if($project_plan_process_task_list['status'] == SUCCESS)
	{
		$project_plan_process_task_list_data = $project_plan_process_task_list['data'];
	}
}

else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Project Plan Process Task</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Project Plan Process Task</h3><span style="float:right; padding-right:20px;"><a href="project_plan_process_task_list.php">Project Plan Process Task List</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Project Plan Process Task</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_add_plan_process_task_form" class="form-horizontal" method="post" action="project_add_plan_process_task.php">
									<fieldset>										
																										
											<div class="control-group">											
											<label class="control-label" for="ddl_process">Process*</label>
											<div class="controls">
												<select name="ddl_process" required>
												<option>- - Select Process - -</option>
												<?php
												for($count = 0; $count < count($project_process_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_process_master_list_data[$count]["project_process_master_id"]; ?>"><?php echo $project_process_master_list_data[$count]["project_process_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
									
										<div class="control-group">											
											<label class="control-label" for="ddl_task_type">Task Type*</label>
											<div class="controls">
												<select name="ddl_task_type" required>
												<option>- - Select Task - -</option>
												<?php
												for($count = 0; $count < count($project_task_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_task_master_list_data[$count]["project_task_master_id"]; ?>"><?php echo $project_task_master_list_data[$count]["project_task_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="actual_start_date">Actual Start Date</label>
											<div class="controls">
												<input type="date" class="span6" name="actual_start_date" placeholder="Start Date" required="required">
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="actual_end_date">Actual End Date</label>
											<div class="controls">
												<input type="date" class="span6" name="actual_end_date" placeholder="End Date" required="required">
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_project_plan_process_task_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
