<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: kns_grn_list.php

CREATED ON	: 29-Sep-2016

CREATED BY	: Lakshmi

PURPOSE     : List of grn for customer withdrawals

*/



/*

TBD: 

*/
$_SESSION['module'] = 'Stock Transactions';


/* DEFINES - START */

define('GRN_FUNC_ID','171');

/* DEFINES - END */



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	

	// Get permission settings for this user for this page

	$view_perms_list   = i_get_user_perms($user,'',GRN_FUNC_ID,'2','1');

	$edit_perms_list   = i_get_user_perms($user,'',GRN_FUNC_ID,'3','1');

	$delete_perms_list = i_get_user_perms($user,'',GRN_FUNC_ID,'4','1');

	$add_perms_list    = i_get_user_perms($user,'',GRN_FUNC_ID,'1','1');



	// Query String Data

	if(isset($_REQUEST["order_id"]))

	{

		$order_id = $_REQUEST["order_id"];

	}

	else

	{

		$order_id = "-1";

	}

	if(isset($_REQUEST["grn_id"]))

	{

		$grn_id = $_REQUEST["grn_id"];

	}

	else

	{

		$grn_id = "-1";

	}

	

	if(isset($_POST['grn_search_submit']))

	{
		$project = $_POST['ddl_project'];
	}

	else

	{
		$project = '-1';
	}

	

	// Get GRN List

	if($order_id != "-1")

	{

		$stock_grn_search_data = array("purchase_order_id"=>$order_id,"active"=>'1');
		if($project != '')
		{
			$stock_grn_search_data['project'] = $project;
		}

		$grn_list = i_get_stock_grn_list($stock_grn_search_data);

		if($grn_list['status'] == SUCCESS)

		{

			$grn_list_data = $grn_list['data'];

		}	

	}

	else

	{

		$stock_grn_search_data = array("active"=>'1');
		if($project != '')
		{
			$stock_grn_search_data['project'] = $project;
		}

		$grn_list = i_get_stock_grn_list($stock_grn_search_data);		

		if($grn_list['status'] == SUCCESS)

		{

			$grn_list_data = $grn_list['data'];

		}		

	}

	
	// Get Project List
	$stock_project_search_data = array();
	$project_result_list = i_get_project_list($stock_project_search_data);
	if($project_result_list['status'] == SUCCESS)
	{
		$project_result_list_data = $project_result_list['data'];		
	}	

	else

	{
		$alert = $project_result_list["data"];
		$alert_type = 0;		

	}

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>GRN List</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>

    



<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">

       

          <div class="span6" style="width:100%;">

          

          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>GRN List</h3><?php if($add_perms_list['status'] == SUCCESS){ ?><span style="float:right; padding-right:20px;"><a href="stock_add_grn.php">Add GRN</a></span><?php } ?>

            </div>

            <!-- /widget-header -->

			<div class="widget-header" style="height:50px; padding-top:10px;">               

			  <form method="post" id="grn_search_form" action="stock_grn_list.php">			  			  

			  <span style="padding-right:20px;">
			  <select name="ddl_project">
				<option value="">- - Select Project - -</option>
				<?php
				for($count = 0; $count < count($project_result_list_data); $count++)
				{

				?>
				<option value="<?php echo $project_result_list_data[$count]["stock_project_id"]; ?>"<?php if($project_result_list_data[$count]["stock_project_id"] == $project){?> selected="selected" <?php } ?>><?php echo $project_result_list_data[$count]["stock_project_name"]; ?></option>
				<?php

				}

				?>

			  </select>

			  </span>

			  <input type="submit" name="grn_search_submit" />

			  </form>			  

            </div>

            <!-- /widget-header -->

            <div class="widget-content">

			

              <table class="table table-bordered" style="table-layout: fixed;">

                <thead>

                  <tr>

				    <th style="word-wrap:break-word;">SL No</th>

					<th style="word-wrap:break-word;">GRN No</th>

					<th style="word-wrap:break-word;">Purchase Order No</th>

					<th style="word-wrap:break-word;">Invoice Number</th>

					<th style="word-wrap:break-word;">Vendor</th>

					<th style="word-wrap:break-word;">GRN Date</th>	

					<th style="word-wrap:break-word;">Invoice Date</th>

					<th style="word-wrap:break-word;">DC Number</th>

				    <th style="word-wrap:break-word;">DC Date</th>

					<th style="word-wrap:break-word;">Vehicle Number</th>

					<th style="word-wrap:break-word;">Project</th>

					<th style="word-wrap:break-word;">Added By</th>		

					<th colspan="3" style="text-align:center;">Action</th>													

				</tr>

				</thead>

				<tbody>							

				<?php

				if($grn_list["status"] == SUCCESS)

				{

					$sl_no = 0;

					for($count = 0; $count < count($grn_list_data); $count++)

					{

						$sl_no++;

					?>

					<tr>

					<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>

					<td style="word-wrap:break-word;"><?php echo $grn_list_data[$count]["stock_grn_no"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo $grn_list_data[$count]["stock_purchase_order_number"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo $grn_list_data[$count]["stock_grn_invoice_number"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo $grn_list_data[$count]["stock_vendor_name"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($grn_list_data[$count]["stock_grn_added_on"])); ?></td>

					<td style="word-wrap:break-word;"><?php echo get_formatted_date($grn_list_data[$count][

					"stock_grn_invoice_date"],"d-M-Y"); ?></td>

					<td style="word-wrap:break-word;"><?php echo $grn_list_data[$count]["stock_grn_dc_number"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo get_formatted_date($grn_list_data[$count]

				    ["stock_grn_dc_date"],"d-M-Y"); ?></td>

					<td style="word-wrap:break-word;"><?php echo $grn_list_data[$count]["stock_grn_vehicle_number"]; ?></td>
					<td><?php echo $grn_list_data[$count]["stock_project_name"]; ?></td>
		
					</td>

					<td style="word-wrap:break-word;"><?php echo $grn_list_data[$count]["user_name"]; ?></td>

					<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){ ?><a style="padding-right:10px" href="#" onclick="return go_to_grn_edit('<?php echo $grn_list_data[$count]

					["stock_grn_id"]; ?>','<?php echo $grn_list_data[$count]["stock_grn_purchase_order_id"]; ?>');">Edit</a><?php } ?></td>

					<!--<td style="word-wrap:break-word;"><?php if(($grn_list_data[$count]["stock_grn_active"] == "1")){?><a href="#" 

					onclick="return delete_grn(<?php echo $grn_list_data[$count]["stock_grn_id"]; ?>);">Delete</a><?php } ?></td> -->

					<td style="word-wrap:break-word;"><a href="stock_grn.php?grn_id=<?php echo $grn_list_data[$count]["stock_grn_id"]; ?>" target="_blank">Print GRN</a></td>
					
					<td><a href="stock_grn_upload_documents.php?grn_id=<?php echo $grn_list_data[$count]["stock_grn_id"]; ?>" target="_blank">Documents</a></td>

					</tr>

					<?php									

					}

				}

				else

				{

				?>

				<td colspan="14">No GRN added yet!</td>

				<?php

				}

				 ?>	



                </tbody>

              </table>

            </div>

            <!-- /widget-content --> 

          </div>

          <!-- /widget --> 

         

          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function delete_grn(grn_id,order_id)

{

	var ok = confirm("Are you sure you want to Delete?")

	{         

		if (ok)

		{

			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "stock_grn_list.php";

					}

				}

			}



			xmlhttp.open("POST", "stock_delete_grn.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("grn_id=" + grn_id + "&order_id=" + order_id + "&action=0");

		}

	}	

}



function go_to_grn_edit(grn_id,order_id)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_edit_grn.php");

	

	var hiddenField1 = document.createElement("input");

	hiddenField1.setAttribute("type","hidden");

	hiddenField1.setAttribute("name","grn_id");

	hiddenField1.setAttribute("value",grn_id);

	

	var hiddenField2 = document.createElement("input");

	hiddenField2.setAttribute("type","hidden");

	hiddenField2.setAttribute("name","order_id");

	hiddenField2.setAttribute("value",order_id);

	

	form.appendChild(hiddenField1);

	

	form.appendChild(hiddenField2);

	

	document.body.appendChild(form);

    form.submit();

}

</script>

<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>



  </body>



</html>