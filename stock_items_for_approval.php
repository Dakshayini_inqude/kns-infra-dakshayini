<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');
 
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$indent_id      = $_POST["indent_id"];
	$status   	    = "Pending";
	$requested_date = date("Y-m-d H:i:s");
	$approved_by    = $user;
	
	$indent_update_data = array("status"=>'Processed');
	$update_indent_uresult = db_update_indent($indent_id,$indent_update_data);

	$indent_items_update_data = array("status"=>$status,"requested_date"=>$requested_date);
	$update_item_result = i_update_indent_items('',$indent_id,$indent_items_update_data);
	
	if($update_indent_uresult["status"] == SUCCESS)
	{
		echo 'SUCCESS';
	}
	else
	{
		echo $update_indent_uresult["data"];
	}
}
else
{
	header("location:login.php");
}
?>