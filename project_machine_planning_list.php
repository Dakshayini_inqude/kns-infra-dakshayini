<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_user_mapping_list.php
CREATED ON	: 28-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/
/* DEFINES - START */
define('PROJECT_MACHINE_PLANNING_LIST_FUNC_ID','271');
/* DEFINES - END */
/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$add_perms_list     = i_get_user_perms($user,'',PROJECT_MACHINE_PLANNING_LIST_FUNC_ID,'1','1');
	$view_perms_list    = i_get_user_perms($user,'',PROJECT_MACHINE_PLANNING_LIST_FUNC_ID,'2','1');
	$edit_perms_list    = i_get_user_perms($user,'',PROJECT_MACHINE_PLANNING_LIST_FUNC_ID,'3','1');
	$delete_perms_list  = i_get_user_perms($user,'',PROJECT_MACHINE_PLANNING_LIST_FUNC_ID,'4','1');


	// Query String Data
	// Nothing

	if(isset($_GET['project_process_task_id']))
	{
		$task_id = $_GET['project_process_task_id'];
	}
	else
	{
		$task_id = '';
	}

	if(isset($_POST['mac_plan_search_submit']))
	{
		$search_project = $_POST['search_project'];
	}
	else
	{
		$search_project = '';
	}

	$search_process  	 = "";

	if(isset($_POST["search_process"]))
	{
		$search_process   = $_POST["search_process"];
	}

	$search_task  	 = "";

	if(isset($_POST["search_task"]))
	{
		$search_task   = $_POST["search_task"];
	}

	$project_machine_planning_search_data = array("active"=>'1',"task_id"=>$task_id,"project_id"=>$search_project,"process"=>$search_process,"task"=>$search_task);
	$project_machine_planning_list = i_get_project_machine_planning($project_machine_planning_search_data);
	if($project_machine_planning_list["status"] == SUCCESS)
	{
		$project_machine_planning_list_data = $project_machine_planning_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_machine_planning_list["data"];
	}

	// Get project master
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_list["data"];
	}

	// Process Master
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list["status"] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_process_master_list["data"];
	}

	// Task Master
	$project_task_master_search_data = array("active"=>'1',"process"=>$search_process);
	$project_task_master_list = i_get_project_task_master($project_task_master_search_data);
	if($project_task_master_list["status"] == SUCCESS)
	{
		$project_task_master_list_data = $project_task_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_task_master_list["data"];
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Machine Planning List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Machine Planning List&nbsp;&nbsp;&nbsp;Total Cost: <span id="total_cost"><i>Calculating</i></span></h3><span style="float:right; padding-right:20px;"><a href="project_add_machine_planning.php">Project Add Machine Planning</a></span>
            </div>
            <!-- /widget-header -->
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
			<div class="widget-header" style="height:*0px; padding-top:10px;">
			  <form method="post" id="mac_plan_search_form" action="project_machine_planning_list.php">

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($project_process_master_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $project_process_master_list_data[$process_count]["project_process_master_id"]; ?>" <?php if($search_process == $project_process_master_list_data[$process_count]["project_process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_process_master_list_data[$process_count]["project_process_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_task">
			  <option value="">- - Select Task - -</option>
			  <?php
			  for($task_count = 0; $task_count < count($project_task_master_list_data); $task_count++)
			  {
			  ?>
			  <option value="<?php echo $project_task_master_list_data[$task_count]["project_task_master_id"]; ?>" <?php if($search_task == $project_task_master_list_data[$task_count]["project_task_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_task_master_list_data[$task_count]["project_task_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <input type="submit" name="mac_plan_search_submit" />
			  </form>

            </div>
			<?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>

            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Project</th>
					<th>Process</th>
					<th>Task</th>
					<th>Machine Type</th>
					<th>Machine Rate</th>
					<th>No of Hours</th>
					<th>Additional Cost</th>
					<th>Total Cost</th>
					<th>Remarks</th>
					<th>Added By</th>
					<th>Added On</th>
					<th colspan="3" style="text-align:center;">Actions</th>

				</tr>
				</thead>
				<tbody>
				<?php
				$total_cost_planned = 0;
				if($project_machine_planning_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_machine_planning_list_data); $count++)
					{
						$sl_no++;
						$total_cost_planned = $total_cost_planned + ($project_machine_planning_list_data[$count]["project_machine_planning_no_of_hours"] * $project_machine_planning_list_data[$count]["project_machine_planning_machine_rate"]) + $project_machine_planning_list_data[$count]["project_machine_planning_additional_cost"];

					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_machine_planning_list_data[$count]["project_master_name"]; ?></td>
					<td><?php echo $project_machine_planning_list_data[$count]["project_process_master_name"]; ?></td>
					<td><?php echo $project_machine_planning_list_data[$count]["project_task_master_name"]; ?></td>
					<td><?php echo $project_machine_planning_list_data[$count]["project_machine_type_master_name"]; ?></td>
					<td><?php echo $project_machine_planning_list_data[$count]["project_machine_planning_machine_rate"]; ?></td>
					<td><?php echo $project_machine_planning_list_data[$count]["project_machine_planning_no_of_hours"]; ?></td>
					<td><?php echo $project_machine_planning_list_data[$count]["project_machine_planning_additional_cost"]; ?></td>
					<td><?php echo ($project_machine_planning_list_data[$count]["project_machine_planning_no_of_hours"] * $project_machine_planning_list_data[$count]["project_machine_planning_machine_rate"]) + $project_machine_planning_list_data[$count]["project_machine_planning_additional_cost"]; ?></td>
					<td><?php echo $project_machine_planning_list_data[$count]["project_machine_planning_remarks"]; ?></td>
					<td><?php echo $project_machine_planning_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_machine_planning_list_data[$count][
					"project_machine_planning_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_machine_planning('<?php echo $project_machine_planning_list_data[$count]["project_machine_planning_id"]; ?>');">Edit </a><?php } ?></td>
					<td><?php if(($project_machine_planning_list_data[$count]["project_machine_planning_active"] == "1") && ($delete_perms_list['status'] == SUCCESS)){?><a href="#" onclick="return project_delete_machine_planning(<?php echo $project_machine_planning_list_data[$count]["project_machine_planning_id"]; ?>);">Delete</a><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_project_plan_cancelation('<?php echo $project_machine_planning_list_data[$count]["project_machine_planning_task_id"]; ?>','machine');">Cancel Plan</a><?php } ?></td>
					</tr>
					<?php
					}

				}
				else
				{
				?>
				<td colspan="12">No machine planning added yet!</td>

				<?php
				}
				 ?>

                </tbody>
              </table>
			  <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
			  <script>
			  document.getElementById('total_cost').innerHTML = '<?php echo 'Rs. '.$total_cost_planned; ?>';
			  </script>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_machine_planning(planning_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_machine_planning_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_machine_planning.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("planning_id=" + planning_id + "&action=0");
		}
	}
}
function go_to_project_edit_machine_planning(planning_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_machine_planning.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","planning_id");
	hiddenField1.setAttribute("value",planning_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}

function go_to_project_plan_cancelation(task_id,source)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_plan_add_cancelation.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","task_id");
	hiddenField1.setAttribute("value",task_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","source");
	hiddenField2.setAttribute("value",source);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);

	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>
