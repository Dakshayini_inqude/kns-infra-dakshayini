<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: kns_grn_engineer_inspection_list.php
CREATED ON	: 30-Sep-2016
CREATED BY	: Lakshmi
PURPOSE     : List of grn engineer inspection for customer withdrawals
*/

/*
TBD: 
*/

/* DEFINES - START */
define('STOCK_MOVE_TO_MANAGEMENT_LIST_FUNC_ID','209');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',STOCK_MOVE_TO_MANAGEMENT_LIST_FUNC_ID,'2','1');
	$add_perms_list    = i_get_user_perms($user,'',STOCK_MOVE_TO_MANAGEMENT_LIST_FUNC_ID,'1','1');

	// Query String Data
	if(isset($_REQUEST["po_id"]))
	{
		$po_id = $_REQUEST["po_id"];
	}
	else
	{
		$po_id = "-1";
	}
	
	// Capture the form data
	if(isset($_POST["move_to_payment_submit"]))
	{
		$po_id 				  = $_POST["hd_po_id"]; 
		$total_value 		  = $_POST["total_grn_value"]; 
		$additional_payment   = $_POST["num_additional_amount"];
		$payment_remarks	  = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if($additional_payment !="")
		{
			$management_payment_iresult = i_add_stock_management_payment($po_id,$total_value,$additional_payment,$payment_remarks,$user);
			if($management_payment_iresult["status"] == SUCCESS)
			{
				header("location:stock_management_payment_list.php?order_id=$po_id");
			}
			else
			{
				$alert_type = 0;
			}
			
			$alert = $management_payment_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	// Temp data
	// Get GRN Engineer Inspection already added
	if($grn_item_id != "-1")
	{
		$stock_grn_engineer_inspection_search_data = array("po_id"=>$po_id,"active"=>'1');
		$grn_engineer_inspection_list = i_get_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data);		
		if($grn_engineer_inspection_list['status'] == SUCCESS)
		{
			$grn_engineer_inspection_list_data = $grn_engineer_inspection_list['data'];
			$item = $grn_engineer_inspection_list_data[0]["stock_grn_engineer_inspection_grn_item_id"];
			$qty = $grn_engineer_inspection_list_data[0]["stock_grn_engineer_inspection_approved_quantity"];
			//Grn Details
		}
		else
		{
			$grn_id = "";
			$item 	= "";
			$qty 	= "";
		}
	}
	else
	{
		$stock_grn_engineer_inspection_search_data = array("active"=>'1');
		$grn_engineer_inspection_list = i_get_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data);		
		if($grn_engineer_inspection_list['status'] == SUCCESS)
		{
			$grn_engineer_inspection_list_data = $grn_engineer_inspection_list['data'];
		}	
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Accounts Approval </title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Accounts Approval</h3>
            </div>
			<!--<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="eng_ins_search_form" action="stock_move_to_payments_list.php">			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_status">			  
			  <option value="Pending" <?php if($search_status == "Pending"){?> selected <?php } ?>>Pending</option>
			  <option value="Completed" <?php if($search_status == "Completed"){?> selected <?php } ?>>Completed</option>			  
			  </select>			  
			  </span>
			  <input type="submit" name="eng_ins_search_submit" />
			  </form>			  
            </div>-->
            <!-- /widget-header -->
            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				  
				    <th>SL No</th>
					<th>PO No</th>
					<th>PO Date</th>
					<th>Item Code</th>
					<th>Item Name</th>
					<th>GRN No</th>
					<th>GRN Date</th>
					<th>Accepted Qty</th>
					<th>value</th>
					<th>Additional Cost</th>
					<th>Total</th>		
				</tr>
				</thead>
				<tbody>							
				<?php
				if($grn_engineer_inspection_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$total_grn_value = 0;
					$status_count = 'true';
					$grn_item_arry = array();
					for($count = 0; $count < count($grn_engineer_inspection_list_data); $count++)
					{			
						// Get PO details				
						
						$stock_purchase_order_items_search_data = array('item'=>$grn_engineer_inspection_list_data[$count]['stock_grn_item'],'order_id'=>$grn_engineer_inspection_list_data[$count]['stock_grn_purchase_order_id']);
						$po_sresult = i_get_stock_purchase_order_items_list($stock_purchase_order_items_search_data);
						for($po_count = 0 ; $po_count < count($po_sresult["data"]) ; $po_count++)
						{
							$po_qty = $po_sresult["data"][$po_count]["stock_purchase_order_item_quantity"];
						}
						$item_cost    = $po_sresult['data'][0]["stock_purchase_order_item_cost"];
						$accepted_qty = $grn_engineer_inspection_list_data[$count]["stock_grn_engineer_inspection_approved_quantity"];
						$item_value   = $item_cost*$accepted_qty;
						$tax_rate     = $po_sresult['data'][0]["stock_tax_type_master_value"];
						$trans_charge = $grn_engineer_inspection_list_data[0]["stock_purchase_order_transportation_charges"];
						$tax_value    = ($po_sresult['data'][0]["stock_tax_type_master_value"] * $item_value)/100;
						$excise_value = ($po_sresult['data'][0]["stock_purchase_order_item_excise_duty"] * $item_value)/100;
						$total_value  = $item_value + $tax_value + $excise_value + $trans_charge;
						
						// Check if moved to payments
						$stock_grn_accounts_search_data = array('item_id'=>$grn_engineer_inspection_list_data[$count]['stock_grn_engineer_inspection_id']);
						$is_moved_to_payment = i_get_stock_grn_accounts($stock_grn_accounts_search_data);						
						if($is_moved_to_payment['status'] == SUCCESS)
						{	
							$sl_no++;
							$total_additional_amt = 0;
							for($amount = 0 ;  $amount < count($is_moved_to_payment["data"]) ; $amount++)
							{
								$additional_amt	 = $is_moved_to_payment["data"][$amount]["stock_grn_accounts_approval_additional_amount"];
								$total_additional_amt = $total_additional_amt + $additional_amt;
							}
							$material_id = $grn_engineer_inspection_list_data[$count]['stock_grn_item'];
							if(array_key_exists($material_id,$grn_item_arry))
							{
							  $grn_item_arry[$material_id] = $grn_item_arry[$material_id] + $grn_engineer_inspection_list_data[$count]["stock_grn_engineer_inspection_approved_quantity"];
							}
							else
							{
								$grn_item_arry[$material_id] = $grn_engineer_inspection_list_data[$count]["stock_grn_engineer_inspection_approved_quantity"];
							}
							if($po_qty == $grn_item_arry[$material_id])
							{
								$status_count = 'true';
							}
							else
							{
								$status_count = 'false';
							}
							var_dump($status_count);
							?>
							<tr>
							<td><?php echo $sl_no; ?></td>
							<td><?php echo $grn_engineer_inspection_list_data[$count]["stock_purchase_order_number"]; ?></td>
							<td><?php echo date("d-M-Y",strtotime($grn_engineer_inspection_list_data[$count]["stock_purchase_order_added_on"])); ?></td>
							<td><?php echo $grn_engineer_inspection_list_data[$count]["stock_material_code"]; ?></td>
							<td><?php echo $grn_engineer_inspection_list_data[$count]["stock_material_name"]; ?></td>
							<!--<td><?php echo $grn_engineer_inspection_list_data[$count]["stock_unit_name"]; ?></td>
							<td><?php echo $grn_engineer_inspection_list_data[$count]["stock_vendor_name"]; ?></td>-->
							<td><?php echo $grn_engineer_inspection_list_data[$count]["stock_grn_no"]; ?></td>
							<td><?php echo date("d-M-Y",strtotime($grn_engineer_inspection_list_data[$count]["stock_grn_added_on"])); ?></td>
							<!--<td><?php echo $grn_engineer_inspection_list_data[$count]["stock_grn_item_inward_quantity"]; ?></td>-->
							<td><?php echo $accepted_qty; ?></td>
							<td><?php echo $total_value; ?></td>
							<td><?php echo $total_additional_amt; ?></td>
							<td><?php echo ($total_value + $total_additional_amt); ?></td>
							</tr>
							<?php
						}
						$total_grn_value = ($total_grn_value + $total_value + $total_additional_amt);
					}
				}
				else
				{
				?>
				<td colspan="15">No grn engineer inspection added yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
			<?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>	
            </div>
			 <br /> <br />
			<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="move_to_payments_form" class="form-horizontal" method="post" action="stock_move_to_management_list.php">
								<input type="hidden" name="hd_po_id" value="<?php echo $po_id; ?>" />
								<input type="hidden" name="total_grn_value" value="<?php echo $total_grn_value; ?>" />
									<fieldset>										
															
										<div class="control-group">											
											<label class="control-label" for="num_additional_amount">Additional Amount </label>
											<div class="controls">
												<input type="number" step="0.01" class="span4" name="num_additional_amount" placeholder="Additional amount to be paid (if any)" value="0">&nbsp;&nbsp;
												<input type="text" class="span4" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										                                                                                   										 <br />
										<?php if($status_count == 'true')
										{?>
										<div class="form-actions">
										<?php			  
										if($add_perms_list['status'] == SUCCESS)
										{
										?>
											<input type="submit" class="btn btn-primary" name="move_to_payment_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										<?php
										}
										else
										{
											echo 'You are not authorized to view this page';
										}
										?>	
										</div> <!-- /form-actions -->
										<?php
										}
										?>
									</fieldset>
								</form>
								</div>
							</div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
function delete_grn_engineer_inspection(inspection_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "stock_grn_engineer_inspection_list.php";
					}
				}
			}

			xmlhttp.open("POST", "stock_delete_grn_engineer_inspection.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("inspection_id=" + inspection_id + "&action=0");
		}
	}	
}
function go_to_edit_grn_engineer_inspection(inspection_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "stock_edit_grn_engineer_inspection.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","inspection_id");
	hiddenField1.setAttribute("value",inspection_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>