<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: reason_list.php
CREATED ON	: 28-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Task Plans for a particular process ID
*/

/*
TBD: 
1. Date display and calculation
2. Session management
3. Linking Tasks
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	$task = $_GET["task"];

	// Temp data
	$alert = "";

	if($role == 1)
	{
		$assigned_to = "";
	}
	else
	{
		$assigned_to = $user;
	}

	// Get list of task plans for this process plan
	if($role == 3)
	{
		$access_user = $user;
	}
	else
	{
		$access_user = "";
	}

	// Get Task Details
	$legal_task_plan_list = i_get_task_plan_list($task,'','','','','','','');
	if($legal_task_plan_list["status"] == SUCCESS)
	{
		$legal_task_plan_list_data = $legal_task_plan_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$legal_task_plan_list["data"];
	}

	// Get reason details
	$reason_details = i_get_delay_reason_list($task,'');
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Reason List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Reason List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<form action="task_list.php" method="post" id="task_update_form">			
			<span style="padding-left:50px;">
			Task Type: <strong><?php echo $legal_task_plan_list_data[0]["task_type_name"]; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			</span>
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>Reason</th>
					<th>Remarks</th>
					<th>Reason Date</th>
					<th>Task</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				 <?php
				 if($reason_details["status"] == SUCCESS)
				 {
					for($count = 0; $count < count($reason_details["data"]); $count++)
					{					
					?>				
					<tr>
						<td><?php echo $reason_details["data"][$count]["reason"]; ?></td>
						<td><?php echo $reason_details["data"][$count]["reason_remarks"]; ?></td>
						<td><?php echo date("d-M-Y",strtotime($reason_details["data"][$count]["reason_added_on"])); ?></td>	
						<?php 
						if($reason_details["data"][$count]["reason_sub_task"] == 1)
						{
							$sub_task = "YES";
						}
						else
						{
							$sub_task = "NO";
						}?>
						<td><?php echo $sub_task; ?></td>	
						<?php if($reason_details["data"][$count]["reason_sub_task"] == 1)
						{?>
						<td style="word-wrap:break-word;"><a href="#" onclick="return complete_task(<?php echo $reason_details['data'][$count]["reason_id"]; ?>,<?php echo $task; ?>);">Complete Task</a></td>
						<?php
						}
						else
						{
						?>
							<td> <?php echo "" ;?></td>
						<?php
						}
						?>
						
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<td colspan="2">No reason added</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  <br />			
			</form>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function complete_task(reason_id,task)
{
	var ok = confirm("Are you sure you want to Complete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "reason_list.php?task=" + task;
					}
				}
			}

			xmlhttp.open("POST", "update_reason_task.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("reason_id=" + reason_id + "&action=0");
		}
	}	
}
</script>
</body>

</html>