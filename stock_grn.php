<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');	
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');	
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'pdf_operations.php');	
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');


// Session Data
$user 		   = $_SESSION["loggedin_user"];
$role 		   = $_SESSION["loggedin_role"];
$loggedin_name = $_SESSION["loggedin_user_name"];

// Temp Data
$alert_type = -1;
$alert = "";

$grn_id = $_REQUEST["grn_id"];

$stock_grn_search_data = array("grn_id"=>$grn_id,"active"=>'1');
$grn_list = i_get_stock_grn_list($stock_grn_search_data);
if($grn_list['status'] == SUCCESS)
{
	$grn_list_data = $grn_list['data'];	
	$grn_no = $grn_list_data[0]["stock_grn_no"];
	$invoice_no = $grn_list_data[0]["stock_grn_invoice_number"];
	$grn_date = $grn_list_data[0]["stock_grn_added_on"];
	$invoice_date = $grn_list_data[0]["stock_grn_invoice_date"];
	$vehicle_no = $grn_list_data[0]["stock_grn_vehicle_number"];
	$po_no = $grn_list_data[0]["stock_purchase_order_number"];
	$po_date = $grn_list_data[0]["stock_purchase_order_added_on"];		$company_name    = $grn_list_data[0]["stock_company_master_name"];		$company_contact = $grn_list_data[0]["stock_company_master_contact_person"];			$company_address = $grn_list_data[0]["stock_company_master_address"];
	$vendor = $grn_list_data[0]["stock_vendor_name"];
	$vendor_address = $grn_list_data[0]["stock_vendor_address"];
	$remarks = $grn_list_data[0]["stock_grn_remarks"];
}
else
{
	$grn_id = "-1";
}	

$stock_grn_items_search_data = array("grn_id"=>$grn_id,"active"=>'1');
$grn_items_list = i_get_stock_grn_items_list($stock_grn_items_search_data);
if($grn_items_list['status'] == SUCCESS)
{
	$grn_items_list_data = $grn_items_list['data'];
}	
else
{
	
}


$grn_print_format = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>GRN</title>
</head>

<body>

<div style="border:1px solid #33F; border-radius:10px; -moz-border-radius:10px; -o-border-radius:10px;">
<table width="100%" border="0" style="border-collapse:collapse" cellpadding="20" cellspacing="20">
  <tr>
    <td height="28" colspan="9" align="center"><img src="kns-logo.png" /></td>
  </tr>
  <tr>
    <td colspan="9" align="center"><h2 style="margin:0px;">'.$company_name.'</h2><span style="font-size:12px;">'.$company_address.'</span></td>
    </tr>
  <tr>
    <td colspan="9" align="center"></td>
    </tr>
  <tr>    
    <td width="100%" colspan="9" align="center"><strong style="text-transform: uppercase">Goods Received Note</strong><hr /></td>    
  </tr>
  <tr>
    <td colspan="3">Invoice No. <strong>'.$invoice_no.'</strong></td>    
    <td colspan="3">GRN No. <strong>'.$grn_no.'</strong></td>
	<td colspan="3">PO No. <strong>'.$po_no.'</strong></td>
  </tr>
  <tr>
    <td colspan="3">Invoice date <strong>'.get_formatted_date($invoice_date,"d-M-Y").'</strong></td>
    <td colspan="3">GRN Date <strong>'.date("Y-m-d H:i:s",strtotime($grn_date)).'</strong></td>
    <td colspan="3">PO Date <strong>'.get_formatted_date($po_date,"d-M-Y").'</strong></td>
  </tr>
  <tr>
    <td colspan="4">Vendor <strong>'.$vendor.' , '.$vendor_address.'</strong></td>    
    <td colspan="5">Vehicle No. <strong>'.$vehicle_no.'</strong></td>
  </tr>
  <tr>
	<td colspan="9" align="center">
	<table width="100%" border="1" style="border-collapse:collapse">
     <tr bgcolor="#F0F0F0">
       <td align="center" width="10%">Sl.No</td>
       <td align="center" width="30%">Material Description</td>
       <td align="center" width="10%">Unit</td>
       <td align="center" width="10%">PO Qty</td>
	   <td align="center" width="10%">In Qty</td>
	   <td align="center" width="10%">Acc Qty</td>	   	   <td align="center" width="10%">Project</td>
       <td align="center" width="20%">Received By</td>
     </tr>';
		
for($count = 0; $count < count($grn_items_list["data"]); $count++)
{	//Get Grn List	$stock_grn_search_data = array("grn_id"=>$grn_items_list_data[$count]["stock_grn_id"]);	$grn_data_list = i_get_stock_grn_list($stock_grn_search_data);	if($grn_data_list["status"] == SUCCESS)	{		$order_id = $grn_data_list["data"][0]["stock_grn_purchase_order_id"];	}	//Get Po List	$stock_purchase_order_items_search_data = array("order_id"=>$order_id);	$po_item_data_list = i_get_stock_purchase_order_items_list($stock_purchase_order_items_search_data);	if($po_item_data_list["status"] == SUCCESS)	{		$po_project = $po_item_data_list["data"][0]["stock_project_name"];	}
	$grn_print_format = $grn_print_format.'<tr>
              <td style="height:30px;">'.($count + 1).'</td>
              <td>'.$grn_items_list_data[$count]["stock_material_name"].'-'.$grn_items_list_data[$count]["stock_material_code"].'</td>
			  <td>'.$grn_items_list_data[$count]["stock_unit_name"].'</td>
              <td>'.$grn_items_list_data[$count]["stock_grn_item_quantity"].'</td>
              <td>'.$grn_items_list_data[$count]["stock_grn_item_inward_quantity"].'</td>
              <td>&nbsp;</td>			  			  <td>'.$po_project.'</td>
			  <td>'.$grn_items_list_data[$count]["user_name"].'</td>
			  </tr>';
};
$grn_print_format = $grn_print_format.'
     
   </table></td>
 </tr>
 <tr>
 <td colspan="1">Remarks</td>
 <td colspan="8">'.$remarks.'</td>
 </tr>
   <tr>
     <td colspan="9" align="left"><table width="100%" border="0">
       <tr>
         <td><br />
           Prepared By</td>
         <td><br />
           Security</td>
         <td><br />
           Site Incharge</td>
         <td><br />
           Site Engineer</td>
       </tr>
     </table></td>
    </tr>
</table>
</div>

</body>
</html>';
$mpdf = new mPDF();
$mpdf->WriteHTML($grn_print_format);
$mpdf->Output('invoice_'.$invoice_no.'.pdf','I');
?>