<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 10-Feb-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'katha_transfer'.DIRECTORY_SEPARATOR.'kt_buy_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	
	if(isset($_GET['request_id']))
	{
		$request_id = $_GET['request_id'];
	}
	else
	{
		$request_id = "";
	}

	// Capture the form data
	if(isset($_POST["edit_kt_request_submit"]))
	{
		$request_id         = $_POST["hd_request_id"];
		$file_id            = $_POST["txt_file_id"];
		$request_date       = $_POST["request_date"];
		$requested_by       = $_POST["requested_by"];
		$requested_on       = $_POST["date_requested_on"];
		$remarks            = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($request_date != "") && ($requested_by != "") && ($requested_on != ""))
		{
			$kt_buy_request_update_data = array("request_date"=>$request_date,"requested_by"=>$requested_by,"requested_on"=>$requested_on,
			"remarks"=>$remarks);
			$kt_buy_request_iresult = i_update_kt_buy_request($request_id,$kt_buy_request_update_data);
			
			if($kt_buy_request_iresult["status"] == SUCCESS)				
			{	
				$alert_type = 1;
		    
				header("location:kt_buy_kt_request_list.php");
			}
			else
			{
				$alert 		= "kt Request Already Exists";
				$alert_type = 0;	
			}
			
			$alert = $kt_buy_request_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get kt Request modes already added
	$kt_buy_request_search_data = array("active"=>'1',"request_id"=>$request_id);
	$kt_buy_request_list = i_get_kt_buy_request($kt_buy_request_search_data);
	if($kt_buy_request_list['status'] == SUCCESS)
	{
		$kt_buy_request_list_data = $kt_buy_request_list['data'];
	}		
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>KT Buy Edit KT Request</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>KT Buy Edit KT Request</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">KT Buy Edit KT Request</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="kt_buy_edit_kt_request_form" class="form-horizontal" method="post" action="kt_buy_edit_kt_request.php">
								<input type="hidden" name="hd_request_id" value="<?php echo $request_id; ?>" />
									<fieldset>	
											
										<div class="control-group">											
											<label class="control-label" for="txt_file_id">File*</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_file_id" placeholder="File" value="<?php echo $kt_buy_request_list_data[0]["kt_buy_request_file_id"] ;?>" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="request_date">Request Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="request_date" placeholder="Date" value="<?php echo $kt_buy_request_list_data[0]["kt_buy_request_date"] ;?>" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="requested_by">Requested By</label>
											<div class="controls">
												<input type="text" class="span6" name="requested_by" placeholder="Name" value="<?php echo $kt_buy_request_list_data[0]["kt_buy_requested_by"] ;?>" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="date_requested_on">Requested On*</label>
											<div class="controls">
												<input type="date" class="span6" name="date_requested_on" placeholder="Date" value="<?php echo $kt_buy_request_list_data[0]["kt_buy_requested_on"] ;?>" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->	
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks" value="<?php echo $kt_buy_request_list_data[0]["kt_buy_request_remarks"] ;?>" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_kt_request_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
