<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: reg_buy_process_list.php
CREATED ON	: 08-Feb-2017
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
//define('REG_BUY_PROCESS_LIST_FUNC_ID','193');
define('REG_BUY_PROCESS_LIST_FUNC_ID','216');
/* DEFINES - END */


/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'registration'.DIRECTORY_SEPARATOR.'reg_buy_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
	
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	$alert_type = -1;
	$alert = "";
	
	// Get permission settings for this user for this page
	$add_perms_list    = i_get_user_perms($user,'',REG_BUY_PROCESS_LIST_FUNC_ID,'1','1');
	$view_perms_list   = i_get_user_perms($user,'',REG_BUY_PROCESS_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',REG_BUY_PROCESS_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',REG_BUY_PROCESS_LIST_FUNC_ID,'4','1');

	// Query String Data
	// Nothing
	
	if(isset($_GET['request_id']))
	{
		$request_id = $_GET['request_id'];
	}
	else
	{
		$request_id = "";
	}

	// Get Reg Buy Process already added
	$reg_buy_reg_process_search_data = array("active"=>'1',"request_id"=>$request_id);
	$reg_buy_reg_process_list = i_get_reg_buy_reg_process($reg_buy_reg_process_search_data);
	if($reg_buy_reg_process_list['status'] == SUCCESS)
	{
		$reg_buy_reg_process_list_data = $reg_buy_reg_process_list['data'];
	}
	else
	{
		
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Reg Buy Process List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Reg Buy Process List</h3><?php if($add_perms_list['status'] == SUCCESS){?><span style="float:right; padding-right:20px;"><a href="reg_buy_add_reg_process.php?request_id=<?php echo $request_id ;?>">Reg Buy Add Process</a></span><?php } ?>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Process</th>
					<th>Request ID</th>
					<th>Process Start Date</th>
					<th>Process End Date</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>									
					<th colspan="3" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($reg_buy_reg_process_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($reg_buy_reg_process_list_data); $count++)
					{
						$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $reg_buy_reg_process_list_data[$count]["reg_buy_process_master_name"]; ?></td>
					<td><?php echo $reg_buy_reg_process_list_data[$count]["reg_buy_request_id"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($reg_buy_reg_process_list_data[$count][
					"reg_buy_reg_process_start_date"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($reg_buy_reg_process_list_data[$count][
					"reg_buy_reg_process_end_date"])); ?></td>
					<td><?php echo $reg_buy_reg_process_list_data[$count]["reg_buy_reg_process_remarks"]; ?></td>
					<td><?php echo $reg_buy_reg_process_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($reg_buy_reg_process_list_data[$count][
					"reg_buy_reg_process_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_reg_buy_edit_process('<?php echo $reg_buy_reg_process_list_data[$count]["reg_buy_reg_process_id"]; ?>','<?php echo $reg_buy_reg_process_list_data[$count]["reg_buy_reg_process_request_id"]; ?>');">Edit </a><?php } ?></td>
					<td><?php if(($reg_buy_reg_process_list_data[$count]["reg_buy_reg_process_active"] == "1") && ($delete_perms_list['status'] == SUCCESS)){?><a href="#" onclick="return reg_buy_delete_process('<?php echo $reg_buy_reg_process_list_data[$count]["reg_buy_reg_process_id"]; ?>','<?php echo $reg_buy_reg_process_list_data[$count]["reg_buy_reg_process_request_id"]; ?>');">Delete</a><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if($add_perms_list['status'] == SUCCESS){ ?><a style="padding-right:10px" href="#" onclick="return go_to_reg_buy_upload_document('<?php echo $reg_buy_reg_process_list_data[$count]["reg_buy_reg_process_id"]; ?>','<?php echo $reg_buy_reg_process_list_data[$count]["reg_buy_reg_process_request_id"]; ?>');">Documents </a><?php } ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			 <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function reg_buy_delete_process(process_id,request_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "reg_buy_reg_process_list.php?request_id=" +request_id;
					}
				}
			}

			xmlhttp.open("POST", "ajax/reg_buy_delete_reg_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("process_id=" + process_id + "&action=0");
		}
	}	
}
function go_to_reg_buy_edit_process(process_id,request_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "reg_buy_edit_reg_process.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","request_id");
	hiddenField2.setAttribute("value",request_id);
	
	form.appendChild(hiddenField1);
	
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_reg_buy_upload_document(process_id,request_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "reg_buy_upload_documents.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","request_id");
	hiddenField2.setAttribute("value",request_id);
	
	form.appendChild(hiddenField1);
	
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>