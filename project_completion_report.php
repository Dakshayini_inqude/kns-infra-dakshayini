<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_plan_process_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/
$_SESSION['module'] = 'Projectmgmnt';

/* DEFINES - START */
define('PROJECT_COMPLETION_REPORT_FUNC_ID','251');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');


if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	$alert_type = -1;
	$alert = "";

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_COMPLETION_REPORT_FUNC_ID,'2','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */


	// Query String Data
	// Nothing

	$search_project = "-1";

	if(isset($_POST["file_search_submit"]))
	{
		$search_project = $_POST["search_project"];
	}
	else {
			$search_project = $_REQUEST["search_project"];
	}


	// background submit remarks
	if(isset($_POST["project_id"])) {
		$search_project = $_POST["project_id"];

		$project_id	 = $_POST["project_id"];
		$process_id   = $_POST["process_id"];

		$process_name = $_POST["process_name"];
		$task_id = $_POST["task_id"];

		$mp		   = $_POST["manpower"];

		$mc = $_POST["machine"];

		$material = $_POST["material"];

		$cw	   = $_POST["contract"];
		$remarks	   = $_POST["remarks"];

		// Check for mandatory fields

		if(($project_id != "") && ($task_id != ""))
		{
			$indent_iresult = db_add_process_remarks($project_id,$process_id,$task_id,$process_name,$material,$mp,$mc,$cw,$remarks,$user);
			// header("location:project_completion_report.php?search_project=$project_id");
			if($indent_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
			}
			else
			{
				$alert_type = 0;
			}
			$alert = $indent_iresult["data"];
		}

	}

	//close remarks in background
	if (isset($_POST["remarks_id"]))
	{

				$remarks_id 			 = $_POST["remarks_id"];
				$project_id		     = $_POST["$project_id"];
				$completed_remarks = $_POST["$completed_remarks"];
				$process_remarks_update_data = array("status"=>"Completed","completed_remarks"=>$completed_remarks);
				$payment_manpower_mapping_iresult = db_update_process_remarks_task($remarks_id,$process_remarks_update_data);
				// header("location:project_completion_report.php?search_project=$project_id");
	}

	if (isset($_POST["complete_remarks_submit"]))
	{

				$remarks_id 			 = $_POST["hd_remarks_id"];
				$project_id		     = $_POST["hd_project_id"];
				$completed_remarks = $_POST["completed_remarks"];
				$process_remarks_update_data = array("status"=>"Completed","completed_remarks"=>$completed_remarks);
				$payment_manpower_mapping_iresult = db_update_process_remarks_task($remarks_id,$process_remarks_update_data);
				header("location:project_completion_report.php?search_project=$project_id");
	}

	// Get Project Management Master modes already added
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list['status'] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list['data'];
	}

	else
	{
		$alert = $project_management_master_list["data"];
		$alert_type = 0;
	}

	 // Get Users modes already added
	$user_list = i_get_user_list('','','','');
	if($user_list['status'] == SUCCESS)
	{
		$user_list_data = $user_list['data'];
	}
	else
	{
		$alert = $user_list["data"];
		$alert_type = 0;
	}

	// Temp data
	$project_plan_process_list = db_get_project_process_list($search_project);
	if($project_plan_process_list["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$project_plan_process_list_data = $project_plan_process_list["data"];
	}
	else
	{
		//$alert = $alert."Alert: ".$project_plan_process_list["data"];
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Plan Process List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	 <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>
	<style>
.modal.fade.in {
	top: 38%;
}

.custom-table td {
	min-width: 0 !important;
}

.display-name {
	color: #00ba8b;
}

	</style>


<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <!-- <h3>Project Plan Process List&nbsp;&nbsp;&nbsp;P Days: <?php echo $total_plan_days; ?>&nbsp;&nbsp;&nbsp;A Days: <?php echo $actual_days; ?>&nbsp;&nbsp;&nbsp;Avg Comp %: <?php echo round($actual_percentage,2); ?>&nbsp;&nbsp;&nbsp;Delay: <?php echo round((round($delay,2)*$actual_days),2).'%'; ?>&nbsp;&nbsp;&nbsp;Avg Pause Days: <?php echo $pause_days; ?>&nbsp;&nbsp;&nbsp;Project Status: <?php echo $comments; ?></h3> -->
							<h3>Project Plan Process List</h3>
            </div>

            <!-- /widget-header -->
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
			<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_completion_report.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <input type="submit" name="file_search_submit" />
			  </form>
            </div>

			<?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>

            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th style="width:3%">SL No</th>
					<th style="width:20%">Process Name</th>
					<th style="width:10%">Plan <br> Start Date</th>
					<th style="width:10%">P End Date</th>
					<th style="width:4%">P Days</th>
					<th style="width:8%"> Actual <br> Start Date</th>
					<th style="width:8%">Actual <br> End Date</th>
					<th style="width:4%"> A Days</th>
					<th style="width:4%">D Days</th>
					<th style="width:7%">Percentage</th>
					<th style="width:10%">Pause Date</th>
					<th style="width:5%">Pause Days</th>
					<th style="width:10%">Pause Reason</th>
					<th style="width:4%">No of Times Paused</th>
					<th colspan="2"  style="width:10%;text-align:center;">Remarks</th>

				</tr>
				</thead>
				<tbody>
				<?php
					if($project_plan_process_list["status"] == DB_RECORD_ALREADY_EXISTS)
				{
					$sl_no = 0;
					$total_actual_days = 0;
					$planned_process_start_date = "0000-00-00";
					$planned_process_end_date = "000-00-00";
					for($count = 0; $count < count($project_plan_process_list_data); $count++)
					{
						$sl_no++;

						$get_actuals_list = i_get_actuals_data($project_plan_process_list_data[$count]["plan_process_id"]);
						$actual_start_date = $get_actuals_list["process_min_start_date"];
						$actual_end_date = $get_actuals_list["process_max_end_date"];

						//Get planned Data
						$createActualDate = new DateTime($actual_start_date);
						$formatted_actual_date = $createActualDate->format('Y-m-d');
						if((strtotime($formatted_actual_date)) <= strtotime(date("2018-04-18")))
						{
						$planned_process_start_date = get_formatted_date($project_plan_process_list_data[$count]["start_date"],"d-M-Y");
						$planned_process_end_date = get_formatted_date($project_plan_process_list_data[$count]["end_date"],"d-M-Y");
						$plan_days = get_date_diff($project_plan_process_list_data[$count]["start_date"],$project_plan_process_list_data[$count]["end_date"]);
					}
					else {
						//planned data
						$project_task_planning_search_data = array("process_id"=>$project_plan_process_list_data[$count]["plan_process_id"]);
						$project_task_planning_list =  db_get_task_planning_list($project_task_planning_search_data);
						if($project_task_planning_list["status"] == DB_RECORD_ALREADY_EXISTS)
						{
									$planned_process_start_date = get_formatted_date($project_task_planning_list["data"][0]["min_start_date"],"d-M-Y");
									$planned_process_end_date = get_formatted_date($project_task_planning_list["data"][0]["max_end_date"],"d-M-Y");
									$toatal_planned_msmrt = $project_task_planning_list["data"][0]["total_msmrt"];
									$plan_days = get_date_diff($project_task_planning_list["data"][0]["min_start_date"],$project_task_planning_list["data"][0]["max_end_date"]);
								}
						else {
							$planned_process_start_date = "0000-00-00";
							$planned_process_end_date = "0000-00-00";
							$planned_days = "0";

						}
					}

						// $total_actual_days = $total_actual_days + $actual_days["data"] ;

						//Get Delay Reason List
						$delay_reason_search_data = array("process_id"=>$project_plan_process_list_data[$count]["plan_process_id"],"delay_end_date"=>"0000-00-00 00:00:00");
						$delay_reason_list = i_get_project_delay_reason($delay_reason_search_data);
						$pause_days = "";
						if($delay_reason_list["status"] == SUCCESS)
						{
							$delay_reason_list_data = $delay_reason_list["data"];
							$pause_date = date("d-M-Y H:i:s", strtotime($delay_reason_list["data"][0]["project_task_delay_reason_start_date"]));
							$pause_reason = $delay_reason_list["data"][0]["project_reason_master_name"];

							// Get date difference
							$pause_duration = get_date_diff($delay_reason_list["data"][0]["project_task_delay_reason_start_date"],date("Y-m-d H:i:s"));
							$pause_days = $pause_duration['data'];
						}
						else
						{
							//$no_of_times_paused = "";
							$pause_date = "";
							$pause_reason = "";
						}
						//Get Delay Reason List
						$delay_reason_search_data = array("process_id"=>$project_plan_process_list_data[$count]["plan_process_id"]);
						$pause_list = i_get_project_delay_reason($delay_reason_search_data);

						if($pause_list["status"] == SUCCESS)
						{
							$no_of_times_paused = count($pause_list["data"]);
						}
						else
						{
							$no_of_times_paused = "";
						}

						?>
						<input type="hidden"  class="row_process_ids" id="row_process_id" value="<?php echo $project_plan_process_list_data[$count]["plan_process_id"] ;?>">
						<?php

					?>


					<tr <?php if ($planned_process_start_date == "") {
                            ?> style="color:red;" <?php
                        } ?>>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_plan_process_list_data[$count]["process_name"]; ?></td>
					<td><?php echo $planned_process_start_date ; ?></td>
					<td><?php echo $planned_process_end_date ; ?></td>
					<td id="plan_days_<?php echo $project_plan_process_list_data[$count]["plan_process_id"];?>" data-pdays="<?php echo $plan_days["data"]+1; ?>"><?php if($plan_days["status"] != "2") {
						?>
						<a target="_blank" href="project_task_planning_list.php?project_id=<? echo $search_project ; ?>&search_process=<?php echo $project_plan_process_list_data[$count]["process_master_id"] ;?>">
						<?php echo $plan_days["data"] + 1;
					}
					 else
					  {
							echo $plan_days["data"] ;
						} ?> </a> </td>
					<td><?php echo get_formatted_date($actual_start_date,"d-M-Y") ; ?></td>
						<td id="aedate_<?php echo $project_plan_process_list_data[$count]["plan_process_id"];?>"
						data-asdate="<?php echo $actual_start_date; ?>"	data-aedate="<?php echo $actual_end_date; ?>"></td>

					<td>
						<a id="actual_days_<?php echo $project_plan_process_list_data[$count]["plan_process_id"];?>" target="_blank" href="project_process_level_details.php?process_id=<?php echo $project_plan_process_list_data[$count]["plan_process_id"];?>"></a>
					</td>

					<td id="varience_<?php echo $project_plan_process_list_data[$count]["plan_process_id"];?>"></td>
					<!-- <td><div class="progress">
				  <div class="progress-bar progress-bar-success" style="width: <?php echo $total_overall_percenatge ;?>%;"><a href="project_process_level_details.php?process_id=<?php echo $project_plan_process_list_data[$count]["plan_process_id"] ;?>" style="color:#000000" target="_blank"><?php echo round($total_overall_percenatge,2) ;?>%</a>					<span class="sr-only">(success)</span>
				  </div>
				  <div class="progress-bar progress-bar-danger" style="width: <?php echo $overall_pending_percentage ;?>%"><a href="project_process_level_details.php?process_id=<?php echo $project_plan_process_list_data[$count]["plan_process_id"] ;?>" style="color:#FFFFFF" target="_blank"><?php echo round($overall_pending_percentage,2) ;?>%</a>
					<span class="sr-only">(danger)</span>
				  </div>
				</div>
			</td> -->
				  <!-- dynamically we'll load this progressbar -->
					<td><span id="cp_<?php echo $project_plan_process_list_data[$count]["plan_process_id"];?>">Loading...</span></td>
					<td><?php echo $pause_date ; ?></td>
					<td><?php echo $pause_days ; ?></td>
					<td><?php echo $pause_reason ; ?></td>
					<td><a href="project_task_delay_reason_list.php?process_id=<?php echo $project_plan_process_list_data[$count]["plan_process_id"];?>&process_name=<?php echo $project_plan_process_list_data[$count]["process_name"]; ?>&project=<?php echo$project_plan_process_list_data[$count]["project_name"] ;?>" target="_blank"><?php echo $no_of_times_paused ; ?></a></td>
					<td><a href="#" data-toggle="modal" onclick="setData('<?php echo $project_plan_process_list_data[$count]["plan_process_id"]?>','<?php echo $project_plan_process_list_data[$count]["process_name"] ?>','<?php echo $project_plan_process_list_data[$count]["project_name"] ?>','<?php echo $project_plan_process_list_data[$count]["process_master_id"] ?>')"  data-target="#myModal"><strong>Add</strong></a></td>
					<td><a href="#" data-toggle="modal" onclick="setData1('<?php echo $project_plan_process_list_data[$count]["plan_process_id"]?>','<?php echo $project_plan_process_list_data[$count]["process_name"] ?>','<?php echo $project_plan_process_list_data[$count]["project_name"] ?>')" data-target="#myModal1"><strong>View</strong></a></td>
					</tr>
					<?php
					}

				}
				else
				{
				?>
				<td colspan="13">No process added yet!</td>

				<?php
				}
				 ?>
				 <!-- <script>
				 document.getElementById('total_plan_days').innerHTML = <?php echo $total_plan_days; ?>;
				 </script> -->

                </tbody>
              </table>

							<!-- Add remerks modal -->
							<!-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width:50%;height:80%"> -->
							<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="background-color:transparent">

								 <div class="modal-dialog" role="document" style="width:100%">

										<div class="modal-content" style="width: 100%;">

											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          								<span aria-hidden="true">&times;</span>
        								</button>
												<h3 class="modal-title"> Project : <span class="display-name" id="project_name_display" name="project_name_display" ></span> &nbsp;&nbsp;&nbsp;
													Process : <strong> 	<span id="process_name_display" class="display-name" name="process_name_display" ></span>
												 </strong></h3>
      								</div>

 										<div class="modal-body">
												<form>

														<input type="hidden" name="hd_project_id" value="<?php echo $search_project; ?>" />

														<input type="hidden" name="hd_process_id" id="hd_process_id" value="" />
														<input type="hidden" name="hd_process_name" id="hd_process_name" value="" />

														<div class="control-group">
																<label class="control-label" for="ddl_task_id">	&nbsp;&nbsp;&nbsp;Task*</label>
																 <div class="controls">
																		<select id="ddl_task_id" class="span5"  name="ddl_task_id">
															 			  <option value="">- - Select Task - -</option>
																		</select>
																	</div>
														</div>

																				 <div class="control-group">

																					 <label class="control-label" for="manpower">&nbsp;&nbsp;&nbsp;Manpower</label>

																					 <div class="controls dropdown">

																						 <textarea rows="4" col="50" name="manpower" class="span5" autocomplete="off" id="manpower"></textarea>

																					 </div> <!-- /controls -->

																				 </div> <!-- /control-group -->

																				 <div class="control-group">

																					 <label class="control-label" for="machine"> &nbsp;&nbsp;&nbsp; Machine</label>

																					 <div class="controls dropdown">

																						 <textarea rows="4" cols="50" name="machine" class="span5" autocomplete="off" id="machine"/></textarea>

																					 </div> <!-- /controls -->

																				 </div> <!-- /control-group -->

																				 <div class="control-group">

																					 <label class="control-label" for="contract">&nbsp;&nbsp;&nbsp;Contract</label>

																					 <div class="controls dropdown">

																						 <textarea rows="4" cols="50" name="contract" class="span5" autocomplete="off" id="contract" /></textarea>

																					 </div> <!-- /controls -->

																				 </div> <!-- /control-group -->

																				 <div class="control-group">

																					<label class="control-label" for="material">&nbsp;&nbsp;&nbsp;Material</label>

																					<div class="controls dropdown">

																						<textarea rows="4" cols="50" name="material" class="span5" autocomplete="off" id="material" /></textarea>

																					</div> <!-- /controls -->

																				</div> <!-- /control-group -->

																				<div class="control-group">

																				 <label class="control-label" for="remarks">&nbsp;&nbsp;&nbsp;To Do List</label>

																				 <div class="controls dropdown">

																					 <textarea rows="4" cols="50" name="remarks" class="span5" autocomplete="off" id="remarks" /></textarea>

																				 </div> <!-- /controls -->

																			 </div> <!-- /control-group -->

																								<div class="modal-footer">

																								 <!-- <button type="submit" name="add_remarks_submit" class="btn btn-primary">Save changes</button> -->
																								 <button type="submit" onclick="return add_remarks()" class="btn btn-primary">Save changes</button>
																								</div>
																								</form>

										</div>
										</div>

								 </div>

							</div>


 						<!-- View Remarks Modal-->
							<!-- <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" 	 style="width:90%;height:50%;left:25%"> -->
								<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width:80%; left: 30%; background-color:transparent">

								 <div class="modal-dialog" role="document" style="width:100%">

										<div class="modal-content" style="width:100%">

											<div class="modal-header">
												 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
													 <span aria-hidden="true">&times;</span>
												 </button>
												 <h3>Project : <span id="project_view_name_display" class="display-name" name="project_view_name_display" ></span> &nbsp;&nbsp;&nbsp;
													 Process : <strong> 	<span id="process_view_name_display" class="display-name" name="process_view_name_display" ></span>
 													</strong>
 												</h3>
											</div>

											<div class="modal-body">
													<form>
														<input type="hidden" id="hd_project_id" name="hd_project_id" value="<?php echo $search_project; ?>" />
															<table id="remarks_data" class="table custom-table table-bordered" style="table-layout: auto;">
																<thead>
																	<tr>
																	<th style="word-wrap:break-word"></th>
																	<th style="word-wrap:break-word">SL No</th>
																	<th style="word-wrap:break-word">Task</th>
																	<th style="word-wrap:break-word">Manpower</th>
																	<th style="word-wrap:break-word">Machine</th>
																	<th style="word-wrap:break-word">Contract</th>
																	<th style="word-wrap:break-word">Material</th>
																	<th style="word-wrap:break-word">Added by</th>
																	<th style="word-wrap:break-word">Added On</th>
																	</tr>
																	</thead>

																<tbody>
																	<input type="hidden" name="hd_remarks_count" value="<?php echo count($process_remarks_list_data); ?>" />
																	<input type="hidden" name="hd_project_id_<?php echo $count; ?>" value="<?php echo $search_project; ?>" />
																	<tr id="strike-me-<?= $count; ?>"></tr>
																</tbody>
															</table>
				 													</form>

											</div>

										</div>

								 </div>

							</div>


							<!-- <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" > -->

							<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="background-color:transparent; border:none; box-shadow:none;">


							   <div class="modal-dialog" role="document"  style="width:100%;">

							     <div class="modal-content" style="width:100%;">

										  <div class="modal-header">
							          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
							          <h4 class="modal-title" id="myModalLabel">Remarks</h4>
							         </div>

							          <div class="modal-body">

							             <form>

							               <input type="hidden" name="hd_remarks_id" id="hd_remarks_id" value="" />
														  <input type="hidden" name="hd_project_id" value="<?php echo $search_project; ?>" />

							               <div class="control-group">
							                 <div class="controls">

							                   <textarea rows="4" cols="50" class="span5" name="completed_remarks" id="completed_remarks" placeholder="Enter Remarks"></textarea>

							                 </div>

							               </div>

							             </div>

							              <div class="modal-footer">
							                 <!-- <button type="button" onclick="close_remarks" name="complete_remarks_submit" class="btn btn-primary">Submit</button> -->
							                 <button type="button" onclick="close_remarks()" class="btn btn-primary">Submit</button>
							              </div>

							              </form>

							             </div>

							           </div>

							            </div>
			   <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>class="span5"
<script src="js/base.js"></script>
<script data-jsfiddle="common" src="js/handsontable-master/demo/js/moment/moment.js"></script>
<script>

function setId(remarks_id)
{
	document.getElementById("hd_remarks_id").value = remarks_id ;
}

function setData(process_id, process_name,project_name,process_master_id){
	document.getElementById("hd_process_id").value = process_id ;
	document.getElementById("hd_process_name").value = process_name ;
	document.getElementById("process_name_display").innerHTML = process_name ;
	document.getElementById("project_name_display").innerHTML = project_name ;

$(document).ready(function() {
	$.ajax({
	  url: 'ajax/project_get_task_master.php',
	  data: {process_id: process_master_id},
	  dataType: 'json',
	  success: function(response) {

	    $("#ddl_task_id").empty();
	    for(var i=0; i< response.length; i++) {
	      var id = response[i]['project_task_master_id'];
	      var name = response[i]['project_task_master_name'];
	      // var class = 'list-group-item';

	       $("#ddl_task_id").append("<option value='"+id+"'>"+name+"</option>");
	    //  $("#sel_user").append("<div id='"+id+"'>"+name+"</div>");
	    }
	  }
	})
})
}

function setData1(process_id, process_name,project_name){
	var process_id = process_id;
	document.getElementById("process_view_name_display").innerHTML = process_name ;
	document.getElementById("project_view_name_display").innerHTML = project_name ;

	show_remarks_table(process_id);
}

$(document).ready(function() {
	var ids=[];


 $('input:hidden.row_process_ids').each(function(){
	 // ids.push($(this).val());
	 var process_id = $(this).val(),
	 	data =  {
			actual_start_date: $('#aedate_'+process_id).data('asdate'),
			actual_end_date: $('#aedate_'+process_id).data('aedate'),
			pdays: $('#plan_days_'+process_id).data('pdays'),
		};
	 $.post(
 		'ajax/project_get_completed_percentage.php?process_id='+process_id,
 		 data,
 		function(response) {
			  // $(response['process_id']).html(response["cp"] + '/'+ response["pending"]);
				var template = '<div class="progress">';
				template += 	'	 <div class="progress-bar progress-bar-success" style="width:'+response.cp+'%;">';
				template += 	'		<a href="project_process_level_details.php?process_id='+response.process_id+'" style="color:#000000" target="_blank">'+response.cp+'%</a>';
				template += 	'		<span class="sr-only">(success)</span>';
				template += 	'	</div>';
				template += 	'	<div class="progress-bar progress-bar-danger" style="width:'+response.pending+'%">';
				template += 	'	<a href="project_process_level_details.php?process_id='+response.process_id+'" style="color:#FFFFFF" target="_blank">'+response.pending+'%</a>';
				template += 	'	<span class="sr-only">(danger)</span>';
				template += 	'	</div>';
				template += 	'	</div>';
			  $(response['element_id']).html(template);
				$('#actual_days_'+response['process_id']).html(response["actual_days"]);

			  $('#aedate_'+response['process_id']).html(response["actual_end_date"]);
			  $('#varience_'+response['process_id']).html(response["varience"]);
 		},
		'json'
 	)
 })
})

function show_remarks_table(process_id) {

	console.log('got process_id ', process_id);

	var process_id = process_id ? process_id : document.getElementById("hd_process_id").value;

	var project_id = document.getElementById("hd_project_id").value;

	$("#remarks_data").empty();
	var header = $("<thead style='background-color:#00ba8b; color:white'><tr><td>Action</td><td style='width:1%'>#</td><td>Task</td><td>To Do List</td><td>Manpower</td><td>Machine</td><td>Contract</td><td>Material</td><td>Added By</td><td style='width:8%'>Added On</td></tr></thead>");
	$("#remarks_data").append(header);
$(document).ready(function() {
	$.ajax({
		url: 'ajax/project_get_process_remarks.php',
		data: {process_id: process_id},
		dataType: 'json',
		success: function(response) {
			console.log('remarks' ,response);
			for(var i=0; i< response.length; i++) {
				var sl_no = i+1 ;
				var cb = $('<input/>').attr({ type: 'checkbox', name:'chk'}).addClass("chk");
				var task = response[i]['project_task_master_name'];
				var mp = response[i]['remarks_list_manpower'];
				var mc = response[i]['remarks_list_machine'];
				var cw = response[i]['remarks_list_contract'];
				var material = response[i]['remarks_list_material'];
				var user_name = response[i]['user_name'];
				var added_on = moment(response[i]['remarks_list_added_on']).format('DD/MM/YY hh:mm a');
				var remarks_id = response[i]['remarks_list_id'];
				var remarks_data = response[i]['remarks_list_remarks'];


				var row = $("<tr><td><a href='#' data-toggle='modal' onclick='setId("+remarks_id+")' data-target='#myModal2' >Complete</a></td><td>"+sl_no+"</td><td>"+task+"</td><td>" + remarks_data +"</td><td>"+mp+"</td><td>"+mc+"</td><td>"+cw+"</td><td>"+material+"</td><td>"+user_name+"</td><td>"+added_on+"</td></tr>");
				$("#remarks_data").append(row);
			}
		}
	})
})
}

function project_delete_project_plan_process(process_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_plan_process_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_project_plan_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("process_id=" + process_id + "&action=0");
		}
	}
}


function project_approve_project_plan_process(process_id)
{
	var ok = confirm("Are you sure you want to Approve?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_plan_process_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_approve_project_plan_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("process_id=" + process_id + "&action=Approved");
		}
	}
}

function close_remarks() {
	var remarks_id 			 = document.getElementById('hd_remarks_id').value;
	var project_id		     = document.getElementById('hd_project_id'.value);
	var completed_remarks = document.getElementById('completed_remarks').value;


	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange = function()
	{
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{
			if(xmlhttp.responseText != "SUCCESS")
			{
			 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
			 document.getElementById("span_msg").style.color = "red";
			}
			else
			{
			 window.location = "project_completion_report.php";
			}
		}
	}

	xmlhttp.open("POST", "project_completion_report.php");
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("remarks_id=" + remarks_id + "&project_id=" +project_id + "&completed_remarks=" + completed_remarks);
	// reset_form_data();

	$('#myModal2').modal('hide');
	show_remarks_table();
	document.getElementById('completed_remarks').value = '';
	return false;
}

function add_remarks()
{

	// alert('am in submit remarks');
	$('#myModal').modal('hide');

	var project_id = document.getElementById('hd_project_id').value;
	var process_id = document.getElementById('hd_process_id').value;
	var process_name = document.getElementById('hd_process_name').value;
	var task_id = document.getElementById('ddl_task_id').value;
	var manpower = document.getElementById('manpower').value;
	var machine = document.getElementById('machine').value;
	var material = document.getElementById('material').value;
	var contract = document.getElementById('contract').value;
	var remarks = document.getElementById('remarks').value;

	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange = function()
	{
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{
			if(xmlhttp.responseText != "SUCCESS")
			{
			 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
			 document.getElementById("span_msg").style.color = "red";
			}
			else
			{
			 window.location = "project_completion_report.php";
			}
		}
	}

	xmlhttp.open("POST", "project_completion_report.php");
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("project_id=" + project_id + "&process_id=" +process_id + "&process_name=" + process_name + "&task_id="+ task_id + "&manpower="+ manpower + "&machine=" + machine + "&material=" + material + "&contract=" + contract + "&remarks=" + remarks);
	reset_form_data();
	return false;
}

function reset_form_data() {
document.getElementById('manpower').value = '';
document.getElementById('machine').value = '';
document.getElementById('material').value = '';
document.getElementById('contract').value = '';
document.getElementById('remarks').value = '';
}

function project_reject_project_plan_process(process_id)
{
	var ok = confirm("Are you sure you want to Approve?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_plan_process_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_reject_project_plan_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("process_id=" + process_id + "&action=Approved");
		}
	}
}

function go_to_project_edit_project_plan_process(process_id,plan_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "project_edit_project_plan_process.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","plan_id");
	hiddenField2.setAttribute("value",plan_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);

	document.body.appendChild(form);
    form.submit();
}

function go_to_project_plan_process_task(project_plan_process_id,plan_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "project_plan_process_task_list.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_plan_process_id");
	hiddenField1.setAttribute("value",project_plan_process_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","plan_id");
	hiddenField2.setAttribute("value",plan_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);

	document.body.appendChild(form);
    form.submit();
}
</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>
