<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 09-Sept-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */


/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');


if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];


	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET['response_id']))
	{
		$response_id = $_GET['response_id'];
	}
	else
	{
		$response_id = "";
	}
	
	
	if(isset($_GET['query_id']))
	{
		$query_id = $_GET['query_id'];
	}
	else
	{
		$query_id = "";
	}
	
	// Capture the form data
	if(isset($_POST["edit_response_submit"]))
	{
		
		$response_id      = $_POST["hd_response_id"];
		$query_id         = $_POST["hd_query_id"];
		$response         = $_POST["txt_response"];
		$status           = $_POST["status"];
		$follow_up_date   = $_POST["follow_up_date"];
		$remarks          = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($response != "") && ($follow_up_date != ""))
		{
			$survey_response_update_data = array("response"=>$response,"status"=>$status,"follow_up_date"=>$follow_up_date,"remarks"=>$remarks);
			$survey_response_iresult = i_update_survey_response($response_id,$survey_response_update_data);
			
			if($survey_response_iresult["status"] == SUCCESS)				
			{	
				$alert_type = 1;
		    
				header("location:survey_response.php?query_id=$query_id&response_id=$response_id");
			}
			else
			{
				$alert 		= $survey_response_iresult["data"];
				$alert_type = 0;	
			}						
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get Response modes already added
	$survey_response_search_data = array("active"=>'1',"response_id"=>$response_id,"query_id"=>$query_id);
	$survey_response_list = i_get_survey_response($survey_response_search_data);
	if($survey_response_list['status'] == SUCCESS)
	{
		$survey_response_list_data = $survey_response_list['data'];
	}	
}
else
{
	header("location:login.php");
}	

?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>survey - Edit Response</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>survey - Edit Response</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">survey - Edit Response</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="survey_edit_response_form" class="form-horizontal" method="post" action="survey_edit_response.php">
								<input type="hidden" name="hd_response_id" value="<?php echo $response_id; ?>" />
								<input type="hidden" name="hd_query_id" value="<?php echo $query_id; ?>" />
									<fieldset>
									
									<div class="control-group">											
											<label class="control-label" for="txt_response">Response*</label>
											<div class="controls">
												<textarea name="txt_response" rows="4" cols="50" class="span6" placeholder="txt_response"><?php echo $survey_response_list_data[0]["survey_response"] ;?></textarea>
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->

									
										 <div class="control-group">											
											<label class="control-label" for="follow_up_date">Follow Up Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="follow_up_date" placeholder="Date" value="<?php echo $survey_response_list_data[0]["survey_response_follow_up_date"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="status">Status</label>
											<div class="controls">
												<input type="radio" name="status" checked value="Completed" <?php if ($survey_response_list_data[0]["survey_response_status"] == "Completed") {?> checked="checked"<?php }?>>&nbsp;&nbsp;&nbsp;Completed&nbsp;&nbsp;&nbsp;
												<input type="radio" name="status" value="Not Completed" <?php if ($survey_response_list_data[0]["survey_response_status"] == "Not Completed") {?> checked="checked"<?php } ?>>&nbsp;&nbsp;&nbsp;&nbsp;Not Completed&nbsp;&nbsp;
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->	
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks" value="<?php echo $survey_response_list_data[0]["survey_response_remarks"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_response_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

  </body>

</html>
