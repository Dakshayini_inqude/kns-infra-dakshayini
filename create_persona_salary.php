<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
/* TBD - START */

// 1. Role dropdown should be from database

/* TBD - END */

/* DEFINES - START */
define('ADD_USER_FUNC_ID', '1');
/* DEFINES - END */

/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_user.php');

/* INCLUDES - END */

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {

    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // // Get permission settings for this user for this page

    $view_perms_list = i_get_user_perms($user, '', ADD_USER_FUNC_ID, '2', '1');

    $add_perms_list  = i_get_user_perms($user, '', ADD_USER_FUNC_ID, '1', '1');

    /* DATA INITIALIZATION - START */
    $alert = "";
    $alert_type = -1;
    /* DATA INITIALIZATION - END */

    // Capture the form data
    if (isset($_POST["create_persona_salary_submit"])) {
        $persona_department_id   = $_POST["ddl_department_id"];
        $persona_user_id         = $_POST["sel_user"];
        $basic                   = $_POST["basic"];
        $hra                     = $_POST["hra"];
        $conveyance_allowance    = $_POST["conveyance_allowance"];
        $special_allowance       = $_POST["special_allowance"];
        $educational_allowance   = $_POST["educational_allowance"];
        $medical_reimbursement   = $_POST["medical_reimbursement"];
        $esi_employee_amount     = $_POST["esi_employee_amount"];
        $esi_company_amount      = $_POST["esi_company_amount"];
        $pf_amount               = $_POST["pf_amount"];
        $statutory_bonus         = $_POST["statutory_bonus"];
        $gratuity                = $_POST["gratuity"];
        $lta                     = $_POST["lta"];
        $pli                     = 0;
        $annual_benefits         = $_POST["annual_benefits"];
        $annual_ctc              = $_POST["annual_ctc"];
        $ddl_company             = $_POST["ddl_company"];
        $bank_name               = $_POST["bank_name"];
        $bank_account_number     = $_POST["bank_account_number"];
        $ifsc_code               = $_POST["ifsc_code"];
        $bank_branch             = $_POST["bank_branch"];
        $bank_city               = $_POST["bank_city"];

          if (($persona_department_id != "0") && ($persona_user_id != "0")  &&($basic != "0") && ($hra != "0") && ($conveyance_allowance != "0") &&
          ($special_allowance != "0")  && ($medical_reimbursement != "0") && ($pf_amount != "0") &&
          ($statutory_bonus != "0") && ($gratuity != "") && ($lta != "0") && ($ddl_company != "") &&
          ($ifsc_code != "") && ($bank_branch != "") && ($bank_city != "")) {

              $isExists = db_persona_salary_exists($persona_user_id);

              if ($isExists["status"] == -103) {
                  ?>
            <script>
              alert('Person record already exists');
            </script>
            <?php
              } elseif ($isExists["status"] == -104) {
                  $response = db_add_persona_salary($persona_department_id,
                  $persona_user_id,
                  $basic,
                  $hra,
                  $conveyance_allowance,
                  $special_allowance,
                  $educational_allowance,
                  $medical_reimbursement,
                  $pf_amount,
                  $statutory_bonus,
                  $gratuity,
                  $lta,
                  $pli,
                  $ddl_company,
                  $bank_name,
                  $bank_account_number,
                  $ifsc_code,
                  $bank_branch,
                  $bank_city,
                  $esi_employee_amount,
                  $esi_company_amount
                      );

                  var_dump($response);
                  if ($response["status"] == 'SUCCESS') {
                      // $_POST = array();
                      ?>
                        <script>
                        alert("Persona saved successfully ");
                         console.log('saved values ', <?= json_encode($_POST); ?>);
                        </script>
                        <?php
                  }
              }

              // var_dump($_POST);
          } else {
              $alert = "Please fill all the mandatory fields";
              $alert_type = 0; ?>
          <script>
          alert("Please fill all the mandatory fields");
            console.log('values ', <?= json_encode($_POST); ?>);
          </script>
          <?php
          }
    }

    // Get list of Department*
    $department_list = i_get_department_list('', '');

    if ($department_list["status"] == SUCCESS) {
        $department_list_data = $department_list["data"];
    } else {
        $alert = $alert."Alert: ".$department_list["data"];
    }

    $bank_list = db_get_salary_bank_list();
    if ($bank_list['status'] == 'DB_RECORD_ALREADY_EXISTS') {
        $bank_list_data = $bank_list['data'];
    }

    $designation_list = db_get_designation_list();
    if ($designation_list['status'] == 'DB_RECORD_ALREADY_EXISTS') {
        $designation_list_data = $designation_list['data'];
    }

    // Get list of Companies
    $stock_company_master_search_data = array();

    $company_list = i_get_company_list($stock_company_master_search_data);

    if ($company_list["status"] == SUCCESS) {
        $company_list_data = $company_list["data"];
    }
} else {
    header("location:login.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>Persona Salary</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->
    <!-- Custom styles -->
    <link href="assets/multistepform/css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
  <?php

  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

  ?>
<!-- MultiStep Form -->
<div class="row">
    <div class="span8" style="margin-top:-50px; margin-bottom:50px">
        <form id="msform" method="post">
            <!-- progressbar -->
            <ul id="progressbar">
                <li class="active">Basic Details</li>
                <li>Salary Details</li>
                <li>Bank Details</li>
            </ul>
            <!-- fieldsets -->
            <fieldset>
                <h2 class="fs-title">Basic Details</h2>
                <h3 class="fs-subtitle">User Information</h3>
                <div class="col-xs-6" style="text-align:left">
                  <div class="form-group">
                    <label style="font-weight:400">Department</label>
                    <select class="form-control" id="ddl_department_id" name="ddl_department_id" required>
                      <option value='0'>- - Select Department - -</option>
                      <?php
                        for ($count = 0; $count < count($department_list_data); $count++) {
                            ?>
                      <option value="<?php echo $department_list_data[$count]["general_task_department_id"]; ?>"><?php echo $department_list_data[$count]["general_task_department_name"]; ?></option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label style="font-weight:400">User</label>
                    <select class="form-control" id="sel_user" name="sel_user">
                    <option value='0'>- - Select User - -</option>
                    </select>
                  </div>
                  <label style="font-weight:400">Manager</label>
                  <input type="text" id="manager" name="manager" placeholder="DOJ" disabled/>
                  <label style="font-weight:400">DOJ</label>
                  <input type="text" id="doj" name="doj" placeholder="DOJ" disabled/>
              </div>
              <div class="col-xs-6" style="text-align:left">
                <label style="font-weight:400">Role</label>
                <input type="text" id="user_role" name="user_role" placeholder="Role" disabled/>
                <label style="font-weight:400">E-mail</label>
                <input type="text" id="user_email_id" name="user_email_id" placeholder="E-mail" disabled/>
                <label style="font-weight:400">Location</label>
                <input type="text" id="user_location" name="user_location" placeholder="Location" disabled/>
              </div>
                <input type="button" name="next" class="next action-button" value="Next"/>
                <br/>
            </fieldset>
            <fieldset>
                <h2 class="fs-title">Salary Details</h2>
                <h3 class="fs-subtitle">Salary Break-down Details</h3>
                <div class="col-xs-6" style="text-align:left">
                  <label style="font-weight:400">Basic</label>
                  <input type="number" min="0" id="basic" name="basic" onchange="return calculate()" placeholder="Basic"/>
                  <label style="font-weight:400">HRA</label>
                  <input type="number" min="0" id="hra" name="hra" onchange="return calculate()" placeholder="HRA"/>
                  <label style="font-weight:400">Leave Travelling Allowance (LTA)</label>
                  <input type="number" min="0" id="conveyance_allowance" name="conveyance_allowance" onchange="return calculate()" placeholder="LTA"/>
                  <label style="font-weight:400">Special Allowance</label>
                  <input type="number" min="0" id="special_allowance" name="special_allowance" onchange="return calculate()" placeholder="Special Allowance"/>
                  <label style="font-weight:400">Eduacational Allowance</label>
                  <input type="number" id="educational_allowance" name="educational_allowance" placeholder="Educational Allowance"/>
                  <label style="font-weight:400">Medical Reimbursement</label>
                  <input type="number" min="0" id="medical_reimbursement" name="medical_reimbursement" onchange="return calculate()" placeholder="Medical Reimbursement"/>
                  <!-- auto-calculate fields -->
                  <label style="font-weight:400">Gross Monthly</label>
                  <input type="number" min="0" id="gross_monthly" name="gross_monthly" placeholder="Gross Monthly" disabled/>
                  <label style="font-weight:400">Gross Annual</label>
                  <input type="number" min="0" id="gross_annual" name="gross_annual" placeholder="Gross Annual" disabled/>
                  <label style="font-weight:400">Employee Contribution ESI Amount</label>
                  <input type="number" id="esi_employee_amount" name="esi_employee_amount" placeholder="Employee Contribution ESI Amount"/>
                  <label style="font-weight:400">Company Contribution ESI Amount</label>
                  <input type="number" id="esi_company_amount" name="esi_company_amount" placeholder="Company Contribution ESI Amount"/>
              </div>
                <div class="col-xs-6" style="text-align:left">
                  <label style="font-weight:400">Company PF Contribution</label>
                  <input type="number" id="pf_amount" name="pf_amount" placeholder="PF Employer"/>
                  <label style="font-weight:400">Employee PF Contribution</label>
                  <input type="number" id="lta" name="lta" placeholder="LTA"/>

                  <label style="font-weight:400">Statutory Bonus</label>
                  <input type="number" id="statutory_bonus" name="statutory_bonus" placeholder="Statutory Bonus"/>
                  <label style="font-weight:400">Gratuity</label>
                  <input type="number" id="gratuity" name="gratuity" placeholder="Gratuity"/>

                  <!-- <label style="font-weight:400">PLI</label>
                  <input type="number" id="pli" name="pli" placeholder="PLI"/> -->
                  <label style="font-weight:400">Monthly Benefits</label>
                  <input type="number" id="monthly_benefits" name="monthly_benefits" placeholder="Monthly Benefits"/>
                  <label style="font-weight:400">Annual Benefits</label>
                  <input type="number" id="annual_benefits" name="annual_benefits" placeholder="Annual Benefits"/>
                  <label style="font-weight:400">Monthly CTC</label>
                  <input type="number" id="monthly_ctc" name="monthly_ctc" placeholder="Monthly CTC"/>
                  <label style="font-weight:400">Annual CTC</label>
                  <input type="number" id="annual_ctc" name="annual_ctc" placeholder="Annual CTC"/>
              </div>
                <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                <input type="button" name="next" class="next action-button" value="Next"/>
            </fieldset>
            <fieldset>
                <h2 class="fs-title">Bank Details</h2>
                <h3 class="fs-subtitle">Salary crediting Bank details</h3>
                <div class="col-xs-6" style="text-align:left">
                  <div class="form-group">
                    <label style="font-weight:400">Company</label>
                    <select class="form-control" id="ddl_company" name="ddl_company" required>
                    <option value="">- - - Select Company - - -</option>
                    <?php
                    for ($count = 0; $count < count($company_list_data); $count++) {
                        ?>
                    <option value="<?php echo $company_list_data[$count]["stock_company_master_id"]; ?>"><?php echo $company_list_data[$count]["stock_company_master_name"]; ?></option>
                    <?php
                    }
                    ?>

                    </select>
                  </div>
                  <label style="font-weight:400">Account Number</label>
                  <input type="text" name="bank_account_number" placeholder="Bank Account Number"/>

                  <!-- Bank list -->
                  <div class="form-group">
                    <label style="font-weight:400">Bank</label>
                    <select class="form-control" id="ddl_bank" name="ddl_bank" required>
                    <option value="0">- - - Select Bank - - -</option>
                    <?php
                    for ($count = 0; $count < count($bank_list_data); $count++) {
                        ?>
                    <option value="<?php echo $bank_list_data[$count]["bank_id"]; ?>"><?php echo $bank_list_data[$count]["bank_name"]; ?></option>
                    <?php
                    }
                    ?>

                    </select>
                  </div>

                  <label style="font-weight:400">Bank Name</label>
                  <input type="text" id="bank_name" name="bank_name" placeholder="Bank Name"/>
                </div>
                <div class="col-xs-6" style="text-align:left">
                  <label style="font-weight:400">IFSC Code</label>
                  <input type="text" id="ifsc_code" name="ifsc_code" placeholder="IFSC Code"/>
                  <label style="font-weight:400">Branch Name</label>
                  <input type="text" id="bank_branch" name="bank_branch" placeholder="Branch"/>
                  <label style="font-weight:400">City</label>
                  <input type="text" id="bank_city" name="bank_city" placeholder="City"/>
                </div>
                <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                <input type="submit" id="create_persona_salary_submit" name="create_persona_salary_submit" class="submit action-button" value="Submit"/>
            </fieldset>
        </form>
    </div>
</div>
<!-- /.MultiStep Form -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/multistepform/js/msform.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->

<script>
function calculate(){

  var basic = document.getElementById('basic').value ? document.getElementById('basic').value : 0,
      hra = document.getElementById('hra').value ? document.getElementById('hra').value : 0,
     conveyance_allowance = document.getElementById('conveyance_allowance').value ? document.getElementById('conveyance_allowance').value : 0,
     special_allowance = document.getElementById('special_allowance').value  ? document.getElementById('special_allowance').value : 0,
     educational_allowance = document.getElementById('educational_allowance').value  ? document.getElementById('educational_allowance').value : 0,
     medical_reimbursement = document.getElementById('medical_reimbursement').value ? document.getElementById('medical_reimbursement').value : 0;

    var gross_monthly = parseInt(basic) + parseInt(hra) + parseInt(educational_allowance) + parseInt(conveyance_allowance) +
                        parseInt(special_allowance) + parseInt(medical_reimbursement);

                        document.getElementById('gross_monthly').value = gross_monthly;

    // ESI amount
    var esi_employee_amount =  parseInt(gross_monthly * (1.75/100));
    var esi_company_amount =   parseInt(gross_monthly * (4.75/100));

    document.getElementById('esi_employee_amount').value = esi_employee_amount;
    document.getElementById('esi_company_amount').value = esi_company_amount;


    document.getElementById('gross_annual').value = gross_monthly * 12;

    var pf_amount = parseInt(((12 * parseInt(basic)) / 100));
    document.getElementById('pf_amount').value = pf_amount;

    var statutory_bonus = parseInt(((8.33 * parseInt(gross_monthly)) / 100));
    document.getElementById('statutory_bonus').value = statutory_bonus;

    var gratuity = parseInt(((4.81 * parseInt(basic)) / 100) * 12);
    document.getElementById('gratuity').value = gratuity;

    document.getElementById('lta').value = parseInt(pf_amount);

    var monthly_benefits = pf_amount + esi_company_amount + statutory_bonus;

    // var annual_benefits = pf_amount + statutory_bonus + gratuity + parseInt(basic);
    document.getElementById('monthly_benefits').value = monthly_benefits;

    document.getElementById('annual_benefits').value = monthly_benefits * 12;

    var monthly_ctc = gross_monthly + pf_amount + esi_company_amount + statutory_bonus;
    document.getElementById('monthly_ctc').value = monthly_ctc;

    document.getElementById('annual_ctc').value = monthly_ctc * 12;

    // document.getElementById('pli').value = 0;

}

$(document).ready(function() {
  var users;
  $("#ddl_department_id").change(function() {
		var department_id = $(this).val();
		console.log('department_id ', department_id);
		if(department_id == 0) {
			$("#sel_user").empty();
      $("#sel_user").append("  <option value='0'>- - Select User - -</option>");
			return false;
		}

		$.ajax({
			url: 'ajax/getUsers.php',
			data: {department_id: department_id},
			dataType: 'json',
			success: function(response) {
				console.log('hope i got all users ', response);
        users = response;
				$("#sel_user").empty();
        $("#sel_user").append("  <option value='0'>- - Select User - -</option>");
				for(var i=0; i< response.length; i++) {
					var id = response[i]['user_id'];
					var name = response[i]['user_name'];

					$("#sel_user").append("<option value='"+id+"'>"+name+"</option>");
				}
			}
		})
	})

  $("#sel_user").change(function() {
		var user_id = $(this).val();
		console.log('sel_user ', user_id, users);
		if(user_id == 0) {
      $("#user_email_id").val('');
      $("#user_role").val('');
      $("#manager").val('');
      $("#user_location").val('');
      $("#doj").val('');
			return false;
		}

    $.ajax({
      url: 'ajax/get_persona_basic_details.php',
      data: {user_id: user_id},
      dataType: 'json',
      success: function(response) {

        $("#user_email_id").val('');
        $("#user_role").val('');
        $("#manager").val('');
        $("#user_location").val('');
        $("#doj").val('');
        if(response && response.length) {
          $("#user_email_id").val(response[0]['user_email_id']);
          $("#user_role").val(get_user_role_name(response[0]['user_role']));
          $("#manager").val(response[0]['manager']);
          $("#user_location").val(response[0]['user_location']);
          $("#doj").val(response[0]['persona_dob']);
        }
      }
    })
	})

  $("#ddl_bank").change(function() {
		var bank_id = $(this).val();
		console.log('select bank ', bank_id);
		if(bank_id == 0) {
      $("#bank_name").val('');
      $("#ifsc_code").val('');
      $("#bank_branch").val('');
      $("#bank_city").val('');
			return false;
		}

    $.ajax({
      url: 'ajax/get_bank_details.php',
      data: {bank_id: bank_id},
      dataType: 'json',
      success: function(response) {

        console.log('bank response ', response);

        $("#bank_name").val('');
        $("#ifsc_code").val('');
        $("#bank_branch").val('');
        $("#bank_city").val('');
        if(response && response.length) {
          $("#bank_name").val(response[0]['bank_name']);
          $("#ifsc_code").val(response[0]['bank_ifsc_code']);
          $("#bank_branch").val(response[0]['bank_branch_name']);
          $("#bank_city").val(response[0]['bank_city']);
        }
      }
    })
	})

function get_user_role_name(role_id) {

  var role = {
  1: 'Admin',
  2: 'Legal HOD',
  3: 'Legal User',
  4: 'Other User',
  5: 'Sales HOD',
  6: 'CRM Telecaller',
  7: 'CRM Sales',
  8: 'CRM Channel Partner',
  9: 'Cab Manager',
  10: 'CRM HOD',
  11: 'CRM User',
  12: 'Finance Manager',
  13: 'HR Manager'
  }
return role[role_id];
}

})

</script>
</body>
</html>
