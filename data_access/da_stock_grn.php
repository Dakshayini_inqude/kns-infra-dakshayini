<?php$base = $_SERVER['DOCUMENT_ROOT'];include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');/*LIST OF TBDs1. Wherever there is name search, there should be a wildcard*//*PURPOSE : To add new Stock GRNINPUT 	: Purchase Order Id,Invoice Number,Invoice Date,Location,DC Number,DC Date,Vehicle Number,Remarks,Added ByOUTPUT 	: GRN Id, success or failure messageBY 		: Lakshmi*/
function db_add_stock_grn($grn_no, $purchase_order_id, $invoice_number, $invoice_date, $location, $project, $doc, $dc_number, $dc_date, $vehicle_number, $remarks, $added_by)
{    // Query
    $stock_grn_iquery = "insert into stock_grn (stock_grn_no,stock_grn_purchase_order_id,stock_grn_invoice_number,stock_grn_invoice_date,stock_grn_location,stock_grn_project,stock_grn_doc,stock_grn_dc_number,stock_grn_dc_date, stock_grn_vehicle_number,stock_grn_remarks,stock_grn_active,stock_grn_added_by,stock_grn_added_on) values(:grn_no,:purchase_order_id,:invoice_number,:invoice_date,:location,:project,:doc,:dc_number,:dc_date,:vehicle_number,:remarks,:active,:added_by,:added_on)";
    try {
        $dbConnection = get_conn_handle();
        $stock_grn_istatement = $dbConnection->prepare($stock_grn_iquery);        
        // Data
        $stock_grn_idata = array(':grn_no'=>$grn_no,':purchase_order_id'=>$purchase_order_id,':invoice_number'=>$invoice_number,':invoice_date'=>$invoice_date,':location'=>$location,':project'=>$project,':doc'=>$doc,':dc_number'=>$dc_number,':dc_date'=>$dc_date,':vehicle_number'=>$vehicle_number,':remarks'=>$remarks,':active'=>'1',':added_by'=>$added_by,        ':added_on'=>date("Y-m-d H:i:s"));    
        $dbConnection->beginTransaction();
        $stock_grn_istatement->execute($stock_grn_idata);
        $stock_grn_id = $dbConnection->lastInsertId();
        $dbConnection->commit();       
        $return["status"] = SUCCESS;
        $return["data"]   = $stock_grn_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }    
    return $return;
}/*PURPOSE : To get Stock GRN listINPUT 	: Grn Id,Purchase Order Id,Invoice Number,Invoice Date,DC Number,DC Date,Vehicle Number,Added By,,Start Date(for added on), End Date(for added on)OUTPUT 	: List of Stock GrnBY 		: Lakshmi*/
function db_get_stock_grn_list($stock_grn_search_data)
{
    if (array_key_exists("grn_id", $stock_grn_search_data)) {
        $grn_id = $stock_grn_search_data["grn_id"];
    } else {
        $grn_id= "";
    }    
    if (array_key_exists("grn_no", $stock_grn_search_data)) {
        $grn_no = $stock_grn_search_data["grn_no"];
    } else {
        $grn_no= "";
    }    
    if (array_key_exists("grn_no_check", $stock_grn_search_data)) {
        $grn_no_check = $stock_grn_search_data["grn_no_check"];
    } else {
        $grn_no_check= "";
    }    
    if (array_key_exists("purchase_order_id", $stock_grn_search_data)) {
        $purchase_order_id= $stock_grn_search_data["purchase_order_id"];
    } else {
        $purchase_order_id = "";
    }    
    if (array_key_exists("invoice_number", $stock_grn_search_data)) {
        $invoice_number= $stock_grn_search_data["invoice_number"];
    } else {
        $invoice_number = "";
    }    
    if (array_key_exists("invoice_date", $stock_grn_search_data)) {
        $invoice_date= $stock_grn_search_data["invoice_date"];
    } else {
        $invoice_date= "";
    }    
    if (array_key_exists("location", $stock_grn_search_data)) {
        $location = $stock_grn_search_data["location"];
    } else {
        $location = "";
    }
    if (array_key_exists("project", $stock_grn_search_data)) {
        $project = $stock_grn_search_data["project"];
    } else {
        $project = "";
    }
    if (array_key_exists("doc", $stock_grn_search_data)) {
        $doc = $stock_grn_search_data["doc"];
    } else {
        $doc = "";
    }    
    if (array_key_exists("dc_number", $stock_grn_search_data)) {
        $dc_number = $stock_grn_search_data["dc_number"];
    } else {
        $dc_number = "";
    }
    if (array_key_exists("dc_date", $stock_grn_search_data)) {
        $dc_date = $stock_grn_search_data["dc_date"];
    } else {
        $dc_date = "";
    }    
    if (array_key_exists("vehicle_number", $stock_grn_search_data)) {
        $vehicle_number= $stock_grn_search_data["vehicle_number"];
    } else {
        $vehicle_number= "";
    }    
    if (array_key_exists("added_by", $stock_grn_search_data)) {
        $added_by= $stock_grn_search_data["added_by"];
    } else {
        $added_by= "";
    }    
    if (array_key_exists("active", $stock_grn_search_data)) {
        $active= $stock_grn_search_data["active"];
    } else {
        $active= "";
    }
    if (array_key_exists("start_date", $stock_grn_search_data)) {
        $start_date= $stock_grn_search_data["start_date"];
    } else {
        $start_date= "";
    }
    if (array_key_exists("end_date", $stock_grn_search_data)) {
        $end_date= $stock_grn_search_data["end_date"];
    } else {
        $end_date= "";
    }
    $get_stock_grn_list_squery_base = "select * from stock_grn SG inner join users U on U.user_id = SG.stock_grn_added_by inner join stock_purchase_order SPO on SPO.stock_purchase_order_id=SG.stock_grn_purchase_order_id inner join stock_vendor_master SVM on SVM.stock_vendor_id= SPO.stock_purchase_order_vendor inner join stock_company_master SCM on SCM.stock_company_master_id=SPO.stock_purchase_order_company inner join stock_project SP on SP.stock_project_id=SG.stock_grn_project";    
    $get_stock_grn_list_squery_where = "";    
    $get_grn_list_squery_order = "";    
    $filter_count = 0;    
    // Data
    $get_stock_grn_list_sdata = array();    
    if ($grn_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_id=:grn_id";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_id=:grn_id";
        }        
        // Data
        $get_stock_grn_list_sdata[':grn_id'] = $grn_id;        
        $filter_count++;
    }    
    if ($grn_no != "") {
        if ($filter_count == 0) {
            if ($grn_no_check == '1') {
                $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_no = :grn_no";                    
                // Data
                $get_stock_grn_list_sdata[':grn_no']  = $grn_no;
            } elseif ($grn_no_check == '2') {
                $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_no like :grn_no";
                // Data
                $get_stock_grn_list_sdata[':grn_no']  = '%'.$grn_no;
            } else {
                $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_no like :grn_no";
                // Data
                $get_stock_grn_list_sdata[':grn_no']  = '%'.$grn_no.'%';
            }
        } else {
            if ($grn_no_check == '1') {
                $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_no = :grn_no";                    
                // Data
                $get_stock_grn_list_sdata[':grn_no']  = $grn_no;
            } elseif ($grn_no_check == '2') {
                $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_no like :grn_no";
                // Data
                $get_stock_grn_list_sdata[':grn_no']  = '%'.$grn_no;
            } else {
                $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_no like :grn_no";                
                // Data
                $get_stock_grn_list_sdata[':grn_no']  = '%'.$grn_no.'%';
            }
        }
        $filter_count++;
    }        
    if ($purchase_order_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_purchase_order_id=:purchase_order_id";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_purchase_order_id=:purchase_order_id";
        }        
        // Data
        $get_stock_grn_list_sdata[':purchase_order_id']  = $purchase_order_id;        
        $filter_count++;
    }    
    if ($invoice_number != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_invoice_number=:invoice_number";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_invoice_number=:invoice_number";
        }        
        // Data
        $get_stock_grn_list_sdata[':invoice_number']  = $invoice_number;        
        $filter_count++;
    }    
    if ($invoice_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_invoice_date=:invoice_date";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_invoice_date=:invoice_date";
        }        
        // Data
        $get_stock_grn_list_sdata[':invoice_date']  = $invoice_date;        
        $filter_count++;
    }    
    if ($location != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_location=:location";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_location=:location";
        }        
        // Data
        $get_stock_grn_list_sdata[':location']  = $location;        
        $filter_count++;
    }
    if ($project != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_project=:project";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_project=:project";
        }
        // Data
        $get_stock_grn_list_sdata[':project']  = $project;
        $filter_count++;
    }
    if ($doc != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_doc=:doc";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_doc=:doc";
        }        
        // Data
        $get_stock_grn_list_sdata[':doc']  = $doc;        
        $filter_count++;
    }    
    if ($dc_number != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_dc_number=:dc_number";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_dc_number=:dc_number";
        }        
        // Data
        $get_stock_grn_list_sdata[':dc_number']  = $dc_number;        
        $filter_count++;
    }    
    if ($vehicle_number != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_vehicle_number=:vehicle_number";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_vehicle_number=:vehicle_number";
        }        
        // Data
        $get_stock_grn_list_sdata[':vehicle_number']  = $vehicle_number;        
        $filter_count++;
    }    
    if ($added_by!= "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_added_by >= :added_by";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_added_by >= :added_by";
        }        
        //Data
        $get_stock_grn_list_sdata[':added_by']  = $added_by;        
        $filter_count++;
    }    
    if ($active != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_active = :active";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_active = :active";
        }        
        //Data
        $get_stock_grn_list_sdata[':active']  = $active;        
        $filter_count++;
    }    
    if ($start_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_added_on >= :start_date";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_added_on >= :start_date";
        }        
        //Data
        $get_stock_grn_list_sdata[':start_date']  = $start_date;        
        $filter_count++;
    }
    if ($end_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_added_on <= :end_date";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_added_on <= :end_date";
        }        
        //Data
        $get_stock_grn_list_sdata['end_date']  = $end_date;        
        $filter_count++;
    }    
    if (array_key_exists("sort", $stock_grn_search_data)) {
        if ($stock_grn_search_data['sort'] == '1') {
            $get_grn_list_squery_order = " order by cast(substring(stock_grn_no,locate('.',stock_grn_no,5)+1) as signed) desc limit 0,1";
        } else {
            $get_grn_list_squery_order = " order by stock_grn_added_on DESC";
        }
    } else {
        $get_grn_list_squery_order = " order by stock_grn_added_on DESC";
    }
    $get_stock_grn_list_squery       = $get_stock_grn_list_squery_base.$get_stock_grn_list_squery_where.$get_grn_list_squery_order;    
    try {
        $dbConnection = get_conn_handle();        
        $get_stock_grn_list_sstatement = $dbConnection->prepare($get_stock_grn_list_squery);        
        $get_stock_grn_list_sstatement -> execute($get_stock_grn_list_sdata);        
        $get_stock_grn_list_sdetails = $get_stock_grn_list_sstatement -> fetchAll();        
        if (false === $get_stock_grn_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_stock_grn_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_stock_grn_list_sdetails;
        }
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }    
    return $return;
}/*PURPOSE : To update GrnINPUT 	: Grn ID, Grn Update ArrayOUTPUT 	: Grn ID; Message of success or failureBY 		: Lakshmi*/function db_update_grn($grn_id, $grn_update_data)
{
    if (array_key_exists("purchase_order_id", $grn_update_data)) {
        $purchase_order_id = $grn_update_data["purchase_order_id"];
    } else {
        $purchase_order_id = "";
    }
    if (array_key_exists("invoice_number", $grn_update_data)) {
        $invoice_number = $grn_update_data["invoice_number"];
    } else {
        $invoice_number = "";
    }
    if (array_key_exists("invoice_date", $grn_update_data)) {
        $invoice_date = $grn_update_data["invoice_date"];
    } else {
        $invoice_date = "";
    }
    if (array_key_exists("doc", $grn_update_data)) {
        $doc = $grn_update_data["doc"];
    } else {
        $doc = "";
    }
    if (array_key_exists("dc_number", $grn_update_data)) {
        $dc_number = $grn_update_data["dc_number"];
    } else {
        $dc_number = "";
    }
    if (array_key_exists("dc_date", $grn_update_data)) {
        $dc_date = $grn_update_data["dc_date"];
    } else {
        $dc_date = "";
    }    
    if (array_key_exists("vehicle_number", $grn_update_data)) {
        $vehicle_number = $grn_update_data["vehicle_number"];
    } else {
        $vehicle_number = "";
    }    
    if (array_key_exists("remarks", $grn_update_data)) {
        $remarks = $grn_update_data["remarks"];
    } else {
        $remarks = "";
    }    
    if (array_key_exists("active", $grn_update_data)) {
        $active = $grn_update_data["active"];
    } else {
        $active = "";
    }    
    if (array_key_exists("added_by", $grn_update_data)) {
        $added_by = $grn_update_data["added_by"];
    } else {
        $added_by = "";
    }    
    if (array_key_exists("added_on", $grn_update_data)) {
        $added_on = $grn_update_data["added_on"];
    } else {
        $added_on = "";
    }    
    // Query
    $grn_update_uquery_base = "update stock_grn set";    
    $grn_update_uquery_set = "";    
    $grn_update_uquery_where = " where stock_grn_id=:grn_id";    
    $grn_update_udata = array(":grn_id"=>$grn_id);    
    $filter_count = 0;    
    if ($purchase_order_id != "") {
        $grn_update_uquery_set = $grn_update_uquery_set." stock_grn_purchase_order_id=:purchase_order_id,";
        $grn_update_udata[":purchase_order_id"] = $purchase_order_id;
        $filter_count++;
    }
    if ($invoice_number != "") {
        $grn_update_uquery_set = $grn_update_uquery_set." stock_grn_invoice_number=:invoice_number,";
        $grn_update_udata[":invoice_number"] = $invoice_number;
        $filter_count++;
    }
    if ($invoice_date != "") {
        $grn_update_uquery_set = $grn_update_uquery_set." stock_grn_invoice_date=:invoice_date,";
        $grn_update_udata[":invoice_date"] = $invoice_date;
        $filter_count++;
    }
    if ($doc != "") {
        $grn_update_uquery_set = $grn_update_uquery_set." stock_grn_doc=:doc,";
        $grn_update_udata[":doc"] = $doc;
        $filter_count++;
    }
    if ($dc_number != "") {
        $grn_update_uquery_set = $grn_update_uquery_set." stock_grn_dc_number=:dc_number,";
        $grn_update_udata[":dc_number"] = $dc_number;
        $filter_count++;
    }
    if ($dc_date != "") {
        $grn_update_uquery_set = $grn_update_uquery_set." stock_grn_dc_date=:dc_date,";
        $grn_update_udata[":dc_date"] = $dc_date;
        $filter_count++;
    }
    if ($vehicle_number != "") {
        $grn_update_uquery_set = $grn_update_uquery_set." stock_grn_vehicle_number=:vehicle_number,";
        $grn_update_udata[":vehicle_number"] = $vehicle_number;
        $filter_count++;
    }    
    if ($remarks != "") {
        $grn_update_uquery_set = $grn_update_uquery_set." stock_grn_remarks = :remarks,";
        $grn_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }    
    if ($active != "") {
        $grn_update_uquery_set = $grn_update_uquery_set." stock_grn_active = :active,";
        $grn_update_udata[":active"] = $active;
        $filter_count++;
    }    
    if ($added_by != "") {
        $grn_update_uquery_set = $grn_update_uquery_set." stock_grn_added_by=:added_by,";
        $grn_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }    
    if ($added_on != "") {
        $grn_update_uquery_set = $grn_update_uquery_set." stock_grn_added_on=:added_on,";
        $grn_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }    
    if ($filter_count > 0) {
        $grn_update_uquery_set = trim($grn_update_uquery_set, ',');
    }    
    $grn_update_uquery = $grn_update_uquery_base.$grn_update_uquery_set.$grn_update_uquery_where;
    try {
        $dbConnection = get_conn_handle();        
        $grn_update_ustatement = $dbConnection->prepare($grn_update_uquery);        
        $grn_update_ustatement -> execute($grn_update_udata);        
        $return["status"] = SUCCESS;
        $return["data"]   = $grn_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }    
    return $return;
}/*PURPOSE : To add new Stock Grn ItemsINPUT 	: Grn Id,Item,Quantity,Uom,Active,Remarks,Added by,Added onOUTPUT 	: Item Id, success or failure messageBY 		: Lakshmi*/function db_add_stock_grn_items($grn_id, $item, $quantity, $inward_qty, $uom, $remarks, $added_by)
{    // Query
    $stock_grn_items_iquery = "insert into stock_grn_items (stock_grn_id,stock_grn_item,stock_grn_item_quantity,stock_grn_item_inward_quantity,stock_grn_item_uom,stock_grn_item_active,stock_grn_item_remarks,	stock_grn_item_added_by,stock_grn_item_added_on)values(:grn_id,:item,:quantity,:inward_qty,:uom,:active,:remarks,:added_by,:added_on)";    
    try {
        $dbConnection = get_conn_handle();
        $stock_grn_items_istatement = $dbConnection->prepare($stock_grn_items_iquery);       
        // Data
        $stock_grn_items_idata = array(':grn_id'=>$grn_id,':item'=>$item,':quantity'=>$quantity,':inward_qty'=>$inward_qty,':uom'=>$uom,':active'=>'1',':remarks'=>$remarks,        ':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));        
        $dbConnection->beginTransaction();
        $stock_grn_items_istatement->execute($stock_grn_items_idata);
        $stock_grn_item_id = $dbConnection->lastInsertId();
        $dbConnection->commit();       
        $return["status"] = SUCCESS;
        $return["data"]   = $stock_grn_item_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }    
    return $return;
}/*PURPOSE : To get Stock Grn Items ListINPUT 	: Item Id,Grn Id,Item,Quantity,Uom,Active,Added by,Start Date(for added on), End Date(for added on)OUTPUT 	: List of stock Grn ItemsBY 		: Lakshmi*/function db_get_stock_grn_items_list($stock_grn_items_search_data)
{
    if (array_key_exists("item_id", $stock_grn_items_search_data)) {
        $item_id = $stock_grn_items_search_data["item_id"];
    } else {
        $item_id= "";
    }    
    if (array_key_exists("grn_id", $stock_grn_items_search_data)) {
        $grn_id= $stock_grn_items_search_data["grn_id"];
    } else {
        $grn_id= "";
    }
    if (array_key_exists("item", $stock_grn_items_search_data)) {
        $item= $stock_grn_items_search_data["item"];
    } else {
        $item= "";
    }
    if (array_key_exists("order_id", $stock_grn_items_search_data)) {
        $order_id= $stock_grn_items_search_data["order_id"];
    } else {
        $order_id= "";
    }
    if (array_key_exists("quantity", $stock_grn_items_search_data)) {
        $quantity= $stock_grn_items_search_data["quantity"];
    } else {
        $quantity= "";
    }
    if (array_key_exists("uninspectioned_id", $stock_grn_items_search_data)) {
        $uninspectioned_id= $stock_grn_items_search_data["uninspectioned_id"];
    } else {
        $uninspectioned_id= "";
    }
    if (array_key_exists("uom", $stock_grn_items_search_data)) {
        $uom= $stock_grn_items_search_data["uom"];
    } else {
        $uom= "";
    }     
    if (array_key_exists("active", $stock_grn_items_search_data)) {
        $active= $stock_grn_items_search_data["active"];
    } else {
        $active= "";
    }
    if (array_key_exists("added_by", $stock_grn_items_search_data)) {
        $added_by= $stock_grn_items_search_data["added_by"];
    } else {
        $added_by= "";
    }
    if (array_key_exists("start_date", $stock_grn_items_search_data)) {
        $start_date= $stock_grn_items_search_data["start_date"];
    } else {
        $start_date= "";
    }
    if (array_key_exists("end_date", $stock_grn_items_search_data)) {
        $end_date= $stock_grn_items_search_data["end_date"];
    } else {
        $end_date= "";
    }
    $get_stock_grn_items_list_squery_base = "select * from stock_grn_items SGI inner join users U on U.user_id = SGI.stock_grn_item_added_by inner join stock_grn SG on SG.stock_grn_id= SGI.stock_grn_id inner join stock_purchase_order SPO on SPO.stock_purchase_order_id= SG.stock_grn_purchase_order_id inner join stock_material_master SMM on SMM.stock_material_id= SGI.stock_grn_item inner join stock_vendor_master SVM on SVM.stock_vendor_id= SPO.stock_purchase_order_vendor inner join stock_unit_measure_master SUMM on SUMM.stock_unit_id= SMM.stock_material_unit_of_measure inner join stock_company_master SCM on SCM.stock_company_master_id=SPO.stock_purchase_order_company";    
    $get_stock_grn_items_list_squery_where = "";    
    $filter_count = 0;    
    // Data
    $get_stock_grn_items_list_sdata = array();    
    if ($item_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." where stock_grn_item_id=:item_id";
        } else {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." and stock_grn_item_id=:item_id";
        }
        // Data
        $get_stock_grn_items_list_sdata[':item_id'] = $item_id;        
        $filter_count++;
    }
    if ($grn_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." where SGI.stock_grn_id=:grn_id";
        } else {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." and SGI.stock_grn_id=:grn_id";
        }        
        // Data
        $get_stock_grn_items_list_sdata[':grn_id']  = $grn_id;        
        $filter_count++;
    }    
    if ($item != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." where stock_grn_item=:item";
        } else {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." and stock_grn_item=:item";
        }        
        // Data
        $get_stock_grn_items_list_sdata[':item']  = $item;        
        $filter_count++;
    }
    if ($order_id != "") {
        if ($filter_count == 0) {                        // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." where SPO.stock_purchase_order_id=:order_id";
        } else {                        // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." and SPO.stock_purchase_order_id=:order_id";
        }
        // Data
        $get_stock_grn_items_list_sdata[':order_id']  = $order_id;
        $filter_count++;
    }    
    if ($quantity != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." where stock_grn_item_quantity=:quantity";
        } else {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." and stock_grn_item_quantity=:quantity";
        }        
        // Data
        $get_stock_grn_items_list_sdata[':quantity']  = $quantity;        
        $filter_count++;
    }
    if ($uninspectioned_id != "") {
        if ($filter_count == 0) {                        // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." where stock_grn_item_id not in (select stock_grn_engineer_inspection_grn_item_id from stock_grn_engineer_inspection)";
        } else {                        // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." and stock_grn_item_id not in (select stock_grn_engineer_inspection_grn_item_id from stock_grn_engineer_inspection)";
        }
        $filter_count++;
    }     
    if ($uom != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." where stock_grn_item_uom=:uom";
        } else {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." and stock_grn_item_uom=:uom";
        }        
        // Data
        $get_stock_grn_items_list_sdata[':uom']  = $uom;        
        $filter_count++;
    }        
    if ($active != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." where stock_grn_item_active=:active";
        } else {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." and stock_grn_item_active=:active";
        }        
        // Data
        $get_stock_grn_items_list_sdata[':active']  = $active;        
        $filter_count++;
    }    
    if ($added_by!= "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." where stock_grn_item_added_by = :added_by";
        } else {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." and stock_grn_item_added_by = :added_by";
        }        
        //Data
        $get_stock_grn_items_list_sdata[':added_by']  = $added_by;        
        $filter_count++;
    }    
    if ($start_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." where stock_grn_item_added_on >= :start_date";
        } else {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." and stock_grn_item_added_on >= :start_date";
        }        
        //Data
        $get_stock_grn_items_list_sdata[':start_date']  = $start_date;        
        $filter_count++;
    }
    if ($end_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." where stock_grn_item_added_on <= :end_date";
        } else {            // Query
            $get_stock_grn_items_list_squery_where = $get_stock_grn_items_list_squery_where." and stock_grn_item_added_on <= :end_date";
        }        
        //Data
        $get_stock_grn_items_list_sdata['end_date']  = $end_date;        
        $filter_count++;
    }    
    $get_stock_grn_items_list_squery = $get_stock_grn_items_list_squery_base.$get_stock_grn_items_list_squery_where;        
    try {
        $dbConnection = get_conn_handle();        
        $get_stock_grn_items_list_sstatement = $dbConnection->prepare($get_stock_grn_items_list_squery);        
        $get_stock_grn_items_list_sstatement -> execute($get_stock_grn_items_list_sdata);        
        $get_stock_grn_items_list_sdetails = $get_stock_grn_items_list_sstatement -> fetchAll();        
        if (false === $get_stock_grn_items_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_stock_grn_items_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_stock_grn_items_list_sdetails;
        }
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }    
    return $return;
}   /*PURPOSE : To update Grn ItemsINPUT 	: Grn Item ID, Grn Items Update ArrayOUTPUT 	: Grn Item ID; Message of success or failureBY 		: Lakshmi*/function db_update_grn_items($item_id, $grn_items_update_data)
{
    if (array_key_exists("grn_id", $grn_items_update_data)) {
        $grn_id = $grn_items_update_data["grn_id"];
    } else {
        $grn_id = "";
    }    
    if (array_key_exists("item", $grn_items_update_data)) {
        $item = $grn_items_update_data["item"];
    } else {
        $item = "";
    }
    if (array_key_exists("quantity", $grn_items_update_data)) {
        $quantity = $grn_items_update_data["quantity"];
    } else {
        $quantity = "";
    }
    if (array_key_exists("uom", $grn_items_update_data)) {
        $uom = $grn_items_update_data["uom"];
    } else {
        $uom = "";
    }
    if (array_key_exists("active", $grn_items_update_data)) {
        $active = $grn_items_update_data["active"];
    } else {
        $active = "";
    }    
    if (array_key_exists("added_by", $grn_items_update_data)) {
        $added_by = $grn_items_update_data["added_by"];
    } else {
        $added_by = "";
    }
    if (array_key_exists("added_on", $grn_items_update_data)) {
        $added_on = $grn_items_update_data["added_on"];
    } else {
        $added_on = "";
    }    
    // Query
    $grn_items_update_uquery_base = "update stock_grn_items set";    
    $grn_items_update_uquery_set = "";    
    $grn_items_update_uquery_where = " where stock_grn_item_id=:item_id";    
    $grn_items_update_udata = array(":item_id"=>$item_id);    
    $filter_count = 0;    
    if ($grn_id != "") {
        $grn_items_update_uquery_set = $grn_items_update_uquery_set." stock_grn_id=:grn_id,";
        $grn_items_update_udata[":grn_id"] = $grn_id;
        $filter_count++;
    }    
    if ($item != "") {
        $grn_items_update_uquery_set = $grn_items_update_uquery_set." stock_grn_Item=:item,";
        $grn_items_update_udata[":item"] = $item;
        $filter_count++;
    }
    if ($quantity != "") {
        $grn_items_update_uquery_set = $grn_items_update_uquery_set." stock_grn_Item_quantity=:quantity,";
        $grn_items_update_udata[":quantity"] = $quantity;
        $filter_count++;
    }
    if ($uom != "") {
        $grn_items_update_uquery_set = $grn_items_update_uquery_set." stock_grn_item_uom=:uom,";
        $grn_items_update_udata[":uom"] = $uom;
        $filter_count++;
    }
    if ($active != "") {
        $grn_items_update_uquery_set = $grn_items_update_uquery_set." stock_grn_item_active=:active,";
        $grn_items_update_udata[":active"] = $active;
        $filter_count++;
    }
    if ($added_by != "") {
        $grn_items_update_uquery_set = $grn_items_update_uquery_set." stock_grn_item_added_by=:added_by,";
        $grn_items_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }
    if ($added_on != "") {
        $grn_items_update_uquery_set = $grn_items_update_uquery_set." stock_grn_item_added_on=:added_on,";
        $grn_items_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }    
    if ($filter_count > 0) {
        $grn_items_update_uquery_set = trim($grn_items_update_uquery_set, ',');
    }    
    $grn_items_update_uquery = $grn_items_update_uquery_base.$grn_items_update_uquery_set.$grn_items_update_uquery_where;
    try {
        $dbConnection = get_conn_handle();        
        $grn_items_update_ustatement = $dbConnection->prepare($grn_items_update_uquery);        
        $grn_items_update_ustatement -> execute($grn_items_update_udata);        
        $return["status"] = SUCCESS;
        $return["data"]   = $item_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }    
    return $return;
}/*PURPOSE : To add new Stock Grn Engineer InspectionINPUT 	: Grn Item Id,Quantity,Remarks,Remarks to Account,Added by,Added onOUTPUT 	: Inspection Id, success or failure messageBY 		: Lakshmi*/function db_add_stock_grn_engineer_inspection($grn_item_id, $quantity, $rejected_quantity, $additional_quantity, $remarks, $remarks_to_account, $added_by)
{    // Query
    $stock_grn_engineer_inspection_iquery = "insert into stock_grn_engineer_inspection	(stock_grn_engineer_inspection_grn_Item_id,stock_grn_engineer_inspection_approved_quantity,stock_grn_engineer_inspection_rejected_quantity,stock_grn_engineer_inspection_additional_quantity,stock_grn_engineer_inspection_active,stock_grn_engineer_inspection_remarks,	stock_grn_engineer_inspection_remarks_to_account,stock_grn_engineer_inspection_added_by,stock_grn_engineer_inspection_added_on)	values(:grn_item_id,:quantity,:rejected_quantity,:additional_quantity,:active,:remarks,:remarks_to_account,:added_by,:added_on)";    
    try {
        $dbConnection = get_conn_handle();
        $stock_grn_engineer_inspection_istatement = $dbConnection->prepare($stock_grn_engineer_inspection_iquery);       
        // Data
        $stock_grn_engineer_inspection_idata = array(':grn_item_id'=>$grn_item_id,':quantity'=>$quantity, ':rejected_quantity'=>$rejected_quantity, ':additional_quantity'=>$additional_quantity,':active'=>'1',':remarks'=>$remarks,        ':remarks_to_account'=>$remarks_to_account,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));        
        $dbConnection->beginTransaction();
        $stock_grn_engineer_inspection_istatement->execute($stock_grn_engineer_inspection_idata);
        $stock_grn_engineer_inspection_id = $dbConnection->lastInsertId();
        $dbConnection->commit();       
        $return["status"] = SUCCESS;
        $return["data"]   = $stock_grn_engineer_inspection_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }    
    return $return;
}/*PURPOSE : To get Stock Grn Engineer Inspection ListINPUT 	: Inspection Id,Grn Item Id,Quantity,Remarks to Account,Added by,Start Date(for added on), End Date(for added on)OUTPUT 	: List of stock Grn Engineer InspectionBY 		: Lakshmi*/function db_get_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data)
{
    if (array_key_exists("inspection_id", $stock_grn_engineer_inspection_search_data)) {
        $inspection_id = $stock_grn_engineer_inspection_search_data["inspection_id"];
    } else {
        $inspection_id = "";
    }    
    if (array_key_exists("grn_item_id", $stock_grn_engineer_inspection_search_data)) {
        $grn_item_id= $stock_grn_engineer_inspection_search_data["grn_item_id"];
    } else {
        $grn_item_id= "";
    }
    if (array_key_exists("po_id", $stock_grn_engineer_inspection_search_data)) {
        $po_id= $stock_grn_engineer_inspection_search_data["po_id"];
    } else {
        $po_id= "";
    }
    if (array_key_exists("grn_id", $stock_grn_engineer_inspection_search_data)) {
        $grn_id = $stock_grn_engineer_inspection_search_data["grn_id"];
    } else {
        $grn_id = "";
    }
    if (array_key_exists("quantity", $stock_grn_engineer_inspection_search_data)) {
        $quantity= $stock_grn_engineer_inspection_search_data["quantity"];
    } else {
        $quantity= "";
    }    
    if (array_key_exists("active", $stock_grn_engineer_inspection_search_data)) {
        $active= $stock_grn_engineer_inspection_search_data["active"];
    } else {
        $active= "";
    }     
    if (array_key_exists("remarks_to_account", $stock_grn_engineer_inspection_search_data)) {
        $remarks_to_account= $stock_grn_engineer_inspection_search_data["remarks_to_account"];
    } else {
        $remarks_to_account= "";
    }    
    if (array_key_exists("material", $stock_grn_engineer_inspection_search_data)) {
        $material = $stock_grn_engineer_inspection_search_data["material"];
    } else {
        $material = "";
    }    
    if (array_key_exists("order_id", $stock_grn_engineer_inspection_search_data)) {
        $order_id = $stock_grn_engineer_inspection_search_data["order_id"];
    } else {
        $order_id = "";
    }    
    if (array_key_exists("added_by", $stock_grn_engineer_inspection_search_data)) {
        $added_by= $stock_grn_engineer_inspection_search_data["added_by"];
    } else {
        $added_by= "";
    }
    if (array_key_exists("start_date", $stock_grn_engineer_inspection_search_data)) {
        $start_date= $stock_grn_engineer_inspection_search_data["start_date"];
    } else {
        $start_date= "";
    }
    if (array_key_exists("end_date", $stock_grn_engineer_inspection_search_data)) {
        $end_date= $stock_grn_engineer_inspection_search_data["end_date"];
    } else {
        $end_date= "";
    }
    if (array_key_exists("project", $stock_grn_engineer_inspection_search_data)) {
        $project= $stock_grn_engineer_inspection_search_data["project"];
    } else {
        $project= "";
    }    
    $get_stock_grn_engineer_inspection_list_squery_base = "select * from stock_grn_engineer_inspection SGEI inner join users U on U.user_id = SGEI.stock_grn_engineer_inspection_added_by inner join stock_grn_items SGI on SGI.stock_grn_item_id = SGEI.stock_grn_engineer_inspection_grn_item_id inner join stock_material_master SMM on SMM.stock_material_id = SGI.stock_grn_item inner join stock_grn SG on SG.stock_grn_id= SGI.stock_grn_id inner join stock_purchase_order SPO on SPO.stock_purchase_order_id = SG.stock_grn_purchase_order_id inner join stock_vendor_master SVM on SVM.stock_vendor_id=SPO.stock_purchase_order_vendor inner join stock_tax_type_master STTM on STTM.stock_tax_type_master_id = SPO.stock_purchase_order_tax_type inner join stock_unit_measure_master SUMM on SUMM.stock_unit_id=SMM.stock_material_unit_of_measure inner join stock_project SP on SP.stock_project_id= SG.stock_grn_project";    
    $get_stock_grn_engineer_inspection_list_squery_where = "";    
    $filter_count = 0;    
    // Data
    $get_stock_grn_engineer_inspection_list_sdata = array();    
    if ($inspection_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where stock_grn_engineer_inspection_id=:inspection_id";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and stock_grn_engineer_inspection_id=:inspection_id";
        }
        // Data
        $get_stock_grn_engineer_inspection_list_sdata[':inspection_id'] = $inspection_id;        
        $filter_count++;
    }    
    if ($grn_item_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where stock_grn_engineer_inspection_grn_item_id=:grn_item_id";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and stock_grn_engineer_inspection_grn_item_id=:grn_item_id";
        }        
        // Data
        $get_stock_grn_engineer_inspection_list_sdata[':grn_item_id']  = $grn_item_id;        
        $filter_count++;
    }
    if ($po_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where SPO.stock_purchase_order_id=:po_id";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and SPO.stock_purchase_order_id=:po_id";
        }
        // Data
        $get_stock_grn_engineer_inspection_list_sdata[':po_id']  = $po_id;
        $filter_count++;
    }
    if ($grn_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where SG.stock_grn_id = :grn_id";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and SG.stock_grn_id = :grn_id";
        }        
        // Data
        $get_stock_grn_engineer_inspection_list_sdata[':grn_id'] = $grn_id;        
        $filter_count++;
    }    
    if ($quantity != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where stock_grn_engineer_inspection_approved_quantity=:quantity";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and stock_grn_engineer_inspection_approved_quantity=:quantity";
        }        
        // Data
        $get_stock_grn_engineer_inspection_list_sdata[':quantity']  = $quantity;        
        $filter_count++;
    }    
    if ($active != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where stock_grn_engineer_inspection_active = :active";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and stock_grn_engineer_inspection_active = :active";
        }        
        // Data
        $get_stock_grn_engineer_inspection_list_sdata[':active']  = $active;        
        $filter_count++;
    }    
    if ($remarks_to_account != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where stock_grn_engineer_inspection_remarks_to_account=:remarks_to_account";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and stock_grn_engineer_inspection_remarks_to_account=:remarks_to_account";
        }        
        // Data
        $get_stock_grn_engineer_inspection_list_sdata[':remarks_to_account']  = $remarks_to_account;        
        $filter_count++;
    }    
    if ($material != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where SGI.stock_grn_item = :material";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and SGI.stock_grn_item = :material";
        }        
        //Data
        $get_stock_grn_engineer_inspection_list_sdata[':material']  = $material;        
        $filter_count++;
    }    
    if ($order_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where SG.stock_grn_purchase_order_id = :order";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and SG.stock_grn_purchase_order_id = :order";
        }        
        //Data
        $get_stock_grn_engineer_inspection_list_sdata[':order']  = $order_id;        
        $filter_count++;
    }    
    if ($added_by!= "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where stock_grn_engineer_inspection_added_by = :added_by";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and stock_grn_engineer_inspection_added_by = :added_by";
        }        
        //Data
        $get_stock_grn_engineer_inspection_list_sdata[':added_by']  = $added_by;        
        $filter_count++;
    }    
    if ($start_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where stock_grn_engineer_inspection_added_on >= :start_date";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and stock_grn_engineer_inspection_added_on >= :start_date";
        }        
        //Data
        $get_stock_grn_engineer_inspection_list_sdata[':start_date']  = $start_date;        
        $filter_count++;
    }
    if ($end_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where stock_grn_engineer_inspection_added_on <= :end_date";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and stock_grn_engineer_inspection_added_on <= :end_date";
        }        
        //Data
        $get_stock_grn_engineer_inspection_list_sdata['end_date']  = $end_date;        
        $filter_count++;
    }
    if ($project != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where SG.stock_grn_project = :project";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and SG.stock_grn_project = :project";
        }        
        //Data
        $get_stock_grn_engineer_inspection_list_sdata['project']  = $project;        
        $filter_count++;
    }
    $get_stock_grn_engineer_inspection_list_squery_order_by = " order by stock_grn_engineer_inspection_added_on DESC";
    $get_stock_grn_engineer_inspection_list_squery = $get_stock_grn_engineer_inspection_list_squery_base.$get_stock_grn_engineer_inspection_list_squery_where.$get_stock_grn_engineer_inspection_list_squery_order_by;    
    try {
        $dbConnection = get_conn_handle();        
        $get_stock_grn_engineer_inspection_list_sstatement = $dbConnection->prepare($get_stock_grn_engineer_inspection_list_squery);        
        $get_stock_grn_engineer_inspection_list_sstatement -> execute($get_stock_grn_engineer_inspection_list_sdata);        
        $get_stock_grn_engineer_inspection_list_sdetails = $get_stock_grn_engineer_inspection_list_sstatement -> fetchAll();        
        if (false === $get_stock_grn_engineer_inspection_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_stock_grn_engineer_inspection_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_stock_grn_engineer_inspection_list_sdetails;
        }
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }    
    return $return;
}   /*PURPOSE : To update Grn Engineer InspectionINPUT 	: Inspection ID, Grn Items Update ArrayOUTPUT 	: Inspection ID; Message of success or failureBY 		: Lakshmi*/function db_update_stock_grn_engineer_inspection($inspection_id, $stock_grn_engineer_inspection_update_data)
{
    if (array_key_exists("grn_item_id", $stock_grn_engineer_inspection_update_data)) {
        $grn_item_id = $stock_grn_engineer_inspection_update_data["grn_item_id"];
    } else {
        $grn_item_id = "";
    }
    if (array_key_exists("quantity", $stock_grn_engineer_inspection_update_data)) {
        $quantity = $stock_grn_engineer_inspection_update_data["quantity"];
    } else {
        $quantity = "";
    }    
    if (array_key_exists("active", $stock_grn_engineer_inspection_update_data)) {
        $active = $stock_grn_engineer_inspection_update_data["active"];
    } else {
        $active = "";
    }    
    if (array_key_exists("remarks", $stock_grn_engineer_inspection_update_data)) {
        $remarks = $stock_grn_engineer_inspection_update_data["remarks"];
    } else {
        $remarks = "";
    }    
    if (array_key_exists("remarks_to_account", $stock_grn_engineer_inspection_update_data)) {
        $remarks_to_account = $stock_grn_engineer_inspection_update_data["remarks_to_account"];
    } else {
        $remarks_to_account = "";
    }    
    if (array_key_exists("added_by", $stock_grn_engineer_inspection_update_data)) {
        $added_by = $stock_grn_engineer_inspection_update_data["added_by"];
    } else {
        $added_by = "";
    }
    if (array_key_exists("added_on", $stock_grn_engineer_inspection_update_data)) {
        $added_on = $stock_grn_engineer_inspection_update_data["added_on"];
    } else {
        $added_on = "";
    }    
    // Query
    $stock_grn_engineer_inspection_update_uquery_base = "update stock_grn_engineer_inspection set";    
    $stock_grn_engineer_inspection_update_uquery_set = "";    
    $stock_grn_engineer_inspection_update_uquery_where = " where stock_grn_engineer_inspection_id = :inspection_id";    
    $stock_grn_engineer_inspection_update_udata = array(":inspection_id"=>$inspection_id);    
    $filter_count = 0;    
    if ($grn_item_id != "") {
        $stock_grn_engineer_inspection_update_uquery_set = $stock_grn_engineer_inspection_update_uquery_set." stock_grn_engineer_inspection_grn_item_id = :grn_item_id,";
        $stock_grn_engineer_inspection_update_udata[":grn_item_id"] = $grn_item_id;
        $filter_count++;
    }    
    if ($quantity != "") {
        $stock_grn_engineer_inspection_update_uquery_set = $stock_grn_engineer_inspection_update_uquery_set." stock_grn_engineer_inspection_approved_quantity = :quantity,";
        $stock_grn_engineer_inspection_update_udata[":quantity"] = $quantity;
        $filter_count++;
    }    
    if ($active != "") {
        $stock_grn_engineer_inspection_update_uquery_set = $stock_grn_engineer_inspection_update_uquery_set." stock_grn_engineer_inspection_active = :active,";
        $stock_grn_engineer_inspection_update_udata[":active"] = $active;
        $filter_count++;
    }    
    if ($remarks != "") {
        $stock_grn_engineer_inspection_update_uquery_set = $stock_grn_engineer_inspection_update_uquery_set." stock_grn_engineer_inspection_remarks = :remarks,";
        $stock_grn_engineer_inspection_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }    
    if ($remarks_to_account != "") {
        $stock_grn_engineer_inspection_update_uquery_set = $stock_grn_engineer_inspection_update_uquery_set." stock_grn_engineer_inspection_remarks_to_account = :remarks_to_account,";
        $stock_grn_engineer_inspection_update_udata[":remarks_to_account"] = $remarks_to_account;
        $filter_count++;
    }
    if ($added_by != "") {
        $stock_grn_engineer_inspection_update_uquery_set = $stock_grn_engineer_inspection_update_uquery_set." stock_grn_engineer_inspection_added_by = :added_by,";
        $stock_grn_engineer_inspection_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }
    if ($added_on != "") {
        $stock_grn_engineer_inspection_update_uquery_set = $stock_grn_engineer_inspection_update_uquery_set." stock_grn_engineer_inspection_added_on = :added_on,";
        $stock_grn_engineer_inspection_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }    
    if ($filter_count > 0) {
        $stock_grn_engineer_inspection_update_uquery_set = trim($stock_grn_engineer_inspection_update_uquery_set, ',');
    }    
    $stock_grn_engineer_inspection_update_uquery = $stock_grn_engineer_inspection_update_uquery_base.$stock_grn_engineer_inspection_update_uquery_set.$stock_grn_engineer_inspection_update_uquery_where;
    try {
        $dbConnection = get_conn_handle();        
        $stock_grn_engineer_inspection_update_ustatement = $dbConnection->prepare($stock_grn_engineer_inspection_update_uquery);        
        $stock_grn_engineer_inspection_update_ustatement -> execute($stock_grn_engineer_inspection_update_udata);        
        $return["status"] = SUCCESS;
        $return["data"]   = $inspection_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }    
    return $return;
}/*PURPOSE : To add new Stock Grn PaymentsINPUT 	: Grn Id,Amount,Mode,Date,Bank,Branch,Active,Added By,Added OnOUTPUT 	: Payment Id, success or failure messageBY 		: Lakshmi*/function db_add_stock_grn_payments($grn_id, $amount, $mode, $date, $bank, $branch, $added_by)
{    // Query
    $stock_grn_payments_iquery = "insert into stock_grn_payments   (stock_grn_id,stock_grn_payment_amount,stock_grn_payment_mode,stock_grn_payment_date,stock_grn_payment_bank,stock_grn_payment_branch,    stock_grn_payment_active,stock_grn_payment_added_by,stock_grn_payment_added_on)	values(:grn_id,:amount,:mode,:date,:bank,:branch,:active,:added_by,:added_on)";    
    try {
        $dbConnection = get_conn_handle();
        $stock_grn_payments_istatement = $dbConnection->prepare($stock_grn_payments_iquery);        
        // Data
        $stock_grn_payments_idata = array(':grn_id'=>$grn_id,':amount'=>$amount,':mode'=>$mode,':date'=>$date,':bank'=>$bank,':branch'=>$branch,        ':active'=>'1',':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));    
        $dbConnection->beginTransaction();
        $stock_grn_payments_istatement->execute($stock_grn_payments_idata);
        $stock_grn_payment_id = $dbConnection->lastInsertId();
        $dbConnection->commit();       
        $return["status"] = SUCCESS;
        $return["data"]   = $stock_grn_payment_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }    
    return $return;
}/*PURPOSE : To get Stock Grn Payments ListINPUT 	: Payment Id,Grn Id,Amount,Mode,Date,Bank,Branch,Active,Added by,Start Date(for added on), End Date(for added on)OUTPUT 	: List of stock Grn PaymentsBY 		: Lakshmi*/function db_get_stock_grn_payments_list($stock_grn_payments_search_data)
{
    if (array_key_exists("payment_id", $stock_grn_payments_search_data)) {
        $payment_id = $stock_grn_payments_search_data["payment_id"];
    } else {
        $payment_id= "";
    }    
    if (array_key_exists("grn_id", $stock_grn_payments_search_data)) {
        $grn_id= $stock_grn_payments_search_data["grn_id"];
    } else {
        $grn_id= "";
    }
    if (array_key_exists("amount", $stock_grn_payments_search_data)) {
        $amount= $stock_grn_payments_search_data["amount"];
    } else {
        $amount= "";
    }    
    if (array_key_exists("mode", $stock_grn_payments_search_data)) {
        $mode= $stock_grn_payments_search_data["mode"];
    } else {
        $mode= "";
    }
    if (array_key_exists("date", $stock_grn_payments_search_data)) {
        $date= $stock_grn_payments_search_data["date"];
    } else {
        $date= "";
    }
    if (array_key_exists("bank", $stock_grn_payments_search_data)) {
        $bank= $stock_grn_payments_search_data["bank"];
    } else {
        $bank= "";
    }
    if (array_key_exists("branch", $stock_grn_payments_search_data)) {
        $branch= $stock_grn_payments_search_data["branch"];
    } else {
        $branch= "";
    }
    if (array_key_exists("active", $stock_grn_payments_search_data)) {
        $active= $stock_grn_payments_search_data["active"];
    } else {
        $active= "";
    }    
    if (array_key_exists("added_by", $stock_grn_payments_search_data)) {
        $added_by= $stock_grn_payments_search_data["added_by"];
    } else {
        $added_by= "";
    }
    if (array_key_exists("start_date", $stock_grn_payments_search_data)) {
        $start_date= $stock_grn_payments_search_data["start_date"];
    } else {
        $start_date= "";
    }
    if (array_key_exists("end_date", $stock_grn_payments_search_data)) {
        $end_date= $stock_grn_payments_search_data["end_date"];
    } else {
        $end_date= "";
    }
    $get_stock_grn_payments_list_squery_base = "select * from stock_grn_payments SGP inner join stock_grn SG on SG.stock_grn_id = SGP.stock_grn_id inner join users U on U.user_id= SGP.stock_grn_payment_added_by";    
    $get_stock_grn_payments_list_squery_where = "";    
    $filter_count = 0;    
    // Data
    $get_stock_grn_payments_list_sdata = array();    
    if ($payment_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." where stock_grn_payment_id=:payment_id";
        } else {            // Query
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." and stock_grn_payment_id=:payment_id";
        }
        // Data
        $get_stock_grn_payments_list_sdata[':payment_id'] = $payment_id;        
        $filter_count++;
    }    
    if ($grn_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." where SGP.stock_grn_id=:grn_id";
        } else {            // Query
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." and SGP.stock_grn_id=:grn_id";
        }        
        // Data
        $get_stock_grn_payments_list_sdata[':grn_id']  = $grn_id;        
        $filter_count++;
    }    
    if ($amount != "") {
        if ($filter_count == 0) {            // Query            
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." where stock_grn_payment_amount=:amount";
        } else {            // Query
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." and stock_grn_payment_amount=:amount";
        }        
        // Data
        $get_stock_grn_payments_list_sdata[':amount']  = $amount;        
        $filter_count++;
    }    
    if ($mode != "") {
        if ($filter_count == 0) {            // Query            
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." where stock_grn_payment_mode=:mode";
        } else {            // Query
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." and stock_grn_payment_mode=:mode";
        }        
        // Data
        $get_stock_grn_payments_list_sdata[':mode']  = $mode;        
        $filter_count++;
    }    
    if ($date != "") {
        if ($filter_count == 0) {            // Query            
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." where stock_grn_payment_date=:date";
        } else {            // Query
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." and stock_grn_payment_date=:date";
        }        
        // Data
        $get_stock_grn_payments_list_sdata[':date']  = $date;        
        $filter_count++;
    }    
    if ($bank != "") {
        if ($filter_count == 0) {            // Query            
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." where stock_grn_payment_bank=:bank";
        } else {            // Query
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." and stock_grn_payment_bank=:bank";
        }        
        // Data
        $get_stock_grn_payments_list_sdata[':bank']  = $bank;        
        $filter_count++;
    }    
    if ($branch != "") {
        if ($filter_count == 0) {            // Query            
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." where stock_grn_payment_branch=:branch";
        } else {            // Query
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." and stock_grn_payment_branch=:branch";
        }        
        // Data
        $get_stock_grn_payments_list_sdata[':branch']  = $branch;        
        $filter_count++;
    }        
    if ($added_by!= "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." where stock_grn_payment_added_by = :added_by";
        } else {            // Query
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." and stock_grn_payment_added_by = :added_by";
        }        
        //Data
        $get_stock_grn_payments_list_sdata[':added_by']  = $added_by;        
        $filter_count++;
    }    
    if ($start_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." where stock_grn_payment_added_on >= :start_date";
        } else {            // Query
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." and stock_grn_payment_added_on >= :start_date";
        }        
        //Data
        $get_stock_grn_payments_list_sdata[':start_date']  = $start_date;        
        $filter_count++;
    }
    if ($end_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." where stock_grn_payment_added_on <= :end_date";
        } else {            // Query
            $get_stock_grn_payments_list_squery_where = $get_stock_grn_payments_list_squery_where." and stock_grn_payment_added_on <= :end_date";
        }        
        //Data
        $get_stock_grn_payments_list_sdata['end_date']  = $end_date;        
        $filter_count++;
    }    
    $get_stock_grn_payments_list_squery = $get_stock_grn_payments_list_squery_base.$get_stock_grn_payments_list_squery_where;    
    try {
        $dbConnection = get_conn_handle();        
        $get_stock_grn_payments_list_sstatement = $dbConnection->prepare($get_stock_grn_payments_list_squery);        
        $get_stock_grn_payments_list_sstatement -> execute($get_stock_grn_payments_list_sdata);        
        $get_stock_grn_payments_list_sdetails = $get_stock_grn_payments_list_sstatement -> fetchAll();        
        if (false === $get_stock_grn_payments_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_stock_grn_payments_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_stock_grn_payments_list_sdetails;
        }
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }    
    return $return;
}   /*PURPOSE : To update Grn PaymentsINPUT 	: Payment ID, Grn Payments Update ArrayOUTPUT 	: Payment ID; Message of success or failureBY 		: Lakshmi*/function db_update_grn_payments($payment_id, $grn_payments_update_data)
{
    if (array_key_exists("grn_id", $grn_payments_update_data)) {
        $grn_id = $grn_payments_update_data["grn_id"];
    } else {
        $grn_id = "";
    }
    if (array_key_exists("amount", $grn_payments_update_data)) {
        $amount = $grn_payments_update_data["amount"];
    } else {
        $amount = "";
    }    
    if (array_key_exists("mode", $grn_payments_update_data)) {
        $mode = $grn_payments_update_data["mode"];
    } else {
        $mode = "";
    }    
    if (array_key_exists("date", $grn_payments_update_data)) {
        $date = $grn_payments_update_data["date"];
    } else {
        $date = "";
    }    
    if (array_key_exists("bank", $grn_payments_update_data)) {
        $bank = $grn_payments_update_data["bank"];
    } else {
        $bank = "";
    }    
    if (array_key_exists("branch", $grn_payments_update_data)) {
        $branch = $grn_payments_update_data["branch"];
    } else {
        $branch = "";
    }    
    if (array_key_exists("active", $grn_payments_update_data)) {
        $active = $grn_payments_update_data["active"];
    } else {
        $active = "";
    }    
    if (array_key_exists("added_by", $grn_payments_update_data)) {
        $added_by = $grn_payments_update_data["added_by"];
    } else {
        $added_by = "";
    }
    if (array_key_exists("added_on", $grn_payments_update_data)) {
        $added_on = $grn_payments_update_data["added_on"];
    } else {
        $added_on = "";
    }    
    // Query
    $grn_payments_update_uquery_base = "update stock_grn_payments set";    
    $grn_payments_update_uquery_set = "";    
    $grn_payments_update_uquery_where = " where stock_grn_payment_id=:payment_id";    
    $grn_payments_update_udata = array(":payment_id"=>$payment_id);    
    $filter_count = 0;    
    if ($grn_id != "") {
        $grn_payments_update_uquery_set = $grn_payments_update_uquery_set." stock_grn_id=:grn_id,";
        $grn_payments_update_udata[":grn_id"] = $grn_id;
        $filter_count++;
    }
    if ($amount != "") {
        $grn_payments_update_uquery_set = $grn_payments_update_uquery_set." stock_grn_payment_amount=:amount,";
        $grn_payments_update_udata[":amount"] = $amount;
        $filter_count++;
    }    
    if ($mode != "") {
        $grn_payments_update_uquery_set = $grn_payments_update_uquery_set." stock_grn_payment_mode=:mode,";
        $grn_payments_update_udata[":mode"] = $mode;
        $filter_count++;
    }    
    if ($date != "") {
        $grn_payments_update_uquery_set = $grn_payments_update_uquery_set." stock_grn_payment_date=:date,";
        $grn_payments_update_udata[":date"] = $date;
        $filter_count++;
    }    
    if ($bank != "") {
        $grn_payments_update_uquery_set = $grn_payments_update_uquery_set." stock_grn_payment_bank=:bank,";
        $grn_payments_update_udata[":bank"] = $bank;
        $filter_count++;
    }    
    if ($branch != "") {
        $grn_payments_update_uquery_set = $grn_payments_update_uquery_set." stock_grn_payment_branch=:branch,";
        $grn_payments_update_udata[":branch"] = $branch;
        $filter_count++;
    }    
    if ($active != "") {
        $grn_payments_update_uquery_set = $grn_payments_update_uquery_set." stock_grn_payment_active=:active,";
        $grn_payments_update_udata[":active"] = $active;
        $filter_count++;
    }    
    if ($added_by != "") {
        $grn_payments_update_uquery_set = $grn_payments_update_uquery_set." stock_grn_payment_added_by=:added_by,";
        $grn_payments_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }
    if ($added_on != "") {
        $grn_payments_update_uquery_set = $grn_payments_update_uquery_set." stock_grn_payment_added_on=:added_on,";
        $grn_payments_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }    
    if ($filter_count > 0) {
        $grn_payments_update_uquery_set = trim($grn_payments_update_uquery_set, ',');
    }    
    $grn_payments_update_uquery = $grn_payments_update_uquery_base.$grn_payments_update_uquery_set.$grn_payments_update_uquery_where;
    try {
        $dbConnection = get_conn_handle();        
        $grn_payments_update_ustatement = $dbConnection->prepare($grn_payments_update_uquery);        
        $grn_payments_update_ustatement -> execute($grn_payments_update_udata);        
        $return["status"] = SUCCESS;
        $return["data"]   = $payment_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }    
    return $return;
}/*PURPOSE : To add new Stock Grn Debit NoteINPUT 	: Grn Id,Amount,Active,Remarks,Added By,Added OnOUTPUT 	: Debit Note Id, success or failure messageBY 		: Lakshmi*/function db_add_stock_grn_debit_note($grn_id, $amount, $remarks, $added_by)
{    // Query
    $stock_grn_debit_note_iquery = "insert into stock_grn_debit_note   (stock_grn_id,stock_grn_debit_note_amount,stock_grn_debit_note_active,stock_grn_debit_note_remarks,stock_grn_debit_note_added_by,   stock_grn_debit_note_added_on)values(:grn_id,:amount,:active,:remarks,:added_by,:added_on)";    
    try {
        $dbConnection = get_conn_handle();
        $stock_grn_debit_note_istatement = $dbConnection->prepare($stock_grn_debit_note_iquery);        
        // Data
        $stock_grn_debit_note_idata = array(':grn_id'=>$grn_id,':amount'=>$amount,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,        ':added_on'=>date("Y-m-d H:i:s"));    
        $dbConnection->beginTransaction();
        $stock_grn_debit_note_istatement->execute($stock_grn_debit_note_idata);
        $stock_grn_debit_note_id = $dbConnection->lastInsertId();
        $dbConnection->commit();       
        $return["status"] = SUCCESS;
        $return["data"]   = $stock_grn_debit_note_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }    
    return $return;
}/*PURPOSE : To get Stock Grn Debit Note ListINPUT 	: Debit Note Id,Grn Id,Amount,Active,Remarks,Added by,Start Date(for added on), End Date(for added on)OUTPUT 	: List of stock Grn Debit NoteBY 		: Lakshmi*/function db_get_stock_grn_debit_note_list($stock_grn_debit_note_search_data)
{
    if (array_key_exists("debit_note_id", $stock_grn_debit_note_search_data)) {
        $debit_note_id = $stock_grn_debit_note_search_data["debit_note_id"];
    } else {
        $debit_note_id= "";
    }    
    if (array_key_exists("grn_id", $stock_grn_debit_note_search_data)) {
        $grn_id= $stock_grn_debit_note_search_data["grn_id"];
    } else {
        $grn_id= "";
    }
    if (array_key_exists("amount", $stock_grn_debit_note_search_data)) {
        $amount= $stock_grn_debit_note_search_data["amount"];
    } else {
        $amount= "";
    }    
    if (array_key_exists("active", $stock_grn_debit_note_search_data)) {
        $active= $stock_grn_debit_note_search_data["active"];
    } else {
        $active= "";
    }    
    if (array_key_exists("added_by", $stock_grn_debit_note_search_data)) {
        $added_by= $stock_grn_debit_note_search_data["added_by"];
    } else {
        $added_by= "";
    }
    if (array_key_exists("start_date", $stock_grn_debit_note_search_data)) {
        $start_date= $stock_grn_debit_note_search_data["start_date"];
    } else {
        $start_date= "";
    }
    if (array_key_exists("end_date", $stock_grn_debit_note_search_data)) {
        $end_date= $stock_grn_debit_note_search_data["end_date"];
    } else {
        $end_date= "";
    }
    $get_stock_grn_debit_note_list_squery_base = "select * from stock_grn_debit_note";    
    $get_stock_grn_debit_note_list_squery_where = "";    
    $filter_count = 0;    
    // Data
    $get_stock_grn_debit_note_list_sdata = array();    
    if ($debit_note_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_debit_note_list_squery_where = $get_stock_grn_debit_note_list_squery_where." where stock_grn_debit_note_id=:debit_note_id";
        } else {            // Query
            $get_stock_grn_debit_note_list_squery_where = $get_stock_grn_debit_note_list_squery_where." and stock_grn_debit_note_id=:debit_note_id";
        }
        // Data
        $get_stock_grn_debit_note_items_list_sdata[':debit_note_id'] = $debit_note_id;        
        $filter_count++;
    }    
    if ($grn_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_debit_note_list_squery_where = $get_stock_grn_debit_note_list_squery_where." where stock_grn_id=:grn_id";
        } else {            // Query
            $get_stock_grn_debit_note_list_squery_where = $get_stock_grn_debit_note_list_squery_where." and stock_grn_id=:grn_id";
        }        
        // Data
        $get_stock_grn_debit_note_list_sdata[':grn_id']  = $grn_id;        
        $filter_count++;
    }    
    if ($amount != "") {
        if ($filter_count == 0) {            // Query            
            $get_stock_grn_debit_note_list_squery_where = $get_stock_grn_debit_note_list_squery_where." where stock_grn_debit_note_amount=:amount";
        } else {            // Query
            $get_stock_grn_debit_note_list_squery_where = $get_stock_grn_debit_note_list_squery_where." and stock_grn_debit_note_amount=:amount";
        }        
        // Data
        $get_stock_grn_debit_note_list_sdata[':amount']  = $amount;        
        $filter_count++;
    }    
    if ($active != "") {
        if ($filter_count == 0) {            // Query            
            $get_stock_grn_debit_note_list_squery_where = $get_stock_grn_debit_note_list_squery_where." where stock_grn_debit_note_active=:active";
        } else {            // Query
            $get_stock_grn_debit_note_list_squery_where = $get_stock_grn_debit_note_list_squery_where." and stock_grn_debit_note_active=:active";
        }        
        // Data
        $get_stock_grn_debit_note_list_sdata[':active']  = $active;        
        $filter_count++;
    }        
    if ($added_by!= "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_debit_note_list_squery_where = $get_stock_grn_debit_note_list_squery_where." where stock_grn_debit_note_added_by = :added_by";
        } else {            // Query
            $get_stock_grn_debit_note_list_squery_where = $get_stock_grn_debit_note_list_squery_where." and stock_grn_debit_note_added_by = :added_by";
        }        
        //Data
        $get_stock_grn_debit_note_list_sdata[':added_by']  = $added_by;        
        $filter_count++;
    }    
    if ($start_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_debit_note_list_squery_where = $get_stock_grn_debit_note_list_squery_where." where stock_grn_debit_note_added_on >= :start_date";
        } else {            // Query
            $get_stock_grn_debit_note_list_squery_where = $get_stock_grn_debit_note_list_squery_where." and stock_grn_debit_note_added_on >= :start_date";
        }        
        //Data
        $get_stock_grn_debit_note_list_sdata[':start_date']  = $start_date;        
        $filter_count++;
    }
    if ($end_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_debit_note_list_squery_where = $get_stock_grn_debit_note_list_squery_where." where stock_grn_debit_note_added_on <= :end_date";
        } else {            // Query
            $get_stock_grn_debit_note_list_squery_where = $get_stock_grn_debit_note_list_squery_where." and stock_grn_debit_note_added_on <= :end_date";
        }        
        //Data
        $get_stock_grn_debit_note_list_sdata['end_date']  = $end_date;        
        $filter_count++;
    }    
    $get_stock_grn_debit_note_list_squery = $get_stock_grn_debit_note_list_squery_base.$get_stock_grn_debit_note_list_squery_where;    
    try {
        $dbConnection = get_conn_handle();        
        $get_stock_grn_debit_note_list_sstatement = $dbConnection->prepare($get_stock_grn_debit_note_list_squery);        
        $get_stock_grn_debit_note_list_sstatement -> execute($get_stock_grn_debit_note_list_sdata);        
        $get_stock_grn_debit_note_list_sdetails = $get_stock_grn_debit_note_list_sstatement -> fetchAll();        
        if (false === $get_stock_grn_debit_note_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_stock_grn_debit_note_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_stock_grn_debit_note_list_sdetails;
        }
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }    
    return $return;
}   /*PURPOSE : To update Grn Debit NoteINPUT 	: Debit Note ID, Grn Debit Note Update ArrayOUTPUT 	: Debit Note ID; Message of success or failureBY 		: Lakshmi*/function db_update_grn_debit_note($debit_note_id, $grn_debit_note_update_data)
{
    if (array_key_exists("grn_id", $grn_debit_note_update_data)) {
        $grn_id = $grn_debit_note_update_data["grn_id"];
    } else {
        $grn_id = "";
    }
    if (array_key_exists("amount", $grn_debit_note_update_data)) {
        $amount = $grn_debit_note_update_data["amount"];
    } else {
        $amount = "";
    }    
    if (array_key_exists("active", $grn_debit_note_update_data)) {
        $active = $grn_debit_note_update_data["active"];
    } else {
        $active = "";
    }    
    if (array_key_exists("added_by", $grn_debit_note_update_data)) {
        $added_by = $grn_debit_note_update_data["added_by"];
    } else {
        $added_by = "";
    }
    if (array_key_exists("added_on", $grn_debit_note_update_data)) {
        $added_on = $grn_debit_note_update_data["added_on"];
    } else {
        $added_on = "";
    }    
    // Query
    $grn_debit_note_update_uquery_base = "update stock_grn_debit_note set";    
    $grn_debit_note_update_uquery_set = "";    
    $grn_debit_note_update_uquery_where = " where stock_grn_debit_note_id=:debit_note_id";    
    $grn_debit_note_update_udata = array(":debit_note_id"=>$debit_note_id);    
    $filter_count = 0;    
    if ($grn_id != "") {
        $grn_debit_note_update_uquery_set = $grn_debit_note_update_uquery_set." stock_grn_id=:grn_id,";
        $grn_debit_note_update_udata[":grn_id"] = $grn_id;
        $filter_count++;
    }
    if ($amount != "") {
        $grn_debit_note_update_uquery_set = $grn_debit_note_update_uquery_set." stock_grn_debit_note_amount=:amount,";
        $grn_debit_note_update_udata[":amount"] = $amount;
        $filter_count++;
    }    
    if ($active != "") {
        $grn_debit_note_update_uquery_set = $grn_debit_note_update_uquery_set." stock_grn_debit_note_active=:active,";
        $grn_debit_note_update_udata[":active"] = $active;
        $filter_count++;
    }
    if ($added_by != "") {
        $grn_debit_note_update_uquery_set = $grn_debit_note_update_uquery_set." stock_grn_debit_note_added_by=:added_by,";
        $grn_debit_note_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }
    if ($added_on != "") {
        $grn_debit_note_update_uquery_set = $grn_debit_note_update_uquery_set." stock_grn_debit_note_added_on=:added_on,";
        $grn_debit_note_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }    
    if ($filter_count > 0) {
        $grn_debit_note_update_uquery_set = trim($grn_debit_note_update_uquery_set, ',');
    }    
    $grn_debit_note_update_uquery = $grn_debit_note_update_uquery_base.$grn_debit_note_update_uquery_set.$grn_debit_note_update_uquery_where;
    try {
        $dbConnection = get_conn_handle();        
        $grn_debit_note_update_ustatement = $dbConnection->prepare($grn_debit_note_update_uquery);        
        $grn_debit_note_update_ustatement -> execute($grn_debit_note_update_udata);        
        $return["status"] = SUCCESS;
        $return["data"]   = $debit_note_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }    
    return $return;
}/*PURPOSE : To add new GRN approvalINPUT 	: Item ID, Amount, Remarks, Added ByOUTPUT 	: GRN Accounts ID, success or failure messageBY 		: Punith*/function db_add_stock_grn_accounts($item_id, $amount, $remarks, $added_by)
{    // Query
    $stock_grn_iquery = "insert into stock_grn_accounts_approval (stock_grn_accounts_approval_item_id,stock_grn_accounts_approval_additional_amount,stock_grn_accounts_approval_remarks,stock_grn_accounts_approval_added_by,stock_grn_accounts_approval_added_on) values(:item_id,:amount,:remarks,:added_by,:added_on)";    
    try {
        $dbConnection = get_conn_handle();
        $stock_grn_istatement = $dbConnection->prepare($stock_grn_iquery);        
        // Data
        $stock_grn_idata = array(':item_id'=>$item_id,':amount'=>$amount,':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));    
        $dbConnection->beginTransaction();
        $stock_grn_istatement->execute($stock_grn_idata);
        $stock_grn_id = $dbConnection->lastInsertId();
        $dbConnection->commit();       
        $return["status"] = SUCCESS;
        $return["data"]   = $stock_grn_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }    
    return $return;
}/*PURPOSE : To get Stock GRN Accounts listINPUT 	: GRN ID, Item ID, Added By, Start Date, End DateOUTPUT 	: List of Stock Account ApprovalsBY 		: Punith*/function db_get_stock_grn_accounts_list($stock_grn_search_data)
{
    if (array_key_exists("stock_grn_approval_id", $stock_grn_search_data)) {
        $stock_grn_approval_id = $stock_grn_search_data["stock_grn_approval_id"];
    } else {
        $stock_grn_approval_id = "";
    }    
    if (array_key_exists("item_id", $stock_grn_search_data)) {
        $item_id = $stock_grn_search_data["item_id"];
    } else {
        $item_id = "";
    }    
    if (array_key_exists("added_by", $stock_grn_search_data)) {
        $added_by= $stock_grn_search_data["added_by"];
    } else {
        $added_by= "";
    }    
    if (array_key_exists("start_date", $stock_grn_search_data)) {
        $start_date = $stock_grn_search_data["start_date"];
    } else {
        $start_date = "";
    }    
    if (array_key_exists("end_date", $stock_grn_search_data)) {
        $end_date = $stock_grn_search_data["end_date"];
    } else {
        $end_date = "";
    }    
    $get_stock_grn_list_squery_base = "select * from stock_grn_accounts_approval SGAP inner join stock_grn_items SGI on SGI.stock_grn_item_id = SGAP.stock_grn_accounts_approval_item_id";    
    $get_stock_grn_list_squery_where = "";        
    $filter_count = 0;    
    // Data
    $get_stock_grn_list_sdata = array();    
    if ($stock_grn_id != "") {
        if ($filter_count == 0) {            // Quer
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_accounts_approval_id=:stock_grn_approval_id";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_accounts_approval_id=:stock_grn_approval_id";
        }        
        // Data
        $get_stock_grn_list_sdata[':stock_grn_approval_id'] = $stock_grn_approval_id;        
        $filter_count++;
    }    
    if ($item_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_accounts_approval_item_id=:item_id";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_accounts_approval_item_id=:item_id";
        }        
        // Data
        $get_stock_grn_list_sdata[':item_id'] = $item_id;        
        $filter_count++;
    }    
    if ($added_by!= "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_accounts_approval_added_by = :added_by";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_accounts_approval_added_by = :added_by";
        }        
        //Data
        $get_stock_grn_list_sdata[':added_by']  = $added_by;        
        $filter_count++;
    }        
    if ($start_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_accounts_approval_added_on >= :start_date";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_accounts_approval_added_on >= :start_date";
        }        
        //Data
        $get_stock_grn_list_sdata[':start_date']  = $start_date;        
        $filter_count++;
    }
    if ($end_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_accounts_approval_added_on <= :end_date";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_accounts_approval_added_on <= :end_date";
        }        
        //Data
        $get_stock_grn_list_sdata['end_date']  = $end_date;        
        $filter_count++;
    }    
    $get_stock_grn_list_squery = $get_stock_grn_list_squery_base.$get_stock_grn_list_squery_where;    
    try {
        $dbConnection = get_conn_handle();        
        $get_stock_grn_list_sstatement = $dbConnection->prepare($get_stock_grn_list_squery);        
        $get_stock_grn_list_sstatement -> execute($get_stock_grn_list_sdata);        
        $get_stock_grn_list_sdetails = $get_stock_grn_list_sstatement -> fetchAll();        
        if (false === $get_stock_grn_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_stock_grn_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_stock_grn_list_sdetails;
        }
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }    
    return $return;
} /*PURPOSE : To update stock_grnINPUT 	: stock_grn ID, stock_grn Update ArrayOUTPUT 	: stock_grn ID; Message of success or failureBY 		: punith*/function db_update_stock_grn_accounts($stock_grn_approval_id, $stock_grn_update_data)
{
    if (array_key_exists("item_id", $stock_grn_update_data)) {
        $item_id = $stock_grn_update_data["item_id"];
    } else {
        $item_id = "";
    }    
    if (array_key_exists("amount", $stock_grn_update_data)) {
        $amount = $stock_grn_update_data["amount"];
    } else {
        $amount = "";
    }    
    if (array_key_exists("remarks", $stock_grn_update_data)) {
        $remarks = $stock_grn_update_data["remarks"];
    } else {
        $remarks = "";
    }    
    if (array_key_exists("added_by", $stock_grn_update_data)) {
        $added_by = $stock_grn_update_data["added_by"];
    } else {
        $added_by = "";
    }    
    $added_on = date('Y-m-d H:i:s');    
    // Query
    $stock_grn_update_uquery_base = "update stock_grn_accounts_approval set";    
    $stock_grn_update_uquery_set = "";    
    $stock_grn_update_uquery_where = " where stock_grn_accounts_approval_id=:stock_grn_approval_id";    
    $stock_grn_update_udata = array(":stock_grn_approval_id"=>$stock_grn_approval_id);    
    $filter_count = 0;    
    if ($item_id != "") {
        $stock_grn_update_uquery_set = $stock_grn_update_uquery_set." stock_grn_accounts_approval_item_id=:item_id,";
        $stock_grn_update_udata[":item_id"] = $item_id;
        $filter_count++;
    }    
    if ($amount != "") {
        $stock_grn_update_uquery_set = $stock_grn_update_uquery_set." stock_grn_accounts_approval_additional_amount=:amount,";
        $stock_grn_update_udata[":amount"] = $amount;
        $filter_count++;
    }    
    if ($remarks != "") {
        $stock_grn_update_uquery_set = $stock_grn_update_uquery_set." stock_grn_accounts_approval_remarks = :remarks,";
        $stock_grn_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }    
    if ($added_by != "") {
        $stock_grn_update_uquery_set = $stock_grn_update_uquery_set." stock_grn_accounts_approval_added_by=:added_by,";
        $stock_grn_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }    
    if ($added_on != "") {
        $stock_grn_update_uquery_set = $stock_grn_update_uquery_set." stock_grn_accounts_approval_added_on=:added_on,";
        $stock_grn_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }    
    if ($filter_count > 0) {
        $stock_grn_update_uquery_set = trim($stock_grn_update_uquery_set, ',');
    }    
    $stock_grn_update_uquery = $stock_grn_update_uquery_base.$stock_grn_update_uquery_set.$stock_grn_update_uquery_where;
    try {
        $dbConnection = get_conn_handle();        
        $stock_grn_update_ustatement = $dbConnection->prepare($stock_grn_update_uquery);        
        $stock_grn_update_ustatement -> execute($stock_grn_update_udata);        
        $return["status"] = SUCCESS;
        $return["data"]   = $stock_grn_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }    
    return $return;
}/*PURPOSE : To add new Stock Management PaymentINPUT 	: PO ID, Total Value, Additional Cost, Remarks, Added ByOUTPUT 	: Payment ID, success or failure messageBY 		: Lakshmi*/function db_add_stock_management_payment($po_id, $total_value, $additional_cost, $remarks, $added_by)
{    // Query
    $stock_management_payment_iquery = "insert into stock_management_payment (stock_management_payment_po_id,stock_management_payment_total_value,stock_management_payment_additional_cost,stock_management_payment_active,stock_management_payment_remarks,   stock_management_payment_added_by,stock_management_payment_added_on)values(:po_id,:total_value,:additional_cost,:active,:remarks,:added_by,:added_on)";
    try {
        $dbConnection = get_conn_handle();
        $stock_management_payment_istatement = $dbConnection->prepare($stock_management_payment_iquery);
        // Data
        $stock_management_payment_idata = array(':po_id'=>$po_id,':total_value'=>$total_value,':additional_cost'=>$additional_cost,':active'=>'1',':remarks'=>$remarks,        ':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
        $dbConnection->beginTransaction();
        $stock_management_payment_istatement->execute($stock_management_payment_idata);
        $stock_management_payment_id = $dbConnection->lastInsertId();
        $dbConnection->commit();
        $return["status"] = SUCCESS;
        $return["data"]   = $stock_management_payment_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }
    return $return;
}/*PURPOSE : To get Stock Management Payment ListINPUT 	: Payment ID, PO ID, Total Value, Additional Cost, Active, Added By, Start Date(for added on), End Date(for added on)OUTPUT 	: List of Stock Management PaymentBY 		: Lakshmi*/function db_get_stock_management_payment($stock_management_payment_search_data)
{
    if (array_key_exists("payment_id", $stock_management_payment_search_data)) {
        $payment_id = $stock_management_payment_search_data["payment_id"];
    } else {
        $payment_id= "";
    }
    if (array_key_exists("po_id", $stock_management_payment_search_data)) {
        $po_id = $stock_management_payment_search_data["po_id"];
    } else {
        $po_id = "";
    }
    if (array_key_exists("total_value", $stock_management_payment_search_data)) {
        $total_value = $stock_management_payment_search_data["total_value"];
    } else {
        $total_value = "";
    }
    if (array_key_exists("additional_cost", $stock_management_payment_search_data)) {
        $additional_cost = $stock_management_payment_search_data["additional_cost"];
    } else {
        $additional_cost = "";
    }
    if (array_key_exists("active", $stock_management_payment_search_data)) {
        $active = $stock_management_payment_search_data["active"];
    } else {
        $active = "";
    }
    if (array_key_exists("added_by", $stock_management_payment_search_data)) {
        $added_by = $stock_management_payment_search_data["added_by"];
    } else {
        $added_by = "";
    }
    if (array_key_exists("start_date", $stock_management_payment_search_data)) {
        $start_date= $stock_management_payment_search_data["start_date"];
    } else {
        $start_date= "";
    }
    if (array_key_exists("end_date", $stock_management_payment_search_data)) {
        $end_date= $stock_management_payment_search_data["end_date"];
    } else {
        $end_date= "";
    }
    $get_stock_management_payment_list_squery_base = "select * from stock_management_payment SMP inner join stock_purchase_order SPO on SPO.stock_purchase_order_id = SMP.stock_management_payment_po_id ";
    $get_stock_management_payment_list_squery_where = "";
    $filter_count = 0;
    // Data
    $get_stock_management_payment_list_sdata = array();
    if ($payment_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_management_payment_list_squery_where = $get_stock_management_payment_list_squery_where." where stock_management_payment_id = :payment_id";
        } else {            // Query
            $get_stock_management_payment_list_squery_where = $get_stock_management_payment_list_squery_where." and stock_management_payment_id = :payment_id";
        }
        // Data
        $get_stock_management_payment_list_sdata[':payment_id'] = $payment_id;
        $filter_count++;
    }
    if ($po_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_management_payment_list_squery_where = $get_stock_management_payment_list_squery_where." where stock_management_payment_po_id = :po_id";
        } else {            // Query
            $get_stock_management_payment_list_squery_where = $get_stock_management_payment_list_squery_where." and stock_management_payment_po_id = :po_id";
        }
        // Data
        $get_stock_management_payment_list_sdata[':po_id'] = $po_id;
        $filter_count++;
    }
    if ($total_value != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_management_payment_list_squery_where = $get_stock_management_payment_list_squery_where." where stock_management_payment_total_value = :total_value";
        } else {            // Query
            $get_stock_management_payment_list_squery_where = $get_stock_management_payment_list_squery_where." and stock_management_payment_total_value = :total_value";
        }
        // Data
        $get_stock_management_payment_list_sdata[':total_value'] = $total_value;
        $filter_count++;
    }
    if ($additional_cost != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_management_payment_list_squery_where = $get_stock_management_payment_list_squery_where." where stock_management_payment_additional_cost = :additional_cost";
        } else {            // Query
            $get_stock_management_payment_list_squery_where = $get_stock_management_payment_list_squery_where." and stock_management_payment_additional_cost = :additional_cost";
        }
        // Data
        $get_stock_management_payment_list_sdata[':additional_cost'] = $additional_cost;
        $filter_count++;
    }
    if ($active!= "") {
        if ($filter_count == 0) {            // Query
            $get_stock_management_payment_list_squery_where = $get_stock_management_payment_list_squery_where." where stock_management_payment_active = :active";
        } else {            // Query
            $get_stock_management_payment_list_squery_where = $get_stock_management_payment_list_squery_where." and stock_management_payment_active = :active";
        }
        //Data
        $get_stock_management_payment_list_sdata[':active']  = $active;
        $filter_count++;
    }
    if ($added_by!= "") {
        if ($filter_count == 0) {            // Query
            $get_stock_management_payment_list_squery_where = $get_stock_management_payment_list_squery_where." where stock_management_payment_added_by = :added_by";
        } else {            // Query
            $get_stock_management_payment_list_squery_where = $get_stock_management_payment_list_squery_where." and stock_management_payment_added_by = :added_by";
        }
        //Data
        $get_stock_management_payment_list_sdata[':added_by']  = $added_by;
        $filter_count++;
    }
    if ($start_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_management_payment_list_squery_where = $get_stock_management_payment_list_squery_where." where stock_management_payment_added_on >= :start_date";
        } else {            // Query
            $get_stock_management_payment_list_squery_where = $get_stock_management_payment_list_squery_where." and stock_management_payment_added_on >= :start_date";
        }
        //Data
        $get_stock_management_payment_list_sdata[':start_date']  = $start_date;
        $filter_count++;
    }
    if ($end_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_management_payment_list_squery_where = $get_stock_management_payment_list_squery_where." where stock_management_payment_added_on <= :end_date";
        } else {            // Query
            $get_stock_management_payment_list_squery_where = $get_stock_management_payment_list_squery_where." and stock_management_payment_added_on <= :end_date";
        }
        //Data
        $get_stock_management_payment_list_sdata['end_date']  = $end_date;
        $filter_count++;
    }
    $get_stock_management_payment_list_squery = $get_stock_management_payment_list_squery_base.$get_stock_management_payment_list_squery_where;    
    try {
        $dbConnection = get_conn_handle();
        $get_stock_management_payment_list_sstatement = $dbConnection->prepare($get_stock_management_payment_list_squery);
        $get_stock_management_payment_list_sstatement -> execute($get_stock_management_payment_list_sdata);
        $get_stock_management_payment_list_sdetails = $get_stock_management_payment_list_sstatement -> fetchAll();
        if (false === $get_stock_management_payment_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_stock_management_payment_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_stock_management_payment_list_sdetails;
        }
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }
    return $return;
} /*PURPOSE : To update Stock Management Payment INPUT 	: Payment ID, Stock Management Payment Update ArrayOUTPUT 	: Payment ID; Message of success or failureBY 		: Lakshmi*/function db_update_stock_management_payment($payment_id, $stock_management_payment_update_data)
{
    if (array_key_exists("po_id", $stock_management_payment_update_data)) {
        $po_id = $stock_management_payment_update_data["po_id"];
    } else {
        $po_id = "";
    }
    if (array_key_exists("total_value", $stock_management_payment_update_data)) {
        $total_value = $stock_management_payment_update_data["total_value"];
    } else {
        $total_value = "";
    }
    if (array_key_exists("additional_cost", $stock_management_payment_update_data)) {
        $additional_cost = $stock_management_payment_update_data["additional_cost"];
    } else {
        $additional_cost = "";
    }
    if (array_key_exists("active", $stock_management_payment_update_data)) {
        $active = $stock_management_payment_update_data["active"];
    } else {
        $active = "";
    }
    if (array_key_exists("remarks", $stock_management_payment_update_data)) {
        $remarks = $stock_management_payment_update_data["remarks"];
    } else {
        $remarks = "";
    }
    if (array_key_exists("added_by", $stock_management_payment_update_data)) {
        $added_by = $stock_management_payment_update_data["added_by"];
    } else {
        $added_by = "";
    }
    if (array_key_exists("added_on", $stock_management_payment_update_data)) {
        $added_on = $stock_management_payment_update_data["added_on"];
    } else {
        $added_on = "";
    }
    // Query
    $stock_management_payment_update_uquery_base = "update stock_management_payment set ";
    $stock_management_payment_update_uquery_set = "";
    $stock_management_payment_update_uquery_where = " where stock_management_payment_id = :payment_id";
    $stock_management_payment_update_udata = array(":payment_id"=>$payment_id);
    $filter_count = 0;
    if ($po_id != "") {
        $stock_management_payment_update_uquery_set = $stock_management_payment_update_uquery_set." stock_management_payment_po_id = :po_id,";
        $stock_management_payment_update_udata[":po_id"] = $po_id;
        $filter_count++;
    }
    if ($total_value != "") {
        $stock_management_payment_update_uquery_set = $stock_management_payment_update_uquery_set." stock_management_payment_total_value = :total_value,";
        $stock_management_payment_update_udata[":total_value"] = $total_value;
        $filter_count++;
    }
    if ($additional_cost != "") {
        $stock_management_payment_update_uquery_set = $stock_management_payment_update_uquery_set." stock_management_payment_additional_cost = :additional_cost,";
        $stock_management_payment_update_udata[":additional_cost"] = $additional_cost;
        $filter_count++;
    }
    if ($active != "") {
        $stock_management_payment_update_uquery_set = $stock_management_payment_update_uquery_set." stock_management_payment_active = :active,";
        $stock_management_payment_update_udata[":active"] = $active;
        $filter_count++;
    }
    if ($remarks != "") {
        $stock_management_payment_update_uquery_set = $stock_management_payment_update_uquery_set." stock_management_payment_remarks = :remarks,";
        $stock_management_payment_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }
    if ($added_by != "") {
        $stock_management_payment_update_uquery_set = $stock_management_payment_update_uquery_set." stock_management_payment_added_by = :added_by,";
        $stock_management_payment_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }
    if ($added_on != "") {
        $stock_management_payment_update_uquery_set = $stock_management_payment_update_uquery_set." stock_management_payment_added_on = :added_on,";
        $stock_management_payment_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }
    if ($filter_count > 0) {
        $stock_management_payment_update_uquery_set = trim($stock_management_payment_update_uquery_set, ',');
    }
    $stock_management_payment_update_uquery = $stock_management_payment_update_uquery_base.$stock_management_payment_update_uquery_set.$stock_management_payment_update_uquery_where;
    try {
        $dbConnection = get_conn_handle();
        $stock_management_payment_update_ustatement = $dbConnection->prepare($stock_management_payment_update_uquery);
        $stock_management_payment_update_ustatement -> execute($stock_management_payment_update_udata);
        $return["status"] = SUCCESS;
        $return["data"]   = $payment_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }
    return $return;
}/*PURPOSE : To add release paymentINPUT 	: PO Id,Amount,Active,Remarks,Added By,Added OnOUTPUT 	: Release Payment Id, success or failure messageBY 		: Lakshmi*/function db_add_stock_release_payment($po_id, $amount, $remarks, $added_by)
{    // Query
    $stock_payment_release_iquery = "insert into stock_release_payment   (stock_release_payment_po_id,stock_release_payment_amount,stock_release_payment_active,stock_release_payment_remarks,stock_release_payment_added_by,   stock_release_payment_added_on)values(:po_id,:amount,:active,:remarks,:added_by,:added_on)"; 
    try {
        $dbConnection = get_conn_handle();
        $stock_payment_release_istatement = $dbConnection->prepare($stock_payment_release_iquery);
        // Data
        $stock_payment_release_idata = array(':po_id'=>$po_id,':amount'=>$amount,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,        ':added_on'=>date("Y-m-d H:i:s"));
        $dbConnection->beginTransaction();
        $stock_payment_release_istatement->execute($stock_payment_release_idata);
        $stock_release_payment_id = $dbConnection->lastInsertId();
        $dbConnection->commit();
        $return["status"] = SUCCESS;
        $return["data"]   = $stock_release_payment_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }
    return $return;
}/*PURPOSE : To get Stock Payment Release ListINPUT 	: Payment Release Id,Po Id,Amount,Active,Remarks,Added by,Start Date(for added on), End Date(for added on)OUTPUT 	: List of Stock Payment Release NoteBY 		: Lakshmi*/function db_get_stock_payment_release_list($stock_payment_release_search_data)
{
    if (array_key_exists("payment_release_id", $stock_payment_release_search_data)) {
        $payment_release_id = $stock_payment_release_search_data["payment_release_id"];
    } else {
        $payment_release_id= "";
    }
    if (array_key_exists("po_id", $stock_payment_release_search_data)) {
        $po_id= $stock_payment_release_search_data["po_id"];
    } else {
        $po_id= "";
    }
    if (array_key_exists("amount", $stock_payment_release_search_data)) {
        $amount= $stock_payment_release_search_data["amount"];
    } else {
        $amount= "";
    }
    if (array_key_exists("active", $stock_payment_release_search_data)) {
        $active= $stock_payment_release_search_data["active"];
    } else {
        $active= "";
    }
    if (array_key_exists("added_by", $stock_payment_release_search_data)) {
        $added_by= $stock_payment_release_search_data["added_by"];
    } else {
        $added_by= "";
    }
    if (array_key_exists("start_date", $stock_payment_release_search_data)) {
        $start_date= $stock_payment_release_search_data["start_date"];
    } else {
        $start_date= "";
    }
    if (array_key_exists("end_date", $stock_payment_release_search_data)) {
        $end_date= $stock_payment_release_search_data["end_date"];
    } else {
        $end_date= "";
    }
    $get_stock_payment_release_list_squery_base = "select * from stock_release_payment SRP inner join stock_purchase_order SPO on SPO.stock_purchase_order_id=SRP.stock_release_payment_po_id inner join stock_vendor_master SVM on SVM.stock_vendor_id=SPO.stock_purchase_order_vendor";
    $get_stock_payment_release_list_squery_where = "";
    $filter_count = 0;
    // Data
    $get_stock_payment_release_list_sdata = array();
    if ($payment_release_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_payment_release_list_squery_where = $get_stock_payment_release_list_squery_where." 			where stock_release_payment_id=:payment_release_id";
        } else {            // Query
            $get_stock_payment_release_list_squery_where = $get_stock_payment_release_list_squery_where." 			and stock_release_payment_id=:payment_release_id";
        }
        // Data
        $get_stock_payment_release_list_sdata[':payment_release_id'] = $payment_release_id;
        $filter_count++;
    }
    if ($po_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_payment_release_list_squery_where = $get_stock_payment_release_list_squery_where." where stock_release_payment_po_id=:po_id";
        } else {            // Query
            $get_stock_payment_release_list_squery_where = $get_stock_payment_release_list_squery_where." and stock_release_payment_po_id=:po_id";
        }
        // Data
        $get_stock_payment_release_list_sdata[':po_id']  = $po_id;
        $filter_count++;
    }
    if ($amount != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_payment_release_list_squery_where = $get_stock_payment_release_list_squery_where." where stock_release_payment_amount=:amount";
        } else {            // Query
            $get_stock_payment_release_list_squery_where = $get_stock_payment_release_list_squery_where." and stock_release_payment_amount=:amount";
        }
        // Data
        $get_stock_payment_release_list_sdata[':amount']  = $amount;
        $filter_count++;
    }
    if ($active != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_payment_release_list_squery_where = $get_stock_payment_release_list_squery_where." where stock_release_payment_active=:active";
        } else {            // Query
            $get_stock_payment_release_list_squery_where = $get_stock_payment_release_list_squery_where." and stock_release_payment_active=:active";
        }
        // Data
        $get_stock_payment_release_list_sdata[':active']  = $active;
        $filter_count++;
    }    
    if ($added_by!= "") {
        if ($filter_count == 0) {            // Query
            $get_stock_payment_release_list_squery_where = $get_stock_payment_release_list_squery_where." where stock_release_payment_added_by = :added_by";
        } else {            // Query
            $get_stock_payment_release_list_squery_where = $get_stock_payment_release_list_squery_where." and stock_release_payment_added_by = :added_by";
        }
        //Data
        $get_stock_payment_release_list_sdata[':added_by']  = $added_by;
        $filter_count++;
    }
    if ($start_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_payment_release_list_squery_where = $get_stock_payment_release_list_squery_where." where stock_release_payment_added_on >= :start_date";
        } else {            // Query
            $get_stock_payment_release_list_squery_where = $get_stock_payment_release_list_squery_where." and stock_release_payment_added_on >= :start_date";
        }
        //Data
        $get_stock_payment_release_list_sdata[':start_date']  = $start_date;
        $filter_count++;
    }
    if ($end_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_payment_release_list_squery_where = $get_stock_payment_release_list_squery_where." where stock_release_payment_added_on <= :end_date";
        } else {            // Query
            $get_stock_payment_release_list_squery_where = $get_stock_payment_release_list_squery_where." and stock_release_payment_added_on <= :end_date";
        }
        //Data
        $get_stock_payment_release_list_sdata['end_date']  = $end_date;
        $filter_count++;
    }
    $get_stock_payment_release_list_squery = $get_stock_payment_release_list_squery_base.$get_stock_payment_release_list_squery_where;
    try {
        $dbConnection = get_conn_handle();
        $get_stock_payment_release_list_sstatement = $dbConnection->prepare($get_stock_payment_release_list_squery);
        $get_stock_payment_release_list_sstatement -> execute($get_stock_payment_release_list_sdata);
        $get_stock_payment_release_list_sdetails = $get_stock_payment_release_list_sstatement -> fetchAll();
        if (false === $get_stock_payment_release_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_stock_payment_release_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_stock_payment_release_list_sdetails;
        }
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }
    return $return;
} /*PURPOSE : To update Payment ReleaseINPUT 	: Payment Release ID, Payment Release Update ArrayOUTPUT 	: Payment Release ID; Message of success or failureBY 		: Lakshmi*/function db_update_payment_release($payment_release_id, $grn_payment_release_update_data)
{
    if (array_key_exists("po_id", $grn_payment_release_update_data)) {
        $po_id = $grn_payment_release_update_data["po_id"];
    } else {
        $po_id = "";
    }
    if (array_key_exists("amount", $grn_payment_release_update_data)) {
        $amount = $grn_payment_release_update_data["amount"];
    } else {
        $amount = "";
    }
    if (array_key_exists("active", $grn_payment_release_update_data)) {
        $active = $grn_payment_release_update_data["active"];
    } else {
        $active = "";
    }
    if (array_key_exists("added_by", $grn_payment_release_update_data)) {
        $added_by = $grn_payment_release_update_data["added_by"];
    } else {
        $added_by = "";
    }
    if (array_key_exists("added_on", $grn_payment_release_update_data)) {
        $added_on = $grn_payment_release_update_data["added_on"];
    } else {
        $added_on = "";
    }
    // Query
    $grn_release_payment_update_uquery_base = "update stock_release_payment set ";
    $grn_payment_release_update_uquery_set = "";
    $grn_payment_release_update_uquery_where = " where stock_release_payment_id=:payment_release_id";
    $grn_payment_release_update_udata = array(":payment_release_id"=>$payment_release_id);
    $filter_count = 0;
    if ($po_id != "") {
        $grn_payment_release_update_uquery_set = $grn_payment_release_update_uquery_set." stock_release_payment_po_id=:po_id,";
        $grn_payment_release_update_udata[":po_id"] = $po_id;
        $filter_count++;
    }
    if ($amount != "") {
        $grn_payment_release_update_uquery_set = $grn_payment_release_update_uquery_set." stock_release_payment_amount=:amount,";
        $grn_payment_release_update_udata[":amount"] = $amount;
        $filter_count++;
    }
    if ($active != "") {
        $grn_payment_release_update_uquery_set = $grn_payment_release_update_uquery_set." stock_release_payment_active=:active,";
        $grn_payment_release_update_udata[":active"] = $active;
        $filter_count++;
    }
    if ($added_by != "") {
        $grn_payment_release_update_uquery_set = $grn_payment_release_update_uquery_set." stock_release_payment_added_by=:added_by,";
        $grn_payment_release_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }
    if ($added_on != "") {
        $grn_payment_release_update_uquery_set = $grn_payment_release_update_uquery_set." stock_release_payment_added_on=:added_on,";
        $grn_payment_release_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }
    if ($filter_count > 0) {
        $grn_payment_release_update_uquery_set = trim($grn_payment_release_update_uquery_set, ',');
    }
    $grn_payment_release_update_uquery = $grn_release_payment_update_uquery_base.$grn_payment_release_update_uquery_set.$grn_payment_release_update_uquery_where;
    try {
        $dbConnection = get_conn_handle();
        $grn_payment_release_update_ustatement = $dbConnection->prepare($grn_payment_release_update_uquery);
        $grn_payment_release_update_ustatement -> execute($grn_payment_release_update_udata);
        $return["status"] = SUCCESS;
        $return["data"]   = $payment_release_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }
    return $return;
}/*PURPOSE : To add new Stock Issue PaymentINPUT 	: release ID, vendor ID, amount, instrument details, remarks, added_byOUTPUT 	: payment ID, success or failure messageBY 		: Ashwini*/function db_add_stock_issue_payment($release_id, $vendor_id, $amount, $mode, $instrument_details, $remarks, $added_by)
{    // Query
    $stock_issue_payment_iquery = "insert into stock_issue_payment(stock_issue_payment_release_id,stock_issue_payment_vendor_id,stock_issue_payment_amount,stock_issue_payment_mode,   stock_issue_payment_instrument_details, stock_issue_payment_active,stock_issue_payment_remarks,stock_issue_payment_added_by,stock_issue_payment_added_on) values(:release_id,:vendor_id,:amount,:mode,:instrument_details,:active,:remarks,:added_by,:added_on)";
    try {
        $dbConnection = get_conn_handle();
        $stock_issue_payment_istatement = $dbConnection->prepare($stock_issue_payment_iquery);
        // Data
        $stock_issue_payment_idata = array(':release_id'=>$release_id,':vendor_id'=>$vendor_id,':amount'=>$amount,':mode'=>$mode,        ':instrument_details'=>$instrument_details,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
        $dbConnection->beginTransaction();
        $stock_issue_payment_istatement->execute($stock_issue_payment_idata);
        $stock_issue_payment_id = $dbConnection->lastInsertId();
        $dbConnection->commit();
        $return["status"] = SUCCESS;
        $return["data"]   = $stock_issue_payment_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }
    return $return;
}/*PURPOSE : To get Stock Issue Payment listINPUT 	: release ID, vendor ID, amount, instrument details, remarks, added_byOUTPUT 	: List of Stock Issue PaymentBY 		: Ashwini*/function db_get_stock_issue_payment($stock_issue_payment_search_data)
{
    if (array_key_exists("payment_id", $stock_issue_payment_search_data)) {
        $payment_id = $stock_issue_payment_search_data["payment_id"];
    } else {
        $payment_id = "";
    }
    if (array_key_exists("release_id", $stock_issue_payment_search_data)) {
        $release_id = $stock_issue_payment_search_data["release_id"];
    } else {
        $release_id = "";
    }
    if (array_key_exists("vendor_id", $stock_issue_payment_search_data)) {
        $vendor_id = $stock_issue_payment_search_data["vendor_id"];
    } else {
        $vendor_id = "";
    }
    if (array_key_exists("amount", $stock_issue_payment_search_data)) {
        $amount = $stock_issue_payment_search_data["amount"];
    } else {
        $amount = "";
    }
    if (array_key_exists("instrument_details", $stock_issue_payment_search_data)) {
        $instrument_details = $stock_issue_payment_search_data["instrument_details"];
    } else {
        $instrument_details = "";
    }
    if (array_key_exists("active", $stock_issue_payment_search_data)) {
        $active = $stock_issue_payment_search_data["active"];
    } else {
        $active = "";
    }        
    if (array_key_exists("added_by", $stock_issue_payment_search_data)) {
        $added_by = $stock_issue_payment_search_data["added_by"];
    } else {
        $added_by = "";
    }
    if (array_key_exists("start_date", $stock_issue_payment_search_data)) {
        $start_date = $stock_issue_payment_search_data["start_date"];
    } else {
        $start_date = "";
    }
    if (array_key_exists("end_date", $stock_issue_payment_search_data)) {
        $end_date = $stock_issue_payment_search_data["end_date"];
    } else {
        $end_date = "";
    }
    $get_stock_issue_payment_list_squery_base = "select * from stock_issue_payment ";
    $get_stock_issue_payment_list_squery_where = "";
    $filter_count = 0;
    // Data
    $get_stock_issue_payment_list_sdata = array();
    if ($payment_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." where stock_issue_payment_id = :payment_id";
        } else {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." and stock_issue_payment_id = :payment_id";
        }
        // Data
        $get_stock_issue_payment_list_sdata[':payment_id'] = $payment_id;
        $filter_count++;
    }
    if ($release_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." where stock_issue_payment_release_id = :release_id";
        } else {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." and stock_issue_payment_release_id = :release_id";
        }
        // Data
        $get_stock_issue_payment_list_sdata[':release_id'] = $release_id;
        $filter_count++;
    }
    if ($vendor_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." where stock_issue_payment_vendor_id = :vendor_id";
        } else {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." and stock_issue_payment_vendor_id = :vendor_id";
        }
        // Data
        $get_stock_issue_payment_list_sdata[':vendor_id'] = $vendor_id;
        $filter_count++;
    }
    if ($amount != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." where stock_issue_payment_amount = :amount";
        } else {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." and stock_issue_payment_amount = :amount";
        }
        // Data
        $get_stock_issue_payment_list_sdata[':amount'] = $amount;
        $filter_count++;
    }
    if ($instrument_details != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." where stock_issue_payment_instrument_details = :instrument_details";
        } else {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." and stock_issue_payment_instrument_details = :instrument_details";
        }
        // Data
        $get_stock_issue_payment_list_sdata[':instrument_details'] = $instrument_details;
        $filter_count++;
    }    
    if ($active != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." where stock_issue_payment_active = :active";
        } else {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." and stock_issue_payment_active = :active";
        }
        // Data
        $get_stock_issue_payment_list_sdata[':active']  = $active;
        $filter_count++;
    }    
    if ($added_by != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." where stock_issue_payment_added_by = :added_by";
        } else {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." and stock_issue_payment_added_by = :added_by";
        }
        //Data
        $get_stock_issue_payment_list_sdata[':added_by']  = $added_by;
        $filter_count++;
    }
    if ($start_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." where stock_issue_payment_added_on = :start_date";
        } else {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." and stock_issue_payment_added_on = :start_date";
        }
        //Data
        $get_stock_issue_payment_list_sdata[':start_date']  = $start_date;
        $filter_count++;
    }
    if ($end_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." where stock_issue_payment_added_on = :end_date";
        } else {            // Query
            $get_stock_issue_payment_list_squery_where = $get_stock_issue_payment_list_squery_where." and stock_issue_payment_added_on = :end_date";
        }
        //Data
        $get_stock_issue_payment_list_sdata[':end_date']  = $end_date;
        $filter_count++;
    }                
    $get_stock_issue_payment_list_squery = $get_stock_issue_payment_list_squery_base.$get_stock_issue_payment_list_squery_where;    
    try {
        $dbConnection = get_conn_handle();
        $get_stock_issue_payment_list_sstatement = $dbConnection->prepare($get_stock_issue_payment_list_squery);
        $get_stock_issue_payment_list_sstatement -> execute($get_stock_issue_payment_list_sdata);
        $get_stock_issue_payment_list_sdetails = $get_stock_issue_payment_list_sstatement -> fetchAll();
        if (false === $get_stock_issue_payment_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_stock_issue_payment_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_stock_issue_payment_list_sdetails;
        }
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }
    return $return;
}   /*PURPOSE : To update Stock Issue Payment listINPUT 	: payment_id,Stock Issue Payment list Update ArrayOUTPUT 	: payment_id; Message of success or failureBY 		: Ashwini*/function db_update_stock_issue_payment($payment_id, $stock_issue_payment_update_data)
{
    if (array_key_exists("release_id", $stock_issue_payment_update_data)) {
        $release_id = $stock_issue_payment_update_data["release_id"];
    } else {
        $release_id = "";
    }
    if (array_key_exists("vendor_id", $stock_issue_payment_update_data)) {
        $vendor_id = $stock_issue_payment_update_data["vendor_id"];
    } else {
        $vendor_id = "";
    }
    if (array_key_exists("amount", $stock_issue_payment_update_data)) {
        $amount = $stock_issue_payment_update_data["amount"];
    } else {
        $amount = "";
    }
    if (array_key_exists("mode", $stock_issue_payment_update_data)) {
        $mode = $stock_issue_payment_update_data["mode"];
    } else {
        $mode = "";
    }
    if (array_key_exists("instrument_details", $stock_issue_payment_update_data)) {
        $instrument_details = $stock_issue_payment_update_data["instrument_details"];
    } else {
        $instrument_details = "";
    }    
    if (array_key_exists("active", $stock_issue_payment_update_data)) {
        $active = $stock_issue_payment_update_data["active"];
    } else {
        $active = "";
    }
    if (array_key_exists("remarks", $stock_issue_payment_update_data)) {
        $remarks = $stock_issue_payment_update_data["remarks"];
    } else {
        $remarks = "";
    }
    if (array_key_exists("added_by", $stock_issue_payment_update_data)) {
        $added_by = $stock_issue_payment_update_data["added_by"];
    } else {
        $added_by = "";
    }
    if (array_key_exists("added_on", $stock_issue_payment_update_data)) {
        $added_on = $stock_issue_payment_update_data["added_on"];
    } else {
        $added_on = "";
    }
    // Query
    $stock_issue_payment_update_uquery_base = "update stock_issue_payment set";
    $stock_issue_payment_update_uquery_set = "";
    $stock_issue_payment_update_uquery_where = " where stock_issue_payment_id = :payment_id";
    $stock_issue_payment_update_udata = array(":payment_id"=>$payment_id);
    $filter_count = 0;
    if ($release_id != "") {
        $stock_issue_payment_update_uquery_set = $stock_issue_payment_update_uquery_set."stock_issue_payment_release_id  = :release_id,";
        $stock_issue_payment_update_udata[":release_id"] = $release_id;
        $filter_count++;
    }
    if ($vendor_id != "") {
        $stock_issue_payment_update_uquery_set = $stock_issue_payment_update_uquery_set."stock_issue_payment_vendor_id  = :vendor_id,";
        $stock_issue_payment_update_udata[":vendor_id"] = $vendor_id;
        $filter_count++;
    }
    if ($amount != "") {
        $stock_issue_payment_update_uquery_set = $stock_issue_payment_update_uquery_set."stock_issue_payment_amount  = :amount,";
        $stock_issue_payment_update_udata[":amount"] = $amount;
        $filter_count++;
    }
    if ($mode != "") {
        $stock_issue_payment_update_uquery_set = $stock_issue_payment_update_uquery_set."stock_issue_payment_mode  = :mode,";
        $stock_issue_payment_update_udata[":mode"] = $mode;
        $filter_count++;
    }
    if ($instrument_details!= "") {
        $stock_issue_payment_update_uquery_set = $stock_issue_payment_update_uquery_set."stock_issue_payment_instrument_details  = :instrument_details,";
        $stock_issue_payment_update_udata[":instrument_details"] = $instrument_details;
        $filter_count++;
    }
    if ($active != "") {
        $stock_issue_payment_update_uquery_set = $stock_issue_payment_update_uquery_set."stock_issue_payment_active = :active,";
        $stock_issue_payment_update_udata[":active"] = $active;
        $filter_count++;
    }
    if ($remarks != "") {
        $stock_issue_payment_update_uquery_set = $stock_issue_payment_update_uquery_set." stock_issue_payment_remarks = :remarks,";
        $stock_issue_payment_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }
    if ($added_by != "") {
        $stock_issue_payment_update_uquery_set = $stock_issue_payment_update_uquery_set."stock_issue_payment_added_by = :added_by,";
        $stock_issue_payment_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }
    if ($added_on != "") {
        $stock_issue_payment_update_uquery_set = $stock_issue_payment_update_uquery_set."stock_issue_payment_added_on = :added_on,";
        $stock_issue_payment_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }
    if ($filter_count > 0) {
        $stock_issue_payment_update_uquery_set = trim($stock_issue_payment_update_uquery_set, ',');
    }
    $stock_issue_payment_update_uquery = $stock_issue_payment_update_uquery_base.$stock_issue_payment_update_uquery_set.    $stock_issue_payment_update_uquery_where;
    try {
        $dbConnection = get_conn_handle();
        $stock_issue_payment_update_ustatement = $dbConnection->prepare($stock_issue_payment_update_uquery);
        $stock_issue_payment_update_ustatement -> execute($stock_issue_payment_update_udata);
        $return["status"] = SUCCESS;
        $return["data"]   = $payment_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }
    return $return;
}/*PURPOSE : To add new Stock Additional Payment MasterINPUT 	: payment type, remarks, added_byOUTPUT 	: master ID, success or failure messageBY 		: Ashwini*/function db_add_stock_additional_payment_master($payment_type, $remarks, $added_by, $added_on)
{    // Query
    $stock_additional_payment_master_iquery = "insert into stock_additional_payment_master(stock_additional_payment_master_payment_type,stock_additional_payment_master_active,stock_additional_payment_master_remarks,stock_additional_payment_master_added_by,stock_additional_payment_master_added_on) values(:payment_type,:active,:remarks,:added_by,:added_on)";      
    try {
        $dbConnection = get_conn_handle();
        $stock_additional_payment_master_istatement = $dbConnection->prepare($stock_additional_payment_master_iquery);
        // Data
        $stock_additional_payment_master_idata = array(':payment_type'=>$payment_type,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));        
        $dbConnection->beginTransaction();
        $stock_additional_payment_master_istatement->execute($stock_additional_payment_master_idata);
        $stock_additional_payment_master_id = $dbConnection->lastInsertId();
        $dbConnection->commit();
        $return["status"] = SUCCESS;
        $return["data"]   = $stock_additional_payment_master_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }
    return $return;
}/*PURPOSE : To get Stock Additional Payment Master listINPUT 	: payment type, remarks, added_byOUTPUT 	: List of Stock Additional Payment MasterBY 		: Ashwini*/function db_get_stock_additional_payment_master($stock_additional_payment_master_search_data)
{
    if (array_key_exists("master_id", $stock_additional_payment_master_search_data)) {
        $master_id = $stock_additional_payment_master_search_data["master_id"];
    } else {
        $master_id = "";
    }
    if (array_key_exists("payment_type", $stock_additional_payment_master_search_data)) {
        $payment_type = $stock_additional_payment_master_search_data["payment_type"];
    } else {
        $payment_type = "";
    }
    if (array_key_exists("active", $stock_additional_payment_master_search_data)) {
        $active = $stock_additional_payment_master_search_data["active"];
    } else {
        $active = "";
    }
    if (array_key_exists("added_by", $stock_additional_payment_master_search_data)) {
        $added_by = $stock_additional_payment_master_search_data["added_by"];
    } else {
        $added_by = "";
    }
    if (array_key_exists("start_date", $stock_additional_payment_master_search_data)) {
        $start_date = $stock_additional_payment_master_search_data["start_date"];
    } else {
        $start_date = "";
    }
    if (array_key_exists("end_date", $stock_additional_payment_master_search_data)) {
        $end_date = $stock_additional_payment_master_search_data["end_date"];
    } else {
        $end_date = "";
    }
    $get_stock_additional_payment_master_list_squery_base = "select * from stock_additional_payment_master ";
    $get_stock_additional_payment_master_list_squery_where = "";
    $filter_count = 0;
    // Data
    $get_stock_additional_payment_master_list_sdata = array();
    if ($master_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_additional_payment_master_list_squery_where = $get_stock_additional_payment_master_list_squery_where." where stock_additional_payment_master_id = :master_id";
        } else {            // Query
            $get_stock_additional_payment_master_list_squery_where = $get_stock_additional_payment_master_list_squery_where." and stock_additional_payment_master_id = :master_id";
        }
        // Data
        $get_stock_additional_payment_master_list_sdata[':master_id'] = $master_id;
        $filter_count++;
    }
    if ($payment_type != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_additional_payment_master_list_squery_where = $get_stock_additional_payment_master_list_squery_where." where stock_additional_payment_master_payment_type = :payment_type";
        } else {            // Query
            $get_stock_additional_payment_master_list_squery_where = $get_stock_additional_payment_master_list_squery_where." and stock_additional_payment_master_payment_type = :payment_type";
        }
        // Data
        $get_stock_additional_payment_master_list_sdata[':payment_type'] = $payment_type;
        $filter_count++;
    }
    if ($active != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_additional_payment_master_list_squery_where = $get_stock_additional_payment_master_list_squery_where." where stock_additional_payment_master_active = :active";
        } else {            // Query
            $get_stock_additional_payment_master_list_squery_where = $get_stock_additional_payment_master_list_squery_where." and stock_additional_payment_master_active = :active";
        }
        // Data
        $get_stock_additional_payment_master_list_sdata[':active']  = $active;
        $filter_count++;
    }    
    if ($added_by != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_additional_payment_master_list_squery_where = $get_stock_additional_payment_master_list_squery_where." where stock_additional_payment_master_added_by = :added_by";
        } else {            // Query
            $get_stock_additional_payment_master_list_squery_where = $get_stock_additional_payment_master_list_squery_where." and stock_additional_payment_master_added_by = :added_by";
        }
        //Data
        $get_stock_additional_payment_master_list_sdata[':added_by']  = $added_by;
        $filter_count++;
    }
    if ($start_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_additional_payment_master_list_squery_where = $get_stock_additional_payment_master_list_squery_where." where stock_additional_payment_master_added_on = :start_date";
        } else {            // Query
            $get_stock_additional_payment_master_list_squery_where = $get_stock_additional_payment_master_list_squery_where." and stock_additional_payment_master_added_on = :start_date";
        }
        //Data
        $get_stock_additional_payment_master_list_sdata[':start_date']  = $start_date;
        $filter_count++;
    }
    if ($end_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_additional_payment_master_list_squery_where = $get_stock_additional_payment_master_list_squery_where." where stock_additional_payment_master_added_on = :end_date";
        } else {            // Query
            $get_stock_additional_payment_master_list_squery_where = $get_stock_additional_payment_master_list_squery_where." and stock_additional_payment_master_added_on = :end_date";
        }
        //Data
        $get_stock_additional_payment_master_list_sdata[':end_date']  = $end_date;
        $filter_count++;
    }
    $get_stock_additional_payment_master_list_order_by = " order by stock_additional_payment_master_payment_type ASC";
    $get_stock_additional_payment_master_list_squery = $get_stock_additional_payment_master_list_squery_base.$get_stock_additional_payment_master_list_squery_where.$get_stock_additional_payment_master_list_order_by;    
    try {
        $dbConnection = get_conn_handle();
        $get_stock_additional_payment_master_list_sstatement = $dbConnection->prepare($get_stock_additional_payment_master_list_squery);
        $get_stock_additional_payment_master_list_sstatement -> execute($get_stock_additional_payment_master_list_sdata);
        $get_stock_additional_payment_master_list_sdetails = $get_stock_additional_payment_master_list_sstatement -> fetchAll();
        if (false === $get_stock_additional_payment_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_stock_additional_payment_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_stock_additional_payment_master_list_sdetails;
        }
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }
    return $return;
}   /*PURPOSE : To update Stock Additional Payment Master  listINPUT 	: master_id,Stock Additional Payment Master list Update ArrayOUTPUT 	: master_id; Message of success or failureBY 		: Ashwini*/function db_update_stock_additional_payment_master($master_id, $stock_additional_payment_master_update_data)
{
    if (array_key_exists("payment_type", $stock_additional_payment_master_update_data)) {
        $payment_type = $stock_additional_payment_master_update_data["payment_type"];
    } else {
        $payment_type = "";
    }
    if (array_key_exists("active", $stock_additional_payment_master_update_data)) {
        $active = $stock_additional_payment_master_update_data["active"];
    } else {
        $active = "";
    }
    if (array_key_exists("remarks", $stock_additional_payment_master_update_data)) {
        $remarks = $stock_additional_payment_master_update_data["remarks"];
    } else {
        $remarks = "";
    }
    if (array_key_exists("added_by", $stock_additional_payment_master_update_data)) {
        $added_by = $stock_additional_payment_master_update_data["added_by"];
    } else {
        $added_by = "";
    }
    if (array_key_exists("added_on", $stock_additional_payment_master_update_data)) {
        $added_on = $stock_additional_payment_master_update_data["added_on"];
    } else {
        $added_on = "";
    }
    // Query
    $stock_additional_payment_master_update_uquery_base = "update stock_additional_payment_master set";
    $stock_additional_payment_master_update_uquery_set = "";
    $stock_additional_payment_master_update_uquery_where = " where stock_additional_payment_master_id = :master_id";
    $stock_additional_payment_master_update_udata = array(":master_id"=>$master_id);
    $filter_count = 0;
    if ($payment_type != "") {
        $stock_additional_payment_master_update_uquery_set = $stock_additional_payment_master_update_uquery_set."stock_additional_payment_master_  = payment_type:payment_type,";
        $stock_additional_payment_master_update_udata[":payment_type"] = $payment_type;
        $filter_count++;
    }
    if ($active != "") {
        $stock_additional_payment_master_update_uquery_set = $stock_additional_payment_master_update_uquery_set."stock_additional_payment_master_active = :active,";
        $stock_additional_payment_master_update_udata[":active"] = $active;
        $filter_count++;
    }
    if ($remarks != "") {
        $stock_additional_payment_master_update_uquery_set = $stock_additional_payment_master_update_uquery_set." stock_additional_payment_master_remarks = :remarks,";
        $stock_additional_payment_master_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }
    if ($added_by != "") {
        $stock_additional_payment_master_update_uquery_set = $stock_additional_payment_master_update_uquery_set."stock_additional_payment_master_added_by = :added_by,";
        $stock_additional_payment_master_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }
    if ($added_on != "") {
        $stock_additional_payment_master_update_uquery_set = $stock_additional_payment_master_update_uquery_set."stock_additional_payment_master_added_on = :added_on,";
        $stock_additional_payment_master_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }
    if ($filter_count > 0) {
        $stock_additional_payment_master_update_uquery_set = trim($stock_additional_payment_master_update_uquery_set, ',');
    }
    $stock_additional_payment_master_update_uquery = $stock_additional_payment_master_update_uquery_base.$stock_additional_payment_master_update_uquery_set.    $stock_additional_payment_master_update_uquery_where;
    try {
        $dbConnection = get_conn_handle();
        $stock_additional_payment_master_update_ustatement = $dbConnection->prepare($stock_additional_payment_master_update_uquery);
        $stock_additional_payment_master_update_ustatement -> execute($stock_additional_payment_master_update_udata);
        $return["status"] = SUCCESS;
        $return["data"]   = $master_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }
    return $return;
}/*PURPOSE : To get Stock Grn Engineer Inspection ListINPUT 	: Inspection Id,Grn Item Id,Quantity,Remarks to Account,Added by,Start Date(for added on), End Date(for added on)OUTPUT 	: List of stock Grn Engineer InspectionBY 		: Lakshmi*/function db_get_sum_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data)
{
    if (array_key_exists("inspection_id", $stock_grn_engineer_inspection_search_data)) {
        $inspection_id = $stock_grn_engineer_inspection_search_data["inspection_id"];
    } else {
        $inspection_id = "";
    }
    if (array_key_exists("grn_item_id", $stock_grn_engineer_inspection_search_data)) {
        $grn_item_id= $stock_grn_engineer_inspection_search_data["grn_item_id"];
    } else {
        $grn_item_id= "";
    }
    if (array_key_exists("po_id", $stock_grn_engineer_inspection_search_data)) {
        $po_id= $stock_grn_engineer_inspection_search_data["po_id"];
    } else {
        $po_id= "";
    }
    if (array_key_exists("grn_id", $stock_grn_engineer_inspection_search_data)) {
        $grn_id = $stock_grn_engineer_inspection_search_data["grn_id"];
    } else {
        $grn_id = "";
    }
    if (array_key_exists("quantity", $stock_grn_engineer_inspection_search_data)) {
        $quantity= $stock_grn_engineer_inspection_search_data["quantity"];
    } else {
        $quantity= "";
    }
    if (array_key_exists("active", $stock_grn_engineer_inspection_search_data)) {
        $active= $stock_grn_engineer_inspection_search_data["active"];
    } else {
        $active= "";
    }
    if (array_key_exists("remarks_to_account", $stock_grn_engineer_inspection_search_data)) {
        $remarks_to_account= $stock_grn_engineer_inspection_search_data["remarks_to_account"];
    } else {
        $remarks_to_account= "";
    }
    if (array_key_exists("material", $stock_grn_engineer_inspection_search_data)) {
        $material = $stock_grn_engineer_inspection_search_data["material"];
    } else {
        $material = "";
    }
    if (array_key_exists("order_id", $stock_grn_engineer_inspection_search_data)) {
        $order_id = $stock_grn_engineer_inspection_search_data["order_id"];
    } else {
        $order_id = "";
    }
    if (array_key_exists("added_by", $stock_grn_engineer_inspection_search_data)) {
        $added_by= $stock_grn_engineer_inspection_search_data["added_by"];
    } else {
        $added_by= "";
    }
    if (array_key_exists("start_date", $stock_grn_engineer_inspection_search_data)) {
        $start_date= $stock_grn_engineer_inspection_search_data["start_date"];
    } else {
        $start_date= "";
    }
    if (array_key_exists("end_date", $stock_grn_engineer_inspection_search_data)) {
        $end_date= $stock_grn_engineer_inspection_search_data["end_date"];
    } else {
        $end_date= "";
    }
    $get_stock_grn_engineer_inspection_list_squery_base = "select *,count(SPO.stock_purchase_order_id) as total_po from stock_grn_engineer_inspection SGEI inner join users U on U.user_id = SGEI.stock_grn_engineer_inspection_added_by inner join stock_grn_items SGI on SGI.stock_grn_item_id = SGEI.stock_grn_engineer_inspection_grn_item_id inner join stock_material_master SMM on SMM.stock_material_id = SGI.stock_grn_item inner join stock_grn SG on SG.stock_grn_id= SGI.stock_grn_id inner join stock_purchase_order SPO on SPO.stock_purchase_order_id = SG.stock_grn_purchase_order_id inner join stock_vendor_master SVM on SVM.stock_vendor_id=SPO.stock_purchase_order_vendor inner join stock_tax_type_master STTM on STTM.stock_tax_type_master_id = SPO.stock_purchase_order_tax_type inner join stock_unit_measure_master SUMM on SUMM.stock_unit_id=SMM.stock_material_unit_of_measure";
    $get_stock_grn_engineer_inspection_list_squery_where = "";
    $filter_count = 0;
    // Data
    $get_stock_grn_engineer_inspection_list_sdata = array();
    if ($inspection_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where stock_grn_engineer_inspection_id=:inspection_id";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and stock_grn_engineer_inspection_id=:inspection_id";
        }
        // Data
        $get_stock_grn_engineer_inspection_list_sdata[':inspection_id'] = $inspection_id;
        $filter_count++;
    }
    if ($grn_item_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where stock_grn_engineer_inspection_grn_item_id=:grn_item_id";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and stock_grn_engineer_inspection_grn_item_id=:grn_item_id";
        }
        // Data
        $get_stock_grn_engineer_inspection_list_sdata[':grn_item_id']  = $grn_item_id;
        $filter_count++;
    }
    if ($po_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where SPO.stock_purchase_order_id=:po_id";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and SPO.stock_purchase_order_id=:po_id";
        }
        // Data
        $get_stock_grn_engineer_inspection_list_sdata[':po_id']  = $po_id;
        $filter_count++;
    }
    if ($grn_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where SG.stock_grn_id = :grn_id";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and SG.stock_grn_id = :grn_id";
        }
        // Data
        $get_stock_grn_engineer_inspection_list_sdata[':grn_id'] = $grn_id;
        $filter_count++;
    }
    if ($quantity != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where stock_grn_engineer_inspection_approved_quantity=:quantity";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and stock_grn_engineer_inspection_approved_quantity=:quantity";
        }
        // Data
        $get_stock_grn_engineer_inspection_list_sdata[':quantity']  = $quantity;
        $filter_count++;
    }
    if ($active != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where stock_grn_engineer_inspection_active = :active";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and stock_grn_engineer_inspection_active = :active";
        }
        // Data
        $get_stock_grn_engineer_inspection_list_sdata[':active']  = $active;
        $filter_count++;
    }
    if ($remarks_to_account != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where stock_grn_engineer_inspection_remarks_to_account=:remarks_to_account";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and stock_grn_engineer_inspection_remarks_to_account=:remarks_to_account";
        }
        // Data
        $get_stock_grn_engineer_inspection_list_sdata[':remarks_to_account']  = $remarks_to_account;
        $filter_count++;
    }
    if ($material != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where SGI.stock_grn_item = :material";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and SGI.stock_grn_item = :material";
        }
        //Data
        $get_stock_grn_engineer_inspection_list_sdata[':material']  = $material;
        $filter_count++;
    }
    if ($order_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where SG.stock_grn_purchase_order_id = :order";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and SG.stock_grn_purchase_order_id = :order";
        }
        //Data
        $get_stock_grn_engineer_inspection_list_sdata[':order']  = $order_id;
        $filter_count++;
    }
    if ($added_by!= "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where stock_grn_engineer_inspection_added_by = :added_by";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and stock_grn_engineer_inspection_added_by = :added_by";
        }
        //Data
        $get_stock_grn_engineer_inspection_list_sdata[':added_by']  = $added_by;
        $filter_count++;
    }
    if ($start_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where stock_grn_engineer_inspection_added_on >= :start_date";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and stock_grn_engineer_inspection_added_on >= :start_date";
        }
        //Data
        $get_stock_grn_engineer_inspection_list_sdata[':start_date']  = $start_date;
        $filter_count++;
    }
    if ($end_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." where stock_grn_engineer_inspection_added_on <= :end_date";
        } else {            // Query
            $get_stock_grn_engineer_inspection_list_squery_where = $get_stock_grn_engineer_inspection_list_squery_where." and stock_grn_engineer_inspection_added_on <= :end_date";
        }
        //Data
        $get_stock_grn_engineer_inspection_list_sdata['end_date']  = $end_date;
        $filter_count++;
    }
    $get_stock_indent_items_list_squery_group_by = " group by SPO.stock_purchase_order_id";
    $get_stock_grn_engineer_inspection_list_squery = $get_stock_grn_engineer_inspection_list_squery_base.$get_stock_grn_engineer_inspection_list_squery_where.$get_stock_indent_items_list_squery_group_by;
    try {
        $dbConnection = get_conn_handle();
        $get_stock_grn_engineer_inspection_list_sstatement = $dbConnection->prepare($get_stock_grn_engineer_inspection_list_squery);
        $get_stock_grn_engineer_inspection_list_sstatement -> execute($get_stock_grn_engineer_inspection_list_sdata);
        $get_stock_grn_engineer_inspection_list_sdetails = $get_stock_grn_engineer_inspection_list_sstatement -> fetchAll();
        if (false === $get_stock_grn_engineer_inspection_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_stock_grn_engineer_inspection_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_stock_grn_engineer_inspection_list_sdetails;
        }
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }
    return $return;
}/*PURPOSE : To get Stock GRN Accounts listINPUT 	: GRN ID, Item ID, Added By, Start Date, End DateOUTPUT 	: List of Stock Account ApprovalsBY 		: Punith*/function db_get_sum_stock_grn_accounts_list($stock_grn_search_data)
{
    if (array_key_exists("stock_grn_approval_id", $stock_grn_search_data)) {
        $stock_grn_approval_id = $stock_grn_search_data["stock_grn_approval_id"];
    } else {
        $stock_grn_approval_id = "";
    }
    if (array_key_exists("item_id", $stock_grn_search_data)) {
        $item_id = $stock_grn_search_data["item_id"];
    } else {
        $item_id = "";
    }
    if (array_key_exists("order_id", $stock_grn_search_data)) {
        $order_id = $stock_grn_search_data["order_id"];
    } else {
        $order_id = "";
    }
    if (array_key_exists("added_by", $stock_grn_search_data)) {
        $added_by= $stock_grn_search_data["added_by"];
    } else {
        $added_by= "";
    }
    if (array_key_exists("start_date", $stock_grn_search_data)) {
        $start_date = $stock_grn_search_data["start_date"];
    } else {
        $start_date = "";
    }
    if (array_key_exists("end_date", $stock_grn_search_data)) {
        $end_date = $stock_grn_search_data["end_date"];
    } else {
        $end_date = "";
    }
    $get_stock_grn_list_squery_base = "select *,sum(SGAP.stock_grn_accounts_approval_additional_amount) as total_additional_amt from stock_grn_accounts_approval SGAP inner join stock_grn_items SGI on SGI.stock_grn_item_id = SGAP.stock_grn_accounts_approval_item_id inner join stock_grn SG on SG.stock_grn_id=SGI.stock_grn_id  inner join stock_purchase_order SPO on SPO.stock_purchase_order_id=SG.stock_grn_purchase_order_id";
    $get_stock_grn_list_squery_where = "";    
    $filter_count = 0;
    // Data
    $get_stock_grn_list_sdata = array();
    if ($stock_grn_id != "") {
        if ($filter_count == 0) {            // Quer
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_accounts_approval_id=:stock_grn_approval_id";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_accounts_approval_id=:stock_grn_approval_id";
        }
        // Data
        $get_stock_grn_list_sdata[':stock_grn_approval_id'] = $stock_grn_approval_id;
        $filter_count++;
    }
    if ($item_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_accounts_approval_item_id=:item_id";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_accounts_approval_item_id=:item_id";
        }
        // Data
        $get_stock_grn_list_sdata[':item_id'] = $item_id;
        $filter_count++;
    }
    if ($order_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where SPO.stock_purchase_order_id=:order_id";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and SPO.stock_purchase_order_id=:order_id";
        }
        // Data
        $get_stock_grn_list_sdata[':order_id'] = $order_id;
        $filter_count++;
    }    
    if ($added_by!= "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_accounts_approval_added_by = :added_by";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_accounts_approval_added_by = :added_by";
        }
        //Data
        $get_stock_grn_list_sdata[':added_by']  = $added_by;
        $filter_count++;
    }
    if ($start_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_accounts_approval_added_on >= :start_date";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_accounts_approval_added_on >= :start_date";
        }
        //Data
        $get_stock_grn_list_sdata[':start_date']  = $start_date;
        $filter_count++;
    }
    if ($end_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." where stock_grn_accounts_approval_added_on <= :end_date";
        } else {            // Query
            $get_stock_grn_list_squery_where = $get_stock_grn_list_squery_where." and stock_grn_accounts_approval_added_on <= :end_date";
        }
        //Data
        $get_stock_grn_list_sdata['end_date']  = $end_date;
        $filter_count++;
    }
    $get_stock_grn_list_squery_group_by = " group by SPO.stock_purchase_order_id";
    $get_stock_grn_list_squery = $get_stock_grn_list_squery_base.$get_stock_grn_list_squery_where.$get_stock_grn_list_squery_group_by;
    //var_dump($get_stock_grn_list_squery);
    //var_dump($get_stock_grn_list_sdata);
    try {
        $dbConnection = get_conn_handle();
        $get_stock_grn_list_sstatement = $dbConnection->prepare($get_stock_grn_list_squery);
        $get_stock_grn_list_sstatement -> execute($get_stock_grn_list_sdata);
        $get_stock_grn_list_sdetails = $get_stock_grn_list_sstatement -> fetchAll();
        if (false === $get_stock_grn_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_stock_grn_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_stock_grn_list_sdetails;
        }
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }
    return $return;
}/*LIST OF TBDs1. Wherever there is name search, there should be a wildcard*//*PURPOSE : To add new Stock Grn DocumentINPUT 	: Grn ID, Name, Active, Remarks, Added By, Added OnOUTPUT 	: Document Id, success or failure messageBY 		: Ashwini*/function db_add_stock_grn_document($grn_id, $name, $remarks, $added_by)
{    // Query
    $stock_grn_document_iquery = "insert into stock_grn_document   (stock_grn_document_grn_id,stock_grn_document_name,stock_grn_document_active,stock_grn_document_remarks,stock_grn_document_added_by,stock_grn_document_added_on)	values(:grn_id,:name,:active,:remarks,:added_by,:added_on)";
    try {
        $dbConnection = get_conn_handle();
        $stock_grn_document_istatement = $dbConnection->prepare($stock_grn_document_iquery);
        // Data
        $stock_grn_document_idata = array(':grn_id'=>$grn_id,':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
        $dbConnection->beginTransaction();
        $stock_grn_document_istatement->execute($stock_grn_document_idata);
        $stock_grn_document_id = $dbConnection->lastInsertId();
        $dbConnection->commit();
        $return["status"] = SUCCESS;
        $return["data"]   = $stock_grn_document_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }
    return $return;
}/*PURPOSE : To get Stock Grn Document listINPUT 	: Document ID, Grn ID, Name, Active, Remarks, Added By, Start Date(for added on), End Date(for added on)OUTPUT 	: List of Stock GrnBY 		: Ashwini*/function db_get_stock_grn_document_list($stock_grn_document_search_data)
{
    if (array_key_exists("document_id", $stock_grn_document_search_data)) {
        $document_id = $stock_grn_document_search_data["document_id"];
    } else {
        $document_id= "";
    }
    if (array_key_exists("grn_id", $stock_grn_document_search_data)) {
        $grn_id= $stock_grn_document_search_data["grn_id"];
    } else {
        $grn_id = "";
    }
    if (array_key_exists("name", $stock_grn_document_search_data)) {
        $name= $stock_grn_document_search_data["name"];
    } else {
        $name = "";
    }
    if (array_key_exists("active", $stock_grn_document_search_data)) {
        $active= $stock_grn_document_search_data["active"];
    } else {
        $active= "";
    }
    if (array_key_exists("added_by", $stock_grn_document_search_data)) {
        $added_by= $stock_grn_document_search_data["added_by"];
    } else {
        $added_by= "";
    }
    if (array_key_exists("start_date", $stock_grn_document_search_data)) {
        $start_date= $stock_grn_document_search_data["start_date"];
    } else {
        $start_date= "";
    }
    if (array_key_exists("end_date", $stock_grn_document_search_data)) {
        $end_date= $stock_grn_document_search_data["end_date"];
    } else {
        $end_date= "";
    }
    $get_stock_grn_document_list_squery_base = "select * from stock_grn_document SGD  inner join users U on U.user_id=SGD.stock_grn_document_added_by";
    $get_stock_grn_document_list_squery_where = "";
    $filter_count = 0;
    // Data
    $get_stock_grn_document_list_sdata = array();
    if ($document_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_document_list_squery_where = $get_stock_grn_document_list_squery_where." where stock_grn_document_id=:document_id";
        } else {            // Query
            $get_stock_grn_document_list_squery_where = $get_stock_grn_document_list_squery_where." and stock_grn_document_id=:document_id";
        }
        // Data
        $get_stock_grn_document_list_sdata[':document_id'] = $document_id;
        $filter_count++;
    }
    if ($grn_id != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_document_list_squery_where = $get_stock_grn_document_list_squery_where." where stock_grn_document_grn_id=:grn_id";
        } else {            // Query
            $get_stock_grn_document_list_squery_where = $get_stock_grn_document_list_squery_where." and stock_grn_document_grn_id=:grn_id";
        }
        // Data
        $get_stock_grn_document_list_sdata[':grn_id']  = $grn_id;
        $filter_count++;
    }
    if ($name != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_document_list_squery_where = $get_stock_grn_document_list_squery_where." where stock_grn_document_name=:name";
        } else {            // Query
            $get_stock_grn_document_list_squery_where = $get_stock_grn_document_list_squery_where." and stock_grn_document_name=:name";
        }
        // Data
        $get_stock_grn_document_list_sdata[':name']  = $name;
        $filter_count++;
    }
    if ($active != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_document_list_squery_where = $get_stock_grn_document_list_squery_where." where stock_grn_document_active = :active";
        } else {            // Query
            $get_stock_grn_document_list_squery_where = $get_stock_grn_document_list_squery_where." and stock_grn_document_active = :active";
        }
        //Data
        $get_stock_grn_document_list_sdata[':active']  = $active;
        $filter_count++;
    }
    if ($added_by!= "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_document_list_squery_where = $get_stock_grn_document_list_squery_where." where stock_grn_document_added_by >= :added_by";
        } else {            // Query
            $get_stock_grn_document_list_squery_where = $get_stock_grn_document_list_squery_where." and stock_grn_document_added_by >= :added_by";
        }
        //Data
        $get_stock_grn_document_list_sdata[':added_by']  = $added_by;
        $filter_count++;
    }    
    if ($start_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_document_list_squery_where = $get_stock_grn_document_list_squery_where." where stock_grn_document_added_on >= :start_date";
        } else {            // Query
            $get_stock_grn_document_list_squery_where = $get_stock_grn_document_list_squery_where." and stock_grn_document_added_on >= :start_date";
        }
        //Data
        $get_stock_grn_document_list_sdata[':start_date']  = $start_date;
        $filter_count++;
    }
    if ($end_date != "") {
        if ($filter_count == 0) {            // Query
            $get_stock_grn_document_list_squery_where = $get_stock_grn_document_list_squery_where." where stock_grn_document_added_on <= :end_date";
        } else {            // Query
            $get_stock_grn_document_list_squery_where = $get_stock_grn_document_list_squery_where." and stock_grn_document_added_on <= :end_date";
        }
        //Data
        $get_stock_grn_document_list_sdata['end_date']  = $end_date;
        $filter_count++;
    }
    $get_stock_grn_document_list_squery = $get_stock_grn_document_list_squery_base.$get_stock_grn_document_list_squery_where;
    try {
        $dbConnection = get_conn_handle();
        $get_stock_grn_document_list_sstatement = $dbConnection->prepare($get_stock_grn_document_list_squery);
        $get_stock_grn_document_list_sstatement -> execute($get_stock_grn_document_list_sdata);
        $get_stock_grn_document_list_sdetails = $get_stock_grn_document_list_sstatement -> fetchAll();
        if (false === $get_stock_grn_document_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_stock_grn_document_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_stock_grn_document_list_sdetails;
        }
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }
    return $return;
}  /*PURPOSE : To update Stock Grn DocumentINPUT 	: Document ID, Stock Grn Document Update ArrayOUTPUT 	: Document ID; Message of success or failureBY 		: Ashwini*/function db_update_stock_grn_document($document_id, $stock_grn_document_update_data)
{
    if (array_key_exists("grn_id", $stock_grn_document_update_data)) {
        $grn_id = $stock_grn_document_update_data["grn_id"];
    } else {
        $grn_id = "";
    }
    if (array_key_exists("name", $stock_grn_document_update_data)) {
        $name = $stock_grn_document_update_data["name"];
    } else {
        $name = "";
    }
    if (array_key_exists("active", $stock_grn_document_update_data)) {
        $active = $stock_grn_document_update_data["active"];
    } else {
        $active = "";
    }
    if (array_key_exists("remarks", $stock_grn_document_update_data)) {
        $remarks = $stock_grn_document_update_data["remarks"];
    } else {
        $remarks = "";
    }    
    if (array_key_exists("added_by", $stock_grn_document_update_data)) {
        $added_by = $stock_grn_document_update_data["added_by"];
    } else {
        $added_by = "";
    }
    if (array_key_exists("added_on", $stock_grn_document_update_data)) {
        $added_on = $stock_grn_document_update_data["added_on"];
    } else {
        $added_on = "";
    }
    // Query
    $stock_grn_document_update_uquery_base = "update stock_grn_document set";
    $stock_grn_document_update_uquery_set = "";
    $stock_grn_document_update_uquery_where = " where stock_grn_document_id=:document_id";
    $stock_grn_document_update_udata = array(":document_id"=>$document_id);
    $filter_count = 0;
    if ($grn_id != "") {
        $stock_grn_document_update_uquery_set = $stock_grn_document_update_uquery_set." stock_grn_document_grn_id=:grn_id,";
        $stock_grn_document_update_udata[":grn_id"] = $grn_id;
        $filter_count++;
    }
    if ($name != "") {
        $stock_grn_document_update_uquery_set = $stock_grn_document_update_uquery_set."stock_grn_document_name=:name,";
        $stock_grn_document_update_udata[":name"] = $name;
        $filter_count++;
    }
    if ($remarks != "") {
        $stock_grn_document_update_uquery_set = $stock_grn_document_update_uquery_set." stock_grn_document_remarks = :remarks,";
        $stock_grn_document_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }
    if ($active != "") {
        $stock_grn_document_update_uquery_set = $stock_grn_document_update_uquery_set." stock_grn_document_active = :active,";
        $stock_grn_document_update_udata[":active"] = $active;
        $filter_count++;
    }
    if ($added_by != "") {
        $stock_grn_document_update_uquery_set = $stock_grn_document_update_uquery_set." stock_grn_document_added_by=:added_by,";
        $stock_grn_document_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }
    if ($added_on != "") {
        $stock_grn_document_update_uquery_set = $stock_grn_document_update_uquery_set." stock_grn_document_added_on=:added_on,";
        $stock_grn_document_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }
    if ($filter_count > 0) {
        $stock_grn_document_update_uquery_set = trim($stock_grn_document_update_uquery_set, ',');
    }
    $stock_grn_document_update_uquery = $stock_grn_document_update_uquery_base.$stock_grn_document_update_uquery_set.$stock_grn_document_update_uquery_where;
    try {
        $dbConnection = get_conn_handle();
        $stock_grn_document_update_ustatement = $dbConnection->prepare($stock_grn_document_update_uquery);
        $stock_grn_document_update_ustatement -> execute($stock_grn_document_update_udata);
        $return["status"] = SUCCESS;
        $return["data"]   = $document_id;
    } catch (PDOException $e) {        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }
    return $return;
}?>
