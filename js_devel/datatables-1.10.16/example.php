<?php
  session_start();
  $base = $_SERVER["DOCUMENT_ROOT"];

  if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")){
  	$user 		   = $_SESSION["loggedin_user"];
  	$role 		   = $_SESSION["loggedin_role"];
  	$loggedin_name = $_SESSION["loggedin_user_name"];
  } else {
  	header("location:login.php");
  }
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script type="text/javascript" src="./jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="./datatables.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#example').DataTable({
          ajax: '/kns/Legal/datatable-example/manpower-ajax.json',
          fixedColumns: {
              leftColumns: 3,
              rightColumns: 2
          },
          scrollX: true,
          columns: [
            { "data": function(){
              return arguments[3].row + 1;
            } },
            { "data": "project_manpower_agency_name" },
            { "data": "project_master_name" },
            { "data": "project_process_master_name" },
            { "data": "project_task_master_name" },
            { "data": "project_task_actual_manpower_road_id" },
            { "data": "project_task_actual_manpower_work_type" },
            { "data": "project_manpower_agency_name" },
            { "data": "project_master_name" },
            { "data": "project_process_master_name" },
            { "data": "project_task_master_name" },
            { "data": "project_task_actual_manpower_road_id" },
            { "data": "project_task_actual_manpower_work_type" }
          ]
        });
      } );
    </script>
    <link href="../../css/style.css" rel="stylesheet">
    <link href="./datatables.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../bootstrap_aku.min.css" rel="stylesheet">
    <style media="screen">
      table.dataTable{
        margin-top: 0px !important;
      }
      .widget-table .widget-content {
        padding: 5px 0px;
      }
      .widget-header h3{
        margin: 0px !important;
      }
      .dataTables_wrapper .row:first-child, .dataTables_wrapper .row:last-child{
        padding: 5px 10px;
      }
      .DTFC_LeftHeadWrapper, .DTFC_LeftBodyWrapper{
        box-shadow: 2px 0px 3px rgba(191, 191, 191, 0.5);
      }
      .DTFC_RightHeadWrapper, .DTFC_RightBodyWrapper{
        box-shadow: -2px 0px 3px rgba(191, 191, 191, 0.5);
      }
    </style>
  </head>
  <body>
    <?php
      include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
    ?>
    <div class="main" style="margin-top: 70px;">
        <div class="main-inner">
          <div class="container">
            <div class="row">

                <div class="span6" style="width:100%;">

                <div class="widget widget-table action-table">
                  <div class="widget-header"> <i class="icon-th-list"></i>
                    <h3>To-Do List</h3>
                  </div>
                  <!-- /widget-header -->
                  <div class="widget-content">
                    <table id="example" class="table table-striped table-bordered display nowrap">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th>Agency</th>
                              <th>Project</th>
                              <th>Process</th>
                              <th>Task Name</th>
                              <th>Road Name</th>
                              <th>Work Type</th>
                              <th>Agency</th>
                              <th>Project</th>
                              <th>Process</th>
                              <th>Task Name</th>
                              <th>Road Name</th>
                              <th>Work Type</th>
                          </tr>
                      </thead>
                  </table>
                </div>
                <!-- /widget-content -->

                </div>
                <!-- /widget -->

                </div>
                <!-- /widget -->
              </div>
              <!-- /span6 -->
            </div>
            <!-- /row -->
          </div>
          <!-- /container -->
        </div>

  </body>
</html>
